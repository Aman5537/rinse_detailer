import React from 'react';
import { createAppContainer,createStackNavigator, createSwitchNavigator } from 'react-navigation';

import MainTabNavigatorSubNot from './MainTabNavigatorSubNot';

import Login from '../src/screens/Login'; 
import Intro from '../src/screens/Intro'; 
import ForgotPassword from '../src/screens/ForgotPassword'; 
import CreateAccount from '../src/screens/CreateAccount'; 
import Term from '../src/screens/Term'; 
import Policy from '../src/screens/Policy'; 
import DetailersWithoutNav from '../src/screens/Detailers'; 
//import SocialProfile from '../src/screens/SocialProfile';
import SetHome from './MainTabNavigatorSubNot'







const LandingStack = createStackNavigator({
  Intro,Login,ForgotPassword,CreateAccount,Term,Policy,DetailersWithoutNav,//SocialProfile
});

const LoginStack = createStackNavigator({
  Login,ForgotPassword,CreateAccount,Term,Policy
});

// export const HomeStack = createStackNavigator({
//   Landing1:LandingStack,
//   LoginNew:LoginStack,
//   Main: MainTabNavigator,
// });

export default createAppContainer(createSwitchNavigator({

  Landing1:LandingStack,
  LoginNew:LoginStack,
  Main: MainTabNavigatorSubNot,
  Home:SetHome
}));