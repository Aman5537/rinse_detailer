import React from 'react';
import { Platform,Image,View,Text,Dimensions } from 'react-native';
import { createStackNavigator, DrawerNavigator, createDrawerNavigator, createBottomTabNavigator} from 'react-navigation';

// import TabBarIcon from '../components/TabBarIcon';

import Login from '../src/screens/Login';
import ForgotPassword from '../src/screens/ForgotPassword';
import CreateAccount from '../src/screens/CreateAccount';
import Term from '../src/screens/Term';
import Policy from '../src/screens/Policy';
import ServicesYouOffer from '../src/screens/servicesYouOffers';
import AddService from '../src/screens/addService';
import PayoutAccount from '../src/screens/payoutAccount';
import Detailers from '../src/screens/Detailers';
import Upgrade from '../src/screens/upgrade';
import UpgradeForm from '../src/screens/UpgradeForm';
import UpgradeDone from '../src/screens/UpgradeDone';
import Home from '../src/screens/Home';
import Requests from '../src/screens/Requests';
import Appointments from '../src/screens/Appointments';
import AppointmentLists from '../src/screens/AppointmentLists';
import AppointmentView from '../src/screens/AppointmentView';
import CanceledAppointment from '../src/screens/CanceledAppointment';
import SendQuote from '../src/screens/SendQuote';
import Directions from '../src/screens/Directions';
import Payout from '../src/screens/Payout';
import ChangePassword from '../src/screens/ChangePassword';
import PendingPayment from '../src/screens/PendingPayment';
import Intro from '../src/screens/Intro';
import AddDetailer from '../src/screens/AddDetailer';
import TrackDetailer from '../src/screens/TrackDetailer';
import TransactionHistory from '../src/screens/TransactionHistory';
import Sales from '../src/screens/Sales';
import Support from '../src/screens/Support';
import Legal from '../src/screens/Legal';
import Profile from '../src/screens/Profile';
import Report from '../src/screens/Report';
import Messages from '../src/screens/Message/Message';
import Chat from '../src/screens/Message/chat';
import SubmiteRequest from '../src/screens/SubmiteRequest';
import Portfolio from '../src/screens/portfolio';

import SideMenu from '../src/screens/SideMenu';
//import SocialProfile from '../src/screens/SocialProfile';
import EditService from '../src/screens/EditService';
import Gallery from '../src/screens/Gallery';
import Camera from '../src/screens/Camera';
import CameraScreen from '../src/screens/CameraScreen';
import Helper from '../src/Lib/Helper';
import MaintananceScreen from '../src/screens/MaintananceScreen';

var navs = {
  
  Intro:Intro,
  Login: Login,
  ForgotPassword: ForgotPassword,
  CreateAccount: CreateAccount,
  Term: Term,
  Policy: Policy,
  ServicesYouOffer:ServicesYouOffer,
  PayoutAccount: PayoutAccount,
  AddService:AddService,
  Detailers: Detailers,
  Upgrade: Upgrade,
  UpgradeForm:UpgradeForm,
  UpgradeDone:UpgradeDone,
  Home:Home,
  Requests:Requests,
  Appointments:Appointments,
  AppointmentLists:AppointmentLists,
  AppointmentView:AppointmentView,
  CanceledAppointment:CanceledAppointment,
  SendQuote:SendQuote,
  Directions:Directions,
  Payout:Payout,
  ChangePassword:ChangePassword,
  PendingPayment:PendingPayment,
  AddDetailer:AddDetailer,
  TrackDetailer:TrackDetailer,
  TransactionHistory:TransactionHistory,
  Sales:Sales,
  Support:Support,
  Legal:Legal,
  Profile:Profile,
  Report:Report,
  Messages:Messages,
  Chat:Chat,
  SubmiteRequest:SubmiteRequest,
  Portfolio:Portfolio,
  //SocialProfile:SocialProfile,
  EditService:EditService,
  Gallery:Gallery,
  Camera:Camera,
  CameraScreen:CameraScreen,
  MaintananceScreen:MaintananceScreen
  

}



checktabvisibility = (navigation) => {
  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName
  
  if (routeName == 'Home' ||
    routeName == 'Intro' ||
    routeName == 'Login' ||
    routeName == 'ForgotPassword' ||
    routeName == 'CreateAccount' ||
    routeName == 'Term' ||
    routeName == 'Policy' ||
    routeName == 'ChangePassword' ||
    routeName == 'ServicesYouOffer' ||
    routeName == 'PayoutAccount' ||
    routeName == 'Upgrade' ||
    routeName == 'UpgradeForm' ||
    routeName == 'UpgradeDone' ||
    routeName == 'SendQuote' ||
    routeName == 'AppointmentView' ||
    routeName == 'Directions' ||
    routeName == 'PendingPayment' ||
    routeName == 'Chat' ||
    routeName == 'AddService' ||
    routeName == 'Portfolio' ||
    routeName == 'Sales' ||
    routeName == 'Legal' ||
    routeName == 'Support' || 
    routeName == 'Profile' || 
    routeName == 'Report' || 
    routeName == 'Payout' || 
    routeName == 'TransactionHistory' || 
    routeName == 'SubmiteRequest' || 
    routeName == 'AppointmentLists' || 
    routeName == 'CanceledAppointment' || 
    routeName == 'AddDetailer' || 
    routeName == 'TrackDetailer' || 
    routeName == 'Report' || 
    //routeName == 'SocialProfile' ||
    routeName == 'EditService' ||
    routeName == 'Gallery' ||
    routeName == 'Camera' ||
    routeName == 'CameraScreen' ||
    routeName == 'MaintananceScreen'
    
    ) {
    tabBarVisible = false
  }

  return tabBarVisible;
}




const RequestsStack = createStackNavigator(navs, { initialRouteName: 'Requests' });

RequestsStack.navigationOptions = ({ navigation }) => {
  
  return {
    tabBarVisible: this.checktabvisibility(navigation),
    tabBarOptions: {
      // activeBackgroundColor:'#fff',
      showLabel: false,
      // activeTintColor: '#4684da', // active icon color
      // inactiveTintColor: '#ffffff',  // inactive icon color
      // style: {
      //     backgroundColor: '#4684da' // TabBar background
      // }
    },
    
    tabBarIcon: ({ focused }) => (
      <View style={{ height: 50, width: Dimensions.get('window').width/3, backgroundColor: focused == true ? "white" : "#1d87dd", alignItems: 'center', borderRightWidth:9, borderColor: 'white' }}>
          <Image
        resizeMode={'contain'}
        source={focused ? require('../assets/Icons/TabMenu/resuests_b.png') : require('../assets/Icons/TabMenu/resuests.png')}
        style={{ width: focused ? 35 : 35, height: focused ? 35 : 35,marginLeft:focused?0:6 }}
      />    
      {Helper.requestCount != 0 && (
          <View style={{position: 'absolute',top: '5%',left: '65%',backgroundColor: 'red',height: 15,minWidth: 15,
        borderRadius: 10,alignItems: 'center',justifyContent: 'center'}}>
              <Text style={{paddingHorizontal: 5,paddingBottom: 0,color: '#fff',fontSize: 10}}>{Helper.requestCount}</Text>
          </View>
        )}  
             <Text numberOfLines={1} style={{ top:-2,color: focused == true ? "#1d87dd" : "white", fontSize: 10,fontWeight:'bold',textAlign:'center'  }}> Requests</Text>      
         
      </View>
      
      
    )
  }
};



const AppointmentsStack = createStackNavigator(navs, { initialRouteName: 'Appointments' });
AppointmentsStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarVisible: this.checktabvisibility(navigation),
    tabBarOptions: {
      // activeBackgroundColor:'#fff',
      showLabel: false,
      // activeTintColor: '#4684da', // active icon color
      // inactiveTintColor: '#ffffff',  // inactive icon color
      // style: {
      //     backgroundColor: '#4684da' // TabBar background
      // }
    },
    tabBarIcon: ({ focused }) => (
      <View style={{ height: 50, width: Dimensions.get('window').width/3, backgroundColor: focused == true ? "white" : "#1d87dd", alignItems: 'center', borderRightWidth: 9, borderColor: 'white' }}>
                
        <Image
        resizeMode={'contain'}
        source={focused ? require('../assets/Icons/TabMenu/appointments_b.png') : require('../assets/Icons/TabMenu/appointments_12.png')}
        style={{ width: focused ? 32 : 35, height: focused ? 32 : 30,marginLeft:focused?0:6,marginTop:4 }}
      />
        {Helper.appoCount != 0 && (
          <View style={{position: 'absolute',top: '5%',left: '65%',backgroundColor: 'red',height: 15,minWidth: 15,
        borderRadius: 10,alignItems: 'center',justifyContent: 'center'}}>
              <Text style={{paddingHorizontal: 5,paddingBottom: 0,color: '#fff',fontSize: 10}}>{Helper.appoCount}</Text>
          </View>
        )}
          <Text numberOfLines={1} style={{ top:focused ? -2 : -2,color: focused == true ? "#1d87dd" : "white", fontSize: 10,fontWeight:'bold',textAlign:'center'  }}>Appointments</Text>      
      </View>
      
      
    )
  }
};

const MessagesStack = createStackNavigator(navs, { initialRouteName: 'Messages' });

MessagesStack.navigationOptions = ({ navigation }) => {

  return {
    tabBarVisible: this.checktabvisibility(navigation),
    tabBarOptions: {
      // activeBackgroundColor:'#fff',
      showLabel: false,
      // activeTintColor: '#4684da', // active icon color
      // inactiveTintColor: '#ffffff',  // inactive icon color
      // style: {
      //     backgroundColor: '#4684da' // TabBar background
      // }
    },
    tabBarIcon: ({ focused }) => (
      <View style={{ height: 50, width: Dimensions.get('window').width/3, backgroundColor: focused == true ? "white" : "#1d87dd", alignItems: 'center' }}>
        <Image
        resizeMode={'contain'}
        source={focused ? require('../assets/Icons/TabMenu/messages_b.png') : require('../assets/Icons/TabMenu/messages_12.png')}
        style={{ width: focused ? 35 : 35, height: focused ? 35 : 35,marginLeft:focused?0:6,marginTop:focused?2:2 }}
      />     
        {Helper.messageCount != 0 && (
          <View style={{position: 'absolute',top: '5%',left: '62%',backgroundColor: 'red',height: 15,minWidth: 15,
          borderRadius: 10,alignItems: 'center',justifyContent: 'center'}}>
                <Text style={{paddingHorizontal: 5,paddingBottom: 0,color: '#fff',fontSize: 10}}>{Helper.messageCount}</Text>
            </View>
        )}
      

       <Text numberOfLines={1} style={{ top:focused ? -4 : -4,color: focused == true ? "#1d87dd" : "white", fontSize: 10,fontWeight:'bold',textAlign:'center'  }}>Messages</Text>  
      </View>
      
      
    )
  }
};

openDrawer = (navigation) => {
  navigation.toggleDrawer();
}






const bottomTabs = createBottomTabNavigator({
  RequestsStack,
  AppointmentsStack,
  MessagesStack
});


export default createDrawerNavigator({
  SetHome:{screen:Home },
  Home: { screen: bottomTabs},
  

}, {
    drawerPosition: 'right',
    drawerLockMode: 'locked-closed',
    contentComponent: SideMenu,
    drawerWidth: 200
});

   
