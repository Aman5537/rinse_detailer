import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { LOGIN_TITLE, LOGIN_BELOW_TITLE, FORGOT_PASSWORD, SIGN_IN, LOGIN_FOOTER_TEXT } from '../../value/String';
import { TITLE_COLOR, FOOTER_TEXT_COLOR } from '../../value/Colors';
import CommonBttn from '../../commonComponant/CommonBttn';
import CountryList from '../../commonComponant/CountryList';
import Helper from '../../Lib/Helper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class LoginView extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      loginform: {
        country_code: "971", mobile_no: "", password: "",
        device_token: "SC9830I", device_type: "ANDROID",
        validators: {
          country_code: { required: true },
          mobile_no: { "required": true, minLengthDigit: 7 },
          password: { "required": true, "maxLength": 51 },
        }
      }
    }
    this.gotoNext = this.gotoNext.bind(this);
  }

  onLogin = () => {
    let isValid = Helper.validate(this.state.loginform);
    if (isValid) {
      Helper.makeRequest({ url: "login", data: this.state.loginform }).then((logindata) => {
        // console.log('----logindata :  ' + JSON.stringify(logindata));
        if (logindata.status) {
          Helper.setData('userdata', logindata.data);
          Helper.setData('token', logindata.data.token);
          this.props.navigation.navigate('Main');
        }
        else {
          Helper.showToast(logindata.message)
        }
      }).catch(err => {
        Helper.showToast("Please try again");
      })
    }
  }

  setValues(key, value) {
    let loginform = { ...this.state.loginform }
    if (key == 'mobile_no') {
      value = value.replace(/[^0-9]/g, '');
    }
    loginform[key] = value;
    this.setState({ loginform })
  }

  selectCat = (selectedItems) => {
    this.setState({ selectedItems });
  }

  gotoNext() {
    this.onLogin();
  }
  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  render() {
    return (
      <KeyboardAwareScrollView>
        <View style={styles.container}>
          <View style={styles.logoView}>
            <Image style={styles.logo} source={require('../../images/logo_new.png')} />
          </View>
          <View style={styles.containtView}>
            {/* <Text style={styles.containtWelcome}>{LOGIN_TITLE}</Text> */}
            <Text style={{ fontSize: 18, color: '#666666' }}>{LOGIN_BELOW_TITLE}</Text>
          </View>
          <View style={{ marginTop: 20 }}>
            <CountryList
              form={this.state.loginform}
              setValues={(mobile_no) => this.setValues('mobile_no', mobile_no)}
              // onSelect={this.selectCat}
              // selectedItems={this.state.loginform.country_code}
              getFocus={(input) => { this.mobile_no = input; }}
              mvalue={this.state.loginform.mobile_no}
              setFocus={() => { this.secondTextInput.focus(); }}
              inputlable="Mobile Number" />
          </View>

          <View style={Platform.OS === 'android' ? styles.inputViewAndroid : styles.inputViewIOS}>
            <View style={styles.locIconView}><Image style={styles.mob_logo} source={require('../../images/lock_ico.png')} /></View>
            <View style={styles.secoundInputView}>
              <TextInput
                selectable={true}
                onChangeText={(password) => this.setValues('password', password)}
                secureTextEntry={this.state.hidePassword}
                inlineImageLeft='search_icon'
                // returnKeyType={'go'}
                ref={(input) => { this.secondTextInput = input; }}
                onSubmitEditing={() => this.onLogin()}
                placeholder="Password"
                placeholderTextColor='gray' />
            </View>
            <TouchableOpacity activeOpacity={0.8} onPress={this.managePasswordVisibility}>
              <View style={styles.ShwoPassIconView}><Image style={styles.mob_logo} source={(this.state.hidePassword) ? require('../../images/eye_ico.png') : require('../../images/eye_open.png')} /></View>
            </TouchableOpacity>
          </View>
          <View style={styles.forgotText}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Forgot') }}>
              <Text style={styles.footerText}>{FORGOT_PASSWORD}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ paddingTop: 20 }}><CommonBttn buttonText={SIGN_IN} gotoNext={this.gotoNext} /></View>
          <View style={styles.footerTextView}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('SignUp') }}>
              <Text style={styles.footerText}>{LOGIN_FOOTER_TEXT}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, flexDirection: 'column',
  },
  logoView: {
    marginTop: 70, alignItems: 'center'
  },
  logo: {
    height: 140, width: 140, resizeMode: 'contain'
  },
  containtView: {
    marginBottom: 10, alignItems: 'center', marginTop: 20
  },
  containtWelcome: {
    fontSize: 24, color: TITLE_COLOR, fontWeight: 'bold', padding: 8
  },
  inputViewIOS: {
    marginHorizontal: 20, marginTop: 20, flexDirection: 'row', borderColor: '#a09a9b', borderWidth: 0.5, borderRadius: 4, padding: 10
  },
  inputViewAndroid: {
    marginHorizontal: 20, marginTop: 20, flexDirection: 'row', borderColor: '#a09a9b', borderWidth: 0.5, borderRadius: 4, paddingHorizontal: 10
  },
  locIconView: {
    flex: 1, justifyContent: 'center', alignItems: 'center'
  },
  ShwoPassIconView: {
    flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7
  },
  secoundInputView: {
    flex: 8, justifyContent: 'center'
  },
  mob_logo: {
    height: 20, width: 20, resizeMode: 'contain'
  },
  forgotText: {
    justifyContent: 'center', width: 140, marginHorizontal: 20
  },
  footerTextView: {
    alignItems: 'center', marginTop: 50
  },
  footerText: {
    textDecorationLine: 'underline', color: FOOTER_TEXT_COLOR, padding: 7
  },
});