import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, Picker, TouchableOpacity, Dimensions } from 'react-native';
import { SIGNUP_TITLE, SIGNUP_BELOW_TITLE, SIGN_UP, SIGNUP_FOOTER_TEXT } from '../../value/String';
import { TITLE_COLOR, FOOTER_TEXT_COLOR, SUBTEXT, BLACK } from '../../value/Colors';
import CommonBttn from '../../commonComponant/CommonBttn';
import CountryList from '../../commonComponant/CountryList';
import { CheckBox } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DropDown from '../../commonComponant/DropDown';
import Helper from '../../Lib/Helper';
import GoogleApiAddressList from '../../commonComponant/GoogleApiAddressList'

const _inputBox = props => {
  return (
    <View style={Platform.OS === 'android' ? styles.inputViewAndroid : styles.inputViewIOS}>
      <View style={styles.inputBoxImgView}>
        <Image style={styles.inputBoxLogo} source={props.img} />
      </View>
      <View style={styles.inputBoxLableView}>
        <TextInput
          returnKeyType={'next'}
          ref={props.getFocus}
          onSubmitEditing={props.setFocus}
          keyboardType={props.keyNumbric}
          maxLength={props.maxLenth}
          placeholder={props.String}
          onChangeText={props.setValues}
          placeholderTextColor='gray' />
      </View>
    </View>
  )
}

export default class SignUpView extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true, hideConfirmPassword: true,
      value: true, language: true, checked: false,
      checkedRadio: true, hundleredio: false,
      categorylist: [],
      selectedCat: '', modalVisible: false,
      registerOtpform: {
        username: '',
        email: '',
        country_code: '971',
        mobile_no: '',
        whatsapp_no: '',
        password: '',
        confirm_password: '',
        category_id: '',
        store_name: '',
        store_number: '',
        store_type: '',
        address: '',
        lat: '',
        long: '',
        landmark: '',
        device_type: "ANDROID",
        device_token: 'SC9830I',
        is_individual: "1",
        validators: {
          store_type: { required: true, "maxLength": 20 },
          store_name: { required: true, "maxLength": 50 },
          store_number: { "required": true, minLengthDigit: 7 },
          username: { required: true, "maxLength": 20 },
          address: { required: true },
          landmark: { required: true, "maxLength": 50 },
          email: { required: true, "email": true },
          country_code: { required: true },
          mobile_no: { "required": true, minLengthDigit: 7 },
          whatsapp_no: { "required": true, minLengthDigit: 7 },
          password: { "required": true, "maxLength": 51 },
          confirm_password: { "required": true, "maxLength": 51 },
        }
      },
    }
    this.gotoNext = this.gotoNext.bind(this);
    this.getCategoriesApi();
  }

  gotoNext() {
    this.onRegisterOtp();
  }
  checkedRadioBotton = () => {
    if (this.state.hundleredio) {
      this.setState({ checkedRadio: !this.state.checkedRadio, hundleredio: false });
    }
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  checkedRadioBottonSecound = () => {
    if (!this.state.hundleredio) {
      this.setState({ checkedRadio: !this.state.checkedRadio, hundleredio: true });
    }
  }
  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }
  manageConfirmPasswordVisibility = () => {
    this.setState({ hideConfirmPassword: !this.state.hideConfirmPassword });
  }

  getCategoriesApi = () => {
    Helper.makeRequest({ url: "get-category-list", method: "GET" }).then((responsedata) => {
      if (responsedata.status) {
        let temparr = [];
        for (let data of responsedata.data) {
          temparr.push({ label: data.category_name, value: data.id });
        }
        this.setState({ categorylist: temparr });
      }
      else {
        Helper.showToast(responsedata.message)
      }
    }).catch(err => {
      Helper.showToast("Please try again");
    })
  }


  onRegisterOtp = () => {
    this.state.registerOtpform.store_type = this.state.hundleredio ? 'individual' : 'store';
    // console.log('------registerOtpform  :: ' + JSON.stringify(this.state.registerOtpform));
    if (!this.state.selectedCat) {
      Helper.showToast("Category is required");
      return;
    }
    this.state.registerOtpform.category_id = this.state.selectedCat;
    let isValid = Helper.validate(this.state.registerOtpform);
    if (isValid) {
      Helper.makeRequest({ url: "register-otp", data: this.state.registerOtpform }).then((data) => {
        if (data.status) {
          this.props.navigation.navigate('Verify', { fromSignup: true, signupData: this.state.registerOtpform });
        }
        else {
          Helper.showToast(data.message)
        }
      }).catch(err => {
        Helper.showToast("Please try again");
      })
    }
  }

  setValues(key, value) {
    let registerOtpform = { ...this.state.registerOtpform }
    if (key == 'mobile_no' || key == 'store_number' || key == 'whatsapp_no') {
      value = value.replace(/[^0-9]/g, '');
      registerOtpform[key] = value;
      this.setState({ registerOtpform })
    }
    if (key == 'username') {
      if (/^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z ]*)*$/.test(value) || value == '') {
        registerOtpform[key] = value;
        this.setState({ registerOtpform })
      }
    } else {
      registerOtpform[key] = value;
      this.setState({ registerOtpform })
    }
  }

  sameaswhatsapp = () => {
    this.setState({ checked: !this.state.checked }, () => {
      let registerOtpform = { ...this.state.registerOtpform }
      if (this.state.checked) {
        registerOtpform['whatsapp_no'] = this.state.registerOtpform.mobile_no;
        this.setState({ registerOtpform });
      } else {
        registerOtpform['whatsapp_no'] = '';
        this.setState({ registerOtpform });
      }
    });

  }

  _checkBox = (title) => {
    return (
      <View style={{ flexDirection: 'row', marginHorizontal: 20, justifyContent: 'flex-end' }}>
        <View style={{ marginLeft: -20 }}>
          <CheckBox
            checkedIcon={<Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={require('../../images/check_box_active.png')} />}
            uncheckedIcon={<Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={require('../../images/check_box_unactive.png')} />}
            checked={this.state.checked}
            onPress={this.sameaswhatsapp}
          />
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: BLACK }}>{title}</Text>
        </View>
      </View>
    )
  }

  _inputWebBox = (img, String, text) => {
    return (
      <View style={Platform.OS === 'android' ? styles.inputViewAndroid : styles.inputViewIOS}>
        <View style={styles.inNumLogoView}><Image style={styles.inputBoxLogo} source={img} /></View>
        <View style={styles.inNumInputView}><TextInput placeholder={String} placeholderTextColor='gray' /></View>
        <View style={styles.webInTextView}><Text>{text}</Text></View>
      </View>
    )
  }

  _inputPasswordBox = (img, String, img2, img3) => {
    return (
      <View style={Platform.OS === 'android' ? styles.inputViewAndroid : styles.inputViewIOS}>
        <View style={styles.inNumLogoView}><Image style={styles.inputBoxLogo} source={img} /></View>
        <View style={styles.inNumInputView}>
          <TextInput
            returnKeyType={'next'}
            ref={(input) => { this.password = input; }}
            onSubmitEditing={() => { this.confirmPassword.focus(); }}
            secureTextEntry={this.state.hidePassword}
            placeholder={String}
            placeholderTextColor='gray'
            onChangeText={(password) => { this.setValues('password', password) }} />
        </View>
        <TouchableOpacity activeOpacity={0.8} onPress={this.managePasswordVisibility}>
          <View style={styles.ShwoPassIconView}><Image style={styles.inputBoxLogo} source={(this.state.hidePassword) ? img2 : img3} /></View>
        </TouchableOpacity>
      </View>
    )
  }

  _inputConfirmPasswordBox = (img, String, img2, img3) => {
    return (
      <View style={Platform.OS === 'android' ? styles.inputViewAndroid : styles.inputViewIOS}>
        <View style={styles.inNumLogoView}><Image style={styles.inputBoxLogo} source={img} /></View>
        <View style={styles.inNumInputView}>
          <TextInput
            ref={(input) => { this.confirmPassword = input; }}
            onSubmitEditing={() => { this.onRegisterOtp(); }}
            secureTextEntry={this.state.hideConfirmPassword}
            placeholder={String}
            placeholderTextColor='gray'
            onChangeText={(confirm_password) => { this.setValues('confirm_password', confirm_password) }} />
        </View>
        <TouchableOpacity activeOpacity={0.8} onPress={this.manageConfirmPasswordVisibility}>
          <View style={styles.ShwoPassIconView}><Image style={styles.inputBoxLogo} source={(this.state.hideConfirmPassword) ? img2 : img3} /></View>
        </TouchableOpacity>
      </View>
    )
  }

  handleAddress = (data) => {
    let registerOtpform = { ...this.state.registerOtpform };
    registerOtpform['address'] = data.addressname;
    registerOtpform['lat'] = data.lat;
    registerOtpform['long'] = data.long;
    this.setState({ registerOtpform });
  }

  render() {
    return (
      <KeyboardAwareScrollView>
        <View style={styles.container}>
          <GoogleApiAddressList
            modalVisible={this.state.modalVisible}
            showModal={() => { this.setModalVisible(true); }}
            hideModal={() => { this.setModalVisible(!this.state.modalVisible) }}
            onSelectAddress={this.handleAddress}
          />
          <View style={styles.logoView}>
            <Image style={styles.logo} source={require('../../images/logo.png')} />
          </View>
          <View style={styles.containtView}>
            <Text style={styles.containtVerify}>{SIGNUP_TITLE}</Text>
            <Text style={{ textAlign: 'center', fontSize: 18 }}>{SIGNUP_BELOW_TITLE}</Text>
          </View>
          <View style={{ flexDirection: 'row', marginHorizontal: 20, paddingBottom: 20 }}>
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={this.checkedRadioBotton}>
              <Image style={styles.radioImg}
                source={(this.state.checkedRadio) ? require('../../images/checkbox_active.png') : require('../../images/checkbox_unactive.png')} />
              <View style={{ justifyContent: 'center' }}><Text style={{ fontSize: 15, paddingHorizontal: 7 }}>Store</Text></View>
            </TouchableOpacity>

            <TouchableOpacity style={{ flexDirection: 'row', paddingLeft: 25 }} onPress={this.checkedRadioBottonSecound}>
              <Image style={styles.radioImg}
                source={(this.state.checkedRadio) ? require('../../images/checkbox_unactive.png') : require('../../images/checkbox_active.png')} />
              <View style={{ justifyContent: 'center' }}><Text style={{ fontSize: 15, paddingHorizontal: 7 }}>Individual</Text></View>
            </TouchableOpacity>
          </View>

          <View style={{ marginHorizontal: 20 }}>
            <DropDown
              placeholderlable="Select Your Category"
              PickerValueHolder={this.state.categorylist}
              onValueChangePicker={(value) => { this.setState({ selectedCat: value }); }}
            />
          </View>
          <_inputBox
            setFocus={() => { this.storeNumber.focus(); }}
            setValues={(store_name) => this.setValues('store_name', store_name)}
            img={require('../../images/store_name.png')}
            String="Store Name" />
          {/* <_inputBox
            getFocus={(input) => { this.storeNumber = input; }}
            setFocus={() => { this.username.focus(); }}
            setValues={(store_number) => this.setValues('store_number', store_number)}
            img={require('../../images/mob_ico.png')}
            String="Store Number"
            maxLenth={10}
            keyNumbric="numeric" /> */}
          <View style={{ marginTop: 20 }}>
            <CountryList
              getFocus={(input) => { this.storeNumber = input; }}
              setFocus={() => { this.username.focus(); }}
              setValues={(store_number) => this.setValues('store_number', store_number)}
              inputlable="Store Number"
              selectedItems={this.state.registerOtpform.country_code} />
          </View>
          <_inputBox
            getFocus={(input) => { this.username = input; }}
            setFocus={() => { this.landmark.focus(); }}
            setValues={(username) => this.setValues('username', username)}
            img={require('../../images/your_name.png')}
            String="Your Name (Owner/Person-in-charge)" />

          <TouchableOpacity style={styles.inputViewIOS} onPress={() => { this.setModalVisible(true); }}>
            <View style={styles.inputBoxImgView}>
              <Image style={styles.inputBoxLogo} source={require('../../images/address.png')} />
            </View>
            <View style={[styles.inputBoxLableView, { paddingVertical: 5 }]}>
              {this.state.registerOtpform.address === '' ?
                <Text>Enter your address</Text>
                :
                <Text>{this.state.registerOtpform.address}</Text>
              }
            </View>
          </TouchableOpacity>

          {/* <_inputBox
            getFocus={(input) => { this.adderess = input; }}
            setFocus={() => { this.landmark.focus(); }}
            setValues={(address) => this.setValues('address', address)}
            img={require('../../images/address.png')}
            String="Address" /> */}
          {/* <_inputBox
            getFocus={(input) => { this.location = input; }}
            setFocus={() => { this.city.focus(); }}
            img={require('../../images/address.png')}
            String="Location" /> */}
          {/* <_inputBox
            getFocus={(input) => { this.city = input; }}
            setFocus={() => { this.landmark.focus(); }}
            img={require('../../images/address.png')}
            String="City" /> */}
          <_inputBox
            getFocus={(input) => { this.landmark = input; }}
            setFocus={() => { this.email.focus(); }}
            setValues={(landmark) => this.setValues('landmark', landmark)}
            img={require('../../images/address.png')}
            String="Landmark" />
          <_inputBox
            getFocus={(input) => { this.email = input; }}
            setFocus={() => { this.mobileNumber.focus(); }}
            setValues={(email) => this.setValues('email', email)}
            img={require('../../images/email.png')}
            String="Email Address" />
          <View style={{ marginTop: 20 }}>
            <CountryList
              getFocus={(input) => { this.mobileNumber = input; }}
              setFocus={() => { this.whatsAppNumber.focus(); }}
              setValues={(mobile_no) => this.setValues('mobile_no', mobile_no)}
              inputlable="Mobile Number"
              selectedItems={this.state.registerOtpform.country_code} />
          </View>
          {this._checkBox("Same as a mobile number")}
          {/* {this._inputWebBox(require('../../images/website.png'), "Website", "(Optional)")} */}
          <CountryList
            getFocus={(input) => { this.whatsAppNumber = input; }}
            setFocus={() => { this.password.focus(); }}
            setValues={(whatsapp_no) => this.setValues('whatsapp_no', whatsapp_no)}
            mvalue={this.state.registerOtpform.whatsapp_no}
            inputlable="WhatsApp Number"
            selectedItems={this.state.registerOtpform.country_code} />
          {this._inputPasswordBox(require('../../images/lock_ico.png'), "Password", require('../../images/eye_ico.png'), require('../../images/eye_open.png'))}
          {this._inputConfirmPasswordBox(require('../../images/lock_ico.png'), "Confirm Password", require('../../images/eye_ico.png'), require('../../images/eye_open.png'))}
          <View style={{ paddingVertical: 40 }}><CommonBttn buttonText={SIGN_UP} gotoNext={this.gotoNext} /></View>
          <View style={styles.footerTextView}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
              <Text style={styles.footerText}>{SIGNUP_FOOTER_TEXT}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, flexDirection: 'column',
  },
  logoView: {
    marginTop: 70, alignItems: 'center'
  },
  logo: {
    height: 120, width: 120, resizeMode: 'contain'
  },
  containtView: {
    marginVertical: 20, alignItems: 'center'
  },
  containtVerify: {
    fontSize: 24, color: TITLE_COLOR, fontWeight: 'bold', padding: 8
  },
  inputViewAndroid: {
    flexDirection: 'row', marginHorizontal: 20, marginTop: 20, borderColor: '#a09a9b', borderWidth: 0.5, borderRadius: 4, paddingHorizontal: 10
  },
  inputViewIOS: {
    flexDirection: 'row', marginHorizontal: 20, marginTop: 20, borderColor: '#a09a9b', borderWidth: 0.5, borderRadius: 4, padding: 10
  },
  inputBoxImgView: {
    flex: 1, justifyContent: 'center', alignItems: 'center'
  },
  inputBoxLogo: {
    height: 20, width: 20, resizeMode: 'contain'
  },
  inputBoxLableView: {
    flex: 8, justifyContent: 'center'
  },
  inNumLogoView: {
    flex: 1, justifyContent: 'center', alignItems: 'center'
  },
  inNumInputView: {
    flex: 6
  },
  webInTextView: {
    flex: 2, justifyContent: 'center', alignItems: 'center'
  },
  footerTextView: {
    alignItems: 'center', paddingBottom: 20
  },
  footerText: {
    textDecorationLine: 'underline', color: FOOTER_TEXT_COLOR, padding: 7
  },
  radioImg: {
    height: 25, width: 25, resizeMode: 'contain'
  },
  ShwoPassIconView: {
    flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7
  },
  inputIOS: {
    fontSize: 16,
    color: SUBTEXT,
    backgroundColor: "transparent"
  },
  inputAndroid: {
    fontSize: 16,
    color: SUBTEXT,
    backgroundColor: "transparent"
  },
});