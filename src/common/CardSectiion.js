import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
    return (
        <View style={[styles.containerStyle, props.style]}>
            {props.children}
        </View>

    );
};


const styles = {
    containerStyle: {
     // borderWidth: 2,
     //   padding: 5,
        //backgroundColor: "red",
      //  justifyContent: 'flex-start', 
       
        // flex:1,
        borderColor: '#ddd',
        position: 'relative'
    }
};
export { CardSection};