import React from 'react';
import {TextInput, View,Text} from 'react-native';



const Input =({placeholder,label,value,onChangeText,secureTextEntry,returnKeyType,blurOnSubmit,onSubmitEditing,ref}) => {
    const{inputStyle,labelStyle,containerStyle} =styles;
    return(
        <TextInput
        allowFontScaling={false}  
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}  
        autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        returnKeyType={returnKeyType}
        blurOnSubmit={blurOnSubmit}
        onSubmitEditing={onSubmitEditing}
        ref={ref}
        />
    );

};

const styles={
    inputStyle:{
        color:'#000',
        paddingLeft:15,
        fontSize:18,
        lineHeight:23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius:5,
        height:50,
        fontFamily:'CenturyGothic'
        
    },

    containerStyle:{
        height:50,
        borderWidth: 1,
        flexDirection:'row',
        alignItems:'center',
        // flex:1
    },
};

export {Input};