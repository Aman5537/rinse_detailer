import React from 'react';
import {TextInput, View,Text} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

const Radio =({placeholder,label,value,onChangeText,secureTextEntry}) => {
    const{inputStyle,labelStyle,containerStyle} =styles;
    return(
        <TextInput
        allowFontScaling={false}  
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        />
    );

};

const styles={
    inputStyle:{
        color:'#000',
        paddingLeft:15,
        fontSize:18,
        lineHeight:23,
        backgroundColor: '#dddd',
        marginBottom: 10,
        borderRadius:5,
        height:50,
    },

    containerStyle:{
        height:50,
        borderWidth: 1,
        flexDirection:'row',
        alignItems:'center',
        // flex:1
    },
};

export {Radio};