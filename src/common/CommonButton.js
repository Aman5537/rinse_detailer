import { Text, View, TouchableOpacity } from "react-native";
import React, { Component, StyleSheet } from "react";

const CommonButton = props => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View
        style={{
          padding:10,
          width: props.width,
          backgroundColor: props.background,
          borderRadius: props.radius,
          justifyContent: "center",
          alignItems: "center",
           marginBottom: 8
        }}
      >
        <Text  allowFontScaling={false}   style={{ color: props.color, fontSize: 16,fontFamily: 'Aileron-Regular' }}>{props.name}</Text>
      </View>
    </TouchableOpacity>
  );
};


export default CommonButton;
