import React from 'react';
import { View, Text } from 'react-native';

const AuthTitle = (props) => {
    return (
        <View style={style.containerStyle}>
            <Text  allowFontScaling={false}   style={{ fontSize: 23,fontFamily: 'Aileron-Regular' }}>{props.TitleName}</Text>
        </View>

    );
};


const style = {
    containerStyle: {
        // backgroundColor: "red",
       // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10, 
        position: 'relative',
        marginTop:10
    }
};

export { AuthTitle };
