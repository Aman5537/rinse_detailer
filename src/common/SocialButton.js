//This is an example code to show image in a button//
import React, { Component } from 'react';
//import react in our code.

import { StyleSheet, View, Text, Image, TouchableOpacity,Modal } from 'react-native';

//import all the components we are going to use.
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { NavigationActions,DrawerActions } from 'react-navigation';
import { GoogleSignin, GoogleSigninButton,statusCodes } from 'react-native-google-signin';

import Helper from '../Lib/Helper';


class SocialButton extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props);
    this.state = {
      isSigninInProgress:false,
      modalVisible: false,
      screenPointer:false
    }
    
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '281406079000-421cdbqnnkmsg7n05u2bmlm248uvbglv.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      accountName: '', // [Android] specifies an account name on the device that should be used
      iosClientId: '', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });

    
    //this.gotoHome = this.gotoHome.bind(this);
  }


  

  setModalVisible(visible) {

    this.setState({ modalVisible: visible });
  }

  modalBlockRender() {
    return (

        <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                this.setModalVisible(true)
                //Alert.alert('Modal has been closed.');
            }}>

            <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH, alignItems: 'center', justifyContent: "center" }}>
                <View style={{ height: 380, marginLeft: -10, marginRight: -10, marginTop: -5, backgroundColor: '#fff', padding: 15, borderRadius: 5 }}>
                    <TouchableOpacity onPress={() => {
                        this.setModalVisible(false)
                    }}

                    >
                        <Text style={{ fontSize: 20, justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                    </TouchableOpacity>

                    <View style={{ padding: 10 }}>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 16, marginTop: 10 }}>Which upgrade package would you like<Text allowFontScaling={false} style={{ fontFamily: 'Aileron-Regular' }}>?</Text></Text>

                        
                    </View>
                </View>

            </View>



        </Modal>



    );
}

  fbauth = () => {
    // const resetAction = NavigationActions.navigate({
    //   routeName: 'SocialProfile'
    // });
    
    // const { navigation } = this.props.navigation.navigate('SocialProfile');
    
    
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      
      function (result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {

          

          AccessToken.getCurrentAccessToken().then(
            (data) => {

              

              console.log(data.accessToken.toString())
              fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + data.accessToken)
                .then((response) => response.json())
                .then((json) => {
                  let form = {
                    social_id: json.id,
                    email: json.email,
                    name: json.name,
                    social_type: 'FACEBOOK',
                    role_id:2,
                    device_token: "SC9830I", device_type: "ANDROID"

                  }

                  console.log('facebook data',form);
                  Helper.makeRequest({ url: "check-user", data: form }).then((responsedata) => {
                    
                    // console.log('----logindata :  ' + JSON.stringify(responsedata));
                    if (responsedata.status == 'true') {
                      
                      
                      Helper.setData('userdata', responsedata.data);
                      Helper.setData('token', responsedata.security_token);

                      if(responsedata.data.is_redirect == 1){
                        
                        Helper.nav.setRoot('SocialProfile');
                      }
                      else{
                        
                        Helper.nav.setNavigate('SetHome');
                        
                        //this.props.navigation.navigate('SetHome');
                      }
                      
                      
                    }
                    else {
                      Helper.showToast(responsedata.message)
                    }
                  }).catch(err => {
                    //Helper.showToast("Please try again");
                  })

                  if (json.picture.data.url) {
                    form.image = json.picture.data.url;
                  }

                  if (json.email) {
                    form.email = json.email;
                    cb(form);

                  } else {
                    cb(form);
                  }

                })
                .catch(() => {
                  // this.hideLoader();
                  console.log('ERROR GETTING DATA FROM FACEBOOK')
                })
            }
          )
        }
      },
      function (error) {
        
        console.log("Login fail with error: " + error);
      }
    );

  }


  handleFirebaseLogin(accessToken, options) {
    Firebase.auth()
      .signInWithCredential(accessToken)
      .then(function (data) {
        var user = Firebase.auth().currentUser;
      })
      .catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
        if (errorCode === 'auth/account-exists-with-different-credential') {
          // Email already associated with another account.
        }
      })
  }




  // Somewhere in your code
  _signIn = async () => {
    this.setState({screenPointer:true})
    try {
        
        //this.setModalVisible(true)
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      
      
      
      let form = {
        social_id: userInfo.user.id,
        email: userInfo.user.email,
        first_name: userInfo.user.givenName,
        last_name: userInfo.user.familyName,
        social_type: 'GOOGLE',
        role_id:2,
        device_token: "SC9830I", device_type: "ANDROID"

      }

      //console.log('google data',form);
      Helper.makeRequest({ url: "check-user", data: form }).then((responsedata) => {
        
        if (responsedata.status == 'true') {
          console.log('checkUser');
          
          Helper.setData('userdata', responsedata.data);
          Helper.setData('token', responsedata.security_token);
          if(responsedata.data.is_redirect == 1){
            Helper.nav.setRoot('SocialProfile');
          }
          else{
            //Helper.nav.push('SetHome');
            this.props.navigation.navigate('SetHome');
          }
          
          
            
          
          
          
        }
        else {
          Helper.showToast(responsedata.message)
        }
      })


    } catch (error) {
      
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        
        // play services not available or outdated
      } else {
        
        // some other error happened
      }
      
      
    }
  };



   
  gotoHome = () => {
    
    this.props.navigation.navigate('SocialProfile');
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white',}} pointerEvents={"auto"}>
        <TouchableOpacity
         //onPress={() => { this.fbauth() }}
         style={{flex:1}}
        >
            <View style={{ padding: 5, backgroundColor: '#475b93',borderRadius:5 }}>
                <View style={{ height: 40, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        resizeMode='contain'
                        source={require('../../assets/Icons/facebook.png')}
                        style={{ height: 35, width: 20, margin: 5, flex: 1.5, }}
                    />

                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ flex: 7, color: '#fff', paddingRight: 5, paddingLeft: 5, fontSize: 18, lineHeight: 23,textAlign:'center' }}>
                        Login with Facebook </Text>

                </View>
            </View>
        </TouchableOpacity>

        

        <TouchableOpacity 
        onPress={() => { this._signIn() }}
        style={{flex:1}}
        >
            <View style={{ padding: 5, backgroundColor: '#c94238', marginTop: 10,borderRadius:5 }}>
                <View style={{ height: 40, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        resizeMode='contain'
                        source={require('../../assets/Icons/google.png')}
                        style={{ height: 35, width: 20, margin: 5, flex: 1.5, }}
                    />

                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ flex: 7, color: '#fff', paddingRight: 5, paddingLeft: 5, fontSize: 18, lineHeight: 23,textAlign:'center' }}>
                        Login with Google </Text>

                </View>
            </View>
        </TouchableOpacity>
        
        {/* {this.state.screenPointer && 
        <View pointerEvents={"none"}
        style={{
            width: "100%",
            height: "100%",
            flex:1,
            backgroundColor: '#cccccc',
            position: "absolute",
            alignSelf: "center",
            justifyContent: "center",
            alignItems: "center"
        }}>
        <Text>Hello</Text>
        </View>
        } */}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column',
    
  },
});


export default SocialButton;