import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import Ionc from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { Avatar, Badge, Rating, AirbnbRating } from 'react-native-elements';
import RadioButton from 'react-native-radio-button'

const ReviewMessage = () => {
    return (
        <View style={{}}>

            <Text  allowFontScaling={false}   style={{ fontWeight: 'bold', fontSize: 18, right: 0 }}>Plase rate your detailer</Text>
            <Text  allowFontScaling={false}   style={{ fontSize: 16, }}>
                By rating your detailer,it helps customers like yourself select who's best
        </Text>
            <Rating style={{ margin: 5 }}
                type='star'
                ratingCount={5}
                imageSize={30}
                ratingBackgroundColor='#f1c40f'
                onFinishRating={this.ratingCompleted}
            />
            <TextInput
                multiline
                numberOfLines={4}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholder='Comment'
                style={{ backgroundColor: '#dddd', borderRadius: 5, textAlignVertical: "top", paddingLeft: 20, width: 300 }} />
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}>
                <View style={{ width: 110, height: 40, backgroundColor: '#007cc9', alignSelf: 'center', margin: 20 }}>
                    <Text  allowFontScaling={false}   style={{ color: 'white', fontSize: 18, padding: 5, textAlign: 'center' }}>Continue</Text>
                </View>
            </TouchableOpacity>


        </View>
    )
}


const RadioButtonMsg = () => {
    return (
        <View style={{}}>

            <Text  allowFontScaling={false}   style={{ fontWeight: 'bold', fontSize: 18, right: 0 }}>Ypur Total is $35.Would you like to leave a tip?</Text>
            <Text  allowFontScaling={false}   style={{ fontSize: 16, }}>
                By rating your detailer,it helps customers like yourself select who's best
        </Text>
            <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center' }}>
                <RadioButton
                    animation={'bounceIn'}
                    isSelected={false}
                    // onPress={() => doSomething('hello')}
                />
                <Text> 10% = $3.5</Text>
            </View>

            <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center' }}>
                <RadioButton
                    animation={'bounceIn'}
                    isSelected={false}
                    // onPress={() => doSomething('hello')}
                />
                <Text> 10% = $3.5</Text>
            </View>

            <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center' }}>
                <RadioButton
                    animation={'bounceIn'}
                    isSelected={false}
                    // onPress={() => doSomething('hello')}
                />
                <Text> 10% = $3.5</Text>
            </View>

            <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center' }}>
                <RadioButton
                    animation={'bounceIn'}
                    isSelected={false}
                    // onPress={() => doSomething('hello')}
                />
                <Text> Enter a different tip ammount</Text>
            </View>
            <TextInput
                style={{ height: 40, width: 100, backgroundColor: '#ddd', marginLeft: 30 }}
            />

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }} style={{ flexDirection: 'row', alignSelf:'center' }}>
                <View style={{ marginRight: 15, width: 110, height: 40, backgroundColor: '#007cc9', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                    <Text  allowFontScaling={false}   style={{ color: 'white', fontSize: 18 }}>Submit</Text>
                </View>
                <View style={{ marginLeft: 15, width: 110, height: 40, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                    <Text  allowFontScaling={false}   style={{ color: 'white', fontSize: 18 }}>No Tip</Text>
                </View>
            </TouchableOpacity>


        </View>
    )
}


export default class ServicedetailCard extends Component {
    constructor(props) {
        super(props);
        this.state = {

            ReviewMessage: <ReviewMessage />,
            RadioButtonMsg: <RadioButtonMsg />


        }
    }
    // checkit(chck) {
    //     if (chck) {
    //         <Text>No</Text>
    //     } else {
    //         <Text>No</Text>
    //     }
    // }

    _onReviewMessagePress = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.ReviewMessage })

    }

    _onRadioPress = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.RadioButtonMsg })

    }

    render() {
        let bgButton, align
        if (this.props.alignS) {
            bgButton = "red",
                align = 'flex-end'
        } else {
            align = 'stretch'
            bgButton = "red"
        }
        return (
            <View style={{ flexDirection: 'row', paddingLeft: 8, paddingRight: 8, justifyContent: 'space-between' }}>
                <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: 'black', marginRight: 8 }}>
                </View>
                <View style={{ flexDirection: 'row', flex: 6, }}>
                    <View style={{ top: 5, }}>
                        <Text>
                            <Text  allowFontScaling={false}   style={{ fontWeight: 'bold', fontSize: 15 }}>{this.props.ServiceName}</Text>
                        </Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', alignContent: 'center' }}>
                            <FontAwesome name='circle' style={{ margin: 2, color: this.props.dotcolor, fontSize: 8 }} />
                            <Text>Detailer:{this.props.DetailerText}</Text>
                        </View>

                        <Text>Ontime Rate:{this.props.OntimeRate}</Text>
                        <Text>0 Cancellations/No Shows</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 3 }}>
                            <FontAwesome name='star' style={{ margin: 2, color: this.props.startColor, fontSize: 12 }} />
                            <FontAwesome name='star' style={{ margin: 2, color: this.props.startColor, fontSize: 12 }} />
                            <FontAwesome name='star' style={{ margin: 2, color: this.props.startColor, fontSize: 12 }} />
                            <FontAwesome name='star' style={{ margin: 2, fontSize: 12 }} />
                            <FontAwesome name='star' style={{ margin: 2, fontSize: 12 }} />

                            <TouchableOpacity onPress={this._onReviewMessagePress}
                            >
                                <Text  allowFontScaling={false}   style={{ left: 10 }}>{this.props.Reviews}</Text>
                            </TouchableOpacity>


                        </View>
                    </View>

                </View>

                <View style={{ flex: 4, justifyContent: 'space-evenly' }}>
                    <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', fontSize: 20, padding: 5, color: '#0083c7' }}>{this.props.Price}</Text>


                    <View>
                        {this.props.redButton && (

                            <TouchableOpacity
                                onPress={this.props.onPress}
                                style={[{ backgroundColor: bgButton, marginBottom: 5, }]}>
                                <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>{this.props.children}</Text>
                            </TouchableOpacity>

                        )}
                        <TouchableOpacity onPress={this._onRadioPress}
                            style={{ backgroundColor: 'grey', alignSelf: align }}>
                            <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>Message</Text>
                        </TouchableOpacity>
                    </View>


                    {/* <TouchableOpacity onPress={this.props.PressMessage}
                        style={{ backgroundColor: 'grey', alignSelf: align }}>
                        <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>Message</Text>
                    </TouchableOpacity> */}


                </View>



            </View>

        )
    }
}







// switch (this.props.Botton) {
//     case "SmallButton":
//         return (
// <View style={{ flexDirection: 'row', paddingLeft: 8, paddingRight: 8, justifyContent: 'space-between' }}>
//     <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: 'black', marginRight: 8 }}>
//     </View>
//     <View style={{ flexDirection: 'row', flex: 6, }}>
//         <View style={{ top: 5, }}>
//             <Text>
//                 <Text  allowFontScaling={false}   style={{ fontWeight: 'bold', fontSize: 15 }}>{this.props.ServiceName}</Text>
//             </Text>
//             <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', alignContent: 'center' }}>
//                 <FontAwesome name='circle' style={{ margin: 2, color: 'red', fontSize: 8 }} />
//                 <Text>Detailer:{this.props.DetailerText}</Text>
//             </View>

//             <Text>Ontime Rate:{this.props.OntimeRate}</Text>
//             <Text>0 Cancellations/No Shows</Text>

//             <View style={{ flexDirection: 'row', alignItems: 'center' }}>
//                 <FontAwesome name='star' style={{ margin: 2, color: 'yellow', fontSize: 12 }} />
//                 <FontAwesome name='star' style={{ margin: 2, color: 'yellow', fontSize: 12 }} />
//                 <FontAwesome name='star' style={{ margin: 2, color: 'yellow', fontSize: 12 }} />
//                 <FontAwesome name='star' style={{ margin: 2, color: 'grey', fontSize: 12 }} />
//                 <FontAwesome name='star' style={{ margin: 2, color: 'grey', fontSize: 12 }} />
//                 <Text  allowFontScaling={false}   style={{ left: 10 }}>{this.props.Reviews}</Text>
//             </View>
//         </View>

//     </View>

//     <View style={{ flex: 4, }}>

//         <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', fontSize: 20, padding: 5, color: 'black' }}>{this.props.Price}</Text>
//         <TouchableOpacity
//             onPress={this.props.onPress}
//             style={{ backgroundColor: 'red', marginBottom: 5, }}>
//             <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>{this.props.children}</Text>
//         </TouchableOpacity>

//         <TouchableOpacity onPress={this.props.PressMessage}
//             style={{ backgroundColor: 'grey', }}>
//             <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>Message</Text>
//         </TouchableOpacity>


//     </View>

// </View>

//         )
//     case "BigButton":
//         return (
//             <View style={{ flexDirection: 'row', paddingLeft: 8, paddingRight: 8, justifyContent: 'space-between' }}>
//                 <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: 'black', marginRight: 8 }}>
//                 </View>
//                 <View style={{ flexDirection: 'row', flex: 6, }}>
//                     <View style={{ top: 5, }}>
//                         <Text>
//                             <Text  allowFontScaling={false}   style={{ fontWeight: 'bold', fontSize: 15 }}>{this.props.ServiceName}</Text>
//                         </Text>
//                         <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', alignContent: 'center' }}>
//                             <FontAwesome name='circle' style={{ margin: 2, color: 'red', fontSize: 8 }} />
//                             <Text>Detailer:{this.props.DetailerText}</Text>
//                         </View>

//                         <Text>Ontime Rate:{this.props.OntimeRate}</Text>
//                         <Text>0 Cancellations/No Shows</Text>

//                         <View style={{ flexDirection: 'row', alignItems: 'center' }}>
//                             <FontAwesome name='star' style={{ margin: 2, color: 'yellow', fontSize: 12 }} />
//                             <FontAwesome name='star' style={{ margin: 2, color: 'yellow', fontSize: 12 }} />
//                             <FontAwesome name='star' style={{ margin: 2, color: 'yellow', fontSize: 12 }} />
//                             <FontAwesome name='star' style={{ margin: 2, color: 'grey', fontSize: 12 }} />
//                             <FontAwesome name='star' style={{ margin: 2, color: 'grey', fontSize: 12 }} />
//                             <Text  allowFontScaling={false}   style={{ left: 10 }}>{this.props.Reviews}</Text>
//                         </View>
//                     </View>

//                 </View>

//                 <View style={{ flex: 4, }}>
//                     <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', fontSize: 20, padding: 5, color: 'black' }}>{this.props.Price}</Text>
//                     <TouchableOpacity onPress={this.props.PressSelect}
//                         style={{ backgroundColor: 'red', marginBottom: 5, }}>
//                         <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>{this.props.children}</Text>
//                     </TouchableOpacity>

//                     <TouchableOpacity onPress={this.props.PressMessage}
//                         style={{ backgroundColor: 'grey', alignSelf: 'flex-end' }}>
//                         <Text  allowFontScaling={false}   style={{ textAlign: 'center', color: 'white', margin: 5 }}>Message</Text>
//                     </TouchableOpacity>

//                 </View>

//             </View>
//         )
//     default:
//         return "No Icon"
// }



{/* <View>
<Text>Ionicons Icons</Text>
<Ionc name='md-bicycle' />

<Text>FontAwesome Icons</Text>
<FontAwesome name='trophy' 
style={{ height:50,width:50,color:'red' ,fontSize:20}} />

<Text>Entypo Icons</Text>
<Entypo name='aircraft' />
</View> */}