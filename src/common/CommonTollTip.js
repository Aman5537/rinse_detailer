import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native';

import Tooltip from 'react-native-walkthrough-tooltip';

export default class CommonTollTip extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            toolTipVisible: false
        }
    }

    openTip() {
        this.setState({ toolTipVisible: true }, () => {
            setTimeout(() => {
                this.setState({ toolTipVisible: false });
            }, 10000)
        });
    }

    render() {
        return (
            <Tooltip
                animated
                isVisible={this.state.toolTipVisible}
                content={<Text>{this.props.tooltiptext}</Text>}
                onClose={() => this.setState({ toolTipVisible: false })}
                contentStyle={{right:-12}}
            >
                <TouchableOpacity hitSlop={{ top: 30,  bottom: 30, left: 20 }} onPress={() => this.openTip()}>
                <Image
                        style={{ height: 20, width: 20, marginLeft: 5,marginTop:this.state.marginTopLip }}
                        source={require('../../assets/Icons/i-icon.png')}
                />
                </TouchableOpacity>
            </Tooltip>
        );
    }
}
