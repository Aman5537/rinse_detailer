import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import Helper from '../Lib/Helper';

export default class Badges extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            appoCount: 0,
            messageCount:0,
            tabType:this.props.tabType
        }
    }

    componentWillMount() {
        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)

            if (data.status == 'true') {
                
                this.setState({appoCount:data.total_unread_appointment,messageCount:data.total_unread_message})
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                alert(this.state.appoCount+'--'+this.state.messageCount);
            } 

        })

    }

    componentWillUnmount() {
        
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()
    }

    

    render() {
        if(this.state.tabType == 3){
            return (
                <View>
                {/* {this.state.appoCount > 0 && ( */}
                    <View style={{position: 'absolute',top: '5%',left: '65%',backgroundColor: 'red',height: 15,minWidth: 15,
                    borderRadius: 10,alignItems: 'center',justifyContent: 'center'}}>
                          <Text style={{paddingHorizontal: 5,paddingBottom: 0,color: '#fff',fontSize: 10}}>{Helper.appoCount}</Text>
                    </View>
                {/* )} */}
                </View>
            );
        }
        
        if(this.state.tabType == 4){
            return (
                <View>
                {/* {this.state.messageCount > 0 && ( */}
                    <View style={{position: 'absolute',top: '5%',left: '65%',backgroundColor: 'red',height: 15,minWidth: 15,
                    borderRadius: 10,alignItems: 'center',justifyContent: 'center'}}>
                        <Text style={{paddingHorizontal: 5,paddingBottom: 0,color: '#fff',fontSize: 10}}>{Helper.messageCount}</Text>
                    </View>
                {/* )} */}
                </View>
            );
        }
    }
}
