import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ImageBackground, SafeAreaView, Keyboard,PermissionsAndroid,Platform } from 'react-native';
import { Header, Avatar } from 'react-native-elements';
import Helper from '../Lib/Helper';
//import { CachedImage,ImageCacheProvider} from 'react-native-cached-image';
import Geolocation from '@react-native-community/geolocation';
import Moment from "moment";
import { EventRegister } from 'react-native-event-listeners';
import ChatController from '../Lib/ChatController';
import { NavigationActions, StackActions } from 'react-navigation';
import Permissions from 'react-native-permissions';

class CustomHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            is_login: false,
            userdata: ''
        }
        
        //this.getPermissions();
        
        
        
        Helper.getData('userdata').then((responseData) => {
            // console.log('------userData  :  ' + JSON.stringify(responseData));
            if (responseData === null || responseData === 'undefined' || responseData === '') {

            } else {
                this.setState({ is_login: true });
                this.setState({ userdata: responseData });

            }
        })

        

        this.myInterval = setInterval(() => {
            this.getPermissions()
        }, 30000);
    } 


    async getPermissions(){
        
        // const granted = await PermissionsAndroid.request(
        //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        // )
        // if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            
        //     Geolocation.getCurrentPosition(info => {});
        //     this.LocationUpdate()
        //     Helper.permissionAlert = false;
        // } else {
        //     //alert( "Please Enable Your Device Location Permission From Settings." )
            
        // }
        
        Permissions.check('location').then(response => {
            if (response == 'authorized') {
                Geolocation.getCurrentPosition(info => {});
                        this.LocationUpdate()
                        Helper.permissionAlert = false;
            }
            else{
                Permissions.request('location').then(response => {
                    if (response == 'authorized') {
                        Geolocation.getCurrentPosition(info => {});
                        this.LocationUpdate()
                        Helper.permissionAlert = false;
                    }
                    else {
                        Helper.confirm("Allow access to your location.", (status) => {
                            if (status) {
                                Permissions.canOpenSettings().then(response => {
                                    if(response){
                                        Permissions.openSettings();
                                    }
                                })
                            }
                        })
                    }
        
                })
        
            }
        })
                
    }

    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener("didFocus", () => {
            
            Helper.getData('userdata').then((responseData) => {
                // console.log('------userData  :  ' + JSON.stringify(responseData));
                if (responseData === null || responseData === 'undefined' || responseData === '') {
    
                } else {
                    this.setState({ is_login: true });
                    this.setState({ userdata: responseData },() => {
                        //if(this.state.userdata.is_sub_detailer == 1){
                            //setInterval(() => {
                                this.LocationUpdate()
                            //}, 20000);
                            
                        //}
                        
                    });
    
                }
            })

            
            

        });
    }


    getBadegesCount() {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            last_id: 0,
            user_id: this.state.userdata.id,
            role_id: 2,
            utc_date_time: gettime
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);
    }


    async LocationUpdate(){

        Permissions.check('location').then(response => {
            if (response == 'authorized') {
                console.log('Location Update Function Call');
            Geolocation.watchPosition(position => {
                                
                    this.setSubDetailerLocation(position);
                    
                });
            }
            else{
                Helper.confirm("Allow access to your location.", (status) => {
                    if (status) {
                        Permissions.canOpenSettings().then(response => {
                            if(response){
                                Permissions.openSettings();
                            }
                        })
                    }
                })
                Helper.permissionAlert = false;
            }

        })
        // const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );

        // if (granted) {
        //     console.log('Location Update Function Call');
        //     Geolocation.watchPosition(position => {
                                
        //             this.setSubDetailerLocation(position);
                    
        //         });
        // } 
        // else{
        //     if(Helper.permissionAlert){
        //         alert( "Please Enable Your Device Location Permission From Settings." )
        //         Helper.permissionAlert = false;
        //     }
        // }
        
    }

    componentWillMount() {
        
        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('get_appointment_message_count_response', data)
            
            if (data.status == 'true') {
                this.setState({ appoinmentCount: data.total_unread_appointment, messageCount: data.total_unread_message,requestTotalCount: data.total_unread_request })
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;

            } else {
                this.setState({ isLoader: false });
            }

        })

    }

   

    componentWillUnmount() {
        // Remove the event listener
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove();
        clearInterval(this.myInterval);
    }


    setSubDetailerLocation = (data) => {
        
        if((Helper.user_location != '') && (Helper.user_location.coords.latitude == data.coords.latitude) && (Helper.user_location.coords.longitude == data.coords.longitude) ){
            Helper.user_location = data;
            return false;
        }
        else{
            Helper.user_location = data;
        }
        
        
        //alert(data.coords.longitude)

        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")
        var TimeZone = tempdate.getTimezoneOffset();
        let tempdata = {
            detailer_id: this.state.userdata.id,
            location_lat: data.coords.latitude,
            location_long: data.coords.longitude,
            angle: data.coords.heading,
            utc_date_time: gettime,
            time:TimeZone
        }

        Helper.makeRequest({ url: "add-sub-detailer-location", method: "POST", data: tempdata,loader:false }).then((data) => {
            if (data.status == 'true') {
                
                
            }
            
        })

    }

    componentWillReceiveProps(nextprops) {
        // console.log("nextprops",nextprops);
        // if(nextprops.userinfo && nextprops.userinfo.id){
        //     this.setState({ userdata: nextprops.userinfo });
        // }
        Helper.getData('userdata').then((responseData) => {
            // console.log('------userData  :  ' + JSON.stringify(responseData));
            if (responseData === null || responseData === 'undefined' || responseData === '') {

            } else {
                this.setState({ is_login: true });
                this.setState({ userdata: responseData });

            }
        })
    }


    gotoHome() {
        if (this.state.is_login) {
            this.getBadegesCount()
            //this.props.navigation.navigate('SetHome');

            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Home' })],
            });

            this.props.navigation.dispatch(resetAction); 
        }


    }

    render() {
        return (

            <SafeAreaView>

                
                <ImageBackground source={require('../../assets/Icons/topbar.png')} style={{ width: "100%", height: 60 }} >
                    <View style={{ height: 60, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <View style={{ flex: 1, marginLeft: 9 }}>
                            {this.props.bArrow && <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={require('../../assets/Icons/harrow.png')} style={{ height: 25, width: 25, }}
                                />
                            </TouchableOpacity>}
                            {this.props.HomeArrow && <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Image source={require('../../assets/Icons/harrow.png')} style={{ height: 25, width: 25, }}
                                />
                            </TouchableOpacity>}
                        </View>
                        <View style={{ flex: 1, alignItems: "center", flexGrow: 2 }}>

                            <View style={{ alignItems: 'center', justifyContent: 'center' }}

                            >
                                <TouchableOpacity onPress={() => this.gotoHome()}>
                                    <Image

                                        source={require('../../assets/Icons/logo.png')}
                                        style={{ height: 50, width: 50, resizeMode: 'contain' }}
                                    />
                                </TouchableOpacity>
                            </View>


                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 12, alignContent: 'center' }}>
                            {this.props.userbar && <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}
                                    style={{ marginRight: 15 }}
                                >
                                    {this.state.userdata.profile_picture != '' &&
                                        // <Avatar rounded
                                        //     size={35}
                                        //     source={{ uri: Helper.getImageUrl(this.state.userdata.profile_picture) }}
                                        // />
                                        <Image
            style={{width:40,height:40,borderRadius:20}}
            source={{uri: Helper.getImageUrl(this.state.userdata.profile_picture),
                cache: 'force-cache',
            }}
           
        />
                                    }
                                    {this.state.userdata.profile_picture == '' &&
                                        // <Avatar rounded
                                        //     size={35}
                                        //     source={require('../../assets/Icons/profile_user.png')}
                                        // />
                                        <Image
            style={{width:40,height:40,borderRadius:20}}
            source={require('../../assets/Icons/profile_user.png')}
           
        />
                                    }
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => {
                                    Keyboard.dismiss();
                                    this.props.navigation.toggleDrawer();
                                }} accessible={false}
                                    style={{ alignSelf: 'center', justifyContent: 'center' }}
                                >
                                    <Image
                                        source={require('../../assets/Icons/bar.png')} style={{ height: 25, width: 25 }}
                                    />
                                </TouchableOpacity>
                            </View>}
                        </View>
                    </View>

                </ImageBackground>
            </SafeAreaView>
        )
    }
}

export { CustomHeader }