import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children  ,BText}) => {
    const { buttonStyle, textStyle } = styles;

    return (
        <TouchableOpacity onPress={onPress}
       style={{ justifyContent: 'center', backgroundColor: '#1d87dd', alignItems: 'center', marginTop: 20 ,borderRadius:5}}>
        <Text  allowFontScaling={false}   style={{ color: 'white', fontSize: 20, padding: 12,fontFamily: 'Aileron-Regular' }}>{BText}</Text>
    
        </TouchableOpacity>

    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },

    buttonStyle: {
        flex: 1,
        borderRadius:5,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5

    }
};

export {Button};