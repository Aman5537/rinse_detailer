import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity,Linking } from 'react-native';
import { Header } from 'react-native-elements';




class HeaderTitle extends Component {
    // constructor(props) {
    //     super(props);

    // }


    render() {
        return (
            <Header
                // leftComponent={

                // }

                centerComponent={
                    <Text allowFontScaling={false} font={'CenturyGothic-bold'}  style={{ fontSize: 22,alignItems:'center',marginTop:-25,height:30}}>{this.props.TitleName}</Text>
                }

                rightComponent={
                    <View> 
                        {this.props.date && (<View style={{ flexDirection: 'row',marginTop:-25 }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Appointments')}
                            >
                                <Image
                                    resizeMode='contain'
                                    style={{ height: 25, width: 25, margin: 5 }}
                                    source={require('../../assets/Icons/calender.png')}

                                /></TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('AppointmentLists')}
                            >
                                <Image
                                    resizeMode='contain'
                                    style={{ height: 25, width: 25, margin: 5 }}
                                    source={require('../../assets/Icons/menu.png')}

                                /></TouchableOpacity>
                        </View>)}


                        {this.props.date2 && (<View style={{ flexDirection: 'row',marginTop:-25 }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Appointments')}
                            >
                                <Image
                                    resizeMode='contain'
                                    style={{ height: 25, width: 25, margin: 5 }}
                                    source={require('../../assets/Icons/calender_io.png')}

                                /></TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('AppointmentLists')}
                            >
                                <Image
                                    resizeMode='contain'
                                    style={{ height: 25, width: 25, margin: 5 }}
                                    source={require('../../assets/Icons/menu_io.png')}

                                /></TouchableOpacity>
                        </View>)}


                        {this.props.message && (<TouchableOpacity style={{marginTop:-25}}
                            onPress={() => this.props.navigation.navigate('TransactionHistory')}
                        >

                            <Image
                                resizeMode='contain'
                                style={{ height: 25, width: 25 }}
                                source={require('../../assets/Icons/message-icon.png')}
                            />
                        </TouchableOpacity>)}


                        {this.props.messageT && (<TouchableOpacity style={{marginTop:-25}}
                        onPress={() => Linking.openURL('mailto:support@rinse.com') }    
                        >

                            <Image
                                resizeMode='contain'
                                style={{ height: 25, width: 25 }}
                                source={require('../../assets/Icons/message-icon.png')}
                            />
                        </TouchableOpacity>)}

                        {/* {this.props.Add && (<TouchableOpacity style={{}}>
                    
                                    <Image
                                    resizeMode='contain'
                                    style={{ height:20 , width:20 }}
                                    source={require('../../assets/Icons/plus.png')}
                                />
                        </TouchableOpacity>)} */}

                        {this.props.AddD && (<TouchableOpacity style={{marginTop:-25}}
                            onPress={this.props.onAddClick}
                        >

                            <Image
                                resizeMode='contain'
                                style={{ height: 30, width: 30 }}
                                source={require('../../assets/Icons/plus_ico.png')}
                            />
                        </TouchableOpacity>)}




                    </View>







                }

                backgroundColor='#ffff'
                containerStyle={{ height: 70 }}
                
            />


        )
    }
}

export { HeaderTitle }