import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Platform } from 'react-native';

export default class RadioButtonNew extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            (this.props.button.selected) ?
                <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                    <View style={{borderRadius: 50,borderWidth: this.props.button.selectedBorderWidth,justifyContent: 'center',alignItems: 'center',height: this.props.button.size, width: this.props.button.size, borderColor: this.props.button.selectedborderColor }}>
                        <View style={[styles.radioIcon, { height: this.props.button.size / 2, width: this.props.button.size / 2, backgroundColor: this.props.button.selectedinnerColor }]}></View>
                    </View>
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                    <View style={[styles.radioButtonHolder, { height: this.props.button.size, width: this.props.button.size, borderColor: this.props.button.unselectedborderColor }]}>
                    </View>
                </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:
        {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 25
        },

        radioButton:
        {
            flexDirection: 'row',
            //margin: 10,
            alignItems: 'center',
            justifyContent: 'center'
        },

        radioButtonHolder:
        {
            borderRadius: 50,
            borderWidth: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },

        radioIcon:
        {
            borderRadius: 50,
            justifyContent: 'center',
            alignItems: 'center'
        },

        label:
        {
            marginLeft: 10,
            fontSize: 20
        },

        selectedTextHolder:
        {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            padding: 15,
            backgroundColor: 'rgba(0,0,0,0.6)',
            justifyContent: 'center',
            alignItems: 'center'
        },

        selectedText:
        {
            fontSize: 18,
            color: 'white'
        }
    });