
import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import color from '../config/color';

const Footer = ({FooterText,onPress,backgroundColor}) => {
    const { textStyle, viewStyle } = styles;

    return (
            <TouchableOpacity  onPress={onPress}
            style={[viewStyle ,{backgroundColor:backgroundColor } ]}>

                <Text  allowFontScaling={false}   style={textStyle}> {FooterText} </Text>

            </TouchableOpacity>
       
    );

};

const styles = {
    viewStyle: {
        flex: 1,
       // backgroundColor: '#1d87dd',
        left: 0,
        bottom: 0,
        right: 0,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        // paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
    },
    textStyle: {
        fontSize: 22,
        color: 'white',

    }
};

export  {Footer}