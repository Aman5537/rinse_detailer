import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity,ImageBackground } from 'react-native';
import { Header } from 'react-native-elements';



class SimpleHeader extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            
            <View style={{backgroundColor:'#33568c'}}>

            
           <View style={{height:60,flexDirection:"row",justifyContent:"center",alignItems:"center",marginTop:23}}>
                    <View style={{flex:1,marginLeft:12}}>
                    {this.props.bArrow &&<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image  source={require('../../assets/Icons/harrow.png')} style={{ height: 25, width: 25, }}
                            />
                        </TouchableOpacity>}
                    </View>
                    <View style={{flex:1,alignItems:"center",flexGrow:2}}>
                    
                            <View style={{alignItems:'center' ,justifyContent:'center'}}
                            
                            >
                            <Text  allowFontScaling={false}   
                            style={{color:'#ffffff',fontSize:19}}
                            
                            >{this.props.pagetitle}</Text>
                            </View>

                        
                    </View>
                    <View style={{flex:1,alignItems:"flex-end",marginRight:15}}>
                    {this.props.userbar && <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}
                            >
                                <Image
                                    style={{ height: 30, width: 30, marginRight: 15 }}
                                    source={require('../../assets/Icons/profile.png')}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                                <Image
                                    source={require('../../assets/Icons/bar.png')} style={{ height: 25, width: 25, }}
                                />
                            </TouchableOpacity>
                        </View>}
                    </View>
                    </View>
            
                
                </View>
        )
    }
}

export { SimpleHeader }














// const CustomHeader = ({placeholder,label,value,onChangeText,secureTextEntry}) => {
//     const{inputStyle,labelStyle,containerStyle} =styles;
//     return (

//         <View style={containerStyle}>
//             <Text  allowFontScaling={false}   style={labelStyle}>{label}</Text>

//             <TextInput
//                 secureTextEntry={secureTextEntry}
//                 placeholder={placeholder}
//                 autoCorrect={false}
//                 style={inputStyle}
//                 value={value}
//                 onChangeText={onChangeText}
//             />
//         </View>

//         // <View style={{ flexDirection: 'row', height:30 ,flex:1,}}>

//         //     {/* leftContainer */}
//         //     {/* <View style={{ flex: 3, backgroundColor: 'yellow', justifyContent: 'center' }}>
//         //         <TouchableOpacity>
//         //             <Image
//         //                 source={require('../../assets/Icons/harrow.png')}
//         //                 style={{ height: 25, width: 25, margin: 10 }}
//         //             />
//         //         </TouchableOpacity>
//         //     </View> */}
//         //     {/* centerContainer */}
//         //     {/* <View style={{ flex: 5, backgroundColor:'green',  alignItems: 'center' ,justifyContent:'center'}}  >
//         //         <Image
//         //             source={require('../../assets/Icons/logo.png')}
//         //             style={{ height: 25, width: 25,  }}
//         //         />
//         //     </View> */}


//         //     {/* rightContaier */}
//         //     <View style={{ flex: 3,backgroundColor:'pink', flexDirection: 'row' }}>
//         //         <TouchableOpacity>
//         //             <Image
//         //                 style={{ height: 30, width: 30, margin: 10 }}
//         //                 source={require('../../assets/Icons/profile.png')}
//         //             />
//         //         </TouchableOpacity>

//         //         <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
//         //             <Image
//         //                 source={require('../../assets/Icons/bar.png')}
//         //                 style={{ height: 25, width: 25, margin: 15 }}
//         //             />
//         //         </TouchableOpacity>
//         //     </View>


//         // </View>


//         // <View style={style.containerStyle}>


//         //     <TouchableOpacity>
//         //         <Image
//         //             source={require('../../assets/Icons/harrow.png')}
//         //             style={{ height: 25, width: 25, margin: 15 }}
//         //         />
//         //     </TouchableOpacity>



//         //     <Image
//         //         source={require('../../assets/Icons/logo.png')}
//         //         style={{ height: 25, width: 25, margin: 15 }}
//         //     />


//         //     <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
//         //         <Image
//         //             source={require('../../assets/Icons/bar.png')}
//         //             style={{ height: 25, width: 25, margin: 15 }}
//         //         />
//         //     </TouchableOpacity>


//         // </View>

//     );
// };


// const styles={
//     inputStyle:{
//         color:'#000',
//         paddingRight:5,
//         paddingLeft:5,
//         fontSize:18,
//         lineHeight:23,
//         flex:2,
//         backgroundColor:'pink'
//     },
//     labelStyle:{
//         fontSize:18,
//         paddingLeft:20,
//         flex:1,
//         backgroundColor:'green'
//     },
//     containerStyle:{
//         height:40,
//         flex:1,
//         flexDirection:'row',
//         alignItems:'center',
//         backgroundColor:'yellow'
//     },
// };



// const style = {
//     containerStyle: {
//         flexDirection: 'row',
//         backgroundColor: '#22578d',


//         // alignItems: 'center',
//         // height: 60,

//         // paddingTop: 15,
//         shadowColor: '#000',
//         shadowOffset: { width: 0, height: 2 },
//         shadowOpacity: 0.2,
//         elevation: 2,
//         position: 'relative',


//         // justifyContent: 'center',
//         // alignItems: 'center',
//         // position: 'relative',
//         //  BottomWidth: 1,
//         // borderWidth: 1,
//         // borderRadius: 2,
//         // borderColor: 'yellow',


//         // shadowColor: '#000',
//         // shadowOffset: { width: 0, height: 2 },
//         // shadowRadius: 2,
//         // elevation: 1,
//         // marginLeft: 5,
//         // marginRight: 5,
//         // marginTop: 10
//     }
// };

// export { CustomHeader };
