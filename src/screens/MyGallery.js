import React, { Component } from 'react'
import { Text, View, TextInput, CheckBox, ScrollView, Dimensions, TouchableOpacity, Image, Button } from 'react-native'
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader } from '../common'
import LinearGradient from 'react-native-linear-gradient';



export default class MyGallery extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            is_skip: this.props.navigation.getParam('is_skip', 0),
           
        }
    }

    

    
    render() {
        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>

                {this.state.is_skip === 1 && (
                    <CustomHeader bArrow navigation={this.props.navigation} />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader bArrow navigation={this.props.navigation} userbar />
                )}


                    <View style={{padding:10,backgroundColor:'#e9e9e9'}}>
                    <Text>All Photos</Text>
                    </View>

                <ScrollView>
                    
                </ScrollView>

                <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Portfolio',{is_skip:this.state.is_skip})}>
                        <LinearGradient 
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 10, alignItems: 'center', marginTop: 20,height:52}}
                        >
                        <Text allowFontScaling={false} font={'CenturyGothic'}  style={{ color:'#fff',fontSize:22}} >
                        ADD 
                        </Text>
                        </LinearGradient>
                </TouchableOpacity>
                </View>

                
            </View>

        )
    }
}



