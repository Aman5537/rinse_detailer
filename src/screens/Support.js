import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity,TextInput } from 'react-native'
import { CustomHeader ,CardSection, AuthHeader,AuthTitle,HeaderTitle} from '../common';
import  CommanStyle  from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import Helper from '../Lib/Helper';
import { ScrollView } from 'react-native-gesture-handler';

const SuccessMsg = (props) => {
    return (
        <View style={{}}>
            
            <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", marginTop: 10,color:'#3bb612',fontFamily: 'Aileron-Regular' }}>Thank you for your inquiry. We will be contact with you shortly.</Text>

            <TouchableOpacity 
                onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                    
                }}
                style={{width:120,alignSelf:'center'}}
                >
                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        borderRadius={5}
                        style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20 }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >OK</Text>
                    </LinearGradient>
                </TouchableOpacity>
           
            
        </View>
    )
}


export default class Support extends Component {
    static navigationOptions = { header: null }
    
    constructor(props) {
        super(props);
        this.state = {
            SuccessMsg: <SuccessMsg navigation={this.props.navigation} />,
            description:''
               
        }
    }

    _onSendPress = () => {
        if(this.state.description == ''){
            Helper.showToast('Description is required!');
            return false;
        }
        Helper.makeRequest({ url: "add-support", data: { description: this.state.description } }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                this.setState({description:''})
                global.state['modal'].setState({ modalVisible: true, Msg: this.state.SuccessMsg })
               
            }
            else {
                Helper.showToast(data.message)
            }
        })
        
    }



    render() {
        return (

            <View style={{ flex: 1, }}>
           
                <CustomHeader navigation={this.props.navigation}  userbar bArrow/>
                <HeaderTitle TitleName='Support' />
                <ScrollView>
                <View style={{padding:20}}>
                <TextInput
                style={{height: 150,justifyContent: "flex-start",backgroundColor:'#e8e8e8',textAlignVertical: 'top',padding:10}}
                underlineColorAndroid="transparent"
                placeholder="Description"
                placeholderTextColor="grey"
                numberOfLines={15}
                multiline={true}
                value={this.state.description}
                onChangeText={(txt) => { this.setState({description:txt})}}
                />


                <View
               
                onTouchStart={() => this._onSendPress()}
                >

                
                <TouchableOpacity 
                //onPress= {this._onSendPress}
                >
                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        borderRadius={5}
                        style={{ padding: 10, alignItems: 'center',height:52,marginTop:20,borderRadius:5 }}
                    >
                        <Text allowFontScaling={false} font={'CenturyGothic'}  style={{ color: '#fff', fontSize: 22 }} >SEND</Text>
                    </LinearGradient>
                </TouchableOpacity>
                </View>
                </View>
                
                </ScrollView>  
                
            </View>
        )
    }
}

const style = StyleSheet.create({
    textArea: {
        
    }
    


})


