import React, { Component } from 'react'
import { Text,Alert, Platform, Modal, Dimensions, View, CheckBox, ScrollView, TouchableOpacity, Image, Button, FlatList } from 'react-native';
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from "react-native-iap";

import Helper from '../Lib/Helper';


const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;

const itemSubs = Platform.select({
    ios: [
        "rinse01",
        "rinse02",
        "rinse03"
    ],
    android: [
        "rinse01",
        "rinse02",
        "rinse03"
    ]
});



export default class Upgrade extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            is_skip: this.props.navigation.getParam('is_skip', 0),
            pakage_plan: [],
            modalVisible: false,
            planList: [],
            appuser:'',
            activePlanIndex:0,
            IosTransID:'',
            newPurchasePlan:''
        }
    }

    async componentDidMount() {

        
        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {
                //ChatController.connectUser(responseData.id);
                this.setState({ appuser: responseData },() => {
                    this.getAllPlans()
                    this.setState({selectedPlan:this.state.appuser.plan_id})
                });

            }

        })
        

        this.getSubscriptionsInfo();

        
        
        purchaseUpdateSubscription = purchaseUpdatedListener(
            async (purchase: InAppPurchase | SubscriptionPurchase) => {
                const receipt = purchase.transactionReceipt;
                if (receipt) {
                    try {
                        console.log('yes purchase');
                        if (Platform.OS === 'ios') {
                            this.setState({IosTransID:purchase.transactionId})
                            await finishTransactionIOS(purchase.transactionId);
                        } else if (Platform.OS === 'android') {
                          // If consumable (can be purchased again)
                          //consumePurchaseAndroid(purchase.purchaseToken);
                          // If not consumable
                          //acknowledgePurchaseAndroid(purchase.purchaseToken);
                        }
                        const ackResult = await finishTransaction(purchase);
                        //alert(JSON.stringify(ackResult)+'---hello')
                    } catch (ackErr) {
                        console.warn('ackErr', ackErr);
                    }

                    this.setState({ receipt }, () => this.goNext());
                }
            },
        );

        purchaseErrorSubscription = purchaseErrorListener(
            (error: PurchaseError) => {
                console.log('purchaseErrorListener', error);
                //alert('purchase error' + JSON.stringify(error));
            },
        );




    }

    getSubscriptionsInfo = async () => {

        try {
            console.log('productsproductsproducts');
            const products = await RNIap.getSubscriptions(itemSubs);
            //console.log(products,'productsproductsproducts');
            // this.setState({
            //     pakage_plan: [...this.state.pakage_plan, ...products]

            // }, () => {
            //     this.setState({
            //         pakage_plan: [...this.state.pakage_plan, { title: '14+ Detailers- Contact Us', productId: 'rinse10' }]

            //     });
            // });


        } catch (err) {
            console.log('getSubscriptionsInfo - Catch');
            console.warn(err.code, err.message);
        }
    };


    getAllPlans(){
        console.log('myApi Call');
        Helper.makeRequest({ url: "get-plans",method:'GET',loader:false}).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                if(data.data.length > 0 ){
                    var newArr = [];
                    var i = 0;
                    var myPlanIndex= 0;
                    data.data.map((item) => {
                        if(this.state.appuser.plan_id == item.plan_id){
                            myPlanIndex = i; 
                        }
                        var extraText = (item.amount != 0) ? '-$'+item.amount+'/month':'';
                        newArr.push({title:item.plan+extraText,productId:item.plan_id})
                    i++;
                    })
                    this.setState({pakage_plan:newArr,activePlanIndex:myPlanIndex,selectedIndex:myPlanIndex})
                    
                }
                else{
                    Helper.showToast('Upgrade Plans Not Found!')
                    
                }
                setTimeout(() => {
                    Helper.hideLoader()
                }, 3000)
            }
            else {
                Helper.showToast(data.message)
                setTimeout(() => {
                    Helper.hideLoader()
                }, 3000)
            }
        })
    }

    componentWillUnmount(): void {
        //if(Platform.OS == 'android'){
            if (purchaseUpdateSubscription) {
                purchaseUpdateSubscription.remove();
                purchaseUpdateSubscription = null;
            }
            if (purchaseErrorSubscription) {
                purchaseErrorSubscription.remove();
                purchaseErrorSubscription = null;
            }
        //}
        

        
    }

    goNext = () => {
        console.log('goNextgoNextgoNext')
        //console.log('Receipt', this.state.IosTransID);

        if(Platform.OS == 'android'){
            var JsonReceipt =  JSON.parse(this.state.receipt);
        
            var form = {
                transaction_id:JsonReceipt.orderId,
                receipt:JSON.stringify(this.state.receipt),
                payment_time:JsonReceipt.purchaseTime,
                platform:Platform.OS,
                user_id:this.state.appuser.id,
                plan_id:JsonReceipt.productId


            }
        }
        if(Platform.OS == 'ios'){
            var form = {
                transaction_id:this.state.IosTransID,
                receipt:this.state.receipt,
                payment_time:(new Date('Y-m-d H:i:s')),
                platform:Platform.OS,
                user_id:this.state.appuser.id,
                plan_id:this.state.newPurchasePlan


            }
        }
        

        if(form.plan_id != ''){
            Helper.makeRequest({ url: "purchase-plan", data: form }).then((data) => {
                //alert(JSON.stringify(data));
                if (data.status == 'true') {
                    //this.setState({description:''})
                    Helper.setData('userdata', data.data);
                    
                    this.setState({planList:[],selectedPlan:data.data.plan_id,pakage_plan:[],appuser:data.data},() => {
                        this.getAllPlans()
                    })
                    Helper.showToast(data.message)
                    //this.props.navigation.goBack();
                }
                else {
                    Helper.showToast(data.message)
                }
            })
        }
        
        

        
        // Alert.alert('Receipt', this.state.receipt);
    };


    setModalVisible(visible) {

        this.setState({ modalVisible: visible });
    }

    planPurchase = async (sku): void => {
        this.setState({newPurchasePlan:sku})
        if(this.state.selectedIndex < this.state.activePlanIndex){
            this.setModalVisible(false);
            alert("You can't downgrade your plan, Please contact with Admin!");
        }
        else if(sku == this.state.appuser.plan_id){
            this.setModalVisible(false);
            alert('This plan already activated!');
        }
        else if (sku == 'rinse0') {
            this.setModalVisible(false);
        }
        else if (sku == 'rinse10') {
            this.setModalVisible(false);
            this.props.navigation.navigate('Support');
        }
        else{
            
        
            try {
                console.log('requestSubscriptionrequestSubscription');
                this.setModalVisible(false);
                RNIap.requestSubscription(sku)

            } catch (err) {
                this.setModalVisible(false);
                alert(err.message);
            }
        }

    };

    

    modalBlockRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH, alignItems: 'center', justifyContent: "center" }}>
                    <View style={{ height: 380, marginLeft: -10, marginRight: -10, marginTop: -5, backgroundColor: '#fff', padding: 15, borderRadius: 5 }}>
                        <TouchableOpacity onPress={() => {
                            this.setModalVisible(false)
                        }}

                        >
                            <Text style={{ fontSize: 20, justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                        </TouchableOpacity>

                        <View style={{ padding: 10 }}>
                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 16, marginTop: 10 }}>Which upgrade package would you like<Text allowFontScaling={false} style={{ fontFamily: 'Aileron-Regular' }}>?</Text></Text>


                            <View style={{ marginTop: 10, marginBottom: 20 }}>
                                {this.state.planList.length > 1 &&
                                    <RadioForm style={{ flexDirection: 'column' }}
                                        radio_props={this.state.planList}
                                        initial={this.state.activePlanIndex}
                                        formHorizontal={false}
                                        labelHorizontal={true}
                                        backgroundColor={'#cacaca'}
                                        buttonColor={'#cacaca'}
                                        selectedButtonColor={'#757171'}
                                        buttonSize={10}
                                        buttonOuterSize={25}
                                        buttonOuterColor={'#757171'}
                                        animation={true}
                                        onPress={(value,index) => this.setState({ selectedPlan: value,selectedIndex:index })}
                                        labelStyle={{ padding: 10, paddingTop: 2, color: '#808080', fontFamily: 'CenturyGothic' }}
                                        style={{ fontFamily: 'CenturyGothic' }}
                                    />
                                }
                            </View>
                            <TouchableOpacity onPress={() => {
                                this.planPurchase(this.state.selectedPlan)
                                
                            }}
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                            >

                                <LinearGradient
                                    colors={['#4fa4d8', '#4fa4d8', '#4fa4d8']}
                                    style={{ padding: 10, paddingLeft: 40, paddingRight: 40, alignItems: 'center', borderRadius: 5 }}


                                >

                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >
                                        Continue
                </Text>

                                </LinearGradient>


                            </TouchableOpacity>
                        </View>
                    </View>

                </View>



            </Modal>



        );
    }


    


    _onSendPress = () => {
        
        var radio_props = [];
        this.state.pakage_plan.map((item) => {
            radio_props.push({ label: item.title.replace('(RinseDetailer)', ''), value: item.productId });
        })

        this.setState({ planList: radio_props })

        this.setModalVisible(true)


    }

    renderPlanItem = ({ item, index }) => {
        return (
            <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext,(this.state.appuser.plan_id == item.productId) ? {color:'green'} : '']}>
                <Text allowFontScaling={false} style={{ fontSize: 24}}>{'\u2022'}</Text>
                {item.title.replace('(RinseDetailer)', '')}
            </Text>

        )
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                {this.modalBlockRender()}
                {this.state.is_skip === 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow userbar />
                )}


                <AuthTitle TitleName='Upgrade' />

                <ScrollView>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltitle, { textAlign: 'center' }]}>Why detailer upgrade their accounts...</Text>

                    <Card >

                        <View style={{ marginTop: 15, flexDirection: 'row' }}>
                            <View >
                                <Image
                                    style={{ width: 18, height: 15, marginTop: 5 }}
                                    source={require('../../assets/Icons/check.png')}
                                />
                            </View>
                            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                                    Get your Money sooner (1-2 Business days to process instead of 4-6 with free account).
                            </Text>

                            </View>
                        </View>



                        <View style={{ marginTop: 15, flexDirection: 'row' }}>
                            <View >
                                <Image
                                    style={{ width: 18, height: 15, marginTop: 5 }}
                                    source={require('../../assets/Icons/check.png')}
                                />
                            </View>
                            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                                    Ability to add sub-accounts for detailers that work for you.
                        </Text>
                                <View style={{ marginTop: 10 }}>
                                {this.state.pakage_plan.length > 0 && 
                                    <FlatList
                                        data={this.state.pakage_plan}
                                        extraData={this.state}
                                        renderItem={this.renderPlanItem}
                                        keyExtractor={(item, index) => index.toString()}

                                    />
                                }

                                    {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>
                        <Text allowFontScaling={false} style={{fontSize:24}}>{'\u2022'}</Text> 1-2 Detailers-$15/month</Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}><Text allowFontScaling={false}  style={{fontSize:24}}>{'\u2022'}</Text> 3-5 Detailers-$20/month</Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}><Text allowFontScaling={false}  style={{fontSize:24}}>{'\u2022'}</Text> 6-8 Detailers-$25/month</Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}><Text allowFontScaling={false}  style={{fontSize:24}}>{'\u2022'}</Text> 9-12 Detailers-$30/month</Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}><Text allowFontScaling={false}  style={{fontSize:24}}>{'\u2022'}</Text> Contact us if you’re adding more {"\n"} then 12 detailers to your account.</Text>
                         */}
                                </View>
                            </View>
                        </View>






                    </Card>
                </ScrollView>
                <View style={{ alignItems: 'center', paddingTop: 10 }} >
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltitle, { textAlign: 'center', fontSize: 23, textDecorationLine: 'underline', margin: 5 }]} >SKIP</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={{ marginTop: 25 }} onPress={this._onSendPress}>
                    <LinearGradient



                        colors={['#5ced31', '#43bc1d', '#37a313']}
                        borderRadius={0}
                        style={{ padding: 10, alignItems: 'center', height: 52 }}


                    >

                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 23 }} >
                            UPGRADE
  </Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }
}
