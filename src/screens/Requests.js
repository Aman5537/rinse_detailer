import React, { Component,useState } from 'react'
import { Animated,ActivityIndicator,Text,Modal, View, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Button, FlatList } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle, HeaderTitle } from '../common';

import CommanStyle from '../config/Styles'
import CommonButton from '../common/CommonButton';
import LinearGradient from 'react-native-linear-gradient';
import Helper from '../Lib/Helper';
const DeviceH =Dimensions.get('window').height;
import Moment from "moment";

import  CommanTollTip  from "../common/CommonTollTip";
import LocalNotification from "../common/LocalNotification";
import ChatController from '../Lib/ChatController';
import { EventRegister } from 'react-native-event-listeners';






class CancelQuoteMsg extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={{ alignItems: 'center', height: 100, }}>

                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>Are you sure want to cancel this quote?</Text>


                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                        this.props.callApi(this.props.ReqID,this.props.ReqIndex);
                    }}

                        style={{ flex: 1 }}
                    >

                        <LinearGradient
                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            style={{ padding: 8, margin: 10, alignItems: 'center' }}


                        >

                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                        </LinearGradient>


                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    }}

                        style={{ flex: 1 }}
                    >

                        <LinearGradient
                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                            style={{ padding: 8, margin: 10, alignItems: 'center' }}
                        >

                            <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                        </LinearGradient>

                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}






const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}


export default class Requests extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
    this.state = {
        CancelQuoteMsg: <CancelQuoteMsg />,
        ActiveReqTab: 1,
        PendingReqData: [],
        SubmitedReqData: [],
        AppReqData: [],
        AppUserData: {},
        notefound1: '',
        notefound2: '',
        notefound3: '',
        penPage: 1, penIsLoading: false,
        penNext_page_url: null,
        penRefreshing: false,
        subPage: 1, subIsLoading: false,
        subNext_page_url: null,
        subRefreshing: false,
        appPage: 1, appIsLoading: false,
        appNext_page_url: null,
        appRefreshing: false,
        modalAppoCancelVisible: false,
        // localNotiShow:Helper.localNotiShow,
        // NotiAllData:''
        
        
    }    



    }

    componentDidMount() {
        
        

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    //this.props.navigation.navigate('LoginNew');
                } else {
                    this.setState({ AppUserData: responseData });
                    ChatController.checkConnection("request", (cb) => {
                        if (cb) {
                            Requests.getBadegesCount(this.state.AppUserData.id)
                        }

                    })

                    
                }
            })


        })
        this._pullToRefresh();

        //this.localNotiShowHide(true)
        
        
    }


    

    /* Bootom Tab Budget event */

    static getBadegesCount(user_id) {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            user_id: user_id,
            role_id:2,
            utc_date_time: gettime
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);
        
    }


    componentWillMount() {
        this.listenerRefresh = EventRegister.addEventListener('requestTabBarEvent', (data) => {
            if(data.refresh == true){
                this.ActiveTabAction(this.state.ActiveReqTab);
            }
            
        })


        this.listenerRefreshApp = EventRegister.addEventListener('appoTabBarEvent', (data) => {
            if(data.refresh == true){
                this.ActiveTabAction(this.state.ActiveReqTab);
            }
            
        })


        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {
                
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;
                
                this.props.navigation.setParams({ badgeCount: 123 })
            } else {
                this.setState({ isLoader: false });
            }

        })


        // this.localNotilistener = EventRegister.addEventListener('service_notification_response', (data) => {
        //     console.log('localNotilistener--', data)
        //     if (data.status == 'true') {
                
        //         //Helper.NotificationJump(data);
        //         this.setState({localNotiShow:true,NotiAllData:data})
        //     }
        // })
        

    }

    componentWillUnmount() {
        
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()
        EventRegister.removeEventListener(this.listenerRefresh);
        EventRegister.removeEventListener(this.listenerRefreshApp);
        // EventRegister.removeEventListener(this.localNotilistener);
        
        
    }

    /* Bootom Tab Budget event */


    _pullToRefresh = () => {
        if(this.state.penRefreshing == false){
            this.setState({ penRefreshing: true,penPage:1,PendingReqData:[],notefound1:'' },() => {
                if(this.state.penPage == 1){
                    
                    this.getAllPendingReq(false);
                }
                
            });
        }
        
        
    }


    _pullToSubRefresh = () => {
        if(this.state.subRefreshing == false){
            this.setState({ subRefreshing: true,subPage:1,SubmitedReqData:[],notefound2:'' },() => {
                if(this.state.subPage == 1){
                    this.getSubmitedReqData(false);
                }
            });
        }
        
    }

    _pullToAppRefresh = () => {
        if(this.state.appRefreshing == false){
            this.setState({ appRefreshing: true,appPage:1,AppReqData:[],notefound3:'' },() => {
                if(this.state.appPage == 1){
                    this.getAppReqData(false);
                }
            });
        }
    }

    
    

    getAllPendingReq = (loadstatus) => {
        //this.setState({ penIsLoading: loadstatus, penRefreshing: false });
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")
        let form = {
            page: this.state.penPage,
            utc_date_time: gettime
        }
        var MyLoader = false;
        if(this.state.penPage == 1){
            MyLoader = true;
            
        }

        Helper.makeRequest({ url: "detailer-pending-service-request", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            //return false;
            if (data.status == 'true') {
                if(this.state.penPage == 1){
                    this.setState({
                        PendingReqData:data.data.data,
                        penIsLoading: false,
                        penRefreshing: false,
                        penNext_page_url: data.data.next_page_url
                    });
                }
                else{
                    this.setState({
                        PendingReqData: [...this.state.PendingReqData, ...data.data.data],
                        penIsLoading: false,
                        penRefreshing: false,
                        penNext_page_url: data.data.next_page_url
                    });
                }
                this.state.penPage++;
                
            }
            else {
                Helper.showToast(data.message)
            }
            this.setState({'notefound1': "No Service Requests available at this time. Sit tight, they're on the way." });
        })
        .catch(err => {
            this.setState({ penIsLoading: false, penRefreshing: false });
           
        })
    }



    getSubmitedReqData = (loadstatus) => {
        //this.setState({ subIsLoading: loadstatus, subRefreshing: false });
        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            page: this.state.subPage,
            utc_date_time: gettime
        }

        var MyLoader = false;
        if(this.state.subPage == 1){
            MyLoader = true;
        }


        Helper.makeRequest({ url: "my-quote-request", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                
                if(this.state.subPage == 1){
                    this.setState({
                        SubmitedReqData:data.data.data,
                        subIsLoading: false,
                        subRefreshing: false,
                        subNext_page_url: data.data.next_page_url
                    });
                }
                else{
                    this.setState({
                        SubmitedReqData: [...this.state.SubmitedReqData, ...data.data.data],
                        subIsLoading: false,
                        subRefreshing: false,
                        subNext_page_url: data.data.next_page_url
                    });
                }
                
                this.state.subPage++;
                

            }
            else {
                Helper.showToast(data.message)
            }

            this.setState({'notefound2': "No quotes have been submitted yet. Sit tight, they're on the way." });
        })
        .catch(err => {
            this.setState({ subIsLoading: false, subRefreshing: false });
           
        })
    }


    getAppReqData = (loadstatus) => {
        //this.setState({ appIsLoading: loadstatus, appRefreshing: false });
        
        let tempdate = new Date();
        
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();
        
        // var firstSign = '+';
        // if(TimeZone < 0){
        //     firstSign = '+'
        // }else{
        //     firstSign = '-'
        // }

        // var getHours = parseInt(Math.abs(TimeZone) / 60);
        // var getMin = Math.abs(TimeZone) - (getHours * 60);
        // var getTotalTimeZone = getHours+':'+getMin;

        // alert(firstSign+getTotalTimeZone);

        let form = {
            page: this.state.appPage,
            utc_date_time: gettime,
            time:TimeZone

        }

        var MyLoader = false;
        if(this.state.appPage == 1){
            MyLoader = true;
        }


        Helper.makeRequest({ url: "detailer-get-service-request-by-date", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                
                if(this.state.appPage == 1){
                    this.setState({
                        AppReqData:data.data.data,
                        appIsLoading: false,
                        appRefreshing: false,
                        appNext_page_url: data.data.next_page_url
                    });
                }
                else{
                    this.setState({
                        AppReqData: [...this.state.AppReqData, ...data.data.data],
                        appIsLoading: false,
                        appRefreshing: false,
                        appNext_page_url: data.data.next_page_url
                    });
                }
                
                this.state.appPage++;

                
            }
            else {
                Helper.showToast(data.message)
                
            }
            this.setState({'notefound3': 'No Appointments Scheduled' });
            
        })
        .catch(err => {
            this.setState({ appIsLoading: false, appRefreshing: false });
           
        })
    }



    onScrollPendding = (e) => {
        if (!this.state.penIsLoading && this.state.penNext_page_url) {
        this.getAllPendingReq(true);
        }
        
    }

    onScrollSubmitted = (e) => {
        if (!this.state.subIsLoading && this.state.subNext_page_url) {
        this.getSubmitedReqData(true);
        }
    }

    onScrollApp = (e) => {
        if (!this.state.appIsLoading && this.state.appNext_page_url) {
        this.getAppReqData(true);
        }
    }



    _onDeletePress = (id,index) => { 
        this.setState({ 'CancelQuoteMsg': <CancelQuoteMsg ReqID={id} ReqIndex={index} callApi={this.callApi} /> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.CancelQuoteMsg })
        });

    }

    callApi = (id,index) => {
        
        // var NewArra = this.state.SubmitedReqData[index].service_quote.status='Canceled';
        // alert(JSON.stringify(NewArra));
        //this.setState({change:true});

        //alert(id);
        Helper.makeRequest({ url: "cancel-quote-request", data: { quote_id: id } }).then((data) => {
            if (data.status == 'true') {

                //this.state.SubmitedReqData.service_quote.status
                setTimeout(() => {
                    Helper.hideLoader()
                    this._pullToSubRefresh();
                }, 1000)
                

            }
            else {
                Helper.showToast(data.message)
            }
        })
    }


    

    

    ActiveTabAction = (val) => {
        if(val == 1){
            this.setState({ PendingReqData: [],penPage:1 },() => {
                this._pullToRefresh();
            })
            
                    
        }
        if(val == 2){
            this.setState({ SubmitedReqData: [] })
            this._pullToSubRefresh();
                    
        }
        if(val == 3){
            this.setState({ AppReqData: [] })
            this._pullToAppRefresh();
        }
        this.setState({ ActiveReqTab: val })

    }


    getTimeAway = (date) =>{
        
        var TimeObj = Helper.getTimeDiffrence(date);
        if(TimeObj != '' && TimeObj != undefined){
            var NewColor ='red';
            if(TimeObj.hasOwnProperty('type')){
                if(TimeObj.type == 1 || TimeObj.type == 2){
                    NewColor ='green';
                }
            }
            
            if(TimeObj.hasOwnProperty('comment')){
            return (
                <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { color: NewColor, fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>{TimeObj.comment}</Text>

            )
            }
        }
        
    }


    


    _renderPenddingItem = ({ item }) => (
        <View style={[style.MySelfDiv,(item.allow == 1) ? '' : {backgroundColor:"#ccc"}]}>
            <View style={{ flex: 2 }}>
                <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>{Helper.getDateNewFormate(item.service_request.service_utc_date_time)}</Text>
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.user.first_name} {item.user.last_name}</Text>
                {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.vehicle.model_detail.name}</Text> */}
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.service_request.vehicles.map((vitem,vindex)=> (
                    (vindex == 0) ? vitem.vehicle.model_detail.name : ', '+vitem.vehicle.model_detail.name
                    
                    
                ))}
                </Text>
                {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.service.service} ({item.service.average_time_from}-{item.service.average_time_to}min)</Text> */}
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>
                {(item.service_request.location.city == '' && item.service_request.location.state == '' && item.service_request.location.zip_code == '') ? '' : item.service_request.location.city+', '+item.service_request.location.state+' '+item.service_request.location.zip_code}</Text>

            </View>
            <View style={{ paddingTop: 25, alignItems: 'center', flex: 1 }}>
                    {item.is_favorite_request == 1 && (
                        <Image
                        source={require("../../assets/Icons/star_y.png")}
                        style={{ width: 20, height: 20,margintop:-10,marginBottom:6}}
                        />
                    )}
                {(item.allow == 1) && 
                <CommonButton name="View Details" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('SendQuote', { 'req_id': item.request_id, 'req_data': item })} />
                
                }
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <Text allowFontScaling={false} font={'CenturyGothic'}>{Helper.getDistanceMile(item.distance_diff)}mi</Text>
                    
                    <CommanTollTip  tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />


                </View>
            </View>

        </View>
    )

    _renderSubmittedItem = ({ item,index }) => (
        <View style={[style.MySelfDiv,(item.allow == 1) ? '' : {backgroundColor:"#ccc"}]}>
            <View style={{ flex: 2 }}>
                <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>{Helper.getDateNewFormate(item.service_request.service_utc_date_time)}</Text>
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.user.first_name} {item.user.last_name}</Text>
                {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.vehicle.model_detail.name}</Text> */}
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>
                {item.service_request.vehicles.map((vitem,vindex)=> (
                    (vindex == 0) ? vitem.vehicle.model_detail.name : ', '+vitem.vehicle.model_detail.name
                    
                    
                ))}
                </Text>
                {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.service.service} ({item.service.average_time_from}-{item.service.average_time_to}min)</Text> */}
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>
                {(item.service_request.location.city == '' && item.service_request.location.state == '' && item.service_request.location.zip_code == '') ? '' : item.service_request.location.city+', '+item.service_request.location.state+' '+item.service_request.location.zip_code}
                </Text>

            </View>
            <View style={{ alignItems: 'center', flex: 1 }}>
                <Text allowFontScaling={false} style={{ color: 'red', fontSize: 20, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>${item.detailer_service_quote.quote}</Text>
                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                    <Text allowFontScaling={false} style={{ fontFamily: 'Aileron-Regular' }} >{Helper.getDistanceMile(item.distance_diff)}mi</Text>
                    <CommanTollTip  tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />

                </View>
                {(item.allow == 1) ? 
                <View style={{ marginTop: 0, width: 108 }} >
                    <CommonButton name="View Details" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('SendQuote', { 'req_id': item.request_id, 'req_data': item })} width={110} />
                </View>
                :
                <View style={{marginVertical:3}}></View>
                }
                <View style={{ marginTop: -5, width: 108 }} >
                    {item.detailer_service_quote.status == 'Canceled' && (
                        <Text style={{color:'red',alignSelf:'center'}}>Canceled</Text>
                    )}
                    <View>
                        {item.detailer_service_quote.status != 'Canceled' && this.state.AppUserData.is_sub_detailer == 0 &&
                            <CommonButton name=" Cancel " background="#5c5c5c" color="#ffffff" onPress={() => this._onDeletePress(item.detailer_service_quote.id,index)}  width={110} />
                        }

                        {item.detailer_service_quote.status != 'Canceled' && this.state.AppUserData.is_sub_detailer == 1 && this.state.AppUserData.id == item.detailer_service_quote.detailer_id &&
                            <CommonButton name=" Cancel " background="#5c5c5c" color="#ffffff" onPress={() => this._onDeletePress(item.detailer_service_quote.id,index)}  width={110} />
                        }




                    </View>
                </View>
            </View>
        </View>
    )


    _renderAppoinmentItem = ({ item,index }) => (
        
        
    <View style={style.MySelfDiv}> 
        <View style={{ flex: 2 }}>

        {item.is_finished != 1 && item.payment_request.status == 'Requested' && (
            <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',fontFamily: 'Aileron-Regular',color:'red',fontWeight: 'bold'}]}>
            Payment Override
            </Text>
        )}
        
        {((item.confirm_quote.is_canceled == 1 || item.confirm_quote.status == 'Canceled' ) && item.confirm_quote.status != 'Completed' && item.is_finished != 1 && item.is_canceled_by_detailer == 1) && (
            <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { color: 'black', fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>Detailer Canceled</Text>

        )}
        {((item.confirm_quote.is_canceled == 1 || item.confirm_quote.status == 'Canceled' || item.is_service_hired_cancel == 1) && item.confirm_quote.status != 'Completed' && item.is_finished != 1 && item.is_canceled_by_detailer == 0) && (
            <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { color: 'black', fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>Customer Canceled</Text>

        )}

        {(item.confirm_quote.status == 'Completed' && item.is_finished != 1 && item.payment_request == '' ) && (
            <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { color: 'red', fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>Payment Pending</Text>

        )}

        {(item.is_finished == 1) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{fontFamily: 'Aileron-Regular',color:'green',fontWeight: 'bold'}]}>
                        Completed
                        </Text>
        )}

        {(item.confirm_quote.is_canceled != 1 && item.confirm_quote.status != 'Canceled' && item.is_service_hired_cancel != 1 && item.confirm_quote.status != 'Completed' && item.is_finished != 1) && (
            
            this.getTimeAway(item.service_utc_date_time)
            
            
        )}

        
        
        
        
            
            <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>{Helper.getDateNewFormate(item.service_utc_date_time)}</Text>
            <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>{item.user.first_name} {item.user.last_name}</Text>
            
            <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>
            {item.vehicles.map((vitem,vindex)=> (
                    (vindex == 0) ? vitem.vehicle.model_detail.name : ', '+vitem.vehicle.model_detail.name
                    
                    
            ))}
             </Text>
            {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Inside/ Outside (60-90min)</Text>
            <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Detailer - Brad Smit</Text> */}

            <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>
            {item.location.location}
            </Text>

        </View>
        <View style={{ alignItems: 'center', flex: 1 }}>
            <Text allowFontScaling={false} style={{ color: 'red', fontSize: 20, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>
            $
            {(item.is_service_hired_cancel == 1 && item.is_canceled_by_detailer == 0) &&
                //'20.00'
                Helper.NotToZero(item.transaction.is_canceled_amount)
            }

            {!(item.is_service_hired_cancel == 1 && item.is_canceled_by_detailer == 0) &&
                item.confirm_quote.quote
            }
            </Text>
            <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                <Text allowFontScaling={false} font={'CenturyGothic'}>{Helper.getDistanceMile(item.distance_diff)}mi</Text>
                

                <CommanTollTip  tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />


            </View>
            
            <View>
            <View style={{ marginTop: 0, width: 108 }} >
                <CommonButton name="View Details" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('AppointmentView',{ 'appo_id': item.id})} width={110} />
            </View>
            
            {(item.confirm_quote.status != 'Completed' && item.confirm_quote.is_canceled != 1 && item.confirm_quote.status != 'Canceled' && item.is_service_hired_cancel != 1) && (
                <View style={{ marginTop: -5, width: 108 }} >
                <CommonButton name=" Cancel " background="#5c5c5c" color="#ffffff" 
                //onPress={this._onAppoinmentCancel(item.id,item.confirm_quote.id)} 
                onPress={() => {
                    this.setState({requestDeleteId:item.id,QuoteDeleteId:item.confirm_quote.id})
                    this.setModalVisible(true)
                }}
                width={110} />
                </View>
            )}
            
            </View>
            
        </View>
    </View>
    
    )

    renderSeparator = () => (
        <View
          style={{
            borderBottomColor:'#ccc',
            borderBottomWidth:1,
            
          }}
        />
    );

    renderFooter = () => (
        <View
          style={{
            borderBottomColor:'#ccc',
            borderBottomWidth:1,
            
          }}
        />
    );
      

    setModalVisible(visible) {
        
        this.setState({ modalAppoCancelVisible: visible });
    }

    appoinmentCancelFun(id,quote_id){
        Helper.makeRequest({ url: "appointment-cancel", data: { service_request_id: id,quote_id:quote_id } }).then((data) => {
            if (data.status == 'true') {

                //this.state.SubmitedReqData.service_quote.status
                this.setModalVisible(false)
                
                setTimeout(() => {
                    Helper.hideLoader()
                    this._pullToAppRefresh();
                }, 1000)

                this.cancelEventCall(data.data.user_id);

            }
            else {
                Helper.showToast(data.message)
            }
        })
    }


    cancelEventCall(user_id) {
       
        
        let form1 = {
            user_id: this.state.AppUserData.id,
            role_id:2
            
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form1);
        

        let form2 = {
            user_id: user_id,
            role_id:1
            
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form2);

    }


    modalAppoCancelRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalAppoCancelVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                        <View style={{ alignItems: 'center', height: 240 }}>

<Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
    We understand situations arise but canceling an appointment negatively affects the customers experience. By
    canceling this appointment, this will be included in your review and could adversely affect quotes you submit in the future.
</Text>

<Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
    Would you like to cancel this appointment?
</Text>

<View style={{ flex: 1, flexDirection: 'row' }}>
    <TouchableOpacity onPress={() => {
        this.appoinmentCancelFun(this.state.requestDeleteId,this.state.QuoteDeleteId);
        
    }}

        style={{ flex: 1 }}
    >

        <LinearGradient
            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
            style={{ padding: 8, margin: 10, alignItems: 'center' }}


        >

            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Yes</Text>

        </LinearGradient>


    </TouchableOpacity>

    <TouchableOpacity onPress={() => {
        this.setModalVisible(false)
    }}

        style={{ flex: 1 }}
    >

        <LinearGradient
            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
            style={{ padding: 8, margin: 10, alignItems: 'center' }}
        >

            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >No</Text>

        </LinearGradient>

    </TouchableOpacity>
</View>
</View>

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }


    


    

    // localNotiShowHide = (val) =>{
    //     this.setState({localNotiShow:val})
    //     if(val == true){
    //         setTimeout(() => {
    //             this.setState({ localNotiShow:false });
    //         }, 5000);
    //     }
    // }

    render() {
        //alert(this.state.localNotiShow)
        return (
            
            <View style={{ flex: 1, }}>
                
                {this.modalAppoCancelRender()}
                <CustomHeader navigation={this.props.navigation} userbar />
                {/* <LocalNotification localNotiShow={this.state.localNotiShow} navigation={this.props.navigation} NotiAllData={this.state.NotiAllData} />
                 */}
                
                
                
                <HeaderTitle TitleName='Requests' />

                <View style={{ flexDirection: 'row', height: 55, justifyContent: 'center' }}>

                    <TouchableOpacity onPress={() => {
                        this.ActiveTabAction(1)

                    }}
                        style={{ flex: 1 }}>
                        <View style={[CommanStyle.tabButton, this.state.ActiveReqTab === 1 ? style.ActiveTab : '']} >

                            <Text allowFontScaling={false} font={'TypoGroteskRounded'} style={[CommanStyle.tabButtonText, { fontFamily: 'Aileron-Regular' }]}>Pending Requests</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.ActiveTabAction(2)
                    }}
                        style={{ flex: 1 }}
                    >
                        <View style={[CommanStyle.tabButton, this.state.ActiveReqTab === 2 ? style.ActiveTab : '']}>

                            <Text allowFontScaling={false} font={'TypoGroteskRounded'} style={[CommanStyle.tabButtonText, { fontFamily: 'Aileron-Regular' }]}>Submitted Quotes</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.ActiveTabAction(3)
                    }}
                        style={{ flex: 1 }}
                    >
                        <View style={[CommanStyle.tabButton, this.state.ActiveReqTab === 3 ? style.ActiveTab : '']} >

                            <Text allowFontScaling={false} font={'TypoGroteskRounded'} style={[CommanStyle.tabButtonText, { fontFamily: 'Aileron-Regular' }]}>Appointments</Text>

                        </View>
                    </TouchableOpacity>
                </View>


                <View style={{ flex: 1, padding: 10, paddingTop: 0 }}>

                    {this.state.ActiveReqTab === 1 && (
                        
                        <View>
                            {this.state.PendingReqData.length > 0 ?
                                <FlatList
                                    data={this.state.PendingReqData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderPenddingItem}
                                    refreshing={this.state.penRefreshing}
                                    onRefresh={this._pullToRefresh}
                                    onEndReached={this.onScrollPendding}
                                    onEndReachedThreshold={0.5}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}

                                />
                                : !this.state.isLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold',textAlign:'center' }}>{this.state.notefound1}</Text>
                                    </View> : null
                            }
                            {this.state.isLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            }



                            {/* <View style={CommanStyle.mycardDiv}>
                                    <View style={{ flex: 2 }}>
                                        <Text allowFontScaling={false}  font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, {fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>Tuesday, April 23rd @ 2:45 pm</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Brandon Fields</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>2012 Nissan Altima</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Inside/ Outside (60-90min)</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Tampa FL 33610</Text>

                                    </View>
                                    <View style={{ paddingTop: 25, alignItems: 'center', flex: 1 }}>
                                    <CommonButton name="View Details" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('SendQuote')}/>
                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                            <Text allowFontScaling={false}  font={'CenturyGothic'}>2.4mi</Text>
                                            <Image
                                                style={{ height: 20, width: 20, marginLeft: 5 }}
                                                source={require('../../assets/Icons/i-icon.png')}
                                            />
                                        </View>
                                    </View>
                                </View> */}





                        </View>

                    )}


                    {this.state.ActiveReqTab === 2 && (
                        <View>

                            {this.state.SubmitedReqData.length > 0 ?
                                <FlatList
                                    data={this.state.SubmitedReqData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderSubmittedItem}
                                    refreshing={this.state.subRefreshing}
                                    onRefresh={this._pullToSubRefresh}
                                    onEndReached={this.onScrollSubmitted}
                                    onEndReachedThreshold={0.5}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}
                                />
                                : !this.state.isLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold',textAlign:'center' }}>{this.state.notefound2}</Text>
                                    </View> : null
                            }
                            {this.state.isLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            }

                            {/* <ScrollView>

                                


                                <View style={CommanStyle.mycardDiv}>
                                    <View style={{ flex: 2 }}>
                                        <Text allowFontScaling={false}  font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, {fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>Tuesday, April 23rd @ 2:45 pm</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Brandon Fields</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>2012 Nissan Altima</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Inside/ Outside (60-90min)</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Detailer - Brad Smit</Text>
                                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>Tampa FL 33610</Text>

                                    </View>
                                    <View style={{ alignItems: 'center', flex: 1 }}>
                                        <Text allowFontScaling={false}  style={{ color: 'red', fontSize: 20, fontWeight: 'bold',fontFamily: 'Aileron-Regular' }}>$35</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                            <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}} >2.4mi</Text>
                                            <Image
                                                style={{ height: 20, width: 20, marginLeft: 5 }}
                                                source={require('../../assets/Icons/i-icon.png')}
                                            />
                                        </View>
                                        <View style={{marginTop: -5, width: 108 }} >
                                        <CommonButton name="View Details" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('SendQuote')} width={110} />
                                        </View>
                                        <View style={{ marginTop: -5, width: 108 }} >
                                        <CommonButton name=" Cancel " background="#5c5c5c" color="#ffffff" onPress={this._onQuoteCancel} width={110}/>
                                        </View>
                                    </View>
                                </View>
                                </ScrollView> */}
                        </View>
                    )}



                    {this.state.ActiveReqTab === 3 && (
                        <View>
                            {this.state.AppReqData.length > 0 ?
                                <FlatList
                                    data={this.state.AppReqData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderAppoinmentItem}
                                    refreshing={this.state.appRefreshing}
                                    onRefresh={this._pullToAppRefresh}
                                    onEndReached={this.onScrollApp}
                                    onEndReachedThreshold={0.3}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}
                                />
                                : !this.state.appIsLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold',textAlign:'center' }}>{this.state.notefound3}</Text>
                                    </View> : null
                            }
                            {this.state.appIsLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            }
                             {/* <View style={CommanStyle.mycardDiv}>
                                <View style={{ flex: 2 }}>
                                    <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { color: 'red', fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>Canceled</Text>

                                    <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>Tuesday, April 23rd @ 2:45 pm</Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Brandon Fields </Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>2012 Nissan Altima</Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Inside/ Outside (60-90min)</Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Tampa FL 33610</Text>

                                </View>
                                <View style={{ alignItems: 'center', flex: 1 }}>
                                    <Text allowFontScaling={false} style={{ color: 'red', fontSize: 20, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>$35</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                        <Text allowFontScaling={false} font={'CenturyGothic'}>2.4mi</Text>
                                        <Image
                                            style={{ height: 20, width: 20, marginLeft: 5 }}
                                            source={require('../../assets/Icons/i-icon.png')}
                                        />
                                    </View>


                                </View>
                            </View>

                            <View style={CommanStyle.mycardDiv}> 
                                <View style={{ flex: 2 }}>
                                    <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { color: 'red', fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>4 Days Away</Text>

                                    <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>Tuesday, April 23rd @ 2:45 pm</Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Brandon Fields </Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>2012 Nissan Altima</Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Inside/ Outside (60-90min)</Text>
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Detailer - Brad Smit</Text>

                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Tampa FL 33610</Text>

                                </View>
                                <View style={{ alignItems: 'center', flex: 1 }}>
                                    <Text allowFontScaling={false} style={{ color: 'red', fontSize: 20, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>$35</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                        <Text allowFontScaling={false} font={'CenturyGothic'}>2.4mi</Text>
                                        <Image
                                            style={{ height: 20, width: 20, marginLeft: 5 }}
                                            source={require('../../assets/Icons/i-icon.png')}
                                        />
                                    </View>
                                    <View style={{ marginTop: -5, width: 108 }} >
                                        <CommonButton name="View Details" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('AppointmentView')} width={110} />
                                    </View>
                                    <View style={{ marginTop: -5, width: 108 }} >
                                        <CommonButton name=" Cancel " background="#5c5c5c" color="#ffffff" onPress={this._onAppoinmentCancel} width={110} />
                                    </View>
                                </View>
                            </View> 
                            <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>No Appointments Scheduled</Text>
                            </View>
                            */}
                        </View>
                    )}
                </View>

            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },
    ActiveTab: {
        backgroundColor: '#4684da'
    },
    MySelfDiv:{
        
        padding:6,
        flexDirection:'row',
        flex:1,
        marginTop:10
    },endofBorder:{
        borderBottomColor:'#ccc',
        borderBottomWidth:1
    }



})