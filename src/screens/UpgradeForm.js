import React, { Component } from 'react'
import { Text, View, TextInput, CheckBox, ScrollView,StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native'
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import RadioButtonNew from '../common/RadioButtonNew';
import RNPickerSelect from 'react-native-picker-select';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

const UpgradeMsg = (props) => {
    return (
        <View style={{ height: 170,marginLeft:-10,marginRight:-10,marginTop:-5 }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })

            }}
                style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }}
            >
                <Image
                    resizeMode='contain'
                    source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15 }} />
            </TouchableOpacity>
            <View style={{padding:10}}>
            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 16, textAlign: 'center' }}>You are agreeing for us to charge you monthly in the amount indicated.</Text>

            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 16, textAlign: 'center', marginTop: 10 }}>Would you like to continue<Text allowFontScaling={false} style={{fontFamily:'Aileron-Regular'}}>?</Text> </Text>

            <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                    props.navigation.navigate('Home')
                }}
                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 8, margin: 10, alignItems: 'center' }}


                    >

                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                    </LinearGradient>


                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                }}

                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                        style={{ padding: 8, margin: 10, alignItems: 'center' }}
                    >

                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                    </LinearGradient>

                </TouchableOpacity>
            </View>
            </View>
        </View>
    )
}


export default class UpgredForm extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            UpgradeMsg: <UpgradeMsg navigation={this.props.navigation} />,
            radioSaved:
                [{
                    size: 20,
                    selectedBorderWidth: 5,
                    value: '0',
                    selectedinnerColor: '#000',
                    selectedborderColor: '#ddd',
                    unselectedborderColor: '#ddd',
                    selected: false
                }],
            radioNew:
            [{
                size: 20,
                selectedBorderWidth: 5,
                value: '1',
                selectedinnerColor: '#000',
                selectedborderColor: '#ddd',
                unselectedborderColor: '#ddd',
                selected: false
            }],
            selectedItem:'',
            accountType: [
                {
                },
                
            ],
            filt_accountType:0,
            MonthList: [
                {
                    label: 'January',
                    value: 1,
                    color:'#000000'
                    
                },
                {
                    label: 'February',
                    value: 2,
                    color:'#000000'
                },
                {
                    label: 'March',
                    value: 3,
                    color:'#000000'
                },
                {
                    label: 'April',
                    value: 4,
                    color:'#000000'
                },
                {
                    label: 'May',
                    value: 5,
                    color:'#000000'
                },
                {
                    label: 'June',
                    value: 6,
                    color:'#000000'
                },
                {
                    label: 'July',
                    value: 7,
                    color:'#000000'
                },
                {
                    label: 'August',
                    value: 8,
                    color:'#000000'
                },
                {
                    label: 'September',
                    value: 9,
                    color:'#000000'
                },
                {
                    label: 'October',
                    value: 10,
                    color:'#000000'
                },
                {
                    label: 'November',
                    value: 11,
                    color:'#000000'
                },
                {
                    label: 'December',
                    value: 12,
                    color:'#000000'
                },
                
            ],
            filt_month:0

        }
    }


    changeActiveRadioButton(value) {

        this.state.radioNew[0].selected = false;
        this.state.radioSaved[0].selected = false;

        if (value == '1') {
          this.state.radioNew[0].selected = true;
          this.state.radioSaved[0].selected = false;
        } else {
          this.state.radioNew[0].selected = false;
          this.state.radioSaved[0].selected = true;
        }
        this.setState({ selectedItem: value });
    }



    _onSendPress = () => {
        this.setState({ passwordChange: true })
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.UpgradeMsg })
    }

    render() {
        

        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>

                <CustomHeader navigation={this.props.navigation} bArrow />

                <AuthTitle TitleName='Upgrade' />

                <KeyboardAwareScrollView extraScrollHeight={70} extraHeight={70} enableOnAndroid={true}>

                    <Card>
                        <View style={{ flexDirection: 'row', height: 40,marginTop:-10 }}>

                            {/* <RadioButtonInput
        initial={1}                
        isSelected={true}
        borderWidth={1}
        buttonInnerColor={'#757171'}
        buttonOuterColor={'#757171'}
        buttonSize={10}
        buttonOuterSize={20}
        buttonStyle={{}}
        buttonWrapStyle={{marginLeft: 10}}
        
        
      /> */}

                            {this.state.radioSaved.map((item, key) =>
                                (
                                    <RadioButtonNew key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, item.value)} />
                                ))}



                            <Text allowFontScaling={false} font={'CenturyGothic'}  style={{ paddingLeft: 10,marginTop:10}}>Credit/Debit Card</Text>
                        </View>

                        
                        <TextInput placeholder='Credit/Debit Card Number' keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} />
                            
                        


                        <View style={{ flexDirection: 'row', }}>
                            <View style={{ width: Dimensions.get('window').width / 2.5, paddingRight: 10 }}>
                                {/* <Input placeholder='Expiry Date' /> */}
                                <View style={{backgroundColor: '#e9e9e9',padding:10,marginBottom:10,color: '#808080',borderRadius:5,paddingLeft:15,height:50,justifyContent:'center'}}>
                            
                            <View style={{}}><RNPickerSelect
                            placeholder={{
                                label: 'Exp. Month',
                                value: null,
                                
                              }}
                            items={this.state.MonthList}
                            onValueChange={(value) => {
                                this.setState({
                                    filt_month: value,
                                });
                            }}
                            useNativeAndroidPickerStyle={false}
                            style={{pickerSelectStyles}}
                            value={this.state.filt_month}
                            font={'CenturyGothic'}
                            
                            placeholderTextColor="rgb(129,129,129)"
                            placeholderfontSize= '100'
                            Icon={() => {
                                return (
                                <Image
                                resizeMode='contain'
                                source={require("../../assets/Icons/dropdown_ico.png")}
                                style={{ width: 10, height: 10,marginTop:5 }}
                                />
                                );
                              }}
                            />
                            </View>
                        </View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width / 3, paddingRight: 10 }}>
                                
                                <TextInput placeholder='Exp. Year' maxLength = {4} keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} />
                            </View>
                            <View style={{ width: Dimensions.get('window').width / 4.2, paddingRight: 10 }}>
                            <TextInput placeholder='CVV' keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} />
                         
                            </View>

                        </View>


                        <Input placeholder='Name On Card' />

                        <View style={{ flexDirection: 'row', height: 40,marginTop:-7 }}>
                            {/* <RadioButtonInput
                                index={1}
                                value={1}
                                borderWidth={1}
                                buttonInnerColor={'#757171'}
                                buttonOuterColor={'#757171'}
                                buttonSize={10}
                                buttonOuterSize={20}
                                buttonStyle={{}}
                                buttonWrapStyle={{ marginLeft: 10 }}

                            /> */}

                            {this.state.radioNew.map((item, key) =>
                            (
                                <RadioButtonNew key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, item.value)} />
                            ))}


                            <Text allowFontScaling={false} font={'CenturyGothic'}  style={{ paddingLeft: 10,marginTop:10 }}>Checking/Savings Account</Text>
                        </View>

                        <Input placeholder="Account Owner's Name" />

                        
                        <TextInput placeholder='Routing Number' keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} />
                        

                        
                        <TextInput placeholder='Account Number' keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} />
                         
                        <TextInput placeholder='Confirm Account #' keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} />
                         
                        

                        <View style={{backgroundColor: '#e9e9e9',padding:10,marginBottom:10,color: '#808080',borderRadius:5,paddingLeft:15,height:50,justifyContent:'center'}}>
                            
                            <View ><RNPickerSelect
                            placeholder={{
                                label: 'Account Type',
                                value: null,
                                
                              }}
                            items={this.state.accountType}
                            onValueChange={(value) => {
                                this.setState({
                                    filt_distance: value,
                                });
                            }}
                            useNativeAndroidPickerStyle={false}
                            style={{pickerSelectStyles}}
                            value={this.state.filt_accountType}
                            font={'CenturyGothic'}
                            
                            placeholderTextColor="rgb(129,129,129)"
                            placeholderfontSize= '100'
                            Icon={() => {
                                return (
                                <Image
                                resizeMode='contain'
                                source={require("../../assets/Icons/dropdown_ico.png")}
                                style={{ width: 10, height: 10,marginTop:5 }}
                                />
                                );
                              }}
                            />
                            </View>
                        </View>

                    </Card>
                </KeyboardAwareScrollView>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('SetHome')} style={{alignItems:'center',marginBottom:20}}>
                        <Text  allowFontScaling={false}  font={'CenturyGothic'} style={{fontSize:23,textDecorationLine:'underline',marginTop:7}}  >SKIP</Text>
                        </TouchableOpacity>
                <TouchableOpacity onPress={this._onSendPress}>
                    <LinearGradient



                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 10, alignItems: 'center',height:52 }}


                    >

                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                        SUBMIT
  </Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>

        )
    }
}



const styles = StyleSheet.create({
    myTextBox: {
        color:'#000',
        paddingLeft:15,
        fontSize:18,
        lineHeight:23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius:5,
        height:50,
    },


})



const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 24,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
        borderRadius:5,
        width:'100%',
        alignItems:'center',
        alignSelf:'center',
        marginBottom: 10,
        fontFamily:'CenturyGothic'
        
    },
    inputAndroid: {
        fontSize: 24,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 1,
        borderColor: '#e8e8e8',
        
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
        width:'100%',
        borderRadius:5,
        alignItems:'center',
        alignSelf:'center',
        
        marginBottom: 10,
        fontFamily:'CenturyGothic'
        
    },
    
});
