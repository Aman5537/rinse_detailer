import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, CheckBox, ScrollView, Dimensions, TouchableOpacity, Image, Button,ImageBackground } from 'react-native'
import CommanStyle from '../config/Styles';

import Helper from '../Lib/Helper';
import { StackActions, NavigationActions } from 'react-navigation';

export default class MaintananceScreen extends Component {
    static navigationOptions = { header: null }

    

    constructor(props) {
        super(props);
        this.state = {
           isLogedin:true     
        }
       
    }

    componentDidMount() {
        
        Helper.getData('userdata').then((responseData) => {
            // console.log('------userData  :  ' + JSON.stringify(responseData));
            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.setState({isLogedin:false});
            } else {
                this.setState({isLogedin:true});
                //this.props.navigation.navigate('Home');
            }
        })

    }
    render() {
        
        return (


            <View style={{ flex: 1, backgroundColor: '#1ba0e2', }}>
            
                <View>
                    <Image
                    src={require('../../assets/Icons/maintan.png')}
                    style={{width:300,height:300}}
                    />
                </View>
                
                    
            </View>
            

        )
        
        
    }



}



const styles = StyleSheet.create({
    wrapper: {
       
    },
    slide1: {
        flex: 1,
        backgroundColor: '#fff',
    },
    slide2: {
        flex: 1,
        backgroundColor: '#fff',
    },
    slide3: {
        flex: 1,
        backgroundColor: '#fff',
    },
    slide4: {
        flex: 1,
        backgroundColor: '#fff',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    screenbtn: {
        backgroundColor: '#fff', 
        textAlign: 'center', 
        padding: 10, 
        color: '#0083c5', 
        borderRadius: 5, 
        fontSize: 20
    },
    slideTitle:{
        textAlign:'center',
    //    fontFamily:'CenturyGothic',
        padding:10,
        fontSize:20,
        
    },
    slideText:{
        textAlign:'center',
        color:'#818181',
        fontSize:16,
        fontFamily: 'Aileron-Regular'
        
    },
    
})