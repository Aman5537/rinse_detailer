import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity, Linking } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle, HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import Helper from '../Lib/Helper';
import Moment from "moment";

class ForwordPopUp extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);

        this.state = {
            subjectMsg:'Revenue Reports',
            bodyMsg:"Revenue Reports download link here \n "+this.props.downLink
        }

        //alert(this.props.downLink);

    }

    render(){
        return (
            <View style={{ alignItems: 'center', height: 100, margin: Dimensions.get('window').height / 2.5 }}>
    
                <TouchableOpacity onPress={() => {
                    global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" })
                    Linking.openURL('mailto:support@rinse.com?subject='+this.state.subjectMsg+'&body='+this.state.bodyMsg);
                }}
                    style={{ flex: 1, width: 200 }}
                >
    
                    <LinearGradient
                        colors={['#3c82c5', '#3c82c5', '#3c82c5']}
                        style={{ padding: 8, margin: 10, alignItems: 'center', borderRadius: 5, height: 40 }}
    
    
                    >
    
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Forward</Text>
    
                    </LinearGradient>
    
    
                </TouchableOpacity>
    
                <TouchableOpacity onPress={() => {
                    global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" })
                }}
    
                    style={{ flex: 1, width: 200 }}
                >
    
                    <LinearGradient
                        colors={['#dfdfdf', '#dfdfdf', '#dfdfdf']}
                        style={{ padding: 8, margin: 10, alignItems: 'center', borderRadius: 5, height: 40 }}
                    >
    
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#1f1f1f', fontSize: 18 }} >Cancel</Text>
    
                    </LinearGradient>
    
                </TouchableOpacity>
    
            </View>
        )
    }
    
    
    
}



export default class Report extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            ForwordPopUp: <ForwordPopUp />,
            AppUserData: '',
            downLinkUrl: '',
            NotFountMsg:''

        }
    }

    componentDidMount() {

        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {

                this.setState({ AppUserData: responseData });
                
                this.genrateReport();
                
            }
        })





    }

    genrateReport() {
        //alert('yes');
        let tempdate = new Date();

        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();

        let formData = {
            detailer_id: this.state.AppUserData.id,
            time: TimeZone
        }
        
        Helper.makeRequest({ url: "revenue-report", data: formData }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                this.setState({ downLinkUrl: data.data.url })
            }else{
                this.setState({NotFoundMsg:'Revenue Reports Not Found!'})
            }
        })
    }



    _onForwordPopUp = () => {
        this.setState({ 'ForwordPopUp': <ForwordPopUp downLink={this.state.downLinkUrl} /> }, () => {
            global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.ForwordPopUp })
        });


    }

    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation} userbar bArrow />
                <HeaderTitle TitleName='Reports' />

                <View style={{ padding: 20 }}>
                    <View>
                        {(this.state.downLinkUrl != '') ?
                            <TouchableOpacity onPress={this._onForwordPopUp} >
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 20, color: '#4584db', textDecorationLine: 'underline' }}>Revenue Reports</Text>
                            </TouchableOpacity>
                            :
                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 20, alignSelf: 'center' }}>{this.state.NotFountMsg}</Text>
                        }
                    </View>

                </View>



            </View>
        )
    }
}

const style = StyleSheet.create({
    textArea: {

    }



})


