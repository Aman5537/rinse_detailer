import React, { Component } from 'react'
import { Text, View, TextInput, CheckBox, ScrollView, TouchableOpacity, Image } from 'react-native'
import { createAccount } from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import  CommanStyle  from '../config/Styles';
import Helper from '../Lib/Helper';



const SuccessMsg = () => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={require('../../assets/Icons/tik.png')} style={{ height: 70, width: 70, margin: 10 }} />
            <Text  allowFontScaling={false}   style={{ fontSize: 16, fontWeight: "bold", textAlign: "center",fontFamily: 'Aileron-Regular' }}>Your Password had been Successful Changed</Text>

            <TouchableOpacity onPress={()=>{
                 global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}>
                <View style={{ width: 110, height: 40, backgroundColor: '#1d87dd', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                    <Text  allowFontScaling={false}   style={{ color: 'white', fontSize: 18,fontFamily: 'Aileron-Regular' }}>Ok</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const Msg = (props) => {
    
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={require('../../assets/Icons/tik.png')} style={{ height: 50, width: 50, margin: 10 }} />
            <Text  allowFontScaling={false}   style={{ fontSize: 18, textAlign: "center",fontFamily: 'Aileron-Regular' }}>
            We just sent an email to <Text  allowFontScaling={false}   style={{fontWeight: "bold",fontFamily: 'Aileron-Regular'}}>{props.forgot_email}</Text></Text>

            <Text  allowFontScaling={false}   style={{ fontSize: 18,textAlign: "center",marginTop:10,fontFamily: 'Aileron-Regular' }}>
          Click the secure link we sent you to reset your password. 
            </Text>

            <TouchableOpacity onPress={()=>{
                 global.state['modal'].setState({ modalVisible: false, Msg: "" });
                 props.navigation.navigate('Login');
            }}>
            <LinearGradient colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        borderRadius={5}
                        style={{ width: 110, height: 40, backgroundColor: '#1d87dd', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}

                        
                        >
                        
  <Text  allowFontScaling={false}   style={{ color:'#fff',fontSize:18,fontFamily: 'Aileron-Regular'}} >
  OK
  </Text>
</LinearGradient>

                
            </TouchableOpacity>


        </View>
    )
}




export default class ForgotPassword extends Component {

    static navigationOptions = { header: null }
    state = {
        signup: false,
        inputcolor: 'red',
        borderColor: '#dddd',
        sendBtnColor:['#4fa4d8', '#488cd8', '#3b55d9'],
        SuccessMsg: <SuccessMsg />,
        Msg: '',
        submitform: {
            email: '',
            role_id: 2,
            validators: {
                email: { required: true, "email": true },
                
            }
        }

    }

    setValues(key, value) {
        let submitform = { ...this.state.submitform }
        submitform[key] = value;
        this.setState({ submitform })
    }

    _onSendPress = () => {
        
        
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.SuccessMsg })

        
    }


    _onSignupPress = () => {

        let isValid = Helper.validate(this.state.submitform);
        if (isValid) {
            Helper.makeRequest({ url: "forgot", data: this.state.submitform }).then((submitData) => {
            // console.log('----logindata :  ' + JSON.stringify(logindata));
            if (submitData.status == 'true') {
                
                this.setState({ Msg: <Msg navigation={this.props.navigation} forgot_email={this.state.submitform.email}/> })
                
                global.state['modal'].setState({ modalVisible: true, Msg: this.state.Msg})
                
                let submitform = { ...this.state.submitform }
                submitform[key] = '';
                this.setState({ submitform })
            }
            else {
                this.setState({ signup: true, borderColor: false })
                
            }
            }).catch(err => {
            //Helper.showToast("Please try again");
            })
        }


        
      
    } 



    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: 'white', }}>
                    <CustomHeader navigation={this.props.navigation} bArrow />
                    <AuthTitle TitleName='Forgot Password?' />
                    <Card>

                        <Text  allowFontScaling={false}   style={[CommanStyle.normaltext,{ textAlign: 'center',marginBottom: 25,fontSize:15 }]}>Enter  your email address below and  we`ll send you a secure link to reset your password.</Text>


                        {this.state.signup === true && (
                            <Text  allowFontScaling={false}   style={{ textAlign: 'center', margin: 20, color: 'red',fontSize:15 }}>No account found for this address.</Text>
                        )}

                        <TextInput 
                        placeholder='Enter your email address' 
                        style={{ backgroundColor: '#e9e9e9', borderRadius: 5, marginBottom: 10, borderColor: this.state.borderColor ? this.state.borderColor : 'red', borderWidth: 1,height:50,padding:10,fontFamily:'CenturyGothic' }} 
                        onChangeText={(email) => this.setValues('email', email)}
                        />
                        
                        <TouchableOpacity 
                        onPress={this._onSignupPress}>
                        <LinearGradient 
                        

                     
                        colors={this.state.signup ? ['#909090','#909090','#909090'] : this.state.sendBtnColor}
                        borderRadius={5}
                        style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20,height:52 }}

                        
                        >
                        
  <Text  allowFontScaling={false}   style={{ color:'#fff',fontSize:22}} >
    SEND
  </Text>
</LinearGradient>
</TouchableOpacity>

                       
                        {this.state.signup === true && (<View>
                            <Text  allowFontScaling={false}   style={[CommanStyle.normaltext,{ textAlign: 'center',marginTop: 30 }]}> To create an account</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateAccount')} >
                            <LinearGradient colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        borderRadius={5}
                        style={{ padding: 10, alignItems: 'center', borderRadius: 5,marginTop:10,height:50 }}

                        
                        >
                        
  <Text  allowFontScaling={false}  style={{ color:'#fff',fontSize:18}} >
  SIGN UP
  </Text>
</LinearGradient>
</TouchableOpacity>
                           
                        </View>
                        )

                        }
                    </Card>
                </View>
            </ScrollView>
        )
    }
}