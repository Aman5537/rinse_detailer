// import React, { Component } from 'react'
// import { Text, View, TextInput, ScrollView, Dimensions, TouchableOpacity, Image, StyleSheet } from 'react-native'
// import { createAccount } from '../config/Styles';
// import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
// import LinearGradient from 'react-native-linear-gradient';
// import CheckBox from "react-native-check-box";
// import RNPickerSelect from 'react-native-picker-select';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import Helper from '../Lib/Helper';
// import GoogleApiAddressList from '../../components/GoogleApiAddressList'
// import CountryList from '../../components/CountryList';
// import CameraController from '../Lib/CameraController';
// import { Avatar } from 'react-native-elements';
// import Config from "../Lib/Config";

// var is_upgraded = false;

// const SignUpMsg = (props) => {
//     return (
//         <View style={{ height: 150, margin: -10, marginTop: -5 }}>

//             <TouchableOpacity onPress={() => {
//                 global.state['modal'].setState({ modalVisible: false, Msg: "" })

//             }}
//                 style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }}
//             >
//                 <Image
//                     resizeMode='contain'
//                     source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15 }} />
//             </TouchableOpacity>

//             <TouchableOpacity onPress={this.chooseImage}>
//                 <Text >Edit Image</Text>
//             </TouchableOpacity>
//             <View style={{ padding: 10 }}>
//                 <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>In order to service areas farther than 5 miles, please upgrade your account.</Text>


//                 <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', }}>
//                     <TouchableOpacity
//                         onPress={() => {
//                             global.state['modal'].setState({ modalVisible: false, Msg: "" });
//                             props.navigation.navigate('Upgrade', { is_skip: 1 });
//                         }}
//                         style={{ justifyContent: 'center' }}
//                     >

//                         <LinearGradient
//                             colors={['#5ced31', '#4acb23', '#37a313']}
//                             borderRadius={5}
//                             style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20, width: 200 }}
//                         >
//                             <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22, fontFamily: 'Aileron-Regular' }} >UPGRADE</Text>
//                         </LinearGradient>
//                     </TouchableOpacity>

//                     {/* <TouchableOpacity 
//                 onPress={() => {
//                     global.state['modal'].setState({ modalVisible: false, Msg: "" });
//                     props.navigation.navigate('ServicesYouOffer',{is_skip:1});
//                 }}
//                 style={{flex:1}}
//             >

//                 <LinearGradient 
//                     colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
//                     style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:200}}
//                 >
                        
//                     <Text allowFontScaling={false}  style={{ color:'#fff',fontSize:18}} >SIGN UP</Text>
               
//                 </LinearGradient>

//             </TouchableOpacity> */}
//                 </View>
//             </View>
//         </View>
//     )
// }



// export default class CreateAccount extends Component {
//     static navigationOptions = { header: null }

//     constructor(props) {
//         super(props);


//         this.state = {
//             SignUpMsg: <SignUpMsg navigation={this.props.navigation} />,
//             distance: [

//                 {
//                     label: '5 miles',
//                     value: 1,

//                 },
//                 {
//                     label: '10 miles',
//                     value: 2,

//                 },
//                 {
//                     label: '15 miles',
//                     value: 3,

//                 },
//                 {
//                     label: '20 miles',
//                     value: 4,

//                 },
//                 {
//                     label: '25 miles',
//                     value: 5,

//                 },
//                 {
//                     label: '35 miles',
//                     value: 6,

//                 },

//             ],
//             addressModalVisible: false,
//             hidePassword: true,
//             hideConfirmPassword: true,
//             checked: false,
//             isChecked:false,
//             countryCode:'',
//             registerform: {
//                 first_name: '',
//                 last_name: '',
//                 country_code: '971',
//                 mobile: '',
//                 email: '',
//                 device_type: "ANDROID",
//                 device_token: 'SC9830I',
//                 user_type: "App",
//                 zipcode: '',
//                 role_id: 2,
//                 business_name: '',
//                 business_address: '',
//                 business_lat: '',
//                 business_long: '',
//                 avatarSource: '',
//                 profile_picture: '',
//                 distance: '',
//                 validators: {
//                     first_name: { required: true, "minLength": 3,"maxLength": 20 },
//                     last_name: { required: true, "minLength": 3, "maxLength": 20 },
//                     business_name: { required: true, "minLength": 6, "maxLength": 100 },
//                     business_address: { required: true, "minLength": 6, "maxLength": 100 },
//                     email: { required: true, "email": true },
//                     country_code: { required: true },
//                     mobile: { required: true, "minLength": 7, "maxLength": 15 },
//                     zipcode: { required: true },
                    

//                 }
//             },
//         }

//         this.gotoNext = this.gotoNext.bind(this);
//         this.getuser(); 
//     }

//     gotoNext() {
//         this.onRegister();
//     }

//     async getuser() {
//         let user = await Helper.getData("userdata");
//         let token = await Helper.getData("token");
//         this.getprofile(user,token);
//     }

//     getprofile(user,token) {
//         let form = {
//             userid: token
//         } 
//         var registerform = { ...this.state.registerform };
//         this.setState({countryCode: parseInt(user.country_code) });
//         if(user.profile_picture !== ''){
//             this.setState({profile_picture: Helper.getImageUrl(user.profile_picture) });
//         }
        
        
        
        
                
                
//         registerform.first_name = user.first_name;
//         registerform.last_name = user.last_name;
//         registerform.email = user.email;
//         registerform.business_name = user.business_name;
//         registerform.business_address = user.business_address;
//         registerform.business_lat = user.business_lat;
//         registerform.business_long = user.business_long;
//         registerform.mobile = user.mobile;
//         registerform.country_code = user.country_code;
//         registerform.zipcode = user.zipcode;
//         registerform.distance = parseInt(user.distance);
//         this.setState({ registerform });
            
            
        
//     }



//     setValues(key, value) {
//         let registerform = { ...this.state.registerform }
//         if (key == 'mobile') {
//             value = value.replace(/[^0-9]/g, '');
//             registerform[key] = value;
//             this.setState({ registerform })
//         }
//         if (key == 'username') {
//             if (/^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z ]*)*$/.test(value) || value == '') {
//                 registerform[key] = value;
//                 this.setState({ registerform })
//             }
//         } else {
//             registerform[key] = value;
//             this.setState({ registerform });

//         }


//     }



//     onRegister = () => {

//         console.log(JSON.stringify(this.state.registerform));
//         if (this.state.distance.value > 3) {
//             //Helper.showToast("Upgrade Opne");
//             //return;
//         }

//         let isValid = Helper.validate(this.state.registerform);
//         if (isValid) {

//             if(!this.state.registerform.distance){
//                 Helper.showToast("Distance is required");
//                 return;
//             }
            

//             let tempdata = new FormData();

//             tempdata.append('first_name', this.state.registerform.first_name);
//             tempdata.append('last_name', this.state.registerform.last_name);
//             tempdata.append('country_code', this.state.registerform.country_code);
//             tempdata.append('mobile', this.state.registerform.mobile);
//             tempdata.append('email', this.state.registerform.email);
//             // tempdata.append('password', this.state.registerform.password);
//             // tempdata.append('confirm_password', this.state.registerform.confirm_password);
//             tempdata.append('device_type', this.state.registerform.device_type);
//             tempdata.append('device_token', this.state.registerform.device_token);
//             tempdata.append('user_type', this.state.registerform.user_type);
//             tempdata.append('zipcode', this.state.registerform.zipcode);
//             tempdata.append('role_id', this.state.registerform.role_id);
//             tempdata.append('business_name', this.state.registerform.business_name);
//             tempdata.append('business_address', this.state.registerform.business_address);
//             tempdata.append('business_lat', this.state.registerform.business_lat);
//             tempdata.append('business_long', this.state.registerform.business_long);
//             tempdata.append('distance', this.state.registerform.distance);

//             if (this.state.profile_picture) {
//                 tempdata.append('profile_picture', {
//                     uri: this.state.profile_picture,
//                     name: 'test.jpg',
//                     type: 'image/jpeg'
//                 });
//             }

//             Helper.makeRequest({ url: "complete-profile", method: "POSTUPLOAD", data: tempdata }).then((data) => {
//                 if (data.status == 'true') {
//                     //this.props.navigation.navigate('Verify', { fromSignup: true, signupData: this.state.registerform });
//                     Helper.showToast(data.message);
//                     this.props.navigation.navigate('Home');
//                 }
//                 else {
//                     Helper.showToast(data.message)
//                 }
//             }).catch(err => {
//                 Helper.showToast("Please try again");
//             })
//         }
//     }

//     setModalVisible(visible) {

//         this.setState({ addressModalVisible: visible });
//     }

//     handleAddress = (data) => {
//         let registerform = { ...this.state.registerform };
//         registerform['business_address'] = data.addressname;
//         registerform['business_lat'] = data.lat;
//         registerform['business_long'] = data.long;
//         this.setState({ registerform });
//     }

//     chooseImage = (data) => {

//         CameraController.selecteImage((response) => {
//             if (response.uri) {
//                 //this.state.profile_picture = response.uri;
//                 this.setState({ profile_picture: response.uri });
//                 this.setState({ avatarSource: response.uri });
//             }
//         });
//     }


//     _onSignupAlert = () => {

//         if ((this.state.filt_distance === 4) || (this.state.filt_distance === 5) || (this.state.filt_distance === 6)) {
//             global.state['modal'].setState({ modalVisible: true, Msg: this.state.SignUpMsg })
//         }
//         else {
//             this.props.navigation.navigate('ServicesYouOffer', { is_skip: 1 })
//         }


//         // {is_upgraded === true && (


//         // )}

//         // {is_upgraded === false && (


//         // )}


//         is_upgraded = !is_upgraded;
//     }



//     render() {
//         return (


//             <View style={{ flex: 1, backgroundColor: '#ffff', }}>
//                 <GoogleApiAddressList
//                     modalVisible={this.state.addressModalVisible}
//                     showModal={() => { this.setModalVisible(true); }}
//                     hideModal={() => { this.setModalVisible(!this.state.addressModalVisible) }}
//                     onSelectAddress={this.handleAddress}
//                 />
//                 <CustomHeader navigation={this.props.navigation} />

//                 <AuthTitle TitleName='Complete Profile' />
//                 <KeyboardAwareScrollView extraScrollHeight={70} extraHeight={70} enableOnAndroid={true}>
//                     <Card>

//                         <View style={{ flexDirection: 'row', }}>

//                             <View style={{ width: Dimensions.get('window').width, alignItems: 'center', marginBottom: 30 }} onPress={this.chooseImage}>

//                                 {this.state.profile_picture && <View>
//                                     <Avatar rounded size={100} source={{ uri: this.state.avatarSource ? this.state.avatarSource : this.state.profile_picture }} />
//                                 </View>}
//                                 {!this.state.profile_picture && <View>
//                                     <Avatar rounded size={100} source={require('../../assets/Icons/profile_ico.png')} />
//                                 </View>}

//                                 <TouchableOpacity onPress={this.chooseImage}
//                                     style={{ width: 50, height: 40, position: 'absolute', bottom: -10, justifyContent: 'flex-end' }}
//                                 >
//                                     <View>
//                                         <Image
//                                             style={{ width: 35, height: 35, alignSelf: 'flex-end', justifyContent: 'flex-end' }}
//                                             source={require('../../assets/Icons/profile_cam.png')}
//                                         />
//                                     </View>
//                                 </TouchableOpacity>
//                             </View>

//                         </View>

//                         <View style={{ flexDirection: 'row', }}>
//                             <View style={{ width: Dimensions.get('window').width / 2.1, paddingRight: 10 }}>
//                                 <TextInput
//                                     placeholder='First Name'
//                                     onChangeText={(txt) => { this.setValues('first_name', txt) }}
//                                     style={styles.myTextBox}
//                                     onSubmitEditing={() => { this.lastNameInput.focus(); }}
//                                     blurOnSubmit={true}
//                                     value={this.state.registerform.first_name}
//                                 />
//                             </View>

//                             <View style={{ width: Dimensions.get('window').width / 2.1, paddingLeft: 10 }}>
//                                 <TextInput
//                                     placeholder='Last Name'
//                                     onChangeText={(txt) => { this.setValues('last_name', txt) }}
//                                     style={styles.myTextBox} 
//                                     onSubmitEditing={() => { this.businessNameInput.focus(); }}
//                                     ref={(input) => { this.lastNameInput = input; }}
//                                     value={this.state.registerform.last_name}
//                                     />
//                             </View>

//                         </View>


//                         <TextInput
//                             placeholder='Business Name'
//                             onChangeText={(txt) => { this.setValues('business_name', txt) }}
//                             // {width: Dimensions.get('window').width / 2}
//                             style={[styles.myTextBox,{}]} 
//                             onSubmitEditing={() => { this.setModalVisible(true); }}
//                             ref={(input) => { this.businessNameInput = input; }}
//                             value={this.state.registerform.business_name}
//                         />

//                         <TouchableOpacity
//                             onPress={() => { this.setModalVisible(true); }}
//                         >
//                             <View style={styles.myTextBoxView} >

//                                 {this.state.registerform.business_address != '' && (
//                                     <Text numberOfLines={1} style={[styles.myTextBoxText, { opacity: 1 }]}>
//                                         {this.state.registerform.business_address}
//                                     </Text>
//                                 )}
//                                 {this.state.registerform.business_address == '' && (
//                                     <Text numberOfLines={1} style={styles.myTextBoxText}>
//                                         Business Address
//                             </Text>
//                                 )}

//                             </View>
//                             {/* <Input 
//                         placeholder='Business Address' 
//                         style={{ width: Dimensions.get('window').width / 2 }} 
//                         onChangeText={(txt) => { this.setValues('business_address', txt) }}
                        
//                         /> */}

//                         </TouchableOpacity>

//                         <TextInput
//                             placeholder='Email Address'
//                             onChangeText={(txt) => { this.setValues('email', txt) }}
//                             style={[styles.myTextBox,{}]} 
//                             value={this.state.registerform.email}
//                         />



//                         <View style={{ flexDirection: 'row', }}>


//                             <View style={{ width: Dimensions.get('window').width / 1.8, paddingRight: 10 }}>

//                                 {/* <CountryList
//                                     getFocus={(input) => { this.mobile = input; }}
//                                     setFocus={() => { this.username.focus(); }}
//                                     setValues={(store_number) => this.setValues('mobile', store_number)}
//                                     inputlable="Phone No."
//                                     selectedItems={this.state.registerform.country_code} /> */}

//             {this.state.countryCode!==''  && ( <CountryList
//             form={this.state.registerform}
//             showtoggleicon={false}
//             setValues={(mobile_number) => this.setValues('mobile', mobile_number)}
//             inputlable="Phone No."
//             mvalue={this.state.registerform.mobile}
//             selectedItems={this.state.registerform.country_code}
//             picked={this.state.countryCode}
//                    />) }


//                                 {/* <TextInput 
//                                 placeholder='Phone' 
//                                 returnKeyType='done' 
//                                 keyboardType="number-pad" 
//                                 style={styles.myTextBox} 
//                                 onChangeText={(txt) => { this.setValues('mobile', txt) }}/> */}
//                             </View>

//                             <View style={{ width: Dimensions.get('window').width / 2.5, paddingLeft: 10 }}>
//                                 <TextInput
//                                     keyboardType="number-pad"
//                                     returnKeyType='done'
//                                     placeholder='Zip Code'
//                                     style={styles.myTextBox}
//                                     onChangeText={(txt) => { this.setValues('zipcode', txt) }} 
//                                     //onSubmitEditing={() => { this.serviceDistanceInput.focus(); }}
//                                     ref={(input) => { this.emailInput = input; }}
//                                     value={this.state.registerform.zipcode}
//                                     />
//                             </View>
//                         </View>

//                         {/* <Input placeholder='Distance you want to service' style={{ width: Dimensions.get('window').width / 2 }} /> */}

//                         <View style={{ backgroundColor: '#e9e9e9', padding: 10, paddingTop: 15, marginBottom: 10, color: '#808080', borderRadius: 5, paddingLeft: 10, height: 50, justifyContent: 'center' }}>

//                             <View style={{}}><RNPickerSelect
//                                 placeholder={{ label: 'Service Distance', value: null }}
//                                 items={this.state.distance}
//                                 onValueChange={(value) => {
//                                     this.setValues('distance', value);
//                                 }}
//                                 useNativeAndroidPickerStyle={false}

//                                 style={pickerSelectStyles}
//                                 placeholderTextColor="rgb(129,129,129)"
//                                 placeholderfontSize='40'
//                                 value={this.state.registerform.distance}
//                                 placeholderfontSize='100'
//                                 Icon={() => {
//                                     return (
//                                         <Image
//                                             resizeMode='contain'
//                                             source={require("../../assets/Icons/dropdown_ico.png")}
//                                             style={{ width: 10, height: 10, marginTop: 15 }}
//                                         />
//                                     );
//                                 }}

//                             />
//                             </View>
//                         </View>




//                         {/* <TextInput
//                             secureTextEntry={true}
//                             placeholder='Password'
//                             onChangeText={(txt) => { this.setValues('password', txt) }}
//                             onSubmitEditing={() => { this.cPasswordInput.focus(); }}
//                             style={styles.myTextBox}
                            
//                         />
//                         <TextInput
//                             secureTextEntry={true}
//                             placeholder='Confirm Password'
//                             onChangeText={(txt) => { this.setValues('confirm_password', txt) }}
//                             ref={(input) => { this.cPasswordInput = input; }}
//                             style={styles.myTextBox}
//                         /> */}

//                         {/* <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
//                             <View style={{ flexDirection: 'row', flex: 1 }}>
                               

//                                 <CheckBox
//                                     style={{ flex: 1, paddingTop: 5 }}
//                                     onClick={() => {
//                                         this.setState({
//                                             isChecked: !this.state.isChecked
//                                         });
//                                     }}
//                                     isChecked={this.state.isChecked}


//                                     checkedImage={
//                                         <Image
//                                             source={require("../../assets/Icons/tick.png")}
//                                             style={{ width: 20, height: 20, resizeMode: 'contain' }}
//                                         />
//                                     }
//                                     unCheckedImage={
//                                         <Image
//                                             source={require("../../assets/Icons/untick.png")}
//                                             style={{ width: 20, height: 20, resizeMode: 'contain' }}
//                                         />
//                                     }
//                                 />



//                                 <View>
//                                     <View style={{ flexDirection: 'row' }}>
//                                         <Text allowFontScaling={false} style={{ fontSize: 15, }}>I have read and agree to the </Text>
//                                         <TouchableOpacity onPress={() => this.props.navigation.navigate('Term')}
//                                         >
//                                             <Text allowFontScaling={false} style={{ fontSize: 15, textDecorationLine: 'underline' }}>Terms of use</Text>
//                                         </TouchableOpacity>
//                                     </View>
//                                     <View style={{ flexDirection: 'row' }}>
//                                         <Text allowFontScaling={false} style={{ fontSize: 15, }}>& </Text>
//                                         <TouchableOpacity onPress={() => this.props.navigation.navigate('Policy')}
//                                         >
//                                             <Text allowFontScaling={false}  ><Text allowFontScaling={false} style={{ fontSize: 15, textDecorationLine: 'underline' }}>Privacy Policy</Text><Text style={{ fontSize: 16, textDecorationLine: 'none' }}>.</Text></Text>
//                                         </TouchableOpacity>
//                                     </View>
//                                 </View>
//                             </View>
//                         </View> */}
//                         {/* <View style={{alignItems:'center',paddingTop:10}}>
//                         <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
//                         <Text  allowFontScaling={false}  font={'CenturyGothic'} style={{fontSize:23,textDecorationLine:'underline',marginTop:7}}  >SKIP</Text>
//                         </TouchableOpacity>
//                         </View> */}
//                         <TouchableOpacity
//                             //onPress={this._onSignupAlert}
//                             onPress={this.gotoNext}
//                         >
//                             <LinearGradient
//                                 colors={['#4fa4d8', '#488cd8', '#3b55d9']}
//                                 borderRadius={5}
//                                 style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20, height: 52 }}


//                             >

//                                 <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >
//                                     SAVE
//                             </Text>
//                             </LinearGradient>
//                         </TouchableOpacity>
                        
//                         {/* <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: 10, }}>
//                             <Text allowFontScaling={false} style={{}}>Already have an account</Text>
//                             <Text allowFontScaling={false} ><Text style={{ fontFamily: 'Aileron-Regular' }}>? </Text></Text>
//                             <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
//                                 <Text allowFontScaling={false} style={{ textDecorationLine: 'underline' }}>Login</Text>
//                             </TouchableOpacity>
//                         </View> */}
//                     </Card>

//                 </KeyboardAwareScrollView>

//             </View>


//         )
//     }
// }


// const styles = StyleSheet.create({
//     myTextBox: {
//         color: '#000',
//         paddingLeft: 15,
//         fontSize: 18,
//         lineHeight: 23,
//         backgroundColor: '#e9e9e9',
//         marginBottom: 10,
//         borderRadius: 5,
//         height: 50,
//     },
//     myTextBoxView: {
//         color: '#000',
//         padding: 10,
//         paddingLeft: 15,
//         fontSize: 18,
//         lineHeight: 23,
//         backgroundColor: '#e9e9e9',
//         marginBottom: 10,
//         borderRadius: 5,
//         height: 50,
//     },
//     myTextBoxText: {
//         fontSize: 17,
//         color: '#000',
//         opacity: 0.4,


//     }


// })


// const pickerSelectStyles = StyleSheet.create({
//     inputIOS: {
//         fontSize: 16,
//         paddingVertical: 10,
//         paddingHorizontal: 8,
//         borderWidth: 1,
//         borderColor: '#e8e8e8',
//         height: 40,
//         color: '#000',
//         paddingRight: 30, // to ensure the text is never behind the icon
//         borderRadius: 5,
//         width: '100%',
//         alignItems: 'center',
//         alignSelf: 'center',
//         marginBottom: 10,
//         fontFamily: 'CenturyGothic'

//     },
//     inputAndroid: {
//         fontSize: 16,
//         paddingHorizontal: 10,
//         paddingVertical: 8,
//         borderWidth: 1,
//         borderColor: '#e8e8e8',
//         height: 40,
//         color: '#000',
//         paddingRight: 30, // to ensure the text is never behind the icon
//         width: '100%',

//         borderRadius: 5,
//         alignItems: 'center',
//         alignSelf: 'center',

//         marginBottom: 10,
//         fontFamily: 'CenturyGothic'

//     },
// });
