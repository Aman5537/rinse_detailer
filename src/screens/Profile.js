import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity,ScrollView,TextInput } from 'react-native'
import { CustomHeader ,CardSection, AuthHeader,AuthTitle,HeaderTitle,Input} from '../common';
import  CommanStyle  from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import RNPickerSelect from 'react-native-picker-select';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import Helper from '../Lib/Helper';
import { Avatar } from 'react-native-elements';
import CameraController from '../Lib/CameraController';
import CountryList from '../../components/CountryList';
import GoogleApiAddressList from '../../components/GoogleApiAddressList'
import StarRating from 'react-native-star-rating';

const SuccessMsg = (props) => {

    return (
        <View style={{  height: 120,marginLeft:-10,marginRight:-10,marginTop:-5 }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}

            >
                <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end', }} />
            </TouchableOpacity>

            <View style={{padding:10}}>
            <Text allowFontScaling={false}  style={{ fontSize: 16, textAlign: 'center',fontFamily: 'Aileron-Regular' }}>
                You will receive a notification when yours address has been updated.
            </Text>

            <TouchableOpacity onPress={() => {
            global.state['modal'].setState({ modalVisible: false, Msg: "" });
            }}
            style={{ flex: 1,alignItems: 'center', }}
            >
                <LinearGradient
                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                    style={{ padding: 8, margin: 10, alignItems: 'center',width:100,height:40 }}
                >

                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >OK</Text>

                </LinearGradient>
            </TouchableOpacity>
            </View>
        </View>
    )
}


class ValidateMsg extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            SuccessMsg: <SuccessMsg navigation={this.props.navigation} />,
        }
    }
    

    _onSuccessMsg = () => {
        
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.SuccessMsg })
    }

    render() {
    return (
        <View style={{ alignItems: 'center', height: 230 }}>

            <Text allowFontScaling={false}  style={{ fontSize: 16, textAlign: 'center',fontFamily: 'Aileron-Regular' }}>
                It will take 1-3 business days for your change of address to update.
            </Text>

            <Text allowFontScaling={false}  style={{ fontSize: 16, marginTop: 10, textAlign: 'center',fontFamily: 'Aileron-Regular' }}>
                Would you like to make this change or you can upgrade to remove this waiting period and the changes will be made immediately.
            </Text>

            <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity onPress={this._onSuccessMsg}

                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 8, margin: 10, alignItems: 'center',borderRadius:3,height:40 }}


                    >

                        <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                    </LinearGradient>


                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                }}

                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                        style={{ padding: 8, margin: 10, alignItems: 'center',borderRadius:3,height:40 }}
                    >

                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >No</Text>

                    </LinearGradient>

                </TouchableOpacity>
            </View>
                
            <TouchableOpacity
               onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" });
                this.props.navigation.navigate('Upgrade');
            }}
                >
                <LinearGradient
                colors={['#5ced31', '#4acb23', '#37a313']}
                style={{ padding: 7, alignItems: 'center', marginTop: 30,borderRadius:5,width:180,height:40}}
                >

                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >UPGRADE</Text>
                </LinearGradient>
            </TouchableOpacity>
        </View>
    )}
}




export default class Profile extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            ValidateMsg: <ValidateMsg navigation={this.props.navigation} />,
            is_back: this.props.navigation.getParam('is_back', 0), 
            distance: [
                    
                {
                    label: '5 miles',
                    value: 5,
                    
                },
                {
                    label: '10 miles',
                    value: 10,
                    
                },
                {
                    label: '15 miles',
                    value: 15,
                    
                },
                {
                    label: '20 miles',
                    value: 20,
                    
                },
                {
                    label: '25 miles',
                    value: 25,
                    
                },
                {
                    label: '35 miles',
                    value: 35,
                    
                },
            
            ],
            addressModalVisible: false,
            filt_distance:'',
            userdata:'',
            avatarSource:'',
            countryCode:'',
            Editable:true,
            editProfileform: {
                first_name: '',
                last_name: '',
                mobile: '',
                country_code: '',
                zipcode: '',
                business_name: '',
                profile_picture:'',
                business_address: '',
                business_lat: '',
                business_long: '',
                validators: {
                    first_name: { required: true, "maxLength": 50 },
                    last_name: { required: true, "maxLength": 50 },
                    business_name: { required: true, "maxLength": 50 },
                    business_address: { required: true },
                    country_code: { required: true },
                    mobile: { "required": true, "minLength": 7, "maxLength": 15 },
                    zipcode: { required: true },
                    
                }
            },
            overall_rating:0,
            total_review:0,
            total_cancellation:0,
            avg_late_time:0
            
            
        }

        

        this.gotoNext = this.gotoNext.bind(this);
        this.getuser();
    }

    
    setValues(key, value) {
        let editProfileform = { ...this.state.editProfileform };
        
        if (key == 'mobile') {

            if(this.state.editProfileform.mobile.length > value.length){
                editProfileform[key] = value;
                this.setState({ editProfileform })
            }
            else{
                value = value.replace(/-/g,"");
                if(value.length > 3){
                    value = value.slice(0,3)+"-"+value.slice(3,6)+"-"+value.slice(6,15);
                }

                editProfileform[key] = value;
                this.setState({ editProfileform });
            }
        }

        if (key == 'name') {
            if (/^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z ]*)*$/.test(value) || value == '') {
                editProfileform[key] = value;
                this.setState({ editProfileform })
            }
        } else {
            editProfileform[key] = value;
            this.setState({ editProfileform })
        }
    }
    
    chooseImage = (data) => {

        var userdata = { ...this.state.userdata };
        CameraController.selecteImage((response) => {
            if (response.uri) { 
                this.setState({ avatarSource: response.uri });
                userdata.profile_picture = response.uri;
                this.setState({ userdata })
            }
        });
    }

    
    async getuser() {
        let user = await Helper.getData("userdata");
        let token = await Helper.getData("token");
        this.getprofile(user,token);
    }

    getprofile(user,token) {
        let form = {
            userid: token
        }
        var editProfileform = { ...this.state.editProfileform };
        Helper.makeRequest({ url: "getProfile", method: "POST", data: form }).then((data) => {
            if (data.status == 'true') {
                let usernew = data.data;
                this.setState({countryCode: parseInt(usernew.country_code) });

                Helper.setData('userdata', data.data);
                
                this.setState({ userdata: data.data });

                

                this.setState({filt_distance: parseInt(usernew.distance) });
                editProfileform.first_name = usernew.first_name;
                editProfileform.last_name = usernew.last_name;
                editProfileform.business_name = usernew.business_name;
                editProfileform.email = usernew.email;
                editProfileform.country_code = usernew.country_code;
                editProfileform.zipcode = usernew.zipcode;
                editProfileform.business_address = usernew.business_address;
                editProfileform.business_lat = usernew.business_lat;
                editProfileform.business_long = usernew.business_long;
                
                this.setState({
                    overall_rating:usernew.overall_rating,
                    total_review:usernew.total_review,
                    total_cancellation:usernew.total_cancellation,
                    avg_late_time:usernew.avg_late_time
                })
                
                editProfileform.mobile = usernew.mobile.slice(0,3)+"-"+usernew.mobile.slice(3,6)+"-"+usernew.mobile.slice(6,15);
                
                
                
                
            } else {
                this.setState({countryCode: parseInt(user.country_code) });
                this.setState({ userdata: user });
                this.setState({filt_distance: parseInt(user.distance) });
                
                
                editProfileform.first_name = user.first_name;
                editProfileform.last_name = user.last_name;
                editProfileform.business_name = user.business_name;
                editProfileform.email = user.email;
                editProfileform.country_code = user.country_code;
                editProfileform.zipcode = user.zipcode;
                editProfileform.business_address = user.business_address;
                editProfileform.business_lat = user.business_lat;
                editProfileform.business_long = user.business_long;
                
                editProfileform.mobile = user.mobile.slice(0,3)+"-"+usernew.mobile.slice(3,6)+"-"+usernew.mobile.slice(6,15);

            }
            this.setState({ editProfileform });
            
            if(this.state.userdata.is_sub_detailer == 1){
                this.setState({Editable:false})
            }
            else{
                this.setState({Editable:true})
            }
            
        })
    }


    gotoNext() {
        this.onEditProfile();
    }

    onEditProfile = () => { 
        let user =  this.state.userdata;
        
        let isValid = Helper.validate(this.state.editProfileform);
        if (isValid) {

            let tempdata = new FormData();

            var location_update = 0;
            var distance_update = 0;


            if(user.is_sub_detailer == 0){
                if((this.state.editProfileform.business_lat != user.business_lat) || (this.state.editProfileform.business_long != user.business_long)){
                    location_update = 1
                }
    
                if(this.state.filt_distance != user.distance){
                    //alert(user.distance+'---'+this.state.filt_distance);
                    distance_update = 1
                }
            }
           

            tempdata.append('first_name', this.state.editProfileform.first_name);
            tempdata.append('last_name', this.state.editProfileform.last_name);
            //tempdata.append('mobile', this.state.editProfileform.mobile);
            tempdata.append('zipcode', this.state.editProfileform.zipcode);
            tempdata.append('business_name', this.state.editProfileform.business_name);
            tempdata.append('distance', this.state.filt_distance);
            tempdata.append('country_code', this.state.editProfileform.country_code);
            tempdata.append('business_address', this.state.editProfileform.business_address);
            tempdata.append('business_lat', this.state.editProfileform.business_lat);
            tempdata.append('business_long', this.state.editProfileform.business_long);
            tempdata.append('location_update', location_update);
            tempdata.append('distance_update', distance_update);
            
            tempdata.append('mobile', this.state.editProfileform.mobile.replace(/-/g,""));

            tempdata.append('role_id', 2);

            if (this.state.avatarSource) {
                tempdata.append('profile_picture', {
                    uri: this.state.avatarSource,
                    name: 'test.jpg',
                    type: 'image/jpeg'
                });
            }

            
            Helper.makeRequest({ url: "updateProfile", method: "POSTUPLOAD", data: tempdata }).then((data) => {
                if (data.status == 'true') {
                    Helper.showToast(data.message);
                    Helper.setData('userdata', data.data);
                    this.setState({'userdata':data.data});

                    if(this.state.is_back === 1){
                        this.props.navigation.goBack(null)
                    }
                }
                else {
                    Helper.showToast(data.message)
                }
            })
        }
    }

    selectCat = (selectedItems) => {
        this.setState({ selectedItems });
    }

    _onValidateMsg = () => {
        
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.ValidateMsg })
    }

    setModalVisible(visible) {

        this.setState({ addressModalVisible: visible });
    }

    handleAddress = (data) => {
        let editProfileform = { ...this.state.editProfileform };
        editProfileform['business_address'] = data.addressname;
        editProfileform['business_lat'] = data.lat;
        editProfileform['business_long'] = data.long;
        this.setState({ editProfileform });
    }

    render() {
console.log(this.state.countryCode,'this.state.countryCodes')

        return (

            <View style={{ flex: 1, }}>
                <GoogleApiAddressList
                    modalVisible={this.state.addressModalVisible}
                    showModal={() => { this.setModalVisible(true); }}
                    hideModal={() => { this.setModalVisible(!this.state.addressModalVisible) }}
                    onSelectAddress={this.handleAddress}
                />
                <CustomHeader userinfo={this.state.userdata} navigation={this.props.navigation}  userbar bArrow/>
                <HeaderTitle TitleName='Profile' />
                <KeyboardAwareScrollView extraScrollHeight={70} extraHeight={70} enableOnAndroid={true}>

                <View style={{alignContent:'center',alignItems:'center'}} >
                    <View style={{ width: Dimensions.get('window').width, alignItems: 'center', marginBottom: 30 }}>
                    <TouchableOpacity onPress={this.chooseImage} 
                            style={{ alignItems: 'center'}}
                            >
                        {this.state.userdata.profile_picture != '' && <View>
                            <Avatar rounded size={100} source={{ uri: this.state.avatarSource ? this.state.avatarSource : Helper.getImageUrl(this.state.userdata.profile_picture) }} />
                        </View>}
                        {this.state.userdata.profile_picture == '' && <View>
                            <Avatar rounded size={100} source={require('../../assets/Icons/profile_ico.png')} />
                        </View>}


                        <TouchableOpacity onPress={this.chooseImage}
                        style={{ width: 50, height: 40, position: 'absolute', bottom: -10, justifyContent: 'flex-end' }}
                        >
                            <View>
                                <Image
                                    style={{ width: 35, height: 35, alignSelf: 'flex-end', justifyContent: 'flex-end' }}
                                    source={require('../../assets/Icons/profile_cam.png')}
                                />
                            </View>
                        </TouchableOpacity>
                        </TouchableOpacity>

                    </View>
                    <Text allowFontScaling={false}  style={[CommanStyle.normaltext,{fontWeight:'bold',marginTop:5,fontFamily: 'Aileron-Regular'}]}>{this.state.userdata.first_name} {this.state.userdata.last_name}</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SubmiteRequest')}
                    style={{paddingTop:5}}
                    >
                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={[CommanStyle.normaltext,{fontSize:14,alignSelf:'center'}]}>Ontime Rate: {Helper.convertHoursMin(this.state.avg_late_time)} late</Text>
                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={[CommanStyle.normaltext,{fontSize:14,alignSelf:'center'}]}>{this.state.total_cancellation} Cancelation/No Shows</Text>
                    <View style={{flexDirection:'row',marginTop:5,justifyContent:'center'}}>
                    <StarRating
                    disabled={true}
                    maxStars={5}
                    rating={this.state.overall_rating}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                    fullStarColor={'#eab803'}
                    halfStarColor={'#eab803'}
                    emptyStarColor={'#d3d3d5'}
                    starSize={20}
                    starStyle={{margin:2}}
                    
                />
                        {/* <Image
                        source={require("../../assets/Icons/star_y.png")}
                        style={{ width: 15, height: 15,margin:2}}
                        />
                        <Image
                        source={require("../../assets/Icons/star_y.png")}
                        style={{ width: 15, height: 15,margin:2}}
                        />
                        <Image
                        source={require("../../assets/Icons/star_xy.png")}
                        style={{ width: 15, height: 15,margin:2}}
                        />
                        <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 15, height: 15,margin:2}}
                        />
                        <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 15, height: 15,margin:2}}
                        /> */}
                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={[CommanStyle.normaltext,{fontSize:14,marginTop:2,marginLeft:5}]}> 
                              {this.state.total_review} Reviews
                        </Text>
                    </View>
                    </TouchableOpacity>
                </View>
                {/* <ScrollView > */}
                <View style={{padding:20}}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:1,marginRight:5}}>
                            <TextInput 
                            placeholder="First Name" 
                            style={styles.myTextBox} 
                            onChangeText={(txt) => { this.setValues('first_name', txt) }}
                            onSubmitEditing={() => { this.lastNameInput.focus(); }}
                            value={this.state.editProfileform.first_name}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <TextInput 
                            placeholder="Last name" 
                            style={styles.myTextBox} 
                            onChangeText={(txt) => { this.setValues('last_name', txt) }}
                            onSubmitEditing={() => { this.businessnameInput.focus(); }}
                            ref={(input) => { this.lastNameInput = input; }}
                            value={this.state.editProfileform.last_name}
                            />
                        </View>
                    </View>
                    <TextInput 
                    placeholder="Email" 
                    editable={false}
                    style={styles.myTextBox}
                    value={this.state.editProfileform.email}
                    />

                    <TextInput 
                    placeholder="Business Name" 
                    editable={this.state.Editable}
                    onChangeText={(txt) => { this.setValues('business_name', txt) }}
                    style={styles.myTextBox}
                    onSubmitEditing={() => { this.zipcodeInput.focus(); }}
                    ref={(input) => { this.businessnameInput = input; }}
                    value={this.state.editProfileform.business_name}
                    />
                    <TouchableOpacity
                            onPress={() => { 
                                if(this.state.userdata.is_sub_detailer == 0){ 
                                    this.setModalVisible(true); 
                                } 
                            }}
                        >
                            <View style={styles.addressTextBoxView} >

                                {this.state.editProfileform.business_address != '' && (
                                    <Text 
                                    //numberOfLines={1} 
                                    style={[styles.myTextBoxText, { opacity: 1 }]}>
                                        {this.state.editProfileform.business_address}
                                    </Text>
                                )}
                                {this.state.editProfileform.business_address == '' && (
                                    <Text numberOfLines={1} style={styles.myTextBoxText}>
                                        Business Address
                            </Text>
                                )}

                            </View>
                            {/* <Input 
                        placeholder='Business Address' 
                        style={{ width: Dimensions.get('window').width / 2 }} 
                        onChangeText={(txt) => { this.setValues('business_address', txt) }}
                        
                        /> */}

                        </TouchableOpacity>

                    {/* <Input placeholder="Name" /> */}
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:1,marginRight:5}}>
                            <TextInput 
                            placeholder="Zip Code" 
                            editable={this.state.Editable}
                            //keyboardType="number-pad" 
                            keyboardType='numeric'
                            returnKeyType='done' 
                            style={styles.myTextBox} 
                            value={this.state.editProfileform.zipcode}
                            onChangeText={(txt) => { this.setValues('zipcode', txt) }} 
                            ref={(input) => { this.zipcodeInput = input; }}
                            /></View>
                        <View style={{flex:1}}>
                        <View style={{backgroundColor: '#e9e9e9',borderRadius:5,height:50,justifyContent:'center',paddingTop:10,paddingLeft:5}}>
                            <View>
                                
                                <RNPickerSelect
                            placeholder={{label:'Service Distance',value:null}}
                            
                            items={this.state.distance}
                            onValueChange={(value) => {
                                this.setState({
                                    filt_distance: value,
                                });
                            }}
                            useNativeAndroidPickerStyle={false}
                            style={pickerSelectStyles}
                            
                            value={this.state.filt_distance}
                            placeholderTextColor="rgb(129,129,129)"
                            placeholderfontSize= '40'
                            itemStyle={{ opacity:0.5}}
                            
                            Icon={() => {
                                return (
                                <Image
                                resizeMode='contain'
                                source={require("../../assets/Icons/dropdown_ico.png")}
                                style={{ width: 10, height: 10,marginTop:15,marginRight:15 }}
                                />
                                );
                              }}

                            />
                          </View>
                        </View>
                        </View>
                    
                    </View>
                   {this.state.countryCode!==''  && ( <CountryList
            form={this.state.editProfileform}
            showtoggleicon={false}
            setValues={(mobile_number) => this.setValues('mobile', mobile_number)}
            onSelect={this.selectCat}
            inputlable="Phone No."
            mvalue={this.state.editProfileform.mobile}
            selectedItems={this.state.editProfileform.country_code}
            picked={this.state.countryCode}
                   />) }
                    

                    {/* <TextInput placeholder='Phone' keyboardType="number-pad" returnKeyType='done' style={styles.myTextBox} /> */}
                    

                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:1}}>

                        </View>
                        <View style={{flex:1}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ChangePassword')}
                    style={{marginTop:10}}
                    >
                    <Text allowFontScaling={false}  style={{textAlign:'right',textDecorationLine:'underline',fontSize:16,fontFamily: 'Aileron-Regular'}}>Change Password</Text>
                    </TouchableOpacity>
                        </View>
                    </View>
                    
                    


                
                </View>
                {/* </ScrollView> */}
                
                
                
                </KeyboardAwareScrollView>
                
                
                    <TouchableOpacity 
                    //onPress= {this._onValidateMsg}
                    onPress={this.gotoNext}
                    >
                        <LinearGradient
                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            style={{ padding: 10, alignItems: 'center',height:52 }}
                        >
                            <Text allowFontScaling={false} font={'CenturyGothic'}  style={{ color: '#fff', fontSize: 22 }} >SAVE</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                
                
            </View>

        )
    }
}

const styles = StyleSheet.create({
    textArea: {
        
    },
    myTextBox: {
        color:'#000',
        paddingLeft:15,
        fontSize:18,
        lineHeight:23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius:5,
        height:50,
    },
    myTextBoxView: {
        color: '#000',
        padding: 10,
        paddingLeft: 15,
        paddingTop: 12,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        height: 50,
    },
    myTextBoxText: {
        fontSize: 18,
        color: '#000',
        opacity: 0.4,
        fontFamily:'CenturyGothic'


    },
    addressTextBoxView: {
        color: '#000',
        padding: 10,
        paddingLeft: 15,
        paddingTop: 12,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        minHeight: 50,
    },
    


})



const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 5,
        height:40,
        color: '#000',
        paddingRight: 30, // to ensure the text is never behind the icon
        borderRadius:5,
        width:'100%',
        alignItems:'center',
        alignSelf:'center',
        marginBottom: 10,
        justifyContent:'center'
        
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 5,
        paddingVertical: 8,
        height:40,
        color: '#000',
        paddingRight: 30, // to ensure the text is never behind the icon
        width:'100%',
        
        borderRadius:5,
        alignItems:'center',
        alignSelf:'center',
        
        marginBottom: 10,
        justifyContent:'center'
        
    },
});