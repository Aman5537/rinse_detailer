import React, { Component } from 'react'
import { Text, View,ScrollView } from 'react-native';
import  CommanStyle  from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import Helper from '../Lib/Helper';
import HTML from 'react-native-render-html';

export default class Term extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
          termsData:''
        }
    }

    componentDidMount() {

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
          this.getPagedata()
          
    
        })
    }


    getPagedata = () => {

        let tempdata = {
            slug:'terms--conditions'
        }

        Helper.makeRequest({ url: "get-static-page", method: "POST",data: tempdata }).then((data) => {
          //alert(JSON.stringify(data));
          if (data.status == 'true') {
            console.log(data);
            this.setState({termsData:data.data})
            
          }
          else {
            Helper.showToast(data.message)
          }
    
    
        })
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
               <CustomHeader navigation={this.props.navigation} bArrow  />
                <AuthTitle TitleName={this.state.termsData.title} />
                

                <ScrollView>
                    <View style={{padding:15}}>
                <HTML html={this.state.termsData.content} />
                </View>
                {/* <Card>
                    <View style={{ marginTop:0 }}>
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                            Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's 
                            stander dummy text ever since the 1500s.when an unknown printer took a galley of type and scambled it to make a
                             type specimen book.</Text>

                    </View>

                    <View style={{ marginTop: 15 }}>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                            it has survived not only five centuries,
                            but also the leap into electronic typestting, remainig essentially unchnage.
    
                        </Text>
                    </View>


                    <View style={{ marginTop: 15 }}>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                            Lorem lpsum is simply dummy text of the printing and typesetting industry.
                            Lorem lpsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it ti make a type specimen book.
                        </Text>
                    </View>



                    <View style={{ marginTop: 15 }}>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                            It has survived not only five centuries, but also the leap into electronice typesetting,
                             remaining essentially unchnaged.
                        </Text>
                    </View>


                    <View style={{ marginTop: 15 }}>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                            It had survived not only five centuries, but also the leap into electronic typestting,
                            remaining essentially unchnaged.
                        </Text>
                    </View>
                </Card>
                 */}
                </ScrollView>
            </View>
        )
    }
}
