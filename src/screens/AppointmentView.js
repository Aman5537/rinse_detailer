
import React, { Component } from 'react'
import { Text, View, Modal, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Button, Linking } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle, HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';

import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from "react-native-check-box";
import Swiper from 'react-native-swiper';
import Helper from '../Lib/Helper';
import MapView, {
    ProviderPropType,
    Marker,
    AnimatedRegion,
} from 'react-native-maps';
const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;
import CommanTollTip from "../common/CommonTollTip";
import ImageP from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import call from 'react-native-phone-call'
import Moment from "moment";
import getDirections from 'react-native-google-maps-directions'
import { EventRegister } from 'react-native-event-listeners';
import ChatController from '../Lib/ChatController';
import Communications from 'react-native-communications';

class PhotosPopUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
            img_count: 1,
            imageData: this.props.photos
        }

        console.log(this.state.imageData);

    }

    render() {
        return (
            <View style={{ width: Dimensions.get('window').width, height: 400, marginTop: 50 }}>

                <View style={{ flexDirection: 'row', marginTop: 70, padding: 20 }}>
                    <Text allowFontScaling={false} style={{ flex: 2, textAlign: 'right', color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }}>{this.state.img_count} of {this.state.imageData.length}
                    </Text>
                    <TouchableOpacity onPress={() => {
                        global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" });

                    }}
                        style={{ flex: 2 }}
                    >
                        <Text allowFontScaling={false} style={{ textAlign: 'right', color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }}>Done</Text>
                    </TouchableOpacity>
                </View>

                <Swiper showsButtons={false} autoplay={false} autoplayTimeout={2.5}
                    dotStyle={{ display: 'none', backgroundColor: '#ccc' }} loop={false}
                    activeDotStyle={{ display: 'none', }}
                    style={{ marginTop: 40 }}
                    onIndexChanged={(index) => {
                        this.setState({ img_count: index + 1 })
                    }}
                >

                    {this.state.imageData.map((items, index) => (
                        <View style={{ flex: 1, padding: 20 }}>

                            <ImageP
                                style={{ width: Dimensions.get('window').width - 20, height: 230, alignSelf: 'center' }}
                                source={{ uri: Helper.getImageUrl(items.image) }}
                                indicator={ProgressBar}
                            />
                        </View>
                    ))}






                </Swiper>

            </View>
        )
    }
}




class CallMsg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            callNumber: this.props.callNumber,
            country_code: this.props.country_code
        }




    }


    callFun = () => {
        const args = {
            number: this.state.country_code + '' + this.state.callNumber, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }

        //call(args).catch(console.error)
        var mobile = this.state.country_code + '' + this.state.callNumber;
        Communications.phonecall(mobile, true)
    }

    render() {
        return (
            <View style={{ width: 300, height: 140, margin: -20, marginLeft: -30, marginRight: -30 }}>
                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })

                }}
                    style={{ margin: 10 }}
                >
                <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>

                    {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                </TouchableOpacity>
                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>

                    {'+' + this.state.country_code + '-' + this.props.callNumber.slice(0, 3) + "-" + this.props.callNumber.slice(3, 6) + "-" + this.props.callNumber.slice(6, 15)}</Text>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 20 }}>
                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1,justifyContent:'center' }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center' }} >Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {

                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                        this.callFun(this.state.callNumber)
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1, borderLeftColor: '#e8e8e8', borderLeftWidth: 1,justifyContent:'center' }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center', color: '#3c81c4' }} >Call</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}


const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}


export default class AppointmentLists extends Component {
    static navigationOptions = { header: null }


    constructor(props) {
        super(props);
        this.state = {
            PaymentDone: false,
            //SuccessMsg: <SuccessMsg navigation={this.props.navigation}  />,
            //CancelAppMsg:<CancelAppMsg navigation={this.props.navigation} />,
            //PaymentMsg: <PaymentMsg  navigation={this.props.navigation}/>,
            CallMsg: <CallMsg navigation={this.props.navigation} />,
            PhotosPopUp: <PhotosPopUp navigation={this.props.navigation} />,
            appo_id: this.props.navigation.getParam('appo_id', 0),
            requestData: {},
            coordinate: {},
            modalInfoVisible: false,
            modalAppoCancelVisible: false,
            modalReleaseVisible: false,
            modalFinishVisible: false,
            modalSubFinishVisible: false,
            dont_show: false,
            current_Status: 0,
            callNumber: '',
            AppUserData: '',
            pageShow: false,
            PhotosAvailable:false

        }
    }


    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {

                } else {
                    this.setState({ AppUserData: responseData });

                }
            })

            this.getAppoDetails();

        })


    }






    getAppoDetails = () => {

        Helper.makeRequest({ url: "detailer-appointment-detail", data: { service_request_id: this.state.appo_id } }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {

                this.setState({ 'requestData': data.data });

                const newCoordinate = {
                    latitude: parseFloat(this.state.requestData.location.latitude),
                    longitude: parseFloat(this.state.requestData.location.longitude),
                    latitudeDelta: 0.012,
                    longitudeDelta: 0.012,
                };


                this.FunctionalityManage();
                this._onPhotosButton();
                setTimeout(() => {
                    this.setState({ 'coordinate': newCoordinate });
                }, 500);






            }
            else {
                Helper.showToast(data.message)
            }
        })

    }


    FunctionalityManage() {
        //pennding = 1
        //finished = 2
        // is_completed = 1
        //finished & release payment = 3
        //payment Done = 4
        this.state.pageShow = true;
        var type;

        if (this.state.requestData.is_finished == 0 && this.state.requestData.is_completed == 0) {
            type = 1;
            this.setState({ current_Status: type });
        }
        else {

            if (this.state.requestData.hasOwnProperty('payment_request') && this.state.requestData.payment_request != null) {
                if (this.state.requestData.payment_request.status == 'Requested') {
                    type = 3;
                    this.setState({ current_Status: type });

                    
                }
            }
            else {

                type = 2;
                this.setState({ current_Status: type });
            }


        }


    }


    _onSendPress = () => {
        if (this.state.AppUserData.id == this.state.requestData.confirm_quote.detailer_id) {
            if (this.state.AppUserData.dont_show == 1) {
                this.setState({ dont_show: true }, () => {
                    this.finishFun()
                })
    
            } else {
                this.setModalFinishVisible(true);
            }
        } else {
            
            this.setModalSubFinishVisible(true)

        }


        



        //global.state['modal'].setState({ modalVisible: true, Msg: this.state.SuccessMsg })
    }

    // _onCancelPress = () => {
    //     global.state['modal'].setState({ modalVisible: true, Msg: this.state.CancelAppMsg })
    // }

    // _onPaymentRelease = () => {

    //     global.state['modal'].setState({ modalVisible: true, Msg: this.state.PaymentMsg })
    // }


    _onCallPress = (country_code, callNumber) => {
        this.setState({ 'CallMsg': <CallMsg callNumber={callNumber} country_code={country_code} /> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.CallMsg })
        })
    }

    _onPhotosPopUp = () => {
        var VichleAllPhotos = [];
        this.state.requestData.vehicles.map((item, index) => {
            if (item.vehicle.vehicle_images.length > 0) {
                item.vehicle.vehicle_images.map((itemV, indexV) => {
                    if (itemV.image != '') {
                        VichleAllPhotos.push({ 'image': itemV.image });

                    }

                })

            }

            if (item.hasOwnProperty('vehicle_service_images') && item.vehicle_service_images != null) {
                if (item.vehicle_service_images.hasOwnProperty('image_1') && item.vehicle_service_images.image_1 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_1 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_2') && item.vehicle_service_images.image_2 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_2 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_3') && item.vehicle_service_images.image_3 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_3 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_4') && item.vehicle_service_images.image_4 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_4 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_5') && item.vehicle_service_images.image_5 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_5 });
                }

            }

        })






        if (VichleAllPhotos.length > 0) {


            this.setState({ 'PhotosPopUp': <PhotosPopUp photos={VichleAllPhotos} /> }, () => {
                global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.PhotosPopUp })
            });
        } else {
            Helper.showToast('Photos not available');
            return false;
        }


        //global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.PhotosPopUp})
    }

    _onPhotosButton = () => {
        var VichleAllPhotos = [];
        this.state.requestData.vehicles.map((item, index) => {
            if (item.vehicle.vehicle_images.length > 0) {
                item.vehicle.vehicle_images.map((itemV, indexV) => {
                    if (itemV.image != '') {
                        VichleAllPhotos.push({ 'image': itemV.image });

                    }

                })

            }

            if (item.hasOwnProperty('vehicle_service_images') && item.vehicle_service_images != null) {
                if (item.vehicle_service_images.hasOwnProperty('image_1') && item.vehicle_service_images.image_1 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_1 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_2') && item.vehicle_service_images.image_2 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_2 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_3') && item.vehicle_service_images.image_3 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_3 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_4') && item.vehicle_service_images.image_4 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_4 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_5') && item.vehicle_service_images.image_5 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_5 });
                }

            }

        })






        if (VichleAllPhotos.length > 0) {

            this.setState({PhotosAvailable:true})
            
        } else {
            this.setState({PhotosAvailable:false})
            
        }

    }




    handleGetDirections = () => {
        Linking.canOpenURL('comgooglemaps://')
            .then((canOpen) => {
                if (canOpen) {
                    console.log('open google maps');

                    var data = {
                        destination: {
                            latitude: Number(this.state.requestData.location.latitude),
                            longitude: Number(this.state.requestData.location.longitude)
                        },
                        params: [
                            {
                                key: "travelmode",
                                value: "driving"        // may be "walking", "bicycling" or "transit" as well
                            }
                        ]
                    }

                    getDirections(data)
                } else {
                    console.log('open apple maps');

                    var data = {
                        source: {
                            latitude: this.state.AppUserData.business_lat,
                            longitude: this.state.AppUserData.business_long
                        },
                        destination: {
                            latitude: Number(this.state.requestData.location.latitude),
                            longitude: Number(this.state.requestData.location.longitude)
                        },
                        params: [
                            {
                                key: "travelmode",
                                value: "driving"        // may be "walking", "bicycling" or "transit" as well
                            }
                        ]
                    }

                    getDirections(data)

                }
            });










    }


    /*Vechile Info Model Start here*/
    setModalInfoVisible(visible) {

        this.setState({ modalInfoVisible: visible });
    }


    getServiceTime(sItem, sIndex){
        if(sItem.service.average_time_from == null || sItem.service.average_time_to == null){
            return (sIndex == 0) ? sItem.service.service : '\n' + sItem.service.service 
        }
        else{
            return (sIndex == 0) ? sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)' : '\n' + sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)'
        }
        
    }

    modalInfoRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalInfoVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "#ffffff", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>

                            {this.state.requestData.vehicles && (
                                <View style={{ minHeight: 100, maxHeight: 400, width: DeviceW - 150 }}>
                                    <TouchableOpacity
                                        onPress={() => this.setModalInfoVisible(false)}
                                        style={{ margin: 10, marginRight: 0, padding: 10 }}
                                    >
                <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                                        
                                        {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                                    </TouchableOpacity>
                                    <ScrollView>
                                        {this.state.requestData.vehicles.map((vmItem, vmindex) =>
                                            <View style={{ flex: 1, paddingVertical: 5, marginBottom: 10 }}>
                                                <View >
                                                    <Text style={{ fontWeight: 'bold' }}>{vmItem.vehicle.model_detail.name}</Text>
                                                    <Text style={{ fontWeight: 'bold' }}>Color: {vmItem.vehicle.color}</Text>
                                                    <Text style={{ fontWeight: 'bold' }}>Vehicle Year:  {vmItem.vehicle.model_year}</Text>
                                                    <Text style={{ fontWeight: 'bold' }}>Plate Number:  {vmItem.vehicle.plate_number}</Text>

                                                </View>
                                                <View >
                                                    <Text>
                                                        {vmItem.services.map((sItem, sIndex) =>
                                                            this.getServiceTime(sItem, sIndex)
                                                            //(sIndex == 0) ? sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)' : '\n' + sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)'
                                                        )}
                                                    </Text>
                                                </View>
                                            </View>
                                        )}
                                        {(this.state.requestData.comment != '' && this.state.requestData.comment != null) && (
                                            <View style={{ paddingVertical: 10, marginBottom: 10 }}>
                                                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Additional Info:</Text>
                                                <Text>{this.state.requestData.comment}</Text>
                                            </View>
                                        )}

                                    </ScrollView>
                                </View>
                            )}

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*Vechile Info Model End here*/



    /*Appoinment Cancel Model start here*/
    setModalVisible(visible) {

        this.setState({ modalAppoCancelVisible: visible });
    }

    appoinmentCancelFun(id, quote_id) {
        Helper.makeRequest({ url: "appointment-cancel", data: { service_request_id: id, quote_id: quote_id } }).then((data) => {
            if (data.status == 'true') {

                //this.state.SubmitedReqData.service_quote.status

                this.getAppoDetails();
                this.setModalVisible(false)

                let backdata = {
                    refresh: true
                }
                EventRegister.emit('appoTabBarEvent', backdata);

                this.cancelEventCall()

            }
            else {
                Helper.showToast(data.message)
            }
        })
    }


    modalAppoCancelRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalAppoCancelVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                            <View style={{ alignItems: 'center', height: 240 }}>

                                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
                                    We understand situations arise but canceling an appointment negatively affects the customers experience. By
                                    canceling this appointment, this will be included in your review and could adversely affect quotes you submit in the future.
</Text>

                                <Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
                                    Would you like to cancel this appointment?
</Text>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => {
                                        this.appoinmentCancelFun(this.state.requestDeleteId, this.state.QuoteDeleteId);

                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center' }}


                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Yes</Text>

                                        </LinearGradient>


                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(false)
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center' }}
                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >No</Text>

                                        </LinearGradient>

                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*Appoinment Cancel Model End here*/



    /*Appoinment ReleasePayment  Model start here*/
    setModalReleaseVisible(visible) {

        this.setState({ modalReleaseVisible: visible });
    }

    releasePaymentFun(id, quote_id) {
        let form = {
            service_id: id,
            quote_id: quote_id

        }

        Helper.makeRequest({ url: "payment-release-request", data: form }).then((data) => {
            if (data.status == 'true') {

                //this.state.SubmitedReqData.service_quote.status

                this.getAppoDetails();
                this.setModalReleaseVisible(false)
                let backdata = {
                    refresh: true
                }
                EventRegister.emit('appoTabBarEvent', backdata);

                Helper.showToast(data.message)

                
            }
            else {
                this.setModalReleaseVisible(false)
                Helper.showToast(data.message)
            }
        })
    }


    modalReleaseRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalReleaseVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                            <View style={{ alignItems: 'center', height: 190 }}>

                                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
                                    We will attempt to contact the customer. If we are unsuccessful after 5 days we will manually release the payment to you.
</Text>

                                <Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
                                    Are you sure want to request a Payment release override?
</Text>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => {
                                        this.releasePaymentFun(this.state.releaseRequestId, this.state.releaseQuoteId);
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}


                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                                        </LinearGradient>


                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalReleaseVisible(false)
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}
                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                                        </LinearGradient>

                                    </TouchableOpacity>
                                </View>
                            </View>


                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*Appoinment ReleasePayment Model End here*/



    /*Appoinment Finish  Model start here*/
    setModalFinishVisible(visible) {

        this.setState({ modalFinishVisible: visible });
    }

    setModalSubFinishVisible(visible) {

        this.setState({ modalSubFinishVisible: visible });
    }


    modalSubFinishRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalSubFinishVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>
                {this.state.requestData.confirm_quote &&
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                            <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                                <View style={{ alignItems: 'center', height: 110 }}>



                                    <Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
                                        Are you sure you want to finish this service request for {this.state.requestData.confirm_quote.detailer.first_name} {this.state.requestData.confirm_quote.detailer.last_name}?
</Text>

                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <TouchableOpacity onPress={() => {
                                            this.setModalSubFinishVisible(false)
                                            //this.myLastFinish(this.state.finishForm);
                                            this.finishFun()
                                        }}

                                            style={{ flex: 1 }}
                                        >

                                            <LinearGradient
                                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                                style={{ padding: 8, margin: 10, alignItems: 'center' }}


                                            >

                                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Yes</Text>

                                            </LinearGradient>


                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={() => {
                                            this.setModalSubFinishVisible(false)
                                        }}

                                            style={{ flex: 1 }}
                                        >

                                            <LinearGradient
                                                colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                                style={{ padding: 8, margin: 10, alignItems: 'center' }}
                                            >

                                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >No</Text>

                                            </LinearGradient>

                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>
                        </View>
                    </View>

                }



            </Modal>



        );
    }


    finishFun() {
        let tempdate = new Date();

        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();

        let form = {
            service_request_id: this.state.appo_id,
            utc_date_time: gettime,
            quote_id: this.state.requestData.confirm_quote.id,
            dont_show: this.state.dont_show

        }


        // if (this.state.AppUserData.id == this.state.requestData.confirm_quote.detailer_id) {
        //     this.myLastFinish(form)
        //     //this.setModalFinishVisible(false)
        // } else {
        //     this.setModalFinishVisible(false)
        //     this.setState({ finishForm: form })
        //     // Helper.confirm('Are you sure you want to finish this service request for '+this.state.requestData.confirm_quote.detailer.first_name+' '+this.state.requestData.confirm_quote.detailer.last_name+'?',(status)=>{
        //     //     if(status){
        //     //         this.myLastFinish(form)
        //     //     }
        //     // })
        //     this.setModalSubFinishVisible(true)

        // }


        this.myLastFinish(form)

    }


    myLastFinish(form) {
        this.setModalFinishVisible(false)
        Helper.makeRequest({ url: "appointment-finish", data: form }).then((data) => {
            if (data.status == 'true') {

                //this.state.SubmitedReqData.service_quote.status
                setTimeout(() => {
                    this.getAppoDetails()
                }, 2000)
                
                Helper.showToast(data.message)
                Helper.setData('userdata', data.data);

                
                let backdata = {
                    refresh: true
                }
                EventRegister.emit('appoTabBarEvent', backdata);
                
                this.getBadegesCount()


            }
            else {
                
                Helper.showToast(data.message)
            }
        })
    }

    cancelEventCall() {


        let form1 = {
            user_id: this.state.AppUserData.id,
            role_id: 2

        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form1);


        let form2 = {
            user_id: this.state.requestData.user.id,
            role_id: 1

        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form2);

    }


    /* Bootom Tab Budget event */

    getBadegesCount() {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        // let form = {
        //     user_id: this.state.AppUserData.id,
        //     role_id:2,
        //     utc_date_time: gettime
        // }
        // ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);



        let form1 = {
            user_id: this.state.requestData.user.id,
            role_id: 1,
            utc_date_time: gettime


        }
        ChatController.getAppoinmentMessageCount('send_service_complete_notification', form1);

    }


    componentWillMount() {
        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;

                this.props.navigation.setParams({ badgeCount: 123 })

            } else {
                //this.setState({ isLoader: false });
            }

        })

    }



    modalFinishRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalFinishVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                            <View style={{ height: 100 }}>


                                {/* <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>It's recommended that you send picture of the finished product to the customer.</Text>

                                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', marginTop: 10, fontFamily: 'Aileron-Regular' }}>Would you like to send photos of a job well done?  </Text> */}
                                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>Are you sure you want to mark this Service Request as Finished?</Text>
                                
                                
                                
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => {
                                        this.finishFun(true);

                                    }}
                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}


                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Yes</Text>

                                        </LinearGradient>


                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalFinishVisible(false);
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}
                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >No</Text>

                                        </LinearGradient>

                                    </TouchableOpacity>
                                </View>

                                {/* <View >
                                    <TouchableOpacity
                                        style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'center', textAlign: 'center', paddingLeft: '5%' }}
                                        onPress={() => {
                                            this.setState({
                                                dont_show: !this.state.dont_show
                                            });
                                        }}
                                    >
                                        <CheckBox
                                            style={{ paddingTop: 5 }}
                                            onClick={() => {
                                                this.setState({
                                                    dont_show: !this.state.dont_show
                                                });
                                            }}
                                            isChecked={this.state.dont_show}


                                            checkedImage={
                                                <Image

                                                    source={require("../../assets/Icons/tick.png")}
                                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                />
                                            }
                                            unCheckedImage={
                                                <Image
                                                    source={require("../../assets/Icons/untick.png")}
                                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                />
                                            }
                                        />

                                        <Text allowFontScaling={false} style={{ padding: 5, textAlign: 'center', marginTop: 3 }}>Don't show this message again.</Text>
                                    </TouchableOpacity>
                                </View> */}

                            </View>



                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*Appoinment Finish Model End here*/

    componentWillUnmount() {
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()
    }

    render() {

        return (

            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                {this.modalInfoRender()}
                {this.modalAppoCancelRender()}
                {this.modalReleaseRender()}
                {this.modalFinishRender()}
                {this.modalSubFinishRender()}
                <CustomHeader navigation={this.props.navigation} userbar bArrow />
                <HeaderTitle TitleName='Appointments' />
                {(this.state.requestData.user && this.state.pageShow) && (
                    <View style={{ flex: 1 }}>
                        <ScrollView style={{ backgroundColor: "#ffffff" }}>
                            <View style={{ height: 200, width: '100%', backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center' }}>
                                {/* <Image
                            style={{ width: '100%', height: 150 }}
                            source={require('../../assets/Icons/map.png')}
                        /> */}

                                {this.state.coordinate.longitudeDelta == 0.012 && (


                                    <MapView
                                        // loadingEnabled
                                        // loadingBackgroundColor="white"
                                        initialRegion={this.state.coordinate}
                                        style={style.map}
                                        //style={{flex: 1, backgroundColor: '#ffffff'}}
                                        //  cacheEnabled={true}
                                        //liteMode // Android
                                    //  loadingBackgroundColor={'#ffffff'}
                                    // pitchEnabled={false}
                                    // rotateEnabled={false}
                                    // scrollEnabled={true}
                                    // zoomEnabled={true}
                                    // cacheLoadingBackgroundColor={'#ffffff'}
                                    // cacheLoadingIndicatorColor={'#ffffff'}
                                    >
                                        <Marker
                                            coordinate={this.state.coordinate}
                                        />
                                    </MapView>
                                )
                                }
                                {this.state.coordinate.longitudeDelta != 0.012 && (
                                    <Text>Map Loading..</Text>
                                )}



                            </View>

                            {(this.state.requestData.confirm_quote.is_canceled != 1 && this.state.requestData.confirm_quote.status != 'Canceled' && this.state.requestData.is_service_hired_cancel != 1) && (
                                <View style={{ padding: 10, marginTop: 0, backgroundColor: "#ffffff" }}>
                                    <View style={{ flexDirection: 'row', backgroundColor: "#ffffff", paddingVertical: 10 }}>
                                        <View style={{ flex: 2.1, backgroundColor: "#ffffff" }}>
                                            <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { fontWeight: 'bold', fontSize: 16, color: 'red', padding: 3, fontFamily: 'Aileron-Regular' }]}>{Helper.getDateNewFormate(this.state.requestData.service_utc_date_time)}</Text>
                                        </View>
                                        <View style={{ flex: 0.9, backgroundColor: "#ffffff" }}>
                                            <Text allowFontScaling={false} style={{ color: 'red', fontSize: 20, fontWeight: 'bold', alignSelf: 'center', fontFamily: 'Aileron-Regular' }}>${this.state.requestData.confirm_quote.quote}</Text>
                                            <View style={{ flexDirection: 'row', marginTop: 2, marginBottom: 5, alignSelf: 'center', backgroundColor: "#ffffff" }}>
                                                <Text allowFontScaling={false} style={{ fontFamily: 'Aileron-Regular' }}>{Helper.getDistanceMile(this.state.requestData.distance_diff)}mi</Text>
                                                <CommanTollTip tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />

                                            </View>
                                        </View>
                                    </View>

                                    <View style={[CommanStyle.viewcardDiv, { paddingRight: 0, marginTop: 0, paddingBottom: 0 }]}>
                                        <View style={{ flex: 4, backgroundColor: "#ffffff" }}>

                                            <View style={{ backgroundColor: "#ffffff" }}>
                                                <Text allowFontScaling={false} style={[CommanStyle.normaltitle, {}]}>{this.state.requestData.user.first_name} {this.state.requestData.user.last_name}</Text>
                                                <Text allowFontScaling={false} style={[CommanStyle.normaltitle, {}]}>Detailer-{this.state.requestData.confirm_quote.detailer.first_name} {this.state.requestData.confirm_quote.detailer.last_name}</Text>


                                                <Text allowFontScaling={false} style={CommanStyle.normaltext}>

                                                    {/* <Text allowFontScaling={false}  >{"\n"}Outside (30-45min)</Text>
                                    <Text allowFontScaling={false}  >{"\n"}Detailer - Larry Berry</Text>
                                    <Text allowFontScaling={false}  >{"\n"}5667 Fotest Ln. Tampa FL</Text> */}
                                                    <Text allowFontScaling={false}  >{"\n"}{this.state.requestData.location.location}</Text>

                                                </Text>
                                            </View>


                                        </View>
                                        <View style={{ alignItems: 'flex-end', flex: 2, backgroundColor: "#ffffff" }}>
                                            {/* <Text allowFontScaling={false}  style={{ color: 'red', fontSize: 20, fontWeight: 'bold',alignSelf:'center',fontFamily: 'Aileron-Regular'}}>$35</Text>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5,alignSelf:'center' }}>
                                    <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>2.4mi</Text>
                                    
                                </View> */}



                                            <View style={{ backgroundColor: "#ffffff" }}>
                                                <CommonButton name="Vehicle Info" background="#4684da" color="#ffffff" width={110}
                                                    onPress={() => this.setModalInfoVisible(true)}
                                                />
                                                <View style={{ marginTop: 2, justifyContent: 'flex-end', backgroundColor: "#ffffff" }} >
                                                    <CommonButton name="View Photos" background={(this.state.PhotosAvailable) ? "#4684da" : "#5c5c5c"} color="#ffffff" onPress={this._onPhotosPopUp} width={110} />
                                                </View>
                                            </View>

                                        </View>
                                    </View>

                                    <View style={{ flexDirection: 'row', marginTop: 5, backgroundColor: "#ffffff" }}>
                                        <View style={{ flex: 1, flexDirection: 'column', backgroundColor: "#ffffff" }}>
                                            <View style={CommanStyle.margin5Button}>
                                                <CommonButton
                                                    name="Message"
                                                    background="#4684da"
                                                    color="#ffffff"
                                                    //onPress={() => this.props.navigation.navigate('Chat')} 
                                                    onPress={() => this.props.navigation.navigate('Chat', { 'service_request_id': this.state.requestData.id, 'other_user_id': this.state.requestData.user.id, 'user_id': this.state.AppUserData.id, 'username': this.state.requestData.user.first_name + ' ' + this.state.requestData.user.last_name, 'country_code': this.state.requestData.user.country_code, 'mobile': this.state.requestData.user.mobile })}
                                                />
                                            </View>
                                            <View style={CommanStyle.margin5Button}>
                                                <CommonButton name="Directions" background="#4684da" color="#ffffff" onPress={() =>
                                                    this.handleGetDirections()
                                                    //this.props.navigation.navigate('Directions',{'coordinate':this.state.coordinate})
                                                } />
                                            </View>

                                        </View>
                                        <View style={{ flex: 2, backgroundColor: "#ffffff" }}>
                                                {(this.state.requestData.is_finished != 1 && this.state.requestData.is_service_hired_cancel != 1) && (
                                            <View style={[CommanStyle.margin5Button, { alignItems: 'flex-end' }]}>

                                                {(this.state.current_Status == 1 || this.state.current_Status == 3) && (
                                                    <CommonButton name="Payment Release Override" background="#5c5c5c" color="#ffffff" width={'100%'} 
                                                    onPress={() => {
                                                        (this.state.current_Status == 1) ? alert("You can't send payment release override request before Appoinment finished") :''
                                                    }}
                                                    />

                                                )}

                                                {this.state.current_Status == 2 && (
                                                    <CommonButton name="Payment Release Override" background="#51ad14" color="#ffffff"
                                                        //onPress={this._onPaymentRelease} 
                                                        onPress={() => {
                                                            this.setState({ releaseRequestId: this.state.requestData.id, releaseQuoteId: this.state.requestData.confirm_quote.id })
                                                            this.setModalReleaseVisible(true)
                                                        }}
                                                        width={'100%'}

                                                    />
                                                )}

                                            </View>
                                            )}
                                            <View style={{ flexDirection: 'row', backgroundColor: "#ffffff" }}>
                                                <View style={(this.state.current_Status == 1) ? [CommanStyle.margin5Button, {}] : [CommanStyle.margin5ButtonEnd, {}]}>

                                                    <CommonButton name="Call" background="#4684da" color="#ffffff" onPress={() => this._onCallPress(this.state.requestData.user.country_code, this.state.requestData.user.mobile)} />

                                                </View>

                                                {this.state.current_Status == 1 && (

                                                    <View style={[CommanStyle.margin5Button]}>
                                                        <CommonButton name="Cancel Appointment " background="#242424" color="#ffffff" style={[CommanStyle.margin5Button]} width={Dimensions.get('window').width / 2.13}
                                                            //onPress={this._onCancelPress}
                                                            onPress={() => {
                                                                this.setState({ requestDeleteId: this.state.requestData.id, QuoteDeleteId: this.state.requestData.confirm_quote.id })
                                                                this.setModalVisible(true)
                                                            }}

                                                        />

                                                    </View>
                                                )}

                                            </View>
                                        </View>



                                    </View>

                                </View>
                            )}
                            {(this.state.requestData.confirm_quote.is_canceled == 1 || this.state.requestData.confirm_quote.status == 'Canceled' || this.state.requestData.is_service_hired_cancel == 1) && (
                                <View style={{ padding: 15, marginTop: 10, backgroundColor: "#ffffff" }}>
                                    <View style={{ backgroundColor: "#ffffff" }}>
                                        <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { color: 'red', fontFamily: 'Aileron-Regular' }]}>Canceled Appointment</Text>
                                    </View>

                                    <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { fontWeight: 'bold', marginTop: 18, fontFamily: 'Aileron-Regular' }]}>{Helper.getDateNewFormate(this.state.requestData.service_utc_date_time)}</Text>

                                    <View style={[CommanStyle.viewcardDiv, { marginLeft: -6, backgroundColor: "#ffffff" }]}>
                                        <View style={{ flex: 3, backgroundColor: "#ffffff" }}>


                                            <Text allowFontScaling={false} style={[CommanStyle.normaltitle]}>{this.state.requestData.user.first_name} {this.state.requestData.user.last_name}</Text>
                                            <Text allowFontScaling={false} style={CommanStyle.normaltext}>
                                                {/* 2006 Nisaan CTS
                                     <Text  allowFontScaling={false}   > {"\n"}Outside (30-45min)</Text>
                                     <Text  allowFontScaling={false}   > {"\n"}Detailer - Larry Berry</Text>
                                     <Text  allowFontScaling={false}   > {"\n"}5667 Fotest Ln. Tampa FL</Text> */
                                                }

                                                <Text allowFontScaling={false}  >{"\n"}{this.state.requestData.location.location}</Text>

                                            </Text>

                                        </View>
                                        <View style={{ alignItems: 'center', flex: 1, backgroundColor: "#ffffff" }}>
                                            <Text allowFontScaling={false} style={{ color: 'red', fontSize: 20, fontWeight: 'bold' }}>$
                                            {(this.state.requestData.is_service_hired_cancel == 1 && this.state.requestData.is_canceled_by_detailer == 0) &&
                                                //'20.00'
                                                Helper.NotToZero(this.state.requestData.transaction.is_canceled_amount)
                                            }

                                            {!(this.state.requestData.is_service_hired_cancel == 1 && this.state.requestData.is_canceled_by_detailer == 0) &&
                                                this.state.requestData.confirm_quote.quote
                                            }

                                            </Text>
                                            <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                                <Text allowFontScaling={false} style={{ fontFamily: 'Aileron-Regular' }}>{Helper.getDistanceMile(this.state.requestData.distance_diff)}mi</Text>
                                                <CommanTollTip tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />

                                            </View>


                                        </View>
                                    </View>


                                </View>


                            )}


                        </ScrollView>
                        {/* this.state.requestData.confirm_quote.is_canceled != 1 || this.state.requestData.confirm_quote.status != 'Canceled' ||  */}
                        {((this.state.requestData.is_service_hired_cancel != 1) && this.state.current_Status == 1) && (
                            <TouchableOpacity onPress={this._onSendPress}>
                                <LinearGradient



                                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                    style={{ padding: 10, alignItems: 'center', height: 52 }}


                                >

                                    <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >
                                        FINISHED
  </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        )}


                        {this.state.current_Status == 3 && (
                            <TouchableOpacity>
                                <LinearGradient
                                    colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                    style={{ padding: 10, alignItems: 'center', height: 52 }}

                                >

                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 20 }} >
                                        PENDING PAYMENT OVERRIDE
   </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        )}



                    </View>
                )}
            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },
    map: {
        //...StyleSheet.absoluteFillObject,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'transparent'
    },



})


