import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView, Dimensions, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { createAccount } from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from "react-native-check-box";
import RNPickerSelect from 'react-native-picker-select';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Helper from '../Lib/Helper';
import GoogleApiAddressList from '../../components/GoogleApiAddressList'
import CountryList from '../../components/CountryList';
import CameraController from '../Lib/CameraController';
import { Avatar } from 'react-native-elements';
import Config from "../Lib/Config";

var is_upgraded = false;

const SignUpMsg = (props) => {
    return (
        <View style={{ height: 150, margin: -10, marginTop: -5 }}>

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })

            }}
                style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }}
            >
                <Image
                    resizeMode='contain'
                    source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15 }} />
            </TouchableOpacity>

            {/* <TouchableOpacity onPress={this.chooseImage}>
                <Text >Edit Image</Text>
            </TouchableOpacity> */}
            <View style={{ padding: 10 }}>
                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>In order to service areas farther than 5 miles, please upgrade your account.</Text>


                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', }}>
                    <TouchableOpacity
                        onPress={() => {
                            global.state['modal'].setState({ modalVisible: false, Msg: "" });
                            props.navigation.navigate('Upgrade', { is_skip: 1 });
                        }}
                        style={{ justifyContent: 'center' }}
                    >

                        <LinearGradient
                            colors={['#5ced31', '#4acb23', '#37a313']}
                            borderRadius={5}
                            style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20, width: 200 }}
                        >
                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22, fontFamily: 'Aileron-Regular' }} >UPGRADE</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    {/* <TouchableOpacity 
                onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                    props.navigation.navigate('ServicesYouOffer',{is_skip:1});
                }}
                style={{flex:1}}
            >

                <LinearGradient 
                    colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:200}}
                >
                        
                    <Text allowFontScaling={false}  style={{ color:'#fff',fontSize:18}} >SIGN UP</Text>
               
                </LinearGradient>

            </TouchableOpacity> */}
                </View>
            </View>
        </View>
    )
}



export default class CreateAccount extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);


        this.state = {
            SignUpMsg: <SignUpMsg navigation={this.props.navigation} />,
            distance: [

                {
                    label: '5 miles',
                    value: 5,

                },
                {
                    label: '10 miles',
                    value: 10,

                },
                {
                    label: '15 miles',
                    value: 15,

                },
                {
                    label: '20 miles',
                    value: 20,

                },
                {
                    label: '25 miles',
                    value: 25,

                },
                {
                    label: '35 miles',
                    value: 35,

                },

            ],
            addressModalVisible: false,
            hidePassword: true,
            hideConfirmPassword: true,
            checked: false,
            isChecked:false,
            registerform: {
                first_name: '',
                last_name: '',
                country_code: '1',
                mobile: '',
                email: '',
                password: '',
                confirm_password: '',
                device_type: "ANDROID",
                device_token: 'SC9830I',
                user_type: "App",
                zipcode: '',
                role_id: 2,
                business_name: '',
                business_address: '',
                business_lat: '',
                business_long: '',
                avatarSource: '',
                profile_picture: '',
                distance: '',
                validators: {
                    first_name: { required: true, "minLength": 3,"maxLength": 20 },
                    last_name: { required: true, "minLength": 3, "maxLength": 20 },
                    business_name: { required: true, "minLength": 8, "maxLength": 100 },
                    business_address: { required: true, "minLength": 6},
                    email: { required: true, "email": true },
                    country_code: { required: true },
                    mobile: { required: true, "minLengthDigit": 8, "maxLengthDigit": 15 },
                    zipcode: { required: true },
                    distance: { numeric: true },
                    password: { required: true, "minLength": 8, "maxLength": 16 },
                    confirm_password: { required: true, "minLength": 8, "maxLength": 16 },
                    

                }
            }
        }

        this.gotoNext = this.gotoNext.bind(this);
    }

    gotoNext() {
        this.onRegister();
    }


    setValues(key, value) {
        let registerform = { ...this.state.registerform }

        
        if (key == 'mobile') {
            
            if(this.state.registerform.mobile.length > value.length){
                registerform[key] = value;
                this.setState({ registerform })
            }
            else{
                value = value.replace(/-/g,"");
                if(value.length > 3){
                    value = value.slice(0,3)+"-"+value.slice(3,6)+"-"+value.slice(6,15);
                }

                registerform[key] = value;
                this.setState({ registerform })
            }
            

            
        }
        if (key == 'username') {
            if (/^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z ]*)*$/.test(value) || value == '') {
                registerform[key] = value;
                this.setState({ registerform })
            }
        } else {
            registerform[key] = value;
            this.setState({ registerform });

        }


        

    }

    setValuesBack(key, value){
        let registerform = { ...this.state.registerform }
        registerform[key] = value;
        this.setState({ registerform });
    }



    onRegister = () => {

        console.log(JSON.stringify(this.state.registerform));
        if (this.state.distance.value > 3) {
            Helper.showToast("Upgrade open");
            return;
        }

        let isValid = Helper.validate(this.state.registerform);
        if (isValid) {

            if(!this.state.registerform.distance){
                Helper.showToast("Distance is required.");
                return;
            }
            if(this.state.registerform.password != this.state.registerform.confirm_password){
                Helper.showToast("Password & Confirm Password  should be the same.");
                return;
            }
            if(this.state.isChecked == false){
                Helper.showToast("Please accept terms & conditions.");
                return;
            }

            let tempdata = new FormData();

            tempdata.append('first_name', this.state.registerform.first_name);
            tempdata.append('last_name', this.state.registerform.last_name);
            tempdata.append('country_code', this.state.registerform.country_code);
            //tempdata.append('mobile', this.state.registerform.mobile);
            tempdata.append('email', this.state.registerform.email);
            tempdata.append('password', this.state.registerform.password);
            tempdata.append('confirm_password', this.state.registerform.confirm_password);
            tempdata.append('device_type', this.state.registerform.device_type);
            tempdata.append('device_token', this.state.registerform.device_token);
            tempdata.append('user_type', this.state.registerform.user_type);
            tempdata.append('zipcode', this.state.registerform.zipcode);
            tempdata.append('role_id', this.state.registerform.role_id);
            tempdata.append('business_name', this.state.registerform.business_name);
            tempdata.append('business_address', this.state.registerform.business_address);
            tempdata.append('business_lat', this.state.registerform.business_lat);
            tempdata.append('business_long', this.state.registerform.business_long);
            tempdata.append('distance', this.state.registerform.distance);

            tempdata.append('mobile', this.state.registerform.mobile.replace(/-/g,""));

            if (this.state.profile_picture) {
                tempdata.append('profile_picture', {
                    uri: this.state.profile_picture,
                    name: 'test.jpg',
                    type: 'image/jpeg'
                });
            }

            Helper.makeRequest({ url: "signup", method: "POSTUPLOAD", data: tempdata }).then((data) => {
                if (data.status == 'true') {
                    //this.props.navigation.navigate('Verify', { fromSignup: true, signupData: this.state.registerform });
                    Helper.showToast(data.message);

                    this.setValues('first_name', '');
                    this.setValues('last_name', '');
                    this.setValues('country_code', 1);
                    this.setValues('mobile', '');
                    this.setValues('email', '');
                    this.setValues('password', '');
                    this.setValues('confirm_password', '');
                    this.setValues('zipcode', '');
                    this.setValues('business_name', '');
                    this.setValues('business_address', '');

                    this.setValues('business_lat', '');
                    this.setValues('business_long', '');
                    this.setValues('avatarSource', '');
                    this.setValues('profile_picture', '');
                    this.setValues('distance', '');
                    this.setState({isChecked:false,filt_distance:''});
                    
                
                    this.props.navigation.navigate('Login');
                }
                else {
                    Helper.showToast(data.message)
                }
            }).catch(err => {
                //Helper.showToast("Please try again");
            })
        }
    }

    setModalVisible(visible) {

        this.setState({ addressModalVisible: visible });
    }

    handleAddress = (data) => {
        let registerform = { ...this.state.registerform };
        registerform['business_address'] = data.addressname;
        registerform['business_lat'] = data.lat;
        registerform['business_long'] = data.long;
        this.setState({ registerform });
    }

    chooseImage = (data) => {

        CameraController.selecteImage((response) => {
            if (response.uri) {
                //this.state.profile_picture = response.uri;
                this.setState({ profile_picture: response.uri });
                this.setState({ avatarSource: response.uri });
            }
        });
    }


    _onSignupAlert = () => {

        if ((this.state.registerform.distance === 20) || (this.state.registerform.distance === 25) || (this.state.registerform.distance === 35)) {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.SignUpMsg })
        }
        else {
            this.props.navigation.navigate('ServicesYouOffer', { is_skip: 1 })
        }


        // {is_upgraded === true && (


        // )}

        // {is_upgraded === false && (


        // )}


        is_upgraded = !is_upgraded;
    }



    render() {
        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>
                <GoogleApiAddressList
                    modalVisible={this.state.addressModalVisible}
                    showModal={() => { this.setModalVisible(true); }}
                    hideModal={() => { this.setModalVisible(!this.state.addressModalVisible) }}
                    onSelectAddress={this.handleAddress}
                />
                <CustomHeader navigation={this.props.navigation} />

                <AuthTitle TitleName='Create Account' />
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" removeClippedSubviews={false} extraScrollHeight={70} extraHeight={70} enableOnAndroid={true}>
                    <Card>

                        <View style={{ flexDirection: 'row', }}>
                        
                            <View style={{ width: Dimensions.get('window').width, alignItems: 'center', marginBottom: 30 }} onPress={this.chooseImage}>
                            <TouchableOpacity onPress={this.chooseImage} 
                            style={{ alignItems: 'center'}}
                            >
                                {this.state.profile_picture && <View>
                                    <Avatar rounded size={100} source={{ uri: this.state.avatarSource ? this.state.avatarSource : this.state.profile_picture }} />
                                </View>}
                                {!this.state.profile_picture && <View>
                                    <Avatar rounded size={100} source={require('../../assets/Icons/profile_ico.png')} />
                                </View>}

                                <TouchableOpacity onPress={this.chooseImage}
                                    style={{ width: 50, height: 40, position: 'absolute', bottom: -10, justifyContent: 'flex-end' }}
                                >
                                    <View>
                                        <Image
                                            style={{ width: 35, height: 35, alignSelf: 'flex-end', justifyContent: 'flex-end' }}
                                            source={require('../../assets/Icons/profile_cam.png')}
                                        />
                                    </View>
                                </TouchableOpacity>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={{ flexDirection: 'row', }}>
                            <View style={{ width: Dimensions.get('window').width / 2.1, paddingRight: 10 }}>
                                <TextInput
                                    placeholder='First Name'
                                    value={this.state.registerform.first_name}
                                    onChangeText={(txt) => { this.setValues('first_name', txt) }}
                                    style={styles.myTextBox}
                                    onSubmitEditing={() => { this.lastNameInput.focus(); }}
                                    blurOnSubmit={true}
                                />
                            </View>

                            <View style={{ width: Dimensions.get('window').width / 2.1, paddingLeft: 10 }}>
                                <TextInput
                                    placeholder='Last Name'
                                    value={this.state.registerform.last_name}
                                    onChangeText={(txt) => { this.setValues('last_name', txt) }}
                                    style={styles.myTextBox} 
                                    onSubmitEditing={() => { this.businessNameInput.focus(); }}
                                    ref={(input) => { this.lastNameInput = input; }}
                                    />
                            </View>

                        </View>


                        <TextInput
                            placeholder='Business Name'
                            onChangeText={(txt) => { this.setValues('business_name', txt) }}
                            value={this.state.registerform.business_name}
                            // {width: Dimensions.get('window').width / 2}
                            style={[styles.myTextBox]} 
                            onSubmitEditing={() => { this.setModalVisible(true); }}
                            ref={(input) => { this.businessNameInput = input; }}
                        />

                        <TouchableOpacity
                            onPress={() => { this.setModalVisible(true); }}
                        >
                            <View style={styles.myTextBoxView} >

                                {this.state.registerform.business_address != '' && (
                                    <Text numberOfLines={1} style={[styles.myTextBoxText, { opacity: 1 }]}>
                                        {this.state.registerform.business_address}
                                    </Text>
                                )}
                                {this.state.registerform.business_address == '' && (
                                    <Text numberOfLines={1} style={styles.myTextBoxText}>
                                        Business Address
                            </Text>
                                )}

                            </View>
                            {/* <Input 
                        placeholder='Business Address' 
                        style={{ width: Dimensions.get('window').width / 2 }} 
                        onChangeText={(txt) => { this.setValues('business_address', txt) }}
                        
                        /> */}

                        </TouchableOpacity>

                        <Input
                            placeholder='Email Address'
                            onChangeText={(txt) => { this.setValues('email', txt) }}
                            value={this.state.registerform.email}
                        />



                        <View style={{ flexDirection: 'row', }}>


                            <View style={{ width: Dimensions.get('window').width / 1.8, paddingRight: 10 }}>

                                {/* <CountryList
                                    getFocus={(input) => { this.mobile = input; }}
                                    setFocus={() => { this.username.focus(); }}
                                    setValues={(store_number) => this.setValues('mobile', store_number)}
                                    inputlable="Phone No."
                                    selectedItems={this.state.registerform.country_code} /> */}

        <CountryList
            form={this.state.registerform}
            showtoggleicon={false}
            setValuesBack={(mobile_number) => this.setMobileValue( mobile_number)}
            setValues={(mobile_number) => {
                
                this.setValues('mobile',mobile_number)
            }}
            onSelect={this.selectCat}
            inputlable="Phone No."
            selectedItems={this.state.registerform.country_code}
            mvalue={this.state.registerform.mobile}
        />


                                {/* <TextInput 
                                placeholder='Phone' 
                                returnKeyType='done' 
                                keyboardType="number-pad" 
                                style={styles.myTextBox} 
                                onChangeText={(txt) => { this.setValues('mobile', txt) }}/> */}
                            </View>

                            <View style={{ width: Dimensions.get('window').width / 2.5, paddingLeft: 10 }}>
                                <TextInput
                                    keyboardType="number-pad"
                                    returnKeyType='done'
                                    placeholder='Zip Code'
                                    value={this.state.registerform.zipcode}
                                    style={styles.myTextBox}
                                    onChangeText={(txt) => { this.setValues('zipcode', txt) }} 
                                    //onSubmitEditing={() => { this.serviceDistanceInput.focus(); }}
                                    ref={(input) => { this.emailInput = input; }}
                                    />
                            </View>
                        </View>

                        {/* <Input placeholder='Distance you want to service' style={{ width: Dimensions.get('window').width / 2 }} /> */}

                        <View style={{ backgroundColor: '#e9e9e9', padding: 10, paddingTop: 18, marginBottom: 10, color: '#808080', borderRadius: 5, paddingLeft: 10, height: 50, justifyContent: 'center' }}>

                            <View style={{}}><RNPickerSelect
                                placeholder={{ label: 'Service Distance', value: null }}
                                items={this.state.distance}
                                onValueChange={(value) => {
                                    this.setValues('distance', value);
                                }}
                                useNativeAndroidPickerStyle={false}

                                style={pickerSelectStyles}
                                placeholderTextColor="#969696"
                                placeholderfontSize='40'
                                value={this.state.filt_distance}
                                Icon={() => {
                                    return (
                                        <Image
                                            resizeMode='contain'
                                            source={require("../../assets/Icons/dropdown_ico.png")}
                                            style={{ width: 10, height: 10, marginTop: 15 }}
                                        />
                                    );
                                }}

                            />
                            </View>
                        </View>




                        <TextInput
                            secureTextEntry={true}
                            placeholder='Password'
                            value={this.state.registerform.password}
                            onChangeText={(txt) => { this.setValues('password', txt) }}
                            onSubmitEditing={() => { this.cPasswordInput.focus(); }}
                            style={styles.myTextBox}
                            
                        />
                        <TextInput
                            secureTextEntry={true}
                            placeholder='Confirm Password'
                            value={this.state.registerform.confirm_password}
                            onChangeText={(txt) => { this.setValues('confirm_password', txt) }}
                            ref={(input) => { this.cPasswordInput = input; }}
                            style={styles.myTextBox}
                        />

                        {/* <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                
                                <CheckBox
                                    style={{ flex: 1, paddingTop: 5 }}
                                    onClick={() => {
                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        });
                                    }}
                                    isChecked={this.state.isChecked}


                                    checkedImage={
                                        <Image
                                            source={require("../../assets/Icons/tick.png")}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                        />
                                    }
                                    unCheckedImage={
                                        <Image
                                            source={require("../../assets/Icons/untick.png")}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                        />
                                    }
                                />



                                <View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text allowFontScaling={false} style={{ fontSize: 15, }}>I have read and agree to the </Text>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Term')}
                                        >
                                            <Text allowFontScaling={false} style={{ fontSize: 15, textDecorationLine: 'underline' }}>Terms of use</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text allowFontScaling={false} style={{ fontSize: 15, }}>& </Text>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Policy')}
                                        >
                                            <Text allowFontScaling={false}  ><Text allowFontScaling={false} style={{ fontSize: 15, textDecorationLine: 'underline' }}>Privacy Policy</Text><Text style={{ fontSize: 16, textDecorationLine: 'none' }}>.</Text></Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View> */}
                        
                        
                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: 10, marginBottom: 10 }}>

                                <CheckBox
                                    style={{ paddingRight:15,paddingTop: 5 }}
                                    onClick={() => {
                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        });
                                    }}
                                    isChecked={this.state.isChecked}


                                    checkedImage={
                                        <Image
                                            source={require("../../assets/Icons/tick.png")}
                                            style={{ width: 16, height: 16, resizeMode: 'contain' }}
                                        />
                                    }
                                    unCheckedImage={
                                        <Image
                                            source={require("../../assets/Icons/untick.png")}
                                            style={{ width: 16, height: 16, resizeMode: 'contain' }}
                                        />
                                    }
                                />







                            <View style={{ position: 'relative' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                    <Text style={{ fontSize: 15, color: 'rgb(31,31,31)', }}>I have read and agree to the </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Term')}
                                    >
                                        <Text style={{ fontSize: 15, color: 'rgb(31,31,31)', textDecorationLine: 'underline' }}>Terms of use</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                    <Text style={{ fontSize: 15, color: 'rgb(31,31,31)', }}>& </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Policy')}
                                    >
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontSize: 15, color: 'rgb(31,31,31)', textDecorationLine: 'underline' }}>Privacy Policy</Text>
                                            <Text>.</Text>
                                        </View>


                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>


                        <TouchableOpacity
                            //onPress={this._onSignupAlert}
                            onPress={this.gotoNext}
                        >
                            <LinearGradient
                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                borderRadius={5}
                                style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20, height: 52 }}


                            >

                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >
                                    CONTINUE
                            </Text>
                            </LinearGradient>
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: 10, }}>
                            <Text allowFontScaling={false} style={{}}>Already have an account</Text>
                            <Text allowFontScaling={false} ><Text style={{ fontFamily: 'Aileron-Regular' }}>? </Text></Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                <Text allowFontScaling={false} style={{ textDecorationLine: 'underline' }}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>

                </KeyboardAwareScrollView>

            </View>


        )
    }
}


const styles = StyleSheet.create({
    myTextBox: {
        color: '#000',
        paddingLeft: 15,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        height: 50,
        fontFamily:'CenturyGothic'

    },
    myTextBoxView: {
        color: '#000',
        padding: 10,
        paddingLeft: 15,
        paddingTop: 12,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        height: 50,
    },
    myTextBoxText: {
        fontSize: 18,
        color: '#000',
        opacity: 0.4,
        fontFamily:'CenturyGothic'


    }


})


const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 18,
        paddingVertical: 10,
        paddingHorizontal: 8,
        borderWidth: 1,
        borderColor: '#e8e8e8',
        height: 40,
        color: '#000',
        paddingRight: 30, // to ensure the text is never behind the icon
        borderRadius: 5,
        width: '100%',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 10,
        fontFamily: 'CenturyGothic'

    },
    inputAndroid: {
        fontSize: 18,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 1,
        borderColor: '#e8e8e8',
        height: 40,
        color: '#000',
        paddingRight: 30, // to ensure the text is never behind the icon
        width: '100%',

        borderRadius: 5,
        alignItems: 'center',
        alignSelf: 'center',

        marginBottom: 10,
        fontFamily: 'CenturyGothic'

    },
});
