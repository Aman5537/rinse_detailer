
import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Button } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle,HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import LinearGradient from 'react-native-linear-gradient';
import Swiper from 'react-native-swiper';

const PaymentMsg = () => {

    return (
        <View style={{ alignItems: 'center', height: 190 }}>

            <Text  allowFontScaling={false}   style={{ fontSize: 16, textAlign: 'center',fontFamily: 'Aileron-Regular' }}>
                We will attempt to contact the customer. if we are unsuccessful after 5 days we will manually release the payment to you
            </Text>

            <Text  allowFontScaling={false}   style={{ fontSize: 16, marginTop: 10, textAlign: 'center',fontFamily: 'Aileron-Regular' }}>
                Are you sure want to request a Payment release override?
            </Text>

            <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                }}

                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 8, margin: 10, alignItems: 'center' }}


                    >

                        <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 18,fontFamily: 'Aileron-Regular' }} >Yes</Text>

                    </LinearGradient>


                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                }}

                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                        style={{ padding: 8, margin: 10, alignItems: 'center' }}
                    >

                        <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 18,fontFamily: 'Aileron-Regular' }} >Cancel</Text>

                    </LinearGradient>

                </TouchableOpacity>
            </View>
        </View>
    )
}


const CallMsg = () => {
    return (
        <View style={{width:300,height:140,margin:-20,marginLeft:-30,marginRight:-30}}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
                
            }}
            style={{margin:10}}
            >
                <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} />
            </TouchableOpacity>
            <Text  allowFontScaling={false}   style={{ fontSize: 16, textAlign: "center", marginTop: 10,fontWeight:'bold',fontFamily: 'Aileron-Regular' }}>(813) 245-6552</Text>

            <View style={{flex:1,flexDirection:'row',marginTop:20}}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}
            style={{ flex:1,padding:10,borderTopColor:'#e8e8e8',borderTopWidth:1,}}
            >
                <Text  allowFontScaling={false}   font={'CenturyGothic'}  style={{ color:'#000',fontSize:18,textAlign:'center'}} >Cancel</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}
            style={{ flex:1,padding:10,borderTopColor:'#e8e8e8',borderTopWidth:1,borderLeftColor:'#e8e8e8',borderLeftWidth:1}}
            >
            <Text  allowFontScaling={false}   font={'CenturyGothic'}  style={{ color:'#000',fontSize:18,textAlign:'center',color:'#3c81c4'}} >Call</Text>
            </TouchableOpacity>
            </View>
        </View>
    )
}


class PhotosPopUp extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            isChecked:false,
            img_count:1,
        }
    }

    render() {
    return (

        <View style={{width:Dimensions.get('window').width,height:400,marginTop:50}}>

                <View style={{flexDirection:'row',marginTop:70,padding:20}}>
                        <Text  allowFontScaling={false}   style={{flex:2,textAlign:'right',color:'#fff',fontSize:18,fontFamily: 'Aileron-Regular'}}>{this.state.img_count} of 3</Text>
                        <TouchableOpacity onPress={() => {
                            global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" });
                            
                        }}
                            style={{ flex: 2 }}
                        >
                        <Text  allowFontScaling={false}   style={{textAlign:'right',color:'#fff',fontSize:18,fontFamily: 'Aileron-Regular'}}>Done</Text>
                        </TouchableOpacity>
                </View>

            <Swiper showsButtons={false} autoplay={false} autoplayTimeout={2.5}
                    dotStyle={{ display:'none',backgroundColor:'#ccc' }} loop={false}
                    activeDotStyle={{display:'none', }}
                    style={{marginTop:40}}
                    onIndexChanged={(index) => {
                        this.setState({img_count:index+1})
                    }}
                >
                    

                    <View style={{flex:1,padding:20}}>
                        
                        <Image
                            style={{ width: Dimensions.get('window').width-20, height: 230,alignSelf:'center' }}
                            source={require('../../assets/Icons/car_img.png')}
                        />
                    </View>



                    <View style={{flex:1,padding:20}}>
                        
                        <Image
                            style={{ width: Dimensions.get('window').width-20, height: 230,alignSelf:'center' }}
                            source={require('../../assets/Icons/car_img.png')}
                        />
                    </View>
                    <View style={{flex:1,padding:20}}>
                        
                        <Image
                            style={{ width: Dimensions.get('window').width-20, height: 230,alignSelf:'center' }}
                            source={require('../../assets/Icons/car_img.png')}
                        />
                    </View>
                    
                </Swiper>
            
        </View>
    
    )
    }
}


const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}


export default class AppointmentLists extends Component {
    static navigationOptions = { header: null }
    // static navigationOptions = {
    //     header: null

    //     // headerVisible: false,
    //     // headerStyle: { backgroundColor: '#22578d' },
    //     // headerLeft: <BackArrrow />,
    //     // headerRight: <Bar />
    // };


    
    state = {
        PaymentDone: false,
        PaymentMsg: <PaymentMsg />,
        CallMsg: <CallMsg navigation={this.props.navigation} />,
        PhotosPopUp: <PhotosPopUp navigation={this.props.navigation} />,

    }


    _onPaymentRelease = () => {
        this.setState({ PaymentDone: !this.state.PaymentDone });
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.PaymentMsg })
    }

    _onCallPress = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.CallMsg})
     }


    _onPhotosPopUp = () => {
        global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.PhotosPopUp})
    }

    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation} userbar bArrow/>
                <HeaderTitle TitleName='Appointments' />
                <ScrollView>
                    <View>
                        <Image
                            style={{ width: '100%', height: 200 }}
                            source={require('../../assets/Icons/map.png')}
                        />
                    </View>

                    <View style={{ padding: 10, marginTop: 10 }}>
                        <View>
                            <Text  allowFontScaling={false}   font={'CenturyGothic'} style={[CommanStyle.normaltitle, { fontWeight: 'bold', color: 'red',paddingLeft:5 }]}>Tuesday, April 30th @ 3:00 pm</Text>
                        </View>

                        <View style={[CommanStyle.viewcardDiv,{paddingRight:0}]}>
                            <View style={{ flex: 4 }}>


                                <Text allowFontScaling={false}  style={[CommanStyle.normaltitle, {fontFamily: 'Aileron-Regular' }]}>Jerome King</Text>
                                <Text allowFontScaling={false}  font={'CenturyGothic'} style={CommanStyle.normaltext}>
                                    2006 Nisaan CTS
                                    <Text allowFontScaling={false}  font={'CenturyGothic'}>{"\n"}Outside (30-45min)</Text>
                                    <Text allowFontScaling={false}  font={'CenturyGothic'}>{"\n"}Detailer - Larry Berry</Text>
                                    <Text allowFontScaling={false}  font={'CenturyGothic'}>{"\n"}5667 Fotest Ln. Tampa FL</Text>
                                </Text>
                                
                            </View>
                            <View style={{ alignItems: 'flex-end', flex: 2 }}>
                                <Text allowFontScaling={false}  style={{ color: 'red', fontSize: 20, fontWeight: 'bold',fontFamily: 'Aileron-Regular' }}>$35</Text>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                    <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>2.4mi</Text>
                                    
                                </View>

                                <CommonButton name="Vehicle Info" background="#4684da" color="#ffffff" />
                                <View style={{ marginTop: 5,justifyContent:'flex-end'}} >
                                    <CommonButton name="View Photos" background="#4684da" color="#ffffff" onPress={this._onPhotosPopUp}/>
                                </View>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={CommanStyle.margin5Button}>
                                <CommonButton name="Message" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('Chat')} />
                                </View>
                                <View style={CommanStyle.margin5Button}>
                                <CommonButton name="Directions" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('Directions')} />
                                </View>

                            </View>
                            <View style={{ flex: 2 }}>
                                <View style={[CommanStyle.margin5Button,{alignItems:'flex-end'}]}>
                                   <CommonButton name="  Payment Release Override  " color="#ffffff" background="#5c5c5c" style={CommanStyle.margin5Button} />
                                    


                                </View>
                                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>

                                    </View>
                                    <View style={[CommanStyle.margin5Button, { flex: 1 }]}>
                                        <CommonButton name="Call" background="#4684da" color="#ffffff"  style={CommanStyle.margin5Button} onPress={this._onCallPress} />
                                    </View>

                                </View>
                            </View>



                        </View>
                    </View>



                </ScrollView>
                <TouchableOpacity>
                    <LinearGradient
                        colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                        style={{ padding: 10, alignItems: 'center', height:52 }}
                        onPress={() => this.props.navigation.navigate('DrawerRoot')}
                    >

                        <Text  allowFontScaling={false}   font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                            PENDING PAYMENT
  </Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },



})


