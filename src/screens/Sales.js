import React, { Component } from 'react'
import { Text, View, Modal, Dimensions, Image,ActivityIndicator, StyleSheet, TouchableOpacity, ScrollView, Button, Picker, TextInput, FlatList,Platform } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle, HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import RNPickerSelect from 'react-native-picker-select';
import Helper from '../Lib/Helper';
import Moment from "moment";

const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;

var d = new Date();
const CurrentYear = d.getFullYear();

export default class Sales extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            apiStatus: false,
            detailers: [],
            days: [
                {
                    label: 'This Month',
                    value: 'month',

                },
                {
                    label: 'Today',
                    value: 'today',

                },
                {
                    label: 'This Week',
                    value: 'week',

                },
                {
                    label: 'This Year',
                    value: 'year',

                },
                {
                    label: 'Customize Year',
                    value: 'c_year',

                },
            ],
            Cyears:[{
                label: (CurrentYear).toString(),
                value: Number(CurrentYear),

            },{
                label: (CurrentYear-1).toString(),
                value: Number(CurrentYear)-1,

            },{
                label: (CurrentYear-2).toString(),
                value: Number(CurrentYear)-2,

            },{
                label: (CurrentYear-3).toString(),
                value: Number(CurrentYear)-3,

            },{
                label: (CurrentYear-4).toString(),
                value: Number(CurrentYear)-4,

            },{
                label: (CurrentYear-5).toString(),
                value: Number(CurrentYear)-5,

            }],
            filt_detailer: 0,
            filt_days: 'month',
            ReqData: [],
            Page: 1, IsLoading: false,
            Next_page_url: null,
            Refreshing: false,
            totalSales: 0,
            totalAmount: 0,
            notFound: '',
            AppUserData: {},
            searchText: '',
            modalInfoVisible: false,
            infoModelData: '',
            stripe_fixed:0,
            stripe_percent:0,
            searchLoader:0,
            selectedYear:CurrentYear,

        }

        
    }

    componentDidMount() {
        
        // this.focusListener = this.props.navigation.addListener('didFocus', () => {
        //   alert('yes boss');  
        //     Helper.getData('userdata').then((responseData) => {

        //         if (responseData === null || responseData === 'undefined' || responseData === '') {

        //         } else {

        //             this.setState({ AppUserData: responseData }, () => {
        //                 if (this.state.AppUserData.is_sub_detailer == 1) {
        //                     this.setState({ filt_detailer: this.state.AppUserData.id })
        //                 }
        //                 else {

        //                     this.getAllDetailers();
        //                 }

        //             });
        //             this._pullToAppRefresh()
        //         }
        //     })
            

        // })

        this.focusListenerWill = this.props.navigation.addListener('willFocus', () => {
            //alert('will boss');  
              Helper.getData('userdata').then((responseData) => {
  
                  if (responseData === null || responseData === 'undefined' || responseData === '') {
  
                  } else {
  
                      this.setState({ AppUserData: responseData }, () => {
                          if (this.state.AppUserData.is_sub_detailer == 1) {
                              this.setState({ filt_detailer: this.state.AppUserData.id })
                          }
                          else {
  
                              this.getAllDetailers();
                          }
  
                      });
                      this._pullToAppRefresh()
                  }
              })
              
  
          })
        
        
    }


    




    getAllDetailers = () => {
        
        let form = {
            page: -1,
            is_password_reset: 1
        }
        Helper.makeRequest({ url: "get-sub-detailers", loader: false, method: "POST", data: form }).then((responsedata) => {
            if (responsedata.status == 'true') {
                var data = responsedata.data.data;
                var newArray = [];
                
                newArray.push({ label: 'All Detailers', value: null })
                newArray.push({ label: this.state.AppUserData.first_name + ' ' + this.state.AppUserData.last_name, value: this.state.AppUserData.id })
                data.map((items, index) =>
                    newArray.push({ label: items.first_name + ' ' + items.last_name, value: items.id.toString() })
                )
                this.setState({ detailers: newArray, apiStatus: true });

            }
            else {
            }
        })
    }


    setSearch = (txt) => {
        this.setState({ searchText: txt }, () => {
            //if(txt.length == 0 || txt.length > 2){

            this.setState({ Refreshing: true, Page: 1,ReqData:'' }, () => {
                this.getReqData(false,false);
            });
            //}

        })
    }


    _pullToAppRefresh = () => {
        this.setState({ Refreshing: true, Page: 1, }, () => {
            this.getReqData(false,true);
        });
    }


    getReqData = (loadstatus,serachLoader) => {
        this.setState({ IsLoading: loadstatus, Refreshing: false });

        let tempdate = new Date();
        //alert(tempdate);
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")
        var TimeZone = tempdate.getTimezoneOffset();

        //alert(gettime);
        let form = {
            page: this.state.Page,
            date: (this.state.filt_days == 'c_year') ? 'year' : this.state.filt_days,
            detailer_id: this.state.filt_detailer,
            utc_date_time: gettime,
            keyword: this.state.searchText,
            year:this.state.selectedYear,
            time:TimeZone

        }

        var MyLoader = false;
        if (this.state.Page == 1) {
            MyLoader = true;
        }

        if(serachLoader == false){
            this.setState({searchLoader:this.state.searchLoader+1},()=>{
                console.log(this.state.searchLoader,'djvgdkjvdkv dvdvkgv');
                
            })
            MyLoader = false;
            
        }

        

        


        Helper.makeRequest({ url: "detailer-get-complete-service-request", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {

                //console.log(this.state.Page,'this.state.Page')
                if (this.state.Page == 1) {
                    
                    this.setState({
                        ReqData: data.data.data,
                        IsLoading: false,
                        Refreshing: false,
                        Next_page_url: data.data.next_page_url,
                        totalSales: data.data.detailer_amount_count,
                        totalAmount: data.data.detailer_amount,
                        notFound: 'Record Not Found!',
                        stripe_fixed:data.data.stripe_fixed,
                        stripe_percent:data.data.stripe_percent
                        

                    });
                }
                else {

                    this.setState({
                        ReqData: [...this.state.ReqData, ...data.data.data],
                        IsLoading: false,
                        Refreshing: false,
                        Next_page_url: data.data.next_page_url,
                        stripe_fixed:data.data.stripe_fixed,
                        stripe_percent:data.data.stripe_percent

                    });
                }

                this.state.Page++;
                if(this.state.searchLoader > 0){
                    this.setState({searchLoader:this.state.searchLoader-1},() =>{
                        console.log(this.state.searchLoader,'minascksajckavc')
                    })
                    
                    
                }

                if(serachLoader == false && this.state.searchLoader > 0){
                    this.setState({Page:1});
                }
                


            }
            else {
                Helper.showToast(data.message)
            }


        })
            .catch(err => {
                this.setState({ IsLoading: false, Refreshing: false });

            })
    }

    setFilterValues = (type, value) => {
        if (type == 1) {
            this.setState({ filt_detailer: value }, () => {
                this._pullToAppRefresh()
            })
        }
        if (type == 2) {
            this.setState({ filt_days: value }, () => {
                
                
                if(value != 'c_year'){
                    this.setState({ selectedYear: CurrentYear })
                    this._pullToAppRefresh()
                }
                
            })
        }
        if (type == 3) {
            this.setState({ selectedYear: value }, () => {
                this._pullToAppRefresh()
            })
        }

    }

    
    

    onScrollApp = (e) => {
        if (!this.state.IsLoading && this.state.Next_page_url) {
            this.getReqData(true,true);
        }
        // if (this.state.ReqData.length > 8) {
        //     this.getReqData(true,true);
        // }


    }


    componentWillUnmount() {
        this.focusListenerWill.remove()
       // this.focusListener.remove()
    }


    _renderSalesItem = ({ item, index }) => (

        <View style={[CommanStyle.AppmycardDiv, { borderTopColor: '#f6f6f6', borderTopWidth: 1, paddingHorizontal: 15 }]}>
            <View style={{ flex: 1, paddingTop: 13 }}>

                <Text allowFontScaling={false} style={[CommanStyle.normaltext, { textAlign: 'center', fontFamily: 'Aileron-Regular',fontSize:12 }]}>
                    {Helper.getDateNewFormate(item.service_utc_date_time).split('@ ')[0] + '\n' + Helper.getDateNewFormate(item.service_utc_date_time).split('@ ')[1]}
                </Text>


            </View>
            <View style={{ flex: 3, padding: 10 }}>

                <View>
                    <Text allowFontScaling={false} style={[CommanStyle.salesTitle]}>
                    Trans ID: {item.transaction.transaction_number}
                    </Text>
                    <Text allowFontScaling={false} style={[CommanStyle.normaltext]}>
                        <Text allowFontScaling={false} font={'CenturyGothic'} >
                            {item.user.first_name} {item.user.last_name}
                        </Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} >{"\n"}
                        Detailer-{item.confirm_quote.detailer.first_name} {item.confirm_quote.detailer.last_name}
                        </Text>
                        

                        <Text allowFontScaling={false} font={'CenturyGothic'} >{"\n"}
                            {item.vehicles.map((vitem, vindex) => (
                                (vindex == 0) ? vitem.vehicle.model_detail.name : ', ' + vitem.vehicle.model_detail.name


                            ))}
                        </Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} >{"\n"}{item.location.location}</Text>
                        {/* <Text allowFontScaling={false} font={'CenturyGothic'} >{"\n"}Clean</Text>
                        <Text allowFontScaling={false} font={'CenturyGothic'} >{"\n"}2126 N Holten St Tampa FL 336114</Text> */}

                    </Text>
                    <TouchableOpacity
                        onPress={() => this.setState({ infoModelData: item.vehicles }, () => {
                            this.setModalInfoVisible(true)
                        })}
                    >
                        <Text style={{ color: '#5b87d4', fontWeight: 'bold' }}>{'\n'}Show Details</Text>
                    </TouchableOpacity>

                </View>
            </View>
            <View style={{ flex: 2, paddingTop: 13 }}>
                {item.transaction.is_canceled_amount > 0 && 
                <View style={{flex:1}}>
                
                
                <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { textAlign: 'right' }]}>${item.transaction.is_canceled_amount} <Text style={{ color: '#c8c8c9' }}>{'\n'}(Cancellation Fee)</Text></Text>
                
                </View>
                }
                {item.transaction.is_canceled_amount == 0 && 
                <View style={{flex:1}}>
                
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { textAlign: 'right',right:5,fontWeight:'bold' }]}>${item.transaction.quote_amount}</Text>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { textAlign: 'right' }]}>-${item.transaction.commission_amount} <Text style={{ color: '#c8c8c9' }}>{'\n'}(Service Fee)</Text></Text>
                    {/* <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { textAlign: 'right' }]}>${Helper.calculateStripeAccount(item.transaction.tip,this.state.stripe_fixed,this.state.stripe_percent)} <Text style={{ color: '#c8c8c9' }}>{'\n'}(tip)</Text></Text> */}
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { textAlign: 'right' }]}>${Number(Number(item.transaction.tip)-Number(item.transaction.tip_stripe_processing_fee)).toFixed(2)} <Text style={{ color: '#c8c8c9' }}>{'\n'}(tip)</Text></Text>

                </View>
                }
                <View style={{flex:1,justifyContent:'flex-end'}}>
                    {/* <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { color: 'red', textAlign: 'right', fontWeight: 'bold', fontFamily: 'Aileron-Regular',marginBottom:8,fontSize: 20 }]}> ${item.transaction.detailer_amount}</Text>
                 */}

                 <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { color: 'red', textAlign: 'right', fontWeight: 'bold', fontFamily: 'Aileron-Regular',marginBottom:8,fontSize: 20 }]}> 
                 ${(item.transaction.is_canceled_amount > 0) ? item.transaction.is_canceled_amount : item.transaction.detailer_amount}</Text>
                
                </View>
                
            </View>
        </View>






    )


    renderSeparator = () => (
        <View
          style={{
            borderBottomColor:'#ededed',
            borderBottomWidth:1,
            
          }}
        />
    );

    renderFooter = () => (
        <View
          style={{
            borderBottomColor:'#ededed',
            borderBottomWidth:1,
            
          }}
        />
    );

    /*Vechile Info Model Start here*/
    setModalInfoVisible(visible) {

        this.setState({ modalInfoVisible: visible });
    }


    getServiceTime(sItem, sIndex){
        if(sItem.service.average_time_from == '' || sItem.service.average_time_to == ''){
            return (sIndex == 0) ? sItem.service.service : '\n' + sItem.service.service 
        }
        else{
            return (sIndex == 0) ? sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)' : '\n' + sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)'
        }
        
    }

    modalInfoRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalInfoVisible}
                onRequestClose={() => {
                    this.setModalInfoVisible(false)
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "#ffffff", borderRadius: 10, padding: 20,margin: 20, alignItems: "center" }}>

                            {this.state.infoModelData && (
                                <View style={{ minHeight: 100, maxHeight: 400, width: DeviceW - 150 }}>
                                    <TouchableOpacity
                                        onPress={() => this.setModalInfoVisible(false)}
                                        style={{ padding: 10 }}
                                    >
                                    <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                                        {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                                    </TouchableOpacity>
                                    <ScrollView>
                                        {this.state.infoModelData.map((vmItem, vmindex) =>
                                            <View style={{ flex: 1, paddingVertical: 5, marginBottom: 10 }}>
                                                <View >
                                                    <Text style={{ fontWeight: 'bold' }}>{vmItem.vehicle.model_detail.name}</Text>
                                                    <Text style={{ fontWeight: 'bold' }}>Color: {vmItem.vehicle.color}</Text>
                                                    <Text style={{ fontWeight: 'bold' }}>Vehicle Year:  {vmItem.vehicle.model_year}</Text>
                                                    <Text style={{ fontWeight: 'bold' }}>Plate Number:  {vmItem.vehicle.plate_number}</Text>

                                                </View>
                                                <View >
                                                    <Text>
                                                        {vmItem.services.map((sItem, sIndex) =>
                                                            this.getServiceTime(sItem, sIndex)
                                                            //(sIndex == 0) ? sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)' : '\n' + sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)'
                                                        )}
                                                    </Text>
                                                </View>
                                            </View>
                                        )}
                                        {/* {this.state.requestData.comment != '' && (
                                <View style={{paddingVertical:10,marginBottom:10}}>
                                    <Text style={{fontSize:15,fontWeight:'bold'}}>Additional Info:</Text>
                                    <Text>{this.state.requestData.comment}</Text>
                                </View>
                            )} */}

                                    </ScrollView>
                                </View>
                            )}

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*Vechile Info Model End here*/


    render() {
        return (

            <View style={{ flex: 1, }}>
                {this.modalInfoRender()}
                <CustomHeader navigation={this.props.navigation} userbar bArrow />
                <HeaderTitle TitleName='Sales' />
                <View style={{ alignSelf: 'center', paddingBottom: 10 }}>
                    <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltitle, { textAlign: 'center' }]}>{this.state.totalSales == 1 ? this.state.totalSales+' Sale' : this.state.totalSales+' Sales'} = ${this.state.totalAmount.toFixed(2)}</Text>

                    <View style={{ marginTop: 15, alignItems: 'center' }}>
                        <View style={{ width: '60%' }}>
                            {this.state.AppUserData.is_sub_detailer != 1 && this.state.apiStatus && (
                                <RNPickerSelect
                                    // placeholder={{
                                    //     label: 'All Detailers',
                                    //     value: null,

                                    // }}
                                    placeholder={{}}
                                    items={this.state.detailers}
                                    //useNativeAndroidPickerStyle={false}
                                    onValueChange={(value) => {
                                        this.setFilterValues(1, value);
                                    }}
                                    style={pickerSelectStyles}
                                    value={this.state.filt_detailer}
                                    
                                    Icon={
                                        Platform.OS == 'ios' ?
                                        () => {
                                        return (
                                            <Image
                                                resizeMode='contain'
                                                source={require("../../assets/Icons/dropdown_ico.png")}
                                                style={{ width: 10, height: 10, marginTop: 12 }}
                                            />
                                        );
                                    }
                                    :
                                    ''
                                }
                                    
                                    

                                />
                            )}
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.state.days}
                                //useNativeAndroidPickerStyle={false}
                                onValueChange={(value) => {
                                    this.setFilterValues(2, value);
                                }}
                                style={pickerSelectStyles}
                                value={this.state.filt_days}
                                Icon={
                                    Platform.OS == 'ios' ?
                                    () => {
                                    return (
                                        <Image
                                            resizeMode='contain'
                                            source={require("../../assets/Icons/dropdown_ico.png")}
                                            style={{ width: 10, height: 10, marginTop: 12 }}
                                        />
                                    );
                                }
                                :
                                ''
                            }
                            />

                            {this.state.filt_days == 'c_year' && 
                            <RNPickerSelect
                            placeholder={{}}
                            items={this.state.Cyears}
                            onValueChange={(value) => {
                                this.setFilterValues(3, value);
                            }}
                            style={pickerSelectStyles}
                            value={this.state.selectedYear}
                            
                            />
                            }
                        </View>

                        {this.state.searchLoader > 0 && 
                        <ActivityIndicator size="small" color="#cccccc" />
                        }
                        
                        <TextInput placeholder="Search"
                            onChangeText={(txt) => { this.setSearch(txt) }}
                            //onSubmitEditing={() => { this._pullToAppRefresh(); }}
                            style={{
                                borderWidth: 1,
                                borderColor: '#e8e8e8',
                                fontSize: 20,
                                padding: 10,
                                borderRadius: 5,
                                width: Dimensions.get('window').width - 25,
                                marginTop: 10

                            }}
                        />

                    </View>
                </View>


                <View style={{ flex: 1 }}>
                    {this.state.ReqData.length > 0 ?
                        <FlatList
                            data={this.state.ReqData}
                            extraData={this.state}
                            keyExtractor={(item, index) => index}
                            renderItem={this._renderSalesItem}
                            refreshing={this.state.Refreshing}
                            onRefresh={this._pullToAppRefresh}
                            onEndReached={this.onScrollApp}
                            onEndReachedThreshold={0.5}
                            ItemSeparatorComponent={this.renderSeparator}
                            ListFooterComponent={this.renderFooter}

                        />
                        : !this.state.IsLoading ?
                            <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>{this.state.notFound}</Text>
                            </View> : null
                    }


                </View>

            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },



})



const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,

        color: '#808080',
        paddingRight: 30, // to ensure the text is never behind the icon
        borderRadius: 5,
        width: 220,
        alignItems: 'center',
        alignSelf: 'center',
        height: 40,

        fontFamily: 'CenturyGothic'

    },
    inputAndroid: {
        fontSize: 16,
        height: 40,
        color: '#808080',
        paddingRight: 30, // to ensure the text is never behind the icon
        width: 220,

        borderRadius: 5,
        alignItems: 'center',
        alignSelf: 'center',


        fontFamily: 'CenturyGothic',


    },
});
