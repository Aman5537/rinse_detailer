import React, { Component } from 'react'
import { Text, Modal,Dimensions, View, ScrollView, TouchableOpacity, Image, Platform, FlatList } from 'react-native';
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from "react-native-check-box";
import Helper from '../Lib/Helper';
import HTML from 'react-native-render-html';
import DraggableFlatList from 'react-native-draggable-dynamic-flatlist'


const DeviceH =Dimensions.get('window').height;

export default class ServicesYouOffer extends Component {
    static navigationOptions = { header: null }




    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            is_skip: this.props.navigation.getParam('is_skip', 0),
            is_back: this.props.navigation.getParam('is_back', 0), 
            checkArray: this.props.navigation.getParam('checkArray', []),
            serviceData: [],
            userdata:''
            
            //     {id: 1, title:'Inside (30-45mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:false},
            //     {id: 2, title:'Outside (30-45mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:true},
            //     {id: 3, title:'Inside/Outside (30-45mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:false},
            //     {id: 4, title:'Inside (30-45mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:false},
            //     {id: 5, title:'Inside (30-45mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:false},
            //     {id: 6, title:'Inside (30-45mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:false},
            //     {id: 7, title:'Inside (30-40mins)',text: "Lorem lpsum is simply dummy text of the printing and typesetting industry Lorem lpsum has been the industry's ",isChecked:false},
            // 

        }

        //alert(this.state.checkArray);
        this.getAllService();


    }


    componentDidMount() {
        
        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {
                this.setState({ userdata: responseData });

            }
        })

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            this.getAllService();
        })
    }



    componentWillUnmount() {
        this.focusListener.remove()
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    getAllService = () => {

        //console.log(JSON.stringify(this.state.registerform));


        Helper.makeRequest({ url: "get-detailer-all-services" }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                this.setState({ 'serviceData': data.data });
                //alert(JSON.stringify(this.state.serviceData));
            }
            else {
                Helper.showToast(data.message)
            }
        })

    }

    setCheckedValue = (index) => {
        //console.log(index,'index')
        const newArray = [...this.state.serviceData];
            newArray[index].is_service = !newArray[index].is_service;
            this.setState({ serviceData: newArray });


            let arr = [];
            for (var key in this.state.serviceData) {
                if (this.state.serviceData[key].is_service === true) {
                    arr.push(this.state.serviceData[key].id);
                }
            }
            
            this.setState({ checkArray: arr });
        
        

        //let data = arr.toString();

    }

    onDeletePress(v) {
        
                Helper.makeRequest({ url: "delete-service", data: { service_id: v } }).then((data) => {
                    if (data.status == 'true') {


                        this.getAllService();
                        this.setModalVisible(false)
                    }
                    else {
                        Helper.showToast(data.message)
                        this.setModalVisible(false)
                    }
                })
            
    }

    autoSelectedValue(item){
        if(item.is_service){
            return item.is_service;
        }
        else{
            if(this.state.checkArray.indexOf(item.id) >= 0){
                // if(this.state.is_back === 1){
                //     this.setCheckedValue(this.state.checkArray.indexOf(item.id))
                // }
                
                return true;
            }
            else{
                return item.is_service;
            }
        }
        
    }


    chooseService = (v) => {

        //console.log(JSON.stringify(this.state.checkArray),'JSON.stringify(this.state.checkArray)');
        //return false;
        Helper.makeRequest({ url: "detailer-assign-services", data: { service_id: JSON.stringify(this.state.checkArray) } }).then((data) => {
            if (data.status == 'true') {

                if(this.state.is_back === 1){
                    this.props.navigation.goBack(null)
                }else{
                    if (v == 1) {
                        if(this.state.userdata.is_sub_detailer == 1){
                            this.props.navigation.navigate('Portfolio', { is_skip: 1 })
                        }
                        else{
                            this.props.navigation.navigate('PayoutAccount', { is_skip: 1 });
                        }
                        
                    }
                    else {
                        //Helper.showToast(data.message);
                        this.props.navigation.navigate('EditService');
                    }
                }

                


            }
            else {
                Helper.showToast(data.message)
            }
        })


    }



    modalRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                            <View style={{ alignItems: 'center', minHeight: 80, maxHeight: 200 }}>

                                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>Do you want to delete this service?</Text>


                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => {
                                        this.onDeletePress(this.state.DeleteId)
                                        

                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                            style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}


                                        >

                                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                                        </LinearGradient>


                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(false)
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                            style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}
                                        >

                                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                                        </LinearGradient>

                                    </TouchableOpacity>
                                </View>
                            </View>


                        </View>
                    </View>
                </View>



            </Modal>



        );
    }


    renderItem = ({ item, index, move, moveEnd, isActive }) => {
        return (
        //   <TouchableOpacity
        //     // style={{ 
        //     //   backgroundColor: isActive ? '#ededed' : item.backgroundColor,
              
        //     // }}
        //     onLongPress={move}
        //     onPressOut={moveEnd}
        //   >
          
            <View style={{ flex: 1, marginTop: 10, flexDirection: 'row' }}>
                                    <View style={{ flex: 1, paddingLeft: 5, marginTop: -5 }} >
                                        <CheckBox
                                            style={{ flex: 1, padding: 10 }}
                                            onClick={() => {
                                                
                                                this.setCheckedValue(
                                                    index
                                                )
                                            }}
                                            isChecked={this.autoSelectedValue(item)}

                                        />

                                        
                                    </View>
                                    <TouchableOpacity
                                    onPress={() => {
                                                
                                        this.setCheckedValue(
                                            index
                                        )
                                    }}
                                    style={{ flex: 5 }}
                                    >
                                    <View >
                                    
                                        <Text allowFontScaling={false} //font={'Aileron-Regular-bold'} 
                                        style={[CommanStyle.normaltitle, { 
                                            //fontFamily: 'Aileron-Regular', 
                                            fontWeight: 'bold' }]}>{item.service} ({item.average_time_from}-{item.average_time_to}mins)</Text>
                                        
                                        <HTML html={item.description} />
                                    
                                    </View>
                                    </TouchableOpacity>
                                    <View style={{ flex: 2, alignItems: 'flex-end', paddingRight: 10 }}>
                                        {item.user_id != 0 && (
                                            <TouchableOpacity
                                                onPress={() => {
                                                    //this.onDeletePress(item.id)
                                                    this.setState({DeleteId:item.id})
                                                    this.setModalVisible(true);
                                                }}
                                                style={{ height: 35, marginTop: 5, width: 35, flex: 1, alignItems: 'flex-end' }}>
                                                <Image
                                                    resizeMode='contain'
                                                    style={{ height: 25, width: 25 }}
                                                    source={require('../../assets/Icons/cross.png')}
                                                />
                                            </TouchableOpacity>
                                        )}

                                    </View>
                                </View>
          
        //   </TouchableOpacity>
        )
      }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                {this.modalRender()}

                {this.state.is_skip === 1 && (
                    <CustomHeader navigation={this.props.navigation} />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow userbar />
                )}


                <AuthTitle TitleName='Services You Offer' />
                <View style={{marginTop:-5,alignSelf:'center',marginBottom:10,width:'70%',alignContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:12}}>(you will only get requests from customers </Text>
                <Text style={{fontSize:12}}>that select the services you offer)</Text>

                </View>
                

                {/* <ScrollView> */}
                    <View style={{flex:1}}>

                        {/* <FlatList
                            data={this.state.serviceData}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item, index }) =>
                                




                            }
                        //keyExtractor={item => item.email}
                        /> */}



                        <FlatList
                        data={this.state.serviceData}
                        renderItem={this.renderItem}
                        //keyExtractor={(item, index) => `draggable-item-${item.key}`}
                        //scrollPercent={5}
                        //onMoveEnd={({ data }) => this.setState({serviceData: data })}
                        />
                    </View>
                {/* </ScrollView> */}

                <View style={{ alignItems: 'center', paddingTop: 10 }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddService',{'checkArray':this.state.checkArray})}>
                        <Text allowFontScaling={false} 
                        font={'CenturyGothic-bold'} style={{ fontSize: 23, textDecorationLine: 'underline', fontWeight: 'bold' }}>ADD SERVICES</Text>

                    </TouchableOpacity>
                    {this.state.is_skip === 1 && (
                        <TouchableOpacity onPress={() => {
                            (this.state.userdata.is_sub_detailer == 1) ? this.props.navigation.navigate('Portfolio', { is_skip: 1 }) : this.props.navigation.navigate('PayoutAccount', { is_skip: 1 })
                           }}>
                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ fontSize: 23, textDecorationLine: 'underline', marginTop: 7 }}  >SKIP</Text>
                        </TouchableOpacity>
                    )}

                </View>

                {this.state.is_skip === 1 && (
                    <TouchableOpacity onPress={() => this.chooseService(1)}>
                        <LinearGradient



                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            borderRadius={0}
                            style={{ padding: 10, alignItems: 'center', marginTop: 20 }}

                            onPress={() => this.props.navigation.navigate('DrawerRoot')}
                        >


                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >CONTINUE</Text>


                        </LinearGradient>
                    </TouchableOpacity>

                )}

                {this.state.is_skip !== 1 && (
                    <TouchableOpacity onPress={() => this.chooseService(2)}>
                        <LinearGradient



                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            borderRadius={0}
                            style={{ padding: 10, alignItems: 'center', marginTop: 20 }}


                        >


                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >SAVE</Text>


                        </LinearGradient>
                    </TouchableOpacity>

                )}

            </View>
        )
    }
}
