
import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Button,TextInput } from 'react-native'
import { Input,CustomHeader, CardSection, AuthHeader, AuthTitle,HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';


const ExeedMsg = () => {
    return (
        <View style={{marginLeft:-10,marginRight:-10,marginTop:-5 }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}
            
            >
            <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15,justifyContent:'flex-end',alignSelf:'flex-end',marginBottom:10 }} />
            </TouchableOpacity>

            <View style={{padding:10}} >
            <Text  allowFontScaling={false}   style={{ fontSize: 16,textAlign: 'center',paddingLeft:10,paddingRight:10,fontFamily: 'Aileron-Regular'}}>You have exceeded amount of money available in your account.</Text>

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}
            style={{justifyContent: 'center', alignItems: 'center'}}
            >

                <LinearGradient

                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                    borderRadius={5}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:100 }}


                >

                    <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 22,fontFamily: 'Aileron-Regular' }} >Ok</Text>
                </LinearGradient>


                
            </TouchableOpacity>
            </View>
        </View>
    )
}


const TransferMsg = (props) => {
    return (
        <View style={{ }}>
            
            <Text  allowFontScaling={false}   style={{ fontSize: 16,textAlign: 'center',paddingLeft:10,paddingRight:10}}>Your transfer has been initiated. The transfer may take 4-6 business days to process.</Text>

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
                props.navigation.navigate('TransactionHistory');
            }}
            style={{justifyContent: 'center', alignItems: 'center'}}
            >

            <LinearGradient
            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
            borderRadius={5}
            style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:100 }}
            >
                <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 22,fontFamily: 'Aileron-Regular' }} >Ok</Text>
            </LinearGradient>


                
            </TouchableOpacity>
        </View>
    )
}


class PayoutMsg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            TransferMsg: <TransferMsg navigation={this.props.navigation} />,
        }
    }

    
    _onPayoutSuccess = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.TransferMsg })
    }

    render() {
        return (
        <View style={{ height:150,marginLeft:-10,marginRight:-10,marginTop:-5  }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}
            
            >
            <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15,justifyContent:'flex-end',alignSelf:'flex-end',marginBottom:10 }} />
            </TouchableOpacity>

            <View style={{padding:10}} >
            <Text  allowFontScaling={false}   style={{ fontSize: 16,textAlign: 'center',paddingLeft:10,paddingRight:10,fontFamily: 'Aileron-Regular'}}>Are you sure you want to send $2,875.88 to Brad?</Text>

            <View style={{flex:1,flexDirection:'row'}}>
            <TouchableOpacity onPress={this._onPayoutSuccess}
            style={{justifyContent: 'center', alignItems: 'center',flex:1}}
            >

                <LinearGradient

                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                    borderRadius={5}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:100 }}


                >

                    <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 18 }} >Yes</Text>
                </LinearGradient>


                
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}
            style={{justifyContent: 'center', alignItems: 'center',flex:1}}
            >

                <LinearGradient

                    colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                    borderRadius={5}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:100 }}


                >

                    <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>
                </LinearGradient>


                
            </TouchableOpacity>

            </View>
            </View>
        </View>
    )
    }
}




export default class Payout extends Component {
    static navigationOptions = { header: null }
    
    constructor(props) {
        super(props);
        this.state = {
            ExeedMsg: <ExeedMsg />,
            PayoutMsg:<PayoutMsg navigation={this.props.navigation} />
        }
    }


    _onSendPress = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.ExeedMsg })
    }

    _onPayoutPress = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.PayoutMsg })
    }


    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation} userbar bArrow/>
                <HeaderTitle TitleName='Payout' message navigation={this.props.navigation}/>
                <Text  allowFontScaling={false} style={[CommanStyle.normaltext,{textAlign:'center'}]}>Total Sales Amount (sales for all accounts)</Text>
                <Text  allowFontScaling={false}   style={{textAlign:'center',fontFamily: 'Aileron-Regular'}}>$7,467.86</Text>
                <ScrollView>
                    

                <View style={{padding:10,flex:1,flexDirection:'row',borderTopColor:'#ccc',borderTopWidth:1,marginTop:10}}>
                    <View style={{flex:2.5}}>
                        <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle,{fontWeight:'bold',fontFamily: 'Aileron-Regular'}]}>Larry Berry</Text>
                        <Text  allowFontScaling={false}   font={'CenturyGothic'} style={[CommanStyle.normaltext]}>Sales $2,675.88</Text>
                        <Text  allowFontScaling={false}   font={'CenturyGothic'} style={[CommanStyle.normaltext]}>Tips $200</Text>
                        <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle,{fontWeight:'bold',fontFamily: 'Aileron-Regular'}]}>Total $2,875.88</Text>
                    </View>
                    <View style={{flex:1.5,flexDirection:'row'}}>
                        <Text  allowFontScaling={false}   style={{fontSize:16,paddingTop:8,fontFamily: 'Aileron-Regular'}}>$ </Text>
                        <TextInput style={{borderColor:'#ccc',borderWidth:1,width:75,height:38,padding:5}} />
                    </View>
                    <View style={{flex:1.5}}>
                    <CommonButton name="Send" background="#4684da" color="#ffffff" onPress={this._onPayoutPress}
  
 
/>
                    </View>
                </View>



                <View style={{padding:10,flex:1,flexDirection:'row',borderTopColor:'#ccc',borderTopWidth:1,marginTop:10}}>
                    <View style={{flex:2.5}}>
                        <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle,{fontWeight:'bold',fontFamily: 'Aileron-Regular'}]}>Larry Berry</Text>
                        <Text  allowFontScaling={false}   font={'CenturyGothic'} style={[CommanStyle.normaltext]}>($434 Requested)</Text>
                        <Text  allowFontScaling={false}   font={'CenturyGothic'} style={[CommanStyle.normaltext]}>Sales $2,675.88</Text>
                        <Text  allowFontScaling={false}   font={'CenturyGothic'} style={[CommanStyle.normaltext]}>Tips $200</Text>
                        <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle,{fontWeight:'bold',fontFamily: 'Aileron-Regular'}]}>Total $2,875.88</Text>
                    </View>
                    <View style={{flex:1.5,flexDirection:'row'}}>
                        <Text  allowFontScaling={false}   style={{fontSize:16,paddingTop:8,fontFamily: 'Aileron-Regular'}}>$ </Text>
                        <TextInput style={{borderColor:'#ccc',borderWidth:1,width:75,height:38,padding:5}} />
                    </View>
                    <View style={{flex:1.5}}>
                    <CommonButton name="Send" background="#4684da" color="#ffffff"  onPress={this._onSendPress}

/>
                    </View>
                </View>

                </ScrollView>
                
            </View>
        )
    }
}


const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },



})


