import React, { Component } from 'react'
import { Text, View, CheckBox, ScrollView, TouchableOpacity, Image } from 'react-native';
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import { Avatar } from 'react-native-elements';
import Helper from '../Lib/Helper';

export default class UpgradeDone extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            business_address: 0,
            profile_photo: 0,
            payout_account: 0,
            offer_service: 0,
            AppUserData: {}
        }


    }

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {

                } else {
                    this.setState({ AppUserData: responseData });

                }
            })
            this.getRequestDetails();
        })
    }

    componentWillUnmount() {
        this.focusListener.remove()
    }


    getRequestDetails = () => {
        var req_id = 1;
        Helper.makeRequest({ url: "service-request-detail", data: { service_request_id: req_id } }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {

                this.setState({ 'business_address': data.data.business_address });
                this.setState({ 'profile_photo': data.data.profile_photo });
                this.setState({ 'payout_account': data.data.payout_account });
                this.setState({ 'offer_service': data.data.offer_service });

                if ((this.state.business_address == 1) && (this.state.profile_photo == 1) && (this.state.payout_account == 1) && (this.state.offer_service == 1)) {
                    this.props.navigation.goBack(null)
                }
            }
            else {
                Helper.showToast(data.message)
            }
        })

    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <CustomHeader navigation={this.props.navigation} />
                <AuthTitle TitleName="" />
                <View style={{flex:1, backgroundColor: '#ffffff' }}>
                <View style={{ backgroundColor: '#ffffff' }}>
                    <Text allowFontScaling={false} style={{ fontSize: 25, fontWeight: 'bold', color: '#4e4e4e', textAlign: 'center', marginTop: 20, fontFamily: 'Aileron-Regular' }}>
                        You're almost done! Just Complete your profile and you're all set!
                    </Text>

                    <View style={{ position: 'absolute', backgroundColor: '#ffffff',right:0 }}>
                        <TouchableOpacity
                            //onPress={() => this.props.navigation.navigate('SendQuote')}
                            onPress={() => this.props.navigation.goBack(null)}
                            style={{ width: 30, height: 30, backgroundColor: '#ffffff',padding:5 }}
                        >

                            <Image
                                style={{ width: 24, height: 24, alignSelf: 'center', right: 10, top:-20 }}
                                source={require('../../assets/Icons/colse.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ backgroundColor: '#ffffff',alignContent:'center',alignItems:'center' }}>
                    {this.state.profile_photo != 1 &&
                        <Image
                            style={{ width: 100, height: 100, alignSelf: 'center' }}
                            source={require('../../assets/Icons/userimg.png')}
                        />
                    }
                    {this.state.profile_photo == 1 && (
                        <Avatar rounded size={100} source={{ uri: Helper.getImageUrl(this.state.AppUserData.profile_picture) }} />
                    )}
                    <Text allowFontScaling={false} font={'CenturyGothic-bold'} style={{ color: '#7e7e7e', fontSize: 20, textAlign: 'center', fontWeight: 'bold' }}>
                        {this.state.AppUserData &&
                            this.state.AppUserData.first_name + ' ' + this.state.AppUserData.last_name
                        }
                    </Text>

                </View>

                <View style={{ backgroundColor: '#ffffff' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Profile', { is_back: 1 })}
                        style={{ backgroundColor: '#ffffff' }}
                    >
                        <View style={{ padding: 10, marginTop: 10, flexDirection: 'row', borderColor: '#bebebe', borderWidth: 1, backgroundColor: '#ffffff' }}>

                            <View style={{ backgroundColor: '#ffffff' }}>
                                {this.state.profile_photo == 1 && (
                                    <Image
                                        style={{ width: 28, height: 28, marginTop: 5,left:-2 }}
                                        source={require('../../assets/Icons/full-checked.png')}
                                    />
                                )}

                                {this.state.profile_photo != 1 &&
                                    <Image
                                        style={{ width: 22, height: 22, marginTop: 5 }}
                                        source={require('../../assets/Icons/cross-full.png')}
                                    />
                                }

                            </View>
                            <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 7, backgroundColor: '#ffffff' }}>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                                    Add a profile photo
                        </Text>

                            </View>
                            <View style={{ right: 5, position: 'absolute', top: 20, backgroundColor: '#ffffff' }}>

                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/Icons/arrowright.png')}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Profile', { is_back: 1 })}
                        style={{ backgroundColor: '#ffffff' }}
                    >
                        <View style={{ padding: 10, flexDirection: 'row', borderBottomColor: '#bebebe', borderBottomWidth: 1, backgroundColor: '#ffffff' }}>
                            <View style={{ backgroundColor: '#ffffff' }}>

                                {this.state.business_address == 1 && (
                                    <Image
                                        style={{ width: 30, height: 30, marginTop: 5,left:-2 }}
                                        source={require('../../assets/Icons/full-checked.png')}
                                    />
                                )}

                                {this.state.business_address != 1 &&
                                    <Image
                                        style={{ width: 22, height: 22, marginTop: 5 }}
                                        source={require('../../assets/Icons/cross-full.png')}
                                    />
                                }


                            </View>
                            <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 7, backgroundColor: '#ffffff' }}>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                                    Enter your business information
                        </Text>

                            </View>
                            <View style={{ right: 5, position: 'absolute', top: 20, backgroundColor: '#ffffff' }}>
                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/Icons/arrowright.png')}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>



                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('ServicesYouOffer', { is_back: 1 })}
                        style={{ backgroundColor: '#ffffff' }}
                    >
                        <View style={{ padding: 10, flexDirection: 'row', borderBottomColor: '#bebebe', borderBottomWidth: 1, backgroundColor: '#ffffff' }}>
                            <View style={{ backgroundColor: '#ffffff' }}>

                                {this.state.offer_service == 1 && (
                                    <Image
                                        style={{ width: 30, height: 30, marginTop: 5,left:-2 }}
                                        source={require('../../assets/Icons/full-checked.png')}
                                    />
                                )}

                                {this.state.offer_service != 1 &&
                                    <Image
                                        style={{ width: 22, height: 22, marginTop: 5 }}
                                        source={require('../../assets/Icons/cross-full.png')}
                                    />
                                }

                            </View>
                            <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 7, backgroundColor: '#ffffff' }}>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                                    Enter the services you offer
                        </Text>

                            </View>
                            <View style={{ right: 5, position: 'absolute', top: 20, backgroundColor: '#ffffff' }}>
                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/Icons/arrowright.png')}
                                />
                            </View>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('PayoutAccount', { is_back: 1 })}
                        style={{ backgroundColor: '#ffffff' }}
                    >
                        <View style={{ padding: 10, flexDirection: 'row', borderBottomColor: '#bebebe', borderBottomWidth: 1, backgroundColor: '#ffffff' }}>
                            <View style={{ backgroundColor: '#ffffff' }}>
                                {this.state.payout_account == 1 && (
                                    <Image
                                        style={{ width: 30, height: 30, marginTop: 5,resizeMode:'contain',left:-2 }}
                                        source={require('../../assets/Icons/full-checked.png')}
                                    />
                                )}

                                {this.state.payout_account != 1 &&
                                    <Image
                                        style={{ width: 22, height: 22, marginTop: 5,resizeMode:'contain' }}
                                        source={require('../../assets/Icons/cross-full.png')}
                                    />
                                }

                            </View>
                            <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 7, backgroundColor: '#ffffff' }}>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >
                                    Enter your payout account information
                        </Text>

                            </View>
                            <View style={{ right: 5, position: 'absolute', top: 20, backgroundColor: '#ffffff' }}>
                                <Image
                                    style={{ width: 20, height: 20 }}
                                    source={require('../../assets/Icons/arrowright.png')}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                </View>

            </View>
        )
    }
}
