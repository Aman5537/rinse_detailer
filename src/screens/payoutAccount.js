import React, { Component } from 'react'
import { Text, WebView, Modal, View, TextInput, CheckBox, ScrollView, StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native'
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import CommonButton from '../common/CommonButton';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Helper from '../Lib/Helper';
import Config from '../Lib/Config';

export default class PayoutAccount extends Component {
    static navigationOptions = { header: null }


    constructor(props) {
        super(props);
        this.state = {
            modalVisible1: false,
            is_skip: this.props.navigation.getParam('is_skip', 0),
            is_back: this.props.navigation.getParam('is_back', 0),
            accountShow1: false,
            accountShow2: false,
            radio_props: [
                { label: 'Checking Account ', value: 0 },
                { label: 'Savings Account', value: 1 }
            ],
            submitform: {
                bank_name: '',
                account_holder_name: '',
                account_number: '',
                confirm_account_number: '',
                routing_number: '',
                account_type: 1,
                validators: {
                    bank_name: { required: true, "minLength": 3, "maxLength": 20 },
                    account_holder_name: { required: true },
                    account_number: { required: true },
                    confirm_account_number: { required: true },
                    routing_number: { required: true, "minLengthDigit": 5, "maxLengthDigit": 12 },

                }
            },
            state_updated: false,
            userdata: '',
            stripeBtnShow: false,
            stripe_user_id: '',
            stripe_token: ''


        }



    }

    componentDidMount() {

        this.focusListener = this.props.navigation.addListener('didFocus', () => {

            this.getAllData(1);

        })

        this.getAllData();
    }

    componentWillUnmount() {
        this.focusListener.remove()
    }

    getAllData = (type) => {
        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {
                this.setState({ userdata: responseData });


            }
        })


        if (this.state.is_skip != 1) {

            this.getAccountDetails(type);

        }
        else {

            this.getAccountDetails(type);
            this.setState({ state_updated: true });
        }
    }

    ShowHideFun(v) {
        if (v === 1) {
            this.setState({ accountShow1: !this.state.accountShow1 });
        }
        if (v === 2) {
            this.setState({ accountShow2: !this.state.accountShow2 });
        }
    }

    setValues(key, value) {
        let submitform = { ...this.state.submitform }
        submitform[key] = value;
        this.setState({ submitform });



    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible1: visible });
        if (visible) {
            this.setState({ state_updated: false });
        }
        else {
            this.getAccountDetails();
            this.setState({ state_updated: true });
        }
    }

    getAccountDetails(type=0) {

        if(type == 1){
            var loader = false
        }
        else{
            var loader = true
        }
        Helper.makeRequest({ url: "get-payout-account" ,loader:loader}).then((data) => {
            if (data.status == 'true') {


                this.setValues('bank_name', data.data.bank_name);
                this.setValues('account_holder_name', data.data.account_holder_name);
                this.setValues('account_number', data.data.account_number);
                this.setValues('confirm_account_number', data.data.account_number);
                this.setValues('routing_number', data.data.routing_number);

                if (data.data.account_type == 0 || data.data.account_type == undefined) {
                    this.setValues('account_type', 1);
                }
                else {
                    this.setValues('account_type', data.data.account_type);
                }



                Helper.setData('userdata', data.data.user_data);


                this.setState({ state_updated: true, stripe_user_id: data.data.stripe_user_id, stripe_token: data.data.stripe_token });


            }
            else {
                Helper.showToast(data.message);
                this.setState({ state_updated: true });
            }

            Helper.hideLoader()
        })


    }

    onSubmit = (v) => {

        if (v == 1) {
            this.props.navigation.navigate('Portfolio', { is_skip: 1 });
        } else {
            this.props.navigation.navigate('SetHome');
            //Helper.showToast(data.message);
        }


        // let isValid = Helper.validate(this.state.submitform);
        // if (isValid) {

        //     if (this.state.submitform.account_number != this.state.submitform.confirm_account_number) {
        //         Helper.showToast('Your Account Number & Confirm Account Number Must Be The Same.');
        //         return false;
        //     }
        //     //alert(JSON.stringify(this.state.submitform));
        //     Helper.makeRequest({ url: "add-payout-account", data: this.state.submitform }).then((data) => {
        //         if (data.status == 'true') {
        //             if (this.state.is_back === 1) {
        //                 this.props.navigation.goBack(null)
        //             } else {
        //                 if (v == 1) {
        //                     this.props.navigation.navigate('Portfolio', { is_skip: 1 });
        //                 } else {
        //                     this.props.navigation.navigate('SetHome');
        //                     Helper.showToast(data.message);
        //                 }
        //             }

        //         }
        //         else {
        //             Helper.showToast(data.message)
        //         }
        //     })
        // }
    }

    _onNavigationStateChange(webViewState) {
        //alert(JSON.stringify(webViewState.url));

        if (webViewState.url.indexOf(Config.imageUrl + 'configure-stripe-account-redirect?code=') == 0) {
            // Do what you want
            this.setState({ stripeBtnShow: true })
        }
    }





    modalRender() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible1}
                onRequestClose={() => {
                    this.setState({ modalVisible1: !this.state.modalVisible1 });
                }}>
                <View style={styles.modalContainer}>
                    <WebView
                        source={{ uri: 'https://connect.stripe.com/express/oauth/authorize?redirect_uri=http://demo2server.com/rinse/configure-stripe-account-redirect&client_id=ca_EVkKG0JdM7YzRo7fGPszg85QPbH3drUu&scope=read_write&stripe_user[email]=' + this.state.userdata.email + '&always_prompt=true&stripe_user[phone_number]=' + this.state.userdata.mobile + '&stripe_user[country]=US&stripe_user[first_name]=' + this.state.userdata.first_name + '&stripe_user[last_name]=' + this.state.userdata.last_name }}
                        style={{ marginTop: 0 }}
                        startInLoadingState
                        onNavigationStateChange={this._onNavigationStateChange.bind(this)}
                    />
                    <TouchableOpacity
                    style={{position:'absolute',right:10,top:20}}
                        onPress={() => { this.setModalVisible(false); }}>
                        <Text style={{ fontSize: 30, justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                    </TouchableOpacity>
                </View>
                {this.state.stripeBtnShow && (
                    <TouchableOpacity
                        onPress={() => { this.setModalVisible(false); }}>
                        <LinearGradient
                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            style={{ padding: 10, alignItems: 'center', height: 50 }} >
                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }}>Close</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                )}
            </Modal>
        );
    }



    render() {

        const { accountShow1, accountShow2 } = this.state

        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>
                {this.modalRender()}

                {this.state.is_skip === 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow userbar />
                )}

                <HeaderTitle TitleName='Payout Account' />
                <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { textAlign: 'center' }]} >The account you would like your money {'\n'} transferred to</Text>

                <KeyboardAwareScrollView extraScrollHeight={70} extraHeight={70} enableOnAndroid={true}>
                    {/* <View style={{ margin: 10 }}>
                        <TextInput
                            placeholder='Name of Bank'
                            value={this.state.submitform.bank_name}
                            onChangeText={(txt) => { this.setValues('bank_name', txt) }}
                            onSubmitEditing={() => { this.holderNameInput.focus(); }}
                            style={styles.myTextBox}
                            returnKeyType='next'
                        />
                        <TextInput
                            placeholder='Name On The Account'
                            style={styles.myTextBox}
                            value={this.state.submitform.account_holder_name}
                            onChangeText={(txt) => { this.setValues('account_holder_name', txt) }}
                            onSubmitEditing={() => { this.accountNumberInput.focus(); }}
                            ref={(input) => { this.holderNameInput = input; }}
                            returnKeyType='next'
                        />
                        <View>
                            <TextInput
                                placeholder='Account Number'
                                secureTextEntry={!accountShow1}
                                keyboardType="number-pad"
                                returnKeyType='done'
                                style={styles.myTextBox}
                                value={this.state.submitform.account_number}
                                onChangeText={(txt) => { this.setValues('account_number', txt) }}
                                //onSubmitEditing={() => { this.cAccountNumberInput.focus(); }}
                                ref={(input) => { this.accountNumberInput = input; }}
                            />
                            <View style={{ position: "absolute", alignSelf: 'flex-end', margin: 15 }}>
                                <TouchableOpacity onPress={() => this.ShowHideFun(1)} >
                                    <Text style={{ fontSize: 16, color: 'blue', marginRight: 10 }}>
                                        {accountShow1 === false && (
                                            'show'
                                        )}
                                        {accountShow1 === true && (
                                            'hide'
                                        )}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            <TextInput
                                placeholder='Confirm Account Number'
                                secureTextEntry={!accountShow2}
                                keyboardType="number-pad"
                                returnKeyType='done'
                                style={styles.myTextBox}
                                value={this.state.submitform.confirm_account_number}
                                onChangeText={(txt) => { this.setValues('confirm_account_number', txt) }}
                                //onSubmitEditing={() => { this.routingNumberInput.focus(); }}
                                ref={(input) => { this.cAccountNumberInput = input; }}
                            />
                            <View style={{ position: "absolute", alignSelf: 'flex-end', margin: 15 }}>
                                <TouchableOpacity onPress={() => this.ShowHideFun(2)} >
                                    <Text style={{ fontSize: 16, color: 'blue', marginRight: 10 }}>
                                        {accountShow2 === false && (
                                            'show'
                                        )}
                                        {accountShow2 === true && (
                                            'hide'
                                        )}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <TextInput
                            placeholder='Routing Number'
                            keyboardType="number-pad"
                            returnKeyType='done'
                            style={styles.myTextBox}
                            value={this.state.submitform.routing_number}
                            onChangeText={(txt) => { this.setValues('routing_number', txt) }}
                            ref={(input) => { this.routingNumberInput = input; }}
                        />
                        <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltitle, { marginTop: 16 }]} >Account Type:</Text>
                        <View>
                            {this.state.state_updated === true && (
                                <RadioForm style={{ flexDirection: 'row', marginTop: 10 }}
                                    radio_props={this.state.radio_props}
                                    initial={this.state.submitform.account_type - 1}
                                    formHorizontal={false}
                                    labelHorizontal={true}
                                    backgroundColor={'#cacaca'}
                                    buttonColor={'#cacaca'}
                                    selectedButtonColor={'#757171'}
                                    buttonSize={10}
                                    buttonOuterSize={30}
                                    buttonOuterColor={'#757171'}
                                    animation={true}
                                    onPress={(value) => {
                                        this.setValues('account_type', value + 1);
                                        //this.setState({value:value})
                                    }}
                                    labelStyle={{ padding: 10, paddingTop: 5, paddingLeft: 2, color: '#808080', fontSize: 15 }}
                                    font={'CenturyGothic'}
                                />
                            )}
                        </View>


                        <View style={{ marginTop: 5 }}>

                        {((this.state.stripe_user_id != '') && (this.state.stripe_token  != '')) &&  (
                            <View>
                             <Text allowFontScaling={false} font={'CenturyGothic'}  style={{fontSize:16,color:'green',fontWeight:'bold'}}>Connected With Stripe </Text>
                             </View>
                        )}

                        {((this.state.stripe_user_id == '') || (this.state.stripe_token == '')) && (
                            <TouchableOpacity
                                onPress={() => this.setModalVisible(true)}
                            >
                                <Text allowFontScaling={false} font={'CenturyGothic'}  style={{fontSize:16,fontWeight:'bold',color:'red'}}>Connect With Stripe </Text>
                            </TouchableOpacity>
                        )}
                            

                        </View>
                    </View>
                 */}


                    <View style={{ marginTop: 20 }}>

                        {((this.state.stripe_user_id != '') && (this.state.stripe_token != '')) && (
                            <View style={{width:300,alignSelf:'center'}}>
                                <CommonButton name="Update Stripe Account" background={"#5aa641"} color="#ffffff" onPress={() => this.setModalVisible(true)} />


                            </View>
                        )}

                        {((this.state.stripe_user_id == '') || (this.state.stripe_token == '')) && (
                            <View style={{width:300,alignSelf:'center'}}>
                            <CommonButton name="Connect With Stripe" background={"#5c5c5c"} color="#ffffff" onPress={() => this.setModalVisible(true)}/>
                            </View>
                                
                        )}


                    </View>

                </KeyboardAwareScrollView>


                {this.state.is_skip === 1 && (
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Portfolio', { is_skip: 1 })}>
                            <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltitle, { textAlign: 'center', fontSize: 20, textDecorationLine: 'underline' }]} >SKIP</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            //onPress={() => this.props.navigation.navigate('Portfolio',{is_skip:1})}
                            onPress={() => this.onSubmit(1)}
                        >
                            <LinearGradient



                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                style={{ padding: 10, alignItems: 'center', }}


                            >

                                <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                                    CONTINUE
  </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                )}

                {this.state.is_skip !== 1 && (
                    <TouchableOpacity
                        onPress={() => this.onSubmit(2)}
                    >
                        <LinearGradient



                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            style={{ padding: 10, alignItems: 'center' }}


                        >

                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                                DONE
</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                )}



            </View>

        )
    }
}



const styles = StyleSheet.create({
    myTextBox: {
        color: '#000',
        paddingLeft: 15,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        height: 50,
    },
    modalContainer: {
        flex: 1, backgroundColor: '#fff', opacity: 0.90, justifyContent: 'center'
    }


})
