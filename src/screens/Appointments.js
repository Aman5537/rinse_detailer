

import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity,ScrollView,Button,FlatList,ActivityIndicator } from 'react-native'
import { CustomHeader ,CardSection, AuthHeader,AuthTitle,HeaderTitle} from '../common';
import  CommanStyle  from '../config/Styles';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import Helper from '../Lib/Helper';
import Moment from "moment";
import ChatController from '../Lib/ChatController';
import { EventRegister } from 'react-native-event-listeners';

const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}> 
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}


export default class Appointments extends Component {
    static navigationOptions = { header: null }
    
    constructor(props) {
        super(props);
        this.state = {
            calSelectedDate:'',
            AppReqData: [],
            appPage: 1, appIsLoading: false,
            appNext_page_url: null,
            appRefreshing: false,
            isCalenderShow:false,
            
            
            
        }

        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {
                this.setState({ AppUserData: responseData });
                ChatController.checkConnection("appointment", (cb) => {
                    if (cb) {
                        Appointments.getBadegesCount(this.state.AppUserData.id)
                    }

                })

                

                let tempdate = new Date();

                this.getAlldates()
                let gettime = Moment(tempdate).format("YYYY-MM-DD")
                this.setState({ calSelectedDate: gettime },() =>
                    this._pullToAppRefresh()
                    
                )
            }
        })

    }



    
    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    //this.props.navigation.navigate('LoginNew');
                } else {
                    this.setState({ AppUserData: responseData });
                    Appointments.getBadegesCount(this.state.AppUserData.id)


                }
            })

            this.getAlldates()
            // let tempdate = new Date();

            // this.getAlldates()
            // let gettime = Moment(tempdate).format("YYYY-MM-DD")
            // this.setState({ calSelectedDate: gettime },() =>
            //     this._pullToAppRefresh()
                
            // )
        })
    }



    /* Bootom Tab Budget event */

    static getBadegesCount(user_id) {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            user_id: user_id,
            role_id:2,
            utc_date_time: gettime
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);
        
    }


    componentWillMount() {
        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;
                
                this.props.navigation.setParams({ badgeCount: 123 })
                
            } else {
                this.setState({ isLoader: false });
            }

        })


        this.listenerRefreshApp = EventRegister.addEventListener('appoTabBarEvent', (data) => {
            if(data.refresh == true){
                this._pullToAppRefresh()
            }
            
        })

        

    }

    componentWillUnmount() {
        
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()
        EventRegister.removeEventListener(this.listenerRefreshApp);
       
    }

    /* Bootom Tab Budget event */

    
    dayCall = () => {
        
        //this.props.navigation.navigate('AppointmentLists');
    }


    _pullToAppRefresh = () => {
        this.setState({ appRefreshing: true,appPage:1 },() => {
            this.getAppReqData(false);
        });
        
    }


    getAlldates = () => {

        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")
        
        var TimeZone = tempdate.getTimezoneOffset();

        let form = {
            utc_date_time: gettime,
            time:TimeZone

        }

        Helper.makeRequest({ url: "appointment-date-list", method: "POST",data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                this.setState({eventDates:data.data,isCalenderShow:true},() => 
                this.customMarkedDateswithState()
                );
                //this.setState({eventDates:['2019-08-18'],isCalenderShow:true});
            }
        })
    }

    
    getAppReqData = (loadstatus) => {
        this.setState({ appIsLoading: loadstatus, appRefreshing: false });
        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();

        let form = {
            page: this.state.appPage,
            date: this.state.calSelectedDate,
            utc_date_time: gettime,
            time:TimeZone

        }

        

        var MyLoader = false;
        if(this.state.appPage == 1){
            MyLoader = true;
        }


        Helper.makeRequest({ url: "detailer-get-service-request-by-date", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                
                if(this.state.appPage == 1){
                    this.setState({
                        AppReqData:data.data.data,
                        appIsLoading: false,
                        appRefreshing: false,
                        appNext_page_url: data.data.next_page_url
                    });
                }
                else{
                    this.setState({
                        AppReqData: [...this.state.AppReqData, ...data.data.data],
                        appIsLoading: false,
                        appRefreshing: false,
                        appNext_page_url: data.data.next_page_url
                    });
                }
                
                this.state.appPage++;


            }
            else {
                Helper.showToast(data.message)
            }

        
        })
        .catch(err => {
            this.setState({ appIsLoading: false, appRefreshing: false });
           
        })
    }

    onScrollApp = (e) => {
        if (!this.state.appIsLoading && this.state.appNext_page_url) {
        this.getAppReqData(true);
        }
    }


    _renderAppoinmentItem = ({ item,index }) => (
        <View style={CommanStyle.AppmyBorderDiv}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView',{ 'appo_id': item.id})} >
                <View style={CommanStyle.AppmycardDiv}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                    
                    {item.is_finished != 1 && item.payment_request.status == 'Requested' && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'red'}]}>
                        Payment Override
                        </Text>
                    )}

                    {((item.confirm_quote.is_canceled == 1 || item.confirm_quote.status == 'Canceled' || item.is_service_hired_cancel == 1) && item.is_finished != 1 && item.is_canceled_by_detailer == 1) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'black'}]}>
                        Detailer Canceled
                        </Text>
                    )}
                    {((item.confirm_quote.is_canceled == 1 || item.confirm_quote.status == 'Canceled' || item.is_service_hired_cancel == 1) && item.is_finished != 1 && item.is_canceled_by_detailer == 0) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'black'}]}>
                        Customer Canceled
                        </Text>
                    )}

                    {(item.confirm_quote.is_canceled != 1 && item.confirm_quote.status != 'Canceled' && item.is_finished != 1 && item.confirm_quote.status != 'Completed' && item.is_service_hired_cancel == 0) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular'}]}>
                        {Helper.getDateNewFormate(item.service_utc_date_time).split('@ ')[1]}
                        </Text>
                    )}    
                    
                    {(item.confirm_quote.status == 'Completed' && item.is_finished != 1 && item.payment_request == '' && item.is_service_hired_cancel == 0) && (
                        <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { textAlign:'center',color: 'red', fontFamily: 'Aileron-Regular' }]}>Payment Pending</Text>

                    )}

                    {(item.is_finished == 1) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'green'}]}>
                        Completed
                        </Text>
                    )}


                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                        {item.user.first_name} {item.user.last_name}
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >
                          {item.vehicles.map((vitem,vindex)=> (
                    (vindex == 0) ? vitem.vehicle.model_detail.name : ', '+vitem.vehicle.model_detail.name
                    
                    
            ))}
                          </Text>
                          {/* <Text allowFontScaling={false}   >{'\n'}Detailer-Larry</Text> */}
                          <Text allowFontScaling={false}   >{'\n'}{item.location.location}</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                   
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
        </TouchableOpacity>    
        </View>
    )


    renderSeparator = () => (
        <View
          style={{
            borderBottomColor:'#ccc',
            borderBottomWidth:1,
            
          }}
        />
    );

    renderFooter = () => (
        <View
          style={{
            borderBottomColor:'#ccc',
            borderBottomWidth:1,
            
          }}
        />
    );


    SelectDate =(day) => {
        this.setState({ calSelectedDate: day.dateString },() => {
            
            this.customMarkedDateswithState()
            this._pullToAppRefresh()
        })
    }


    customMarkedDateswithState() {
        this.state.customMarkedDates = {};
        for (let leave of this.state.eventDates) {
          this.state.customMarkedDates[leave] = { marked: true, dotColor: 'blue', activeOpacity: 1 };
        }

        var setDate = this.state['calSelectedDate'];
        this.state.customMarkedDates[setDate] = {selected: true,
            customStyles: {
              container: {
                backgroundColor: 'red',
              },
              text: {
                color: '#FFFFFF',
                textAlign: 'center',
                textAlignVertical: 'center',
                paddingTop: 0
              }
            },
          }
        
                  
             
    }
    


    render() {
        
        //alert(JSON.stringify(this.state.customMarkedDates));

        return (

            <View style={{ flex: 1,backgroundColor:'#ffffff' }}>

                <CustomHeader navigation={this.props.navigation}  userbar />
                <HeaderTitle TitleName='Appointments' date navigation={this.props.navigation}/>
                <View style={{borderTopColor:'#d9d9d9',borderTopWidth:1}}></View>
                <ScrollView style={{
                             backgroundColor:'#ffffff'
                           }}>
                  <View style={{backgroundColor: "#ffffff"}}>
                     {this.state.isCalenderShow && (
                         <Calendar
                         style={{
                             backgroundColor:'#ffffff'
                           }}
                           onDayPress={(day) => { this.SelectDate(day)}}
                           //onDateSelect={(date) => this.SelectDate(date)}
                           //onDateLongPress={(date) => this.SelectDate(date)}
                      //showEventIndicators={true}
                     //eventDates={['2019-08-01', '2019-08-07', '2019-08-19']}
                    //  markingType={'custom'}
                           
                    hideExtraDays={true}
                     markedDates={
                        //  [this.state['calSelectedDate']]: {selected: true,
                        //    customStyles: {
                        //      container: {
                        //        backgroundColor: 'red',
                        //      },
                        //      text: {
                        //        color: '#FFFFFF',
                        //        textAlign: 'center',
                        //        textAlignVertical: 'center',
                        //        paddingTop: 0
                        //      }
                        //    },
                        //  },
                        
                         this.state.customMarkedDates
                        
                       }
                    
            
                   
                     
                                       theme={{
                                         
                                         // calendarBackground: '#e1e1e1',
                                         // textSectionTitleColor: '#000',
                                         // selectedDayBackgroundColor: 'red',
                                         // todayTextColor: 'red',
                                         
                                         // dayTextColor: '#848484',
                                         // textDisabledColor: '#d9e1e8',
                                         // selectedDotColor: '#ffffff',
                                         // arrowColor: 'blue',
                                         // monthTextColor: '#000',
                                         // //textDayFontFamily: 'monospace',
                                         // //textMonthFontFamily: 'monospace',
                                         // //textDayHeaderFontFamily: 'monospace',
                                         // textMonthFontWeight: 'bold',
                                         // textDayFontSize: 16,
                                         // textMonthFontSize: 16,
                                         // textDayHeaderFontSize: 16,
                                         // 'stylesheet.calendar.header': {
                                         //     week: {
                                               
                                         //       flexDirection: 'row',
                                         //       justifyContent: 'space-between',
                                         //       backgroundColor:'#fff',
                                         //       textTransform:'uppercase'
                                               
                                         //     },
                                             
                                             
                                         // }
                     
                                         calendarBackground: '#e1e1e1',
                                         textSectionTitleColor: '#000',
                                         selectedDayBackgroundColor: 'red',
                                         todayTextColor: 'red',
                                         
                                         dayTextColor: '#848484',
                                         textDisabledColor: '#d9e1e8',
                                         selectedDotColor: '#000',
                                         arrowColor: 'blue',
                                         monthTextColor: '#000',
                                         // textDayFontFamily: 'monospace',
                                         // textMonthFontFamily: 'monospace',
                                         // textDayHeaderFontFamily: 'monospace',
                                         textMonthFontWeight: 'bold',
                                         textDayFontSize: 16,
                                         textMonthFontSize: 16,
                                         textDayHeaderFontSize: 16,
                                         'stylesheet.calendar.header': {
                                           week: {
                           
                                             flexDirection: 'row',
                                             justifyContent: 'space-between',
                                             backgroundColor: '#fff',
                                             textTransform: 'uppercase'
                           
                                           },
                           
                           
                                         }
                     
                                         
                                         
                                       }}
                                       showControls={true}
                                       
                                       /> 
                     )}
                  
                  </View>

                    <View style={{backgroundColor:'#ffffff'}}>
                        
                            {this.state.AppReqData.length > 0 ?
                                <FlatList
                                    data={this.state.AppReqData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderAppoinmentItem}
                                    refreshing={this.state.appRefreshing}
                                    onRefresh={this._pullToAppRefresh}
                                    onEndReached={this.onScrollApp}
                                    onEndReachedThreshold={0.5}
                                    style={{ backgroundColor: '#ffffff' }}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}
                                />
                                : !this.state.appIsLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center',backgroundColor: "#ffffff" }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>No Appointments Scheduled</Text>
                                    </View> : null
                            }
                            {/* {this.state.appIsLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            } */}
                             
                    </View>
                    
                {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')} >
                <View style={CommanStyle.AppmycardDiv}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',paddingLeft:10,fontFamily: 'Aileron-Regular'}]}>10:00 am</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >Outside (30-45min)</Text>
                          <Text allowFontScaling={false}   >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false}   >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                   
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')}>
                <View style={CommanStyle.AppmycardDiv}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',paddingLeft:10,fontFamily: 'Aileron-Regular'}]}>10:00 am</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >Outside (30-45min)</Text>
                          <Text allowFontScaling={false} >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false} >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')}>
                
                <View style={CommanStyle.AppmycardDiv}>
                    <View style={{flex:2,borderRightColor:'#ccc',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',color:'red',paddingLeft:10,fontFamily: 'Aileron-Regular'}]}>Canceled</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false} style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false} >Outside (30-45min)</Text>
                          <Text allowFontScaling={false} >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false} >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                   
                    </View>
                </View>
                </TouchableOpacity> */}


                

                
                </ScrollView>
            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },
    


})


