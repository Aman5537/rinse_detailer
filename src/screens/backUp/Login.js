import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView, Image, TouchableOpacity, StyleSheet,Platform } from 'react-native'
// import { Button } from '../common/Button';
import { createAccount } from '../config/Styles';
import { Header } from 'react-native-elements';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
// import GradientBox from 'react-native-gradient-box-expo';
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from "react-native-check-box";
import Helper from '../Lib/Helper';
//import SocialButton from '../common/SocialButton';
import { NavigationActions,DrawerActions,StackActions } from 'react-navigation';
import { EventRegister } from 'react-native-event-listeners'


export default class Login extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
            userdata: '',
            loginform: {
                email: "", password: "",
                device_token: Helper.device_token, device_type: Helper.device_type,role_id:2,
                validators: {
                    email: { required: true, "email": true },
                    password: { required: true },
                }
            }
        }
        this.gotoNext = this.gotoNext.bind(this);

        this.isRemember();
    }

    componentDidMount() {
        Helper.getData('userdata').then((responseData) => {
            if (responseData === null || responseData === 'undefined' || responseData === '') {

            } else {

                this.props.navigation.navigate('Home');
            }
        })

    }


    isRemember() {
        Helper.getData('remember_email').then((responseData) => {
            // console.log('------userData  :  ' + JSON.stringify(responseData));
            if (responseData === null || responseData === 'undefined' || responseData === '') {

            } else {

                this.setValues('email', responseData);

                Helper.getData('remember_pass').then((responseData1) => {

                    this.setValues('password', responseData1);
                })

                this.setState({ isChecked: true });
            }
        })
    }

    onLogin = () => {
        let isValid = Helper.validate(this.state.loginform);
        // const reset= StackActions.reset({
            
        //     actions: [NavigationActions.navigate({routeName:"bottomTabs"})],
           
        // })
        
        //loader:false,
        if (isValid) {
            Helper.makeRequest({ url: "signin", data: this.state.loginform }).then((logindata) => {
                console.log('----logindata :  ' + JSON.stringify(logindata));
                if (logindata.status == 'true') {
                    //Helper.showToast(logindata.message);
                    
                    Helper.setData('userdata', logindata.data);
                    Helper.setData('token', logindata.security_token);

                    let userAData = {
                        is_sub_detailer:logindata.data.is_sub_detailer,
                        quotes_permission:logindata.data.quotes_permission
                    }

                    
                    if (this.state.isChecked) {
                        Helper.setData('remember_email', this.state.loginform.email);
                        Helper.setData('remember_pass', this.state.loginform.password);
                    }
                    else {
                        Helper.setData('remember_email', '');
                        Helper.setData('remember_pass', '');
                    }


                    
                 
                    // this.props.navigation.dispatch(reset);

                    
                    EventRegister.emit('userTabBarEvent', userAData);
                    
                    if(logindata.data.is_password_reset == 1 && logindata.data.is_sub_detailer == 1){
                        this.props.navigation.navigate('ChangePassword', { pass_reset: 1 })
                    }else{
                        if(logindata.data.services == 0 && logindata.data.is_sub_detailer == 1){
                            this.props.navigation.navigate('Portfolio', { is_skip: 1 })
                        }
                        else if(logindata.data.services == 0 && logindata.data.is_sub_detailer == 0){
                            this.props.navigation.navigate('ServicesYouOffer', { is_skip: 1 })
                        }
                        else{
                            this.props.navigation.navigate('SetHome');
                        }
                    }
                        

                    
                    
                    
                }
                else {
                    //Helper.alert(logindata.message);
                    Helper.showToast(logindata.message)
                }
            }).catch(err => {
                //Helper.showToast("Please try again");
            })
        }
    }

    setValues(key, value) {
        let loginform = { ...this.state.loginform }
        loginform[key] = value;
        this.setState({ loginform })
    }

    selectCat = (selectedItems) => {
        this.setState({ selectedItems });
    }

    gotoNext() {
        this.onLogin();
    }
    managePasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }


    render() {
        //console.log("object", this.state.Msg)
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: 'white', }}>

                    <CustomHeader navigation={this.props.navigation} />

                    <AuthTitle TitleName='Mobile Detailer Login' />

                    <Card>


                        <TextInput placeholder='Email'
                            style={styles.myTextBox}
                            onChangeText={(email) => this.setValues('email', email)}
                            onSubmitEditing={() => this.passwordInput.focus()}
                            value={this.state.loginform.email}
                        />

                        <TextInput placeholder='Password'
                            style={styles.myTextBox}
                            // onChangeText={(email) => this.setValues('email', email)}
                            onChangeText={(password) => this.setValues('password', password)}
                            ref={(input) => { this.passwordInput = input; }}
                            //onSubmitEditing={() => this.onLogin()}
                            secureTextEntry={true}
                            value={this.state.loginform.password}
                        />




                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                {/* <CheckBox
                                    //style={{borderColor:'green',borderWidth:1}}
                                    value={this.state.checked}
                                    onValueChange={() => this.setState({ checked: !this.state.checked })}
                                /> */}

                                <CheckBox
                                    style={{ flex: 1, padding: 10, paddingLeft: 10, marginLeft: -10 }}
                                    onClick={() => {
                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        });
                                    }}
                                    isChecked={this.state.isChecked}
                                    rightText={"Remember Me"}
                                    rightTextStyle={{ color: "black", fontSize: 14 }}
                                    checkedImage={
                                        <Image
                                            source={require("../../assets/Icons/tick.png")}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                        />
                                    }
                                    unCheckedImage={
                                        <Image
                                            source={require("../../assets/Icons/untick.png")}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                        />
                                    }
                                />


                            </View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')} style={{ marginLeft: -170 }}>
                                <Text allowFontScaling={false} style={{}}>Forgot Password<Text style={{ fontFamily: 'Aileron-Regular' }}>?</Text> </Text>
                            </TouchableOpacity>

                        </View>
                        <View 
                        onTouchStart={() => this.gotoNext()}
                        >
                            <TouchableOpacity 
                            //onPress={() => this.gotoNext()}
                            >
                                <LinearGradient
                                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                    borderRadius={5}
                                    style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20, height: 52 }}
                                >
                                    <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >LOGIN</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>



                        {/* <Button BText='LOGIN' 
                        // onPress={() => {global.state['modal'].setState({ modalVisible: true, Msg:this.state.Msg  })}} 
                         /> */}

                        <Text allowFontScaling={false} style={{ paddingTop: 10, paddingBottom: 10, textAlign: 'center', fontSize: 16 }}>or</Text>

                        {/* <SocialButton navigation={this.props.navigation} /> */}

                        {/* <TouchableOpacity>
                            <View style={{ padding: 5, backgroundColor: '#475b93',borderRadius:5 }}>
                                <View style={{ height: 40, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        resizeMode='contain'
                                        source={require('../../assets/Icons/facebook.png')}
                                        style={{ height: 35, width: 20, margin: 5, flex: 1.5, }}
                                    />

                                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ flex: 7, color: '#fff', paddingRight: 5, paddingLeft: 5, fontSize: 18, lineHeight: 23,textAlign:'center' }}>
                                        Login with Facebook </Text>

                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={{ padding: 5, backgroundColor: '#c94238', marginTop: 10,borderRadius:5 }}>
                                <View style={{ height: 40, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        resizeMode='contain'
                                        source={require('../../assets/Icons/google.png')}
                                        style={{ height: 35, width: 20, margin: 5, flex: 1.5, }}
                                    />

                                    <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ flex: 7, color: '#fff', paddingRight: 5, paddingLeft: 5, fontSize: 18, lineHeight: 23,textAlign:'center' }}>
                                        Login with Google </Text>

                                </View>
                            </View>
                        </TouchableOpacity> */}



                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: 20, flexDirection: 'row' }}>
                            <Text allowFontScaling={false} font={'CenturyGothic'}>Don't have an account</Text><Text allowFontScaling={false}><Text style={{ fontFamily: 'Aileron-Regular' }}>? </Text></Text>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('CreateAccount')}
                            >
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={{ textDecorationLine: 'underline' }}>Create Account</Text></TouchableOpacity>

                        </View>

                    </Card>

                </View>

            </ScrollView>
        )
    }


}


const styles = StyleSheet.create({
    myTextBox: {
        color: '#000',
        paddingLeft: 15,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        height: 50,
    }
})