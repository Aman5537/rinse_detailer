import React, { Component } from 'react'
import { Text,Modal, View, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Platform } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle, HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import { SimpleHeader } from '../common/SimpleHeader';
import LinearGradient from 'react-native-linear-gradient';

import Helper from '../Lib/Helper';
import CameraController from '../Lib/CameraController';
import Config from "../Lib/Config";
import Permissions from 'react-native-permissions';
const DeviceH =Dimensions.get('window').height;

class PhotosPopUp extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            
            isChecked:false,
            img_count:1,
        }

        
        
        


    }

    render() {
    return (
        <View style={{width:Dimensions.get('window').width,height:400,marginTop:50}}>

                <View style={{flexDirection:'row',marginTop:70,padding:20}}>
                        <TouchableOpacity onPress={() => {
                            global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" });
                            
                        }}
                            style={{ flex: 2 }}
                        >
                        <Text  allowFontScaling={false}  style={{textAlign:'right',color:'#fff',fontSize:18,fontFamily: 'Aileron-Regular'}}>Close</Text>
                        </TouchableOpacity>
                </View>

                <Image
                    style={{ width: '90%', height: '90%',alignSelf:'center',resizeMode:'contain' }}
                    source={{uri:this.props.photo}}
                />
                   
        </View>
    )
    }
}



export default class Portfolio extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            is_skip: this.props.navigation.getParam('is_skip', 0),
            photoData: [],
            NewphotoData: [],
            photosCount: this.props.navigation.getParam('is_skip', 0),
            NewPhotoCount: 0,
            isDelete: false,
            PhotosPopUp: <PhotosPopUp navigation={this.props.navigation} />,
            userdata:''
        }



        
        this.getUserPortfolio(true);

        Permissions.request('storage').then(response => {
            
            if (response == 'authorized') {
                Permissions.request('storage').then(response => {
                    if (response == 'authorized') {
                        //this.selecteImage(cb);
                    } else {
                        Helper.confirm("Allow " + Config.app_name + " access to your device's photo,media and camera.", (status) => {
                            if (status) {
                                Permissions.canOpenSettings().then(response => {
                                    if(response){
                                        Permissions.openSettings();
                                    }
                                })
                            }
                        })
                    }
                })
            } else {
                Helper.confirm("Allow " + Config.app_name + " access to your device's photo,media and camera.", (status) => {
                    if (status) {
                        Permissions.canOpenSettings().then(response => {
                            if(response){
                                Permissions.openSettings();
                            }
                        })
                    }
                })
            }
        })


        

    }


    componentDidMount() {
        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {
                this.setState({ userdata: responseData });

            }
        })

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            

            

            var arr = this.props.navigation.state.params.images;
            
            var newArray = [...this.state.NewphotoData]; 
            //var newArray = [];

            
            
            if (Platform.OS === 'ios') {
                arr.map((items, index) => (
                    newArray.push(items)
                ))
            }
            else {
                arr.map((items, index) => (
                    newArray.push('file://storage/' + items)
                ))
            }


            this.setState({ NewPhotoCount: arr.length })
            this.setState({ NewphotoData: newArray });

        })



        


    }



    componentWillUnmount() {
        this.focusListener.remove()
    }



    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }


    _onPhotosPopUp = (url) => {
       
        this.setState({'PhotosPopUp':<PhotosPopUp navigation={this.props.navigation} photo={url}  />},()=>{

            global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.PhotosPopUp})
        })
       
    }

    getUserPortfolio(MyLoader) {
        Helper.makeRequest({ url: "get-detailer-portfolio",loader: MyLoader }).then((data) => {
            if (data.status == 'true') {

                //alert(JSON.stringify(data.data));
                this.setState({ photoData: data.data })
                //this.setState({photosCount:data.data.length})
                //Helper.showToast(data.message);



            }
            else {
                //Helper.showToast(data.message)
            }
        })
    }

    onDeletePress(index, id) {
        var portfolio_id = [];
        portfolio_id.push(id);

        

                Helper.makeRequest({ url: "delete-detailer-portfolio", data: { portfolio_image_id: JSON.stringify(portfolio_id) } }).then((data) => {
                    if (data.status == 'true') {


                        this.removePhoto(index);

                    }
                    else {
                        Helper.showToast(data.message)
                    }
                })

            
    }

    isDeleteFun = () => {
        this.setState({ isDelete: !this.state.isDelete });
    }

    removePhoto(e) {
        var array = [...this.state.photoData];

        array = array.slice(0); // make copy
        array.splice(e, 1);
        this.setState({ photoData: array });
        this.setModalVisible(false)
        //this.setState({NewPhotoCount:this.state.NewPhotoCount-1})   
    }

    removeNewPhoto(e) {

        var array = [...this.state.NewphotoData];

        array = array.slice(0); // make copy
        array.splice(e, 1);
        this.setState({ NewphotoData: array });

        this.setState({ NewPhotoCount: this.state.NewPhotoCount - 1 })

        this.setModalVisible(false)    
    }


    chooseImage = (data) => {

        CameraController.selecteImage((response) => {
            if (response.uri) {
                //this.state.photoData.push({image:response.uri});

                //alert(JSON.stringify(this.state.photoData));
                //this.setState({photoData:this.state.photoData})

                //alert(response.uri);
                this.state.NewphotoData.unshift(response.uri);
                this.setState({ NewphotoData: this.state.NewphotoData })
                this.setState({ NewPhotoCount: this.state.NewPhotoCount + 1 })
            }
        });
    }

    addPhotos(v) {
        let tempdata = new FormData();

        tempdata.append('image_count', this.state.NewPhotoCount);

        if((this.state.NewPhotoCount+this.state.photoData.length) > 30){
            Helper.showToast("You have reached the 30 picture limit. To add more pictures, please delete some pictures currently uploaded.");
            return false;
        }

        if(this.state.NewphotoData.length == 0){
            this.props.navigation.navigate('SetHome');
            return false;
        }

        this.state.NewphotoData.map((items, index) => (
            tempdata.append('image_' + (index + 1), {
                uri: items,
                name: 'test.jpg',
                type: 'image/jpeg'
            })
            
        ))


        Helper.makeRequest({ url: "detailer-add-portfolio", method: "POSTUPLOAD", data: tempdata }).then((data) => {
            if (data.status == 'true') {

                if (v == 1) {
                    //this.props.navigation.navigate('Home');
                    this.props.navigation.navigate('SetHome');
                }

                this.setState({ NewphotoData: [] })
                this.setState({ NewPhotoCount: 0 })
                this.getUserPortfolio(false);

                Helper.showToast(data.message);



            }
            else {
                Helper.showToast(data.message)
            }
        })
    }


    modalRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                            <View style={{ alignItems: 'center', minHeight: 80, maxHeight: 200 }}>

                                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>Do you want to delete this photo?</Text>


                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => {
                                        if(this.state.DeleteType === 1){
                                            this.removeNewPhoto(this.state.DeleteIndex)
                                        }else{
                                            this.onDeletePress(this.state.DeleteIndex,this.state.DeleteId )
                                        }
                                        
                                        

                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                            style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}


                                        >

                                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                                        </LinearGradient>


                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(false)
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                            style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}
                                        >

                                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                                        </LinearGradient>

                                    </TouchableOpacity>
                                </View>
                            </View>


                        </View>
                    </View>
                </View>



            </Modal>



        );
    }


    render() {


        if (this.state.photosCount != 0) {
            return (

                <View style={{ flex: 1, }}>
                    
                    {this.state.is_skip === 1 && (
                        <SimpleHeader pagetitle='Portfolio' navigation={this.props.navigation} bArrow />
                    )}

                    {this.state.is_skip !== 1 && (
                        <SimpleHeader pagetitle='Portfolio' navigation={this.props.navigation} bArrow userbar />
                    )}


                    <HeaderTitle TitleName='Examples' />
                    {/* <Text  allowFontScaling={false}   font={'CenturyGothic'} style={{fontSize:20,fontWeight:'bold',textAlign:'center',marginTop:20}}>Examples</Text> */}

                    <ScrollView>
                        <View style={{ padding: 10 }}>
                            <Text allowFontScaling={false} style={[CommanStyle.normaltext, { fontSize: 17, fontFamily: 'Aileron-Regular' }]}><Text allowFontScaling={false} style={{ fontSize: 24, alignItems: 'center', alignContent: 'center', fontFamily: 'Aileron-Regular' }}>{'\u2022'} </Text> Photos of your work</Text>
                            <Text allowFontScaling={false} style={[CommanStyle.normaltext, { fontSize: 17, fontFamily: 'Aileron-Regular' }]}><Text allowFontScaling={false} style={{ fontSize: 24, alignItems: 'center', alignContent: 'center', fontFamily: 'Aileron-Regular' }}>{'\u2022'} </Text> Photos of you working</Text>
                            <Text allowFontScaling={false} style={[CommanStyle.normaltext, { fontSize: 17, fontFamily: 'Aileron-Regular' }]}><Text allowFontScaling={false} style={{ fontSize: 24, alignItems: 'center', alignContent: 'center', fontFamily: 'Aileron-Regular' }}>{'\u2022'} </Text> Photos of equipment</Text>
                            <Text allowFontScaling={false} style={[CommanStyle.normaltext, { fontSize: 17, fontFamily: 'Aileron-Regular' }]}><Text allowFontScaling={false} style={{ fontSize: 24, alignItems: 'center', alignContent: 'center', fontFamily: 'Aileron-Regular' }}>{'\u2022'} </Text> Before & after photos</Text>

                            <Text allowFontScaling={false} style={{ marginTop: 15, paddingLeft: 3, color: '#4d4d4d', fontFamily: 'Aileron-Regular' }}>Long press on a photo to drag and rearrange.</Text>
                        </View>

                        <View style={{ padding: 12, flexDirection: 'row', flexWrap: 'wrap' }}>
                            <TouchableOpacity
                                //onPress={this.chooseImage}
                                onPress={() => this.props.navigation.navigate('Camera', { is_skip: this.state.is_skip })}

                                //onPress={() => this.props.navigation.navigate('Gallery',{is_skip:this.state.is_skip})}
                                style={style.photoItem}
                            >
                                <Image
                                    source={require("../../assets/Icons/add-mode-upload.png")}
                                    style={{ width: '100%', height: '100%' }}
                                />
                            </TouchableOpacity>

                            {this.state.NewphotoData.map((items, index) =>
                                <TouchableOpacity key={index}
                                    style={style.photoItem}
                                    onPress={() => this._onPhotosPopUp(items)}
                                >
                                    <Image
                                        source={{ uri: items }}
                                        style={{ width: '100%', height: '100%',resizeMode:'cover' }}
                                    />

                                </TouchableOpacity>
                            )}


                            {this.state.photoData.map((items, index) =>
                                <TouchableOpacity key={index}
                                    style={style.photoItem}
                                    onPress={() => this._onPhotosPopUp(Helper.getImageUrl(items.image))}
                                >

                                    <Image
                                        source={{ uri: Helper.getImageUrl(items.image) }}
                                        style={{ width: '100%', height: '100%',resizeMode:'cover' }}
                                    />
                                </TouchableOpacity>
                            )}
                        </View>


                    </ScrollView>
                    <View>
                        {/* {(this.state.userdata.is_sub_detailer == 1) && ( */}
                            <TouchableOpacity onPress={() => this.addPhotos(1)}>
                            <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltitle, { textAlign: 'center', fontSize: 20, textDecorationLine: 'underline' }]} >SKIP</Text>
                            </TouchableOpacity>
                        {/* )} */}
                        {/* {(this.state.NewPhotoCount > 0 || this.state.userdata.is_sub_detailer == 1) && ( */}
                            <TouchableOpacity
                                //onPress={() => this.addPhotos()}
                                onPress={() => this.addPhotos(1)}
                            //onPress={() => this.props.navigation.navigate('DetailersWithoutNav',{is_skip:1})}
                            >
                                <LinearGradient
                                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                    style={{ padding: 10, alignItems: 'center', marginTop: 20, height: 52 }}
                                >
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                                        CONTINUE
                                </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        {/* )} */}
                    </View>
                </View>
            )
        }

        if (this.state.photosCount == 0) {
            return (
                <View style={{ flex: 1, backgroundColor: '#ffff', }}>
                    {this.modalRender()}
                    {/* {this.state.is_skip === 1 && (
                    <CustomHeader bArrow navigation={this.props.navigation} />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader bArrow navigation={this.props.navigation} userbar />
                )} */}


                    {this.state.is_skip === 1 && (
                        <SimpleHeader pagetitle='Portfolio' navigation={this.props.navigation} bArrow />
                    )}

                    {this.state.is_skip !== 1 && (
                        <SimpleHeader pagetitle='Portfolio' navigation={this.props.navigation} bArrow userbar />
                    )}


                    <View style={{ padding: 10, backgroundColor: '#d9d9d9', flexDirection: 'row' }}>
                        <Text style={{ flex: 6 }}>All Photos</Text>
                        <TouchableOpacity
                            onPress={this.isDeleteFun}
                            style={{ flex: 1, alignItems: 'flex-end' }}>
                            {this.state.isDelete == false && (
                            <Image
                                source={require("../../assets/Icons/pencil.png")}
                                style={{ width: 20, height: 20 }}
                            />
                            )}
                            {this.state.isDelete == true && (
                            <Image
                                source={require("../../assets/Icons/checked-circle.png")}
                                style={{ width: 20, height: 20 }}
                            />
                            )}
                        </TouchableOpacity>
                    </View>

                    <ScrollView>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <TouchableOpacity
                                //onPress={this.chooseImage}
                                onPress={() => this.props.navigation.navigate('Camera', { is_skip: this.state.is_skip })}

                                //onPress={() => this.props.navigation.navigate('Gallery',{is_skip:this.state.is_skip})}
                                style={style.imageView}
                            >
                                <Image
                                    source={require("../../assets/Icons/add-mode-upload.png")}
                                    style={{ width: '100%', height: '100%' }}
                                />
                            </TouchableOpacity>
                            {this.state.NewphotoData.map((items, index) =>
                                <View key={index}
                                    style={style.imageView}
                                >
                                <TouchableOpacity
                                onPress={() => this._onPhotosPopUp(items)}
                                >
                                    <Image
                                        source={{ uri: items }}
                                        style={{ width: '100%', height: '100%',resizeMode: "cover" }}
                                    />
                                </TouchableOpacity>
                                    {this.state.isDelete == true && (
                                        <TouchableOpacity
                                            onPress={() => {
                                                //this.removeNewPhoto(index)}
                                                this.setState({
                                                    DeleteIndex:index,
                                                    DeleteType:1
                                                })
                                                this.setModalVisible(true)
                                            }}
                                            style={{ height: 20, width: 20, backgroundColor: 'white', borderRadius: 50, alignItems: 'flex-end', position: 'absolute', right: 5, top: 3 }}>
                                            <Image
                                                resizeMode='contain'
                                                style={{ height: 20, width: 20, alignSelf: 'flex-end' }}
                                                source={require('../../assets/Icons/cross.png')}
                                            />
                                        </TouchableOpacity>
                                    )}


                                </View>
                            )}

                            {this.state.photoData.map((items, index) =>
                                <View key={index} style={style.imageView}>
                                 <TouchableOpacity
                                onPress={() => this._onPhotosPopUp(Helper.getImageUrl(items.image))}
                                >
                                    <Image
                                        source={{ uri: Helper.getImageUrl(items.image) }}
                                        style={{ width: '100%', height: '100%',resizeMode: "cover" }}
                                    />
                                </TouchableOpacity>
                                    {this.state.isDelete == true && (
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({
                                                    DeleteIndex:index,
                                                    DeleteId:items.id,
                                                    DeleteType:2
                                                })
                                                //this.onDeletePress(index, items.id)
                                                this.setModalVisible(true)
                                            }}
                                            style={{ height: 20, width: 20, backgroundColor: 'white', borderRadius: 50, alignItems: 'flex-end', position: 'absolute', right: 5, top: 3 }}>
                                            <Image
                                                resizeMode='contain'
                                                style={{ height: 20, width: 20, alignSelf: 'flex-end' }}
                                                source={require('../../assets/Icons/cross.png')}
                                            />
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )}
                        </View>
                    </ScrollView>

                    <View>

                        {this.state.NewPhotoCount > 0 && (
                            <TouchableOpacity
                                onPress={() => this.addPhotos(2)}
                            //onPress={() => this.props.navigation.navigate('Portfolio',{is_skip:this.state.is_skip})}
                            >
                                <LinearGradient
                                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                    style={{ padding: 10, alignItems: 'center', height: 52 }}
                                >
                                    <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                                        Upload
                        </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        )}

                    </View>


                </View>
            )
        }
    }
}

const style = StyleSheet.create({
    photoItem: {
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        marginRight: 6,
        marginBottom: 6
    },
    imageView: {
        width: Dimensions.get('window').width / 3,
        height: Dimensions.get('window').width / 3,
        borderWidth: 2,
        borderColor: 'gray'


    }
})


