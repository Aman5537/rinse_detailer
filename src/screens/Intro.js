import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, CheckBox, ScrollView, Dimensions, TouchableOpacity, Image, Button,ImageBackground } from 'react-native'
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import Swiper from 'react-native-swiper';
import SplashScreen from 'react-native-splash-screen'
import Helper from '../Lib/Helper';
import { StackActions, NavigationActions } from 'react-navigation';

const BottomFoot = (props) => {
    return (
        <View style={{ position:'absolute',flexDirection: 'row',bottom:0 }}>
        <View style={{ flex: 2, justifyContent: 'flex-end', margin: 10, marginRight: 60, marginBottom: 40 }}>
            <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
            <Text allowFontScaling={false}  allowFontScaling={false} style={styles.screenbtn}>SIGN IN</Text>
            </TouchableOpacity>
        </View>
        <View style={{ flex: 2, justifyContent: 'flex-end', margin: 10, marginLeft: 60, marginBottom: 40 }}>
            <TouchableOpacity onPress={() => props.navigation.navigate('CreateAccount')}>
            <Text allowFontScaling={false}   style={styles.screenbtn}>JOIN</Text>
            </TouchableOpacity>
        </View>

    </View>
    )
}
export default class Intro extends Component {
    static navigationOptions = { header: null }

    

    constructor(props) {
        super(props);
        this.state = {
           BottomFoot : <BottomFoot navigation={this.props.navigation}/>,
           isLogedin:true     
        }
        Helper.navigationRef=this.props.navigation;
    }

    componentDidMount() {
        SplashScreen.hide();

        //this.props.navigation.navigate('MaintananceScreen');

        Helper.getData('userdata').then((responseData) => {
            
            if (responseData === null || responseData === 'undefined' || responseData === '') {
                this.setState({isLogedin:false});
            } else {
                this.setState({isLogedin:true});
                this.props.navigation.navigate('Home');
                

            }
        })

    }
    render() {
        //alert(this.state.isLogedin);
        if(this.state.isLogedin === false){
            return (


                <View style={{ flex: 1, backgroundColor: '#ffff', }}>
               
                   <Swiper style={styles.wrapper} showsButtons={false} loop={false} autoplay={false} autoplayTimeout={2.5}
                       dotStyle={{ marginBottom: 32,backgroundColor:'#ccc' }}
                       activeDotStyle={{ marginBottom: 32 }}
                   >
                       <View style={{flex:1}}>
                       <ImageBackground source={require('../../assets/Icons/splash-bg.png')} style={{height:'100%'}} >
                       {/* <View style={{alignItems:'center',alignContent:'center',justifyContent:'flex-end'}}>
                       <Image
                       resizeMode='contain'
                       source={require('../../assets/Icons/splash-logo.png')}
                       style={{ width:220,height:220}}
                       />
                       </View> */}
                       
                       
                       </ImageBackground>
                       
                       </View>
   
   
   
                       <View style={styles.slide2}>
                           <View style={{flex:2.8,backgroundColor:'red',width:Dimensions.get('window').width,height:Dimensions.get('window').height}}>
                           <Image
   
                           source={require('../../assets/Icons/slide-1.png')}
                           style={{ width:'100%',height:'100%'}}
                           />
                           </View>
                           
                           <View style={{flex:2.2}}>
                               <View style={{alignItems:'center',padding:10,marginTop:20}}>
                                   <Image
                                   source={require('../../assets/Icons/location.png')}
                                   style={{ width:40,height:40}}
                                   />
   
                                   <Text allowFontScaling={false}   style={[styles.slideTitle]}>Location</Text>
                                   <Text allowFontScaling={false}  style={styles.slideText}>You select the area you wish to service.</Text>
                               </View>
                              
                           </View>
                       </View>
                       <View style={styles.slide3}>
                       <View style={{flex:2.8,backgroundColor:'red',width:Dimensions.get('window').width,height:Dimensions.get('window').height}}>
                           <Image
   
                           source={require('../../assets/Icons/slide-2.jpg')}
                           style={{ width:'100%',height:'100%'}}
                           />
                           </View>
                           
                           <View style={{flex:2.2}}>
                               <View style={{alignItems:'center',padding:10,marginTop:20}}>
                                   <Image
                                   source={require('../../assets/Icons/quotes.png')}
                                   style={{ width:40,height:40}}
                                   />
   
                                   <Text allowFontScaling={false}   style={styles.slideTitle}>Submit Quotes</Text>
                                   <Text allowFontScaling={false}  style={styles.slideText}>Send quotes to customer when you see jobs that interest you.</Text>
                               </View>
                              
                           </View>
                       </View>
                       <View style={styles.slide3}>
                       <View style={{flex:2.8,backgroundColor:'red',width:Dimensions.get('window').width,height:Dimensions.get('window').height}}>
                           <Image
   
                           source={require('../../assets/Icons/slide-3.jpg')}
                           style={{ width:'100%',height:'100%'}}
                           />
                           </View>
                           
                           <View style={{flex:2.2}}>
                               <View style={{alignItems:'center',padding:10,marginTop:20}}>
                                   <Image
                                   source={require('../../assets/Icons/doller.png')}
                                   style={{ width:40,height:40}}
                                   />
   
                                   <Text allowFontScaling={false}   style={styles.slideTitle}>Get paid $$$</Text>
                                   <Text allowFontScaling={false}  style={styles.slideText}>Once the customer accepts your quote, you complete the job and get paid!</Text>
                               </View>
                             
                           </View>
                       </View>
                   </Swiper>
                   
                       {this.state.BottomFoot}
               </View>
               
   
           )
        }
        else{
            return (


                <View style={{ flex: 1, backgroundColor: '#ffff', }}>
               
                   <Swiper style={styles.wrapper} showsButtons={false} loop={false} autoplay={false} autoplayTimeout={2.5}
                       dotStyle={{ marginBottom: 32,backgroundColor:'#ccc' }}
                       showsPagination={false}
                       activeDotStyle={{ marginBottom: 32 }}
                   >
                       <View style={{flex:1}}>
                       {/* <ImageBackground source={require('../../assets/Icons/splash-bg.png')} style={{height:'100%'}} >
                       </ImageBackground> */}
                       
                       </View>
   
   
   
                      
                       
                   </Swiper>
                   
                       
               </View>
               
   
           )
        }
        
    }



}



const styles = StyleSheet.create({
    wrapper: {
       
    },
    slide1: {
        flex: 1,
        backgroundColor: '#fff',
    },
    slide2: {
        flex: 1,
        backgroundColor: '#fff',
    },
    slide3: {
        flex: 1,
        backgroundColor: '#fff',
    },
    slide4: {
        flex: 1,
        backgroundColor: '#fff',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    screenbtn: {
        backgroundColor: '#fff', 
        textAlign: 'center', 
        padding: 10, 
        color: '#0083c5', 
        borderRadius: 5, 
        fontSize: 20
    },
    slideTitle:{
        textAlign:'center',
    //    fontFamily:'CenturyGothic',
        padding:10,
        fontSize:20,
        
    },
    slideText:{
        textAlign:'center',
        color:'#818181',
        fontSize:16,
        fontFamily: 'Aileron-Regular'
        
    },
    
})