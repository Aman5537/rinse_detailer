import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity, Linking,ActivityIndicator } from 'react-native'
import { CustomHeader, CardSection, AuthHeader } from '../common';
import { NavigationActions, DrawerActions } from 'react-navigation';
import Helper from '../Lib/Helper';
import LinearGradient from 'react-native-linear-gradient';
import ChatController from '../Lib/ChatController';
import { EventRegister } from 'react-native-event-listeners';
import Moment from "moment";
import firebase, {
    Notification,
    NotificationOpen,
    Firebase
} from 'react-native-firebase';

class StripeMsg extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <View style={{ height: 110, marginLeft: -10, marginRight: -10, marginTop: -5 }}>
                <View style={{ padding: 10 }}>
                    <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", marginTop: 10, fontFamily: 'Aileron-Regular' }}>Please setup your Stripe Account</Text>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => {
                            global.state['modal'].setState({ modalVisible: false, Msg: "" });
                            this.props.navigation.navigate('PayoutAccount');
                        }}
                            style={{ flex: 1 }}
                        >
                            <LinearGradient
                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}
                            >
                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >SETUP</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

}



var activeTab = 'RequestsStack';
export default class Home extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            userdata: '',
            StripeMsg: <StripeMsg navigation={this.props.navigation} />,
            appoinmentCount: 0,
            messageCount: 0,
            requestTotalCount: 0,
            add_data: '',
            comeToNotification: true
        }


        Helper.getData('userdata').then((data) => {
            if (data) {
                ChatController.connectUser(data.id);
            }
        })




    }





    componentDidMount() {
        setTimeout(() => { this.setState({ comeToNotification: false }) }, 2000)


        this.createNotificationListeners();

        this.AddManager();

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            //this.setState({comeToNotification:false})
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    //this.props.navigation.navigate('LoginNew');
                } else {
                    //ChatController.connectUser(responseData.id);
                    this.setState({ userdata: responseData }, () => {

                        this.getBadegesCount();
                    });

                }

                if (this.state.userdata.is_password_reset == 1 && this.state.userdata.is_sub_detailer == 1) {
                    this.props.navigation.navigate('ChangePassword', { pass_reset: 1 })
                }
                else {

                    if ((this.state.userdata.stripe_user_id == '' || this.state.userdata.stripe_token == '') && this.state.userdata.is_sub_detailer == 0) {

                        //if(global.state.stripeAlert == 1){
                        global.state['modal'].setState({ modalVisible: true, Msg: this.state.StripeMsg })
                        //global.state.stripeAlert = 0;
                        //}

                    }
                }

            })
        })

        //this.getBadegesCount();



    }

    // componentWillUnmount() {
    //     this.focusListener.remove()
    // }



    AddManager = () => {
        Helper.makeRequest({ url: "get-ads", data: { role_id: 1 }, loader: false })
            .then((data) => {
                if (data.status == 'true') {
                    this.setState({ add_data: data.data[0] || "" })

                }
            })


    }

    async createNotificationListeners() {

        firebase.notifications().onNotification(notification => {
            notification.android.setChannelId('insider').setSound('default')
            firebase.notifications().displayNotification(notification)
        });

        firebase.messaging().hasPermission().then(enabled => {
            if (enabled) {
                this.notificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {
                    if (notificationOpen) {
                        this.setState({ comeToNotification: true })
                        console.log("666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666");
                        var type = notificationOpen.notification.data.type;
                        var dicto = notificationOpen.notification.data.dictionary;
                        //console.log(dicto,'notificationOpen.notification.data.dictionary.id');

                        var newdec = JSON.parse(dicto);

                        if (type == 'ServiceRequest') {
                            if ((Helper.NotiType != 'ServiceRequest') && (Helper.NotiId != newdec.id)) {
                                Helper.NotiType = 'ServiceRequest';
                                Helper.NotiId = newdec.id;
                                this.props.navigation.navigate('SendQuote', { 'req_id': newdec.id, 'req_data': {} })
                            }
                        }
                        if (type == 'ServiceRequestCanceled') {
                            if (Helper.NotiType != 'ServiceRequestCanceled') {
                                Helper.NotiType = 'ServiceRequestCanceled';

                                this.navigateToScreen('Requests')
                            }
                        }
                        if (type == 'Hire') {
                            if ((Helper.NotiType != 'Hire') && (Helper.NotiId != newdec.id)) {
                                Helper.NotiType = 'Hire';
                                Helper.NotiId = newdec.id;
                                this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                            }
                        }
                        if (type == 'AppointmentUserCancel') {
                            if ((Helper.NotiType != 'AppointmentUserCancel') && (Helper.NotiId != newdec.id)) {
                                Helper.NotiType = 'AppointmentUserCancel';
                                Helper.NotiId = newdec.id;
                                this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                            }
                        }
                        if (type == 'PaymentRelease') {
                            if ((Helper.NotiType != 'PaymentRelease') && (Helper.NotiId != newdec.id)) {
                                Helper.NotiType = 'PaymentRelease';
                                Helper.NotiId = newdec.id;
                                this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                            }
                        }
                        if (type == 'ServiceNoShow') {
                            if ((Helper.NotiType != 'ServiceNoShow') && (Helper.NotiId != newdec.id)) {
                                Helper.NotiType = 'ServiceNoShow';
                                Helper.NotiId = newdec.id;
                                this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                            }
                        }
                        if (type == 'Chat') {
                            if (Helper.NotiType != 'Chat') {
                                Helper.NotiType = 'Chat';

                                this.props.navigation.navigate('MessagesStack')
                            }
                        }
                        if (type == 'AddReview') {
                            if (Helper.NotiType != 'AddReview') {
                                Helper.NotiType = 'AddReview';

                                this.props.navigation.navigate('SubmiteRequest')
                            }
                        }
                        if (type == 'ReminderCompleteAppointment') {
                            if ((Helper.NotiType != 'ReminderCompleteAppointment') && (Helper.NotiId != newdec.id)) {
                                Helper.NotiType = 'ReminderCompleteAppointment';
                                Helper.NotiId = newdec.id;
                                this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                            }
                        }


                    }
                    else {
                        this.setState({ comeToNotification: false })
                    }
                });
            }
        });


        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            console.log("notificationOpen-----", notificationOpen)


            this.setState({ comeToNotification: true })

            var type = notificationOpen.notification.data.type;
            var dicto = notificationOpen.notification.data.dictionary;
            //console.log(dicto,'notificationOpen.notification.data.dictionary.id');

            var newdec = JSON.parse(dicto);

            if (type == 'ServiceRequest') {
                if ((Helper.NotiType != 'ServiceRequest') && (Helper.NotiId != newdec.id)) {
                    Helper.NotiType = 'ServiceRequest';
                    Helper.NotiId = newdec.id;
                    this.props.navigation.navigate('SendQuote', { 'req_id': newdec.id, 'req_data': {} })

                }
            }
            if (type == 'ServiceRequestCanceled') {
                if (Helper.NotiType != 'ServiceRequestCanceled') {
                    Helper.NotiType = 'ServiceRequestCanceled';

                    this.navigateToScreen('Requests')
                }
            }
            if (type == 'Hire') {
                if ((Helper.NotiType != 'Hire') && (Helper.NotiId != newdec.id)) {
                    Helper.NotiType = 'Hire';
                    Helper.NotiId = newdec.id;
                    this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                }
            }
            if (type == 'AppointmentUserCancel') {
                if ((Helper.NotiType != 'AppointmentUserCancel') && (Helper.NotiId != newdec.id)) {
                    Helper.NotiType = 'AppointmentUserCancel';
                    Helper.NotiId = newdec.id;
                    this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                }
            }
            if (type == 'PaymentRelease') {
                if ((Helper.NotiType != 'PaymentRelease') && (Helper.NotiId != newdec.id)) {
                    Helper.NotiType = 'PaymentRelease';
                    Helper.NotiId = newdec.id;
                    this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                }
            }
            if (type == 'ServiceNoShow') {
                if ((Helper.NotiType != 'ServiceNoShow') && (Helper.NotiId != newdec.id)) {
                    Helper.NotiType = 'ServiceNoShow';
                    Helper.NotiId = newdec.id;
                    this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                }
            }
            if (type == 'Chat') {
                if (Helper.NotiType != 'Chat') {
                    Helper.NotiType = 'Chat';
                    this.props.navigation.navigate('MessagesStack')
                }
            }
            if (type == 'AddReview') {
                if (Helper.NotiType != 'AddReview') {
                    Helper.NotiType = 'AddReview';
                    this.props.navigation.navigate('SubmiteRequest')
                }
            }
            if (type == 'ReminderCompleteAppointment') {
                if ((Helper.NotiType != 'ReminderCompleteAppointment') && (Helper.NotiId != newdec.id)) {
                    Helper.NotiType = 'ReminderCompleteAppointment';
                    Helper.NotiId = newdec.id;
                    this.props.navigation.navigate('AppointmentView', { 'appo_id': newdec.id })
                }
            }

        }
        else {
            this.setState({ comeToNotification: false })
        }
    }



    getBadegesCount() {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            last_id: 0,
            user_id: this.state.userdata.id,
            role_id: 2,
            utc_date_time: gettime
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);


    }


    componentWillMount() {

        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('get_appointment_message_count_response', data)

            if (data.status == 'true') {
                this.setState({ appoinmentCount: data.total_unread_appointment, messageCount: data.total_unread_message, requestTotalCount: data.total_unread_request })
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;

            } else {
                this.setState({ isLoader: false });
            }

        })

    }

    componentWillUnmount() {

        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()

    }








    navigateToScreen = (route) => () => {

        const navigateAction = NavigationActions.navigate({
            routeName: route + 'Stack'
        });

        this.props.navigation.navigate('LoginNew');
        this.props.navigation.dispatch(navigateAction);

        this.props.navigation.dispatch(DrawerActions.closeDrawer())


    }

    render() {

        return (

            <View style={{ flex: 1, }}>
                {!this.state.comeToNotification ?
                    <View>
                        <CustomHeader navigation={this.props.navigation} userbar />

                        <View style={{ flex: 1, marginTop: 40 }}>

                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', }}>

                                {this.state.userdata.quotes_permission == 1 &&
                                    <View style={style.containerView}>
                                        <TouchableOpacity onPress={this.navigateToScreen('Requests')}>

                                            <View style={style.BoxView}>
                                                <Image
                                                    resizeMode='contain'
                                                    style={{ height: 110, width: 110, }}
                                                    source={require('../../assets/Icons/requests.png')} />
                                                {this.state.requestTotalCount > 0 && (
                                                    <View style={style.badge}>
                                                        <Text style={style.badgetxt}>{this.state.requestTotalCount}</Text>
                                                    </View>
                                                )}
                                            </View>
                                        </TouchableOpacity>
                                        <Text allowFontScaling={false} style={[style.textStyle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>REQUESTS</Text>
                                    </View>
                                }
                                {this.state.userdata.quotes_permission == 0 &&
                                    <View style={style.containerView}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('NotQuotePermission')}>

                                            <View style={style.BoxView}>
                                                <Image
                                                    resizeMode='contain'
                                                    style={{ height: 110, width: 110, }}
                                                    source={require('../../assets/Icons/requests.png')} />

                                            </View>
                                        </TouchableOpacity>
                                        <Text allowFontScaling={false} style={[style.textStyle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>REQUESTS</Text>
                                    </View>
                                }

                                {this.state.userdata.is_sub_detailer == 0 &&
                                    <View style={style.containerView}>
                                        <TouchableOpacity onPress={this.navigateToScreen('Detailers')}>
                                            <View style={style.BoxView}>
                                                <Image
                                                    resizeMode='contain'
                                                    style={{ height: 110, width: 110, }}
                                                    source={require('../../assets/Icons/detailers.png')} />
                                            </View>
                                        </TouchableOpacity>
                                        <Text allowFontScaling={false} style={[style.textStyle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>DETAILERS</Text>
                                    </View>
                                }



                                {/* <View style={style.containerView}>
                            <TouchableOpacity onPress={this.navigateToScreen('Appointments')}>
                                <View style={style.BoxView}>
                                    <Image
                                        resizeMode='contain'
                                        style={{ height: 105, width: 105, }}
                                        source={require('../../assets/Icons/appointments.png')} />
                                </View>
                            </TouchableOpacity>
                            <Text allowFontScaling={false}  font={'TypoGroteskRounded-bold'} style={style.textStyle}>APPOINTMENTS</Text>
                        </View>


                        <View style={style.containerView}>
                            <TouchableOpacity onPress={this.navigateToScreen('Message')}>
                                <View style={style.BoxView}>
                                    <Image
                                        resizeMode='contain'
                                        style={{ height: 105, width: 105, }}
                                        source={require('../../assets/Icons/messages.png')} />
                                </View>
                            </TouchableOpacity>
                            <Text allowFontScaling={false}  font={'TypoGroteskRounded-bold'} style={style.textStyle}>MESSAGES</Text>
                        </View> */}

                                <View style={style.containerView}>
                                    <TouchableOpacity onPress={this.navigateToScreen('Appointments')}>
                                        <View style={style.BoxView}>
                                            <Image
                                                resizeMode='contain'
                                                style={style.popIcons}
                                                source={require('../../assets/Icons/appointments1.png')} />
                                            {this.state.appoinmentCount > 0 && (
                                                <View style={style.badge}>
                                                    <Text style={style.badgetxt}>{this.state.appoinmentCount}</Text>
                                                </View>
                                            )}

                                        </View>
                                    </TouchableOpacity>
                                    <Text style={[style.textStyle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>APPOINTMENTS</Text>
                                </View>


                                <View style={this.state.userdata.is_sub_detailer == 0 ? style.containerView : style.containerViewCenter}>
                                    <TouchableOpacity onPress={this.navigateToScreen('Messages')}>
                                        <View style={style.BoxView}>
                                            <Image
                                                resizeMode='contain'
                                                style={style.popIcons}
                                                source={require('../../assets/Icons/messages1.png')} />

                                            {this.state.messageCount > 0 && (
                                                <View style={style.badge}>
                                                    <Text style={style.badgetxt}>{this.state.messageCount}</Text>
                                                </View>
                                            )}

                                        </View>
                                    </TouchableOpacity>
                                    <Text style={[style.textStyle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>MESSAGES</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                
            :
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                 <ActivityIndicator size="large" color="#0000ff" />
                </View>
            }

                {((this.state.add_data != '') && (!this.state.comeToNotification)) &&
                    <View style={{ position: 'absolute', bottom: 0, height: 70, marginBottom: 20, width: Dimensions.get('window').width, }}>
                        <TouchableOpacity onPress={() => Linking.openURL(this.state.add_data.url)}
                            style={{ flex: 1, }}>
                            <Image
                                resizeMode='contain'
                                style={{ flex: 1, marginHorizontal: 10 }}
                                source={{ uri: Helper.getImageUrl(this.state.add_data.image) }}
                            />
                        </TouchableOpacity>
                    </View>
                }

            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 190,
        justifyContent: 'center',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    containerViewCenter: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width - 6
    },
    textStyle: {

        fontSize: 19,

    },
    popIcons: {
        height: 110, width: 110,
    },

    BoxView: {
        // backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5 + 10,
        alignItems: 'center', justifyContent: 'center'
    },
    badge: {
        position: 'absolute',
        top: '18%',
        left: '55%',
        backgroundColor: 'red',
        height: 20,
        minWidth: 20,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    badgetxt: {
        paddingHorizontal: 5,
        paddingBottom: 0,
        color: '#fff',
        fontSize: 15
    }


})