
import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet, TouchableOpacity, ScrollView, Button, TextInput, Platform,Modal } from 'react-native'
import { Input, CustomHeader, CardSection, AuthHeader, AuthTitle, HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import Swiper from 'react-native-swiper';
import Helper from '../Lib/Helper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNPickerSelect from 'react-native-picker-select';
import MapView, {
    ProviderPropType,
    Marker,
    AnimatedRegion,
} from 'react-native-maps';

import ImageP from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import CommanTollTip from "../common/CommonTollTip";
import ChatController from '../Lib/ChatController';
import Config from "../Lib/Config";

import { EventRegister } from 'react-native-event-listeners';
import Moment from "moment";

import CheckBox from "react-native-check-box";
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from "react-native-iap";


const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;

const itemSkus = Platform.select({
    ios: [
        "rinse_p01"
    ],
    android: [
        "rinse_p01"
    ]
});


class QuoteConfirmation extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={{ alignItems: 'center', minHeight: 80, maxHeight: 200 }}>

                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>Are you sure you want to submit this quote?</Text>


                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                        this.props.callApi2(this.props.id);
                    }}

                        style={{ flex: 1 }}
                    >

                        <LinearGradient
                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}


                        >

                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>

                        </LinearGradient>


                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    }}

                        style={{ flex: 1 }}
                    >

                        <LinearGradient
                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                            style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}
                        >

                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                        </LinearGradient>

                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}



class PhotosPopUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
            img_count: 1,
            imageData: this.props.photos
        }

        console.log(this.state.imageData);



    }



    // componentDidMount() {

    //     this.focusListener = this.props.navigation.addListener('didFocus', () => {
    //         var imageData = { ...this.state.imageData };
    //         var NewArray = [];
    //         for(var i=0; i<this.state.imageData.length;i++){

    //             var myImage = imageData[i].image;
    //             alert(this.imageWidth(Helper.getImageUrl(myImage)));
    //             NewArray.push({"image":myImage,width:this.imageWidth(Helper.getImageUrl(myImage)),height:this.imageHeight(Helper.getImageUrl(myImage))})

    //         }
    //         alert(JSON.stringify(NewArray));
    //         this.setState({ imageData:NewArray },() =>
    //             alert('yes')
    //             )
    //     })
    // }



    // componentWillUnmount() {
    //     this.focusListener.remove()
    // }





    // imageWidth(url){
    //     Image.getSize(url, (width, height) => {
    //         // calculate image width and height 
    //         screenWidth = Dimensions.get('window').width
    //         return parseInt(screenWidth);

    //     })
    // }

    // imageHeight(url){
    //     Image.getSize(url, (width, height) => {
    //         // calculate image width and height 
    //         const screenWidth = Dimensions.get('window').width
    //         const scaleFactor = width / screenWidth
    //         imageHeight = height / scaleFactor
    //         return parseInt(imageHeight);

    //     })
    // }

    // _renderImages = (items,index) => {

    //     var imageNewWidth='80%';
    //     var imageNewHeight='55%';
    //     Image.getSize(Helper.getImageUrl(items.image), (width, height) => {
    //             // calculate image width and height 
    //             const screenWidth = Dimensions.get('window').width
    //             const scaleFactor = width / screenWidth
    //             imageHeight = height / scaleFactor



    //     })


    //     return (
    //         <ImageP
    //         //style={{ width: Dimensions.get('window').width - 20, height: 500, alignSelf: 'center' }}
    //         style={{ width:imageNewWidth,height:imageNewHeight,alignSelf:'center',resizeMode:'contain' }}
    //         source={{uri: Helper.getImageUrl(items.image)}}
    //         indicator={ProgressBar} 
    //         //style={style.uploadImg}

    //     />
    //     )
    // }



    render() {
        // alert(JSON.stringify(this.state.imageData));
        return (
            <View style={{ width: Dimensions.get('window').width, height: 800, marginTop: 40 }}>

                <View style={{ flexDirection: 'row', marginTop: 0, padding: 20 }}>
                    <Text allowFontScaling={false} style={{ flex: 2, textAlign: 'right', color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }}>{this.state.img_count} of {this.state.imageData.length}
                    </Text>
                    <TouchableOpacity onPress={() => {
                        global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" });

                    }}
                        style={{ flex: 2 }}
                    >
                        <Text allowFontScaling={false} style={{ textAlign: 'right', color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }}>Done</Text>
                    </TouchableOpacity>
                </View>

                <Swiper showsButtons={false} autoplay={false} autoplayTimeout={2.5}
                    dotStyle={{ display: 'none', backgroundColor: '#ccc' }} loop={false}
                    activeDotStyle={{ display: 'none', }}
                    style={{ marginTop: 10 }}
                    onIndexChanged={(index) => {
                        this.setState({ img_count: index + 1 })
                    }}
                >

                    {this.state.imageData.map((items, index) => (
                        <View style={{ paddingHorizontal: 20, alignItems: 'center', width: '90%', height: '65%', alignSelf: 'center' }}>

                            {/* {this._renderImages(items, index)} */}
                            <Image
                                //style={{ width: Dimensions.get('window').width - 20, height: 500, alignSelf: 'center' }}
                                style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
                                source={{ uri: Helper.getImageUrl(items.image) }}
                                indicator={ProgressBar}
                            //style={style.uploadImg}

                            />
                        </View>
                    ))}






                </Swiper>

            </View>
        )
    }
}


const ProfileMsg = (props) => {
    return (
        <View style={{ alignItems: 'center', height: 140 }}>

            <Text allowFontScaling={false} style={{ fontSize: 18, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>You're almost there!</Text>

            <Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>To start sending quotes to customers, complete your profile.</Text>

            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    props.navigation.navigate('UpgradeDone');
                }}

                    style={{ flex: 2 }}
                >

                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}


                    >

                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 16 }} >Complete Profile</Text>

                    </LinearGradient>


                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                }}

                    style={{ flex: 1 }}
                >

                    <LinearGradient
                        colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                        style={{ padding: 6, margin: 2, alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 5, height: 40 }}
                    >

                        <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 16 }} >Cancel</Text>

                    </LinearGradient>

                </TouchableOpacity>
            </View>
        </View>
    )
}




class DetailerMsg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            DetailersData: [],
            select_detailer: null,
            Userdata: this.props.Userdata,


        }



        this.getAllDetailers();

    }


    getAllDetailers = () => {

        let form = {
            page: -1,
            is_password_reset: 1
        }
        Helper.makeRequest({ url: "get-sub-detailers", loader: false, method: "POST", data: form }).then((responsedata) => {
            if (responsedata.status == 'true') {
                var data = responsedata.data.data;
                //alert(JSON.stringify(responsedata.data.data));
                var newArray = [];
                data.map((items, index) =>
                    newArray.push({ label: items.first_name + ' ' + items.last_name, value: items.id })
                )
                this.setState({ DetailersData: newArray })

            }
            else {

                //Helper.showToast(responsedata.message);
            }
        })
    }

    QuoteConfirmation = () => {
        // if(this.state.select_detailer == null){
        //     Helper.alert('Please select a detailer');
        //     return false;

        // }
        // else{
        global.state['modal'].setState({ modalVisible: false, Msg: "" });

        // Helper.confirm('Do you want to send this quote?',(status)=>{
        //     if(status){
        this.props.callApi(this.state.select_detailer, 1);
        //     }
        // })
        // }
    }




    render() {
        return (
            <View style={{ alignItems: 'center', minHeight: 130, maxHeight: 200 }}>

                <Text allowFontScaling={false} style={{ fontSize: 18, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>Which Detailer?</Text>

                <View style={{ flex: 1 }}>
                    <View style={{ borderWidth: 1, borderColor: '#e9e9e9', borderRadius: 5, width: Dimensions.get('window').width - 110, height: 40, justifyContent: 'center', paddingTop: 10, paddingLeft: 5, marginTop: 10 }}>
                        <View>
                            <RNPickerSelect
                                placeholder={{ label: this.state.Userdata.first_name + ' ' + this.state.Userdata.last_name, value: null }}
                                items={this.state.DetailersData}
                                onValueChange={(value) => {
                                    this.setState({
                                        select_detailer: value,
                                    });
                                }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}


                                placeholderTextColor="rgb(129,129,129)"
                                placeholderfontSize='40'
                                itemStyle={{ opacity: 0.5 }}

                                Icon={() => {
                                    return (
                                        <Image
                                            resizeMode='contain'
                                            source={require("../../assets/Icons/dropdown_ico.png")}
                                            style={{ width: 10, height: 10, marginTop: 15, marginRight: 15 }}
                                        />
                                    );
                                }}

                            />

                        </View>
                    </View>
                </View>
                {/* <TextInput placeholder="select Detailer" style={{borderWidth:1,borderColor:'#ccc',padding:5,width: Dimensions.get('window').width-110,marginTop:20}} /> */}
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>


                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    }}

                        style={{ flex: 1 }}
                    >

                        <LinearGradient
                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                            style={{ padding: 8, margin: 5, alignItems: 'center' }}
                        >

                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>

                        </LinearGradient>

                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {

                        this.QuoteConfirmation();
                    }}
                        style={{ flex: 1, paddingRight: 10, paddingTop: 5 }}
                    >
                        {/* <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 8, margin: 5, alignItems: 'center' }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >Next</Text>

                    </LinearGradient> */}
                        <Image source={require('../../assets/Icons/next.png')} style={{ height: 40, width: 40, justifyContent: 'flex-end', alignSelf: 'flex-end' }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const OverlapMsg = () => {
    return (
        <View style={{ height: 120, marginLeft: -10, marginRight: -10, marginTop: -5 }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}

            >
                <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} />
            </TouchableOpacity>

            <View style={{ padding: 10 }}>
                <Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>You have an overlapping appointment with this detailer during this time.</Text>


                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                }}
                    style={{ flex: 2, alignItems: 'center', }}
                >
                    <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 8, margin: 5, alignItems: 'center', width: 120 }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >OK</Text>
                    </LinearGradient>


                </TouchableOpacity>

            </View>
        </View>
    )
}


const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}




export default class SendQuote extends Component {
    static navigationOptions = { header: null }
    constructor(props) {
        super(props);

        this.state = {
            ProfileMsg: <ProfileMsg navigation={this.props.navigation} />,
            DetailerMsg: <DetailerMsg />,
            QuoteConfirmation: <QuoteConfirmation />,
            OverlapMsg: <OverlapMsg />,
            PhotosPopUp: <PhotosPopUp navigation={this.props.navigation} />,
            req_id: this.props.navigation.getParam('req_id', 0),
            //requestData: this.props.navigation.getParam('req_data', {}),
            requestData: {},
            coordinate: {},
            profile_completed: 0,
            quotes: 0,
            quote_value: 0,
            marginBottom: 0,
            AppUserData: {},
            hidecontent: true, QuoteButtonShow: false,
            PhotosAvailable: false,
            isChecked: false,
            productList: [],
            assignDetailerId: 0,
            modalSponsVisible: false,
            IosTransID:'',
            receipt:''

        }


    }




    async componentDidMount() {
        
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    //this.props.navigation.navigate('LoginNew');
                } else {
                    this.setState({ AppUserData: responseData });

                }
            })
            
            this.getRequestDetails();
            this.getItems()
        })


        purchaseUpdateSubscription = purchaseUpdatedListener(
            async (purchase) => {
                const receipt = purchase.transactionReceipt;
                if (receipt) {
                    console.log(receipt, 'receiptreceipt')
                    try {
                        if (Platform.OS === 'ios') {
                            this.setState({IosTransID:purchase.transactionId})
                          finishTransactionIOS(purchase.transactionId);
                        } else if (Platform.OS === 'android') {
                          // If consumable (can be purchased again)
                          consumePurchaseAndroid(purchase.purchaseToken);
                          // If not consumable
                          //acknowledgePurchaseAndroid(purchase.purchaseToken);
                        }
                        const ackResult = await finishTransaction(purchase);
                    } catch (ackErr) {
                        console.warn('ackErr', ackErr);
                    }

                    this.setState({ receipt }, () => this.goNext());
                }
            },
        );

        purchaseErrorSubscription = purchaseErrorListener(
            (error) => {
                console.log('purchaseErrorListener', error);
                //alert('purchase error'+JSON.stringify(error));
            },
        );

    }



    /** Sponsed InApp Code start here */

    setModalVisible(visible) {

        this.setState({ modalSponsVisible: visible });
    }

    getItems = async () => {
        try {
            const products = await RNIap.getProducts(itemSkus);
            // const products = await RNIap.getSubscriptions(itemSkus);
            console.log('Products', products);
            this.setState({ productList: products });
        } catch (err) {
            console.warn(err.code, err.message);
        }
    };


    requestPurchase = async (sku) => {
        if (sku == 'rinse_p01') {
            try {
                RNIap.requestPurchase(sku);
            } catch (err) {
                console.warn(err.code, err.message);
            }
        }

    };


    modalSponserRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalSponsVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "#ffffff", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>

                            
                                <View style={{ minHeight: 100, maxHeight: 400, width: DeviceW - 150 }}>
                                    <TouchableOpacity
                                        onPress={() => this.setModalVisible(false)}
                                        style={{ margin: -10, padding: 10 }}
                                    >
                <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                                        
                                        {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                                    </TouchableOpacity>
                                   
                                   <View style={{alignItems:'center',marginTop:20}}>
                                  
                                    <Text style={{fontSize:22}}>Sponser Fee : <Text style={{color:'green'}}>${this.state.requestData.sponsored_amount}</Text></Text>

                                    
                                

                                       </View>
                                       <View style={{marginTop:40}}>
                                       <CommonButton name="Pay Now" background={"#5aa641"} color="#ffffff"  onPress={() => this._callSponserPay()} />

                                       </View>
                                </View>
                            

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }


    _callSponserPay(){
        this.setModalVisible(false);
        this.requestPurchase('rinse_p01')
    }
    goNext() {

        if(Platform.OS == 'android'){
            var JsonReceipt =  JSON.parse(this.state.receipt);
        
            var form = {
                transaction_id:JsonReceipt.orderId,
                receipt:JSON.stringify(this.state.receipt),
                payment_time:(new Date('Y-m-d H:i:s')),
                platform:Platform.OS,
                service_request_id: this.state.requestData.request_id
                

            }
        }
        if(Platform.OS == 'ios'){
            var form = {
                transaction_id:this.state.IosTransID,
                receipt:this.state.receipt,
                payment_time:(new Date('Y-m-d H:i:s')),
                platform:Platform.OS,
                service_request_id: this.state.requestData.request_id
                

            }
        }
        

        if(form.plan_id != ''){
            Helper.makeRequest({ url: "purchase-sponsored", data: form }).then((data) => {
                //alert(JSON.stringify(data));
                if (data.status == 'true') {
                    //this.setState({description:''})
                    setTimeout(() => {
                        this.callApi3(this.state.assignDetailerId)
                    }, 1000)
                    

                    Helper.showToast(data.message)
                    Helper.hideLoader()
                    //this.props.navigation.goBack();
                }
                else {
                    var quotes_new = this.state.quotes;
                    this.getRequestDetails();
                    setTimeout(() => {
                        this.setState({quotes:quotes_new})
                    }, 2000)
                    Helper.showToast(data.message)
                    Helper.hideLoader()
                }
            })
        }

        
    }

    /** Sponsed InApp Code end here */



    /* Bootom Tab Budget event */

    getBadegesCount(id) {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            user_id: this.state.AppUserData.id,
            role_id: 2,
            detailer_request_id: id,
            utc_date_time: gettime

        }
        ChatController.getAppoinmentMessageCount('get_appointment_request_count_read', form);

    }


    componentWillMount() {
        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {

                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;

                this.props.navigation.setParams({ badgeCount: 123 })
            } else {
                //this.setState({ isLoader: false });
            }

        })

    }


    componentWillUnmount() {
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()

        if (purchaseUpdateSubscription) {
            purchaseUpdateSubscription.remove();
            purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
            purchaseErrorSubscription.remove();
            purchaseErrorSubscription = null;
        }
    }



    getRequestDetails = () => {

        //console.log(JSON.stringify(this.state.registerform));


        Helper.makeRequest({ url: "service-request-detail", data: { service_request_id: this.state.req_id } }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {

                this.setState({ 'requestData': data.data });
                if (data.data.detailer_service_quote.hasOwnProperty('quote')) {
                    if (data.data.detailer_service_quote.quote != '') {
                        this.setState({ 'quote_value': data.data.detailer_service_quote.quote });
                        this.setState({ QuoteButtonShow: false })
                    }

                } else {
                    this.setState({ QuoteButtonShow: true })
                }



                if (this.state.quote_value == 0) {
                    this.setState({ 'marginBottom': 50 });
                }

                let getOtherAddress;

                // if (this.state.requestData.service_request.location.city != '' && this.state.requestData.service_request.location.city != null) {
                //     getOtherAddress = this.state.requestData.service_request.location.city;
                // }
                // else if (this.state.requestData.service_request.location.state != '' && this.state.requestData.service_request.location.state != null) {
                //     getOtherAddress = this.state.requestData.service_request.location.state;
                // } else if (this.state.requestData.service_request.location.zip_code != '' && this.state.requestData.service_request.location.zip_code != null) {
                //     getOtherAddress = this.state.requestData.service_request.location.zip_code;
                // }
                // else if (this.state.requestData.service_request.location.location != '' && this.state.requestData.service_request.location.location != null) {
                //     getOtherAddress = this.state.requestData.service_request.location.location;
                // }
                //alert(getOtherAddress);
                if (getOtherAddress) {
                    this.getotherLatLong(getOtherAddress);
                } else {
                    const newCoordinate = {
                        latitude: parseFloat(this.state.requestData.service_request.location.latitude),
                        longitude: parseFloat(this.state.requestData.service_request.location.longitude),
                        latitudeDelta: 0.012,
                        longitudeDelta: 0.012,
                    };

                    setTimeout(() => {
                        this.setState({ 'coordinate': newCoordinate });
                    }, 500);
                }



                //alert(JSON.stringify(this.state.requestData));





                var profile_completed = (parseFloat(this.state.requestData.business_address) + parseFloat(this.state.requestData.profile_photo)
                    + parseFloat(this.state.requestData.payout_account) + parseFloat(this.state.requestData.offer_service))

                this.setState({ 'profile_completed': profile_completed });
                //this.state.requestData.business_address
                //profile_photo
                //payout_account
                //offer_service

                this.getBadegesCount(data.data.id)


                this._onPhotosButton()

            }
            else {
                Helper.showToast(data.message)
            }
        })

    }


    _onSendPress = () => {


        if (this.state.AppUserData.is_sub_detailer == 1) {
            if (this.state.quotes < 1) {
                Helper.showToast('Please enter valid quote value');
                return false;
            }
            else if (this.state.quotes.length > 6) {
                Helper.showToast('Quote value should be smaller than or equal to 6 digit');
                return false;
            }
            else if (this.state.quotes < Number(this.state.requestData.minimum_quote_amount)) {
                Helper.showToast('Quote value should be greater than $' + this.state.requestData.minimum_quote_amount + '.');
                return false;
            }
            else {

                this.callApi(this.state.AppUserData.id);
            }

            // else if((/^(?=.)([+-]?([1-9]*)(\.([0-9]+))?)$/.test(this.state.quotes) || this.state.quotes == '') ){
            //     Helper.showToast('Please enter valid quote value');
            //     return false;
            // }
        }
        else {
            if (this.state.profile_completed == 4) {
                if (this.state.quotes < 1) {
                    Helper.showToast('Please enter valid quote value');
                    return false;
                }
                else if (this.state.quotes.length > 6) {
                    Helper.showToast('Quote value should be smaller than or equal to 6 digit');
                    return false;
                }
                else if (this.state.quotes < Number(this.state.requestData.minimum_quote_amount)) {
                    Helper.showToast('Quote value should be greater than $' + this.state.requestData.minimum_quote_amount + '.');
                    return false;
                }
                else {
                    this._onSelectDetailer();
                }

                // else if(!(/^(?=.)([+-]?([1-9]*)(\.([0-9]+))?)$/.test(this.state.quotes) || this.state.quotes == '') ){
                //     Helper.showToast('Please enter valid quote value');
                //     return false;
                // }

            }
            else {
                global.state['modal'].setState({ modalVisible: true, Msg: this.state.ProfileMsg })
            }
        }




    }
    _onSelectDetailer = () => {

        this.setState({ 'DetailerMsg': <DetailerMsg callApi={this.callApi} Userdata={this.state.AppUserData} /> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.DetailerMsg })
        });

        //global.state['modal'].setState({ modalVisible: true, Msg: this.state.DetailerMsg })
    }

    _onOverlapPress = () => {

        global.state['modal'].setState({ modalVisible: true, Msg: this.state.OverlapMsg })
    }

    _onPhotosPopUp = () => {

        var VichleAllPhotos = [];
        this.state.requestData.service_request.vehicles.map((item, index) => {
            if (item.vehicle.vehicle_images.length > 0) {
                item.vehicle.vehicle_images.map((itemV, indexV) => {
                    if (itemV.image != '') {
                        VichleAllPhotos.push({ 'image': itemV.image });

                    }

                })

            }

            if (item.hasOwnProperty('vehicle_service_images')) {
                if (item.vehicle_service_images.hasOwnProperty('image_1') && item.vehicle_service_images.image_1 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_1 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_2') && item.vehicle_service_images.image_2 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_2 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_3') && item.vehicle_service_images.image_3 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_3 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_4') && item.vehicle_service_images.image_4 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_4 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_5') && item.vehicle_service_images.image_5 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_5 });
                }

            }

        })






        if (VichleAllPhotos.length > 0) {


            this.setState({ 'PhotosPopUp': <PhotosPopUp photos={VichleAllPhotos} navigation={this.props.navigation} /> }, () => {
                global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.PhotosPopUp })
            });
        } else {
            Helper.showToast('Photos not available');
            return false;
        }


    }

    _onPhotosButton = () => {

        var VichleAllPhotos = [];
        this.state.requestData.service_request.vehicles.map((item, index) => {
            if (item.vehicle.vehicle_images.length > 0) {
                item.vehicle.vehicle_images.map((itemV, indexV) => {
                    if (itemV.image != '') {
                        VichleAllPhotos.push({ 'image': itemV.image });

                    }

                })

            }

            if (item.hasOwnProperty('vehicle_service_images')) {
                if (item.vehicle_service_images.hasOwnProperty('image_1') && item.vehicle_service_images.image_1 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_1 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_2') && item.vehicle_service_images.image_2 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_2 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_3') && item.vehicle_service_images.image_3 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_3 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_4') && item.vehicle_service_images.image_4 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_4 });
                }
                if (item.vehicle_service_images.hasOwnProperty('image_5') && item.vehicle_service_images.image_5 != '') {
                    VichleAllPhotos.push({ 'image': item.vehicle_service_images.image_5 });
                }

            }

        })






        if (VichleAllPhotos.length > 0) {
            this.setState({ PhotosAvailable: true })


        } else {
            this.setState({ PhotosAvailable: false })
        }


    }



    callApi = (id) => {
        //alert(id);


        this.setState({ 'QuoteConfirmation': <QuoteConfirmation callApi2={this.callApi2} id={id} /> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.QuoteConfirmation })
        });


    }


    callApi2 = (id) => {
        //alert(id);

        // if(Platform.OS == 'ios'){
        //     var loader=false;
        // }
        // else{
        //     var loader=true;
        // }

        if (this.state.isChecked) {
            this.setState({ assignDetailerId: id }, () => {
                this.setModalVisible(true);
                 //this.requestPurchase('rinse_p01')
            })

        } else {
            this.callApi3(id)
        }


    }

    callApi3 = (id) => {
        Helper.makeRequest({ url: "quote-request", data: { detailer_id: id, quote: this.state.quotes, request_id: this.state.requestData.id, service_id: this.state.requestData.request_id,is_sponsored:this.state.isChecked } }).then((data) => {
            if (data.status == 'true') {
                this.QuoteEventSend()
                this.setState({ 'quote_value': this.state.quotes, QuoteButtonShow: false });
                Helper.showToast(data.message)

                let backdata = {
                    refresh: true
                }

                EventRegister.emit('requestTabBarEvent', backdata);
                Helper.hideLoader()
            }
            else {

                Helper.showToast(data.message)
                Helper.hideLoader()
            }
            
        })
    }


    QuoteEventSend() {
        Helper.hideLoader()
        let form = {
            other_user_id: this.state.requestData.user.id,
            //request_id:this.state.requestData.request_id,
            role_id: 2
        }
        ChatController.getAppoinmentMessageCount('send_service_request_notification_to_user', form);

    }

    getServiceTime(sItem, sIndex) {
        if (sItem.service.average_time_from == '' || sItem.service.average_time_to == '') {
            return (sIndex == 0) ? sItem.service.service : '\n' + sItem.service.service
        }
        else {
            return (sIndex == 0) ? sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)' : '\n' + sItem.service.service + '(' + sItem.service.average_time_from + '-' + sItem.service.average_time_to + 'mins)'
        }

    }

    getotherLatLong(state) {

        let form;
        let varheaders;
        let finalUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + state + '&key=' + Config.google_place_api_key;
        varheaders = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }

        console.log(finalUrl, 'finalUrlfinalUrlfinalUrlfinalUrl')
        return fetch(finalUrl, {
            body: form,
            method: 'GET',
            headers: varheaders,
        })
            .then((response) => {

                console.log(response, "response")
                return response.json()
            })
            .then((responseJson) => {
                let NewResData = responseJson.results[0];
                console.log(NewResData.geometry.location, "responseJson")

                const newCoordinate = {
                    latitude: parseFloat(NewResData.geometry.location.lat),
                    longitude: parseFloat(NewResData.geometry.location.lng),
                    latitudeDelta: 0.012,
                    longitudeDelta: 0.012,
                };

                setTimeout(() => {
                    this.setState({ 'coordinate': newCoordinate });
                }, 500);
            })
            .catch((error, a) => {

                console.log(error);
            });
    }


    render() {

        return (

            <View style={{ flex: 1, }}>
                {this.modalSponserRender()}
                <CustomHeader navigation={this.props.navigation} userbar bArrow />
                <HeaderTitle TitleName='Requests' />

                {this.state.requestData.user && (

                    <View style={{ flex: 1 }}>
                        <KeyboardAwareScrollView style={{ marginBottom: this.state.marginBottom }} extraScrollHeight={Platform.OS == 'ios' ? 70 : 170} enableOnAndroid={true}>

                            <View style={{ height: 200, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                                {/* <Image
                            style={{ width: '100%', height: 200 }}
                            source={require('../../assets/Icons/map.png')}
                        /> */}
                                {this.state.coordinate.longitudeDelta == 0.012 && (


                                    <MapView
                                        initialRegion={this.state.coordinate}
                                        style={style.map}
                                        //cacheEnabled={false}
                                        liteMode
                                    >
                                        {/* <Marker
                                            coordinate={this.state.coordinate}
                                        /> */}
                                        <MapView.Circle
                                            center={this.state.coordinate}
                                            radius={500}
                                            strokeWidth={2}
                                            strokeColor="#5aa641"
                                            fillColor="rgba(0,255,0,0.2)"
                                        />
                                    </MapView>
                                )}
                                {this.state.coordinate.longitudeDelta != 0.012 && (
                                    <Text>Map Loading..</Text>
                                )}
                            </View>

                            <View style={{ marginTop: 0 }}>

                                <View style={[CommanStyle.viewcardDiv, { padding: 10, marginBottom: 20 }]}>
                                    <View style={{ flex: 4 }}>


                                        <Text allowFontScaling={false} style={CommanStyle.normaltext}>
                                            <Text allowFontScaling={false} font={'CenturyGothic'}>{this.state.requestData.user.first_name} {this.state.requestData.user.last_name}</Text>
                                            {/* <Text allowFontScaling={false} font={'CenturyGothic'}> {"\n"}{this.state.requestData.service.service}</Text> */}
                                            <Text allowFontScaling={false} font={'CenturyGothic'}> {"\n"}
                                                {(this.state.requestData.service_request.location.city == '' && this.state.requestData.service_request.location.state == '' && this.state.requestData.service_request.location.zip_code == '') ? '' : this.state.requestData.service_request.location.city + ', ' + this.state.requestData.service_request.location.state + ' ' + this.state.requestData.service_request.location.zip_code}
                                            </Text>
                                            {/* <Text allowFontScaling={false} font={'CenturyGothic'}> {"\n"}Vechicle(s): Z</Text> */}
                                            <Text allowFontScaling={false} font={'CenturyGothic'}> {"\n"}{Helper.getDateNewFormate(this.state.requestData.service_request.service_utc_date_time)} </Text>
                                        </Text>


                                    </View>
                                    <View style={{ flex: 2 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignContent: 'center', alignItems: 'center', textAlign: 'center', paddingTop: '25%', paddingLeft: '10%' }}>
                                            <Text allowFontScaling={false} style={{ fontFamily: 'Aileron-Regular' }} >{Helper.getDistanceMile(this.state.requestData.distance_diff)}mi</Text>
                                            <CommanTollTip tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />

                                        </View>

                                    </View>

                                </View>
                                {this.state.hidecontent === false &&
                                    <View>
                                        <View style={[CommanStyle.viewcardDiv, { padding: 10 }]}>
                                            <View style={{ flex: 1 }}>
                                                {this.state.requestData.service_request.vehicles.map((vmItem, vmindex) =>
                                                    <View style={{ flex: 1, paddingVertical: 5, marginBottom: 10 }}>
                                                        <View >
                                                            <Text style={{ fontWeight: 'bold' }}>{vmItem.vehicle.model_detail.name}</Text>
                                                            <Text style={{ fontWeight: 'bold' }}>Color: {vmItem.vehicle.color}</Text>
                                                            <Text style={{ fontWeight: 'bold' }}>Vehicle Year:  {vmItem.vehicle.model_year}</Text>

                                                        </View>
                                                        <View >
                                                            <Text>
                                                                {vmItem.services.map((sItem, sIndex) =>
                                                                    this.getServiceTime(sItem, sIndex)

                                                                )}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                )}


                                            </View>

                                        </View>
                                        {this.state.requestData.service_request.comment != '' && (
                                            <View style={{ padding: 10, marginBottom: 10 }}>
                                                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Additional Info:</Text>
                                                <Text>{this.state.requestData.service_request.comment}</Text>
                                            </View>
                                        )}

                                        <View style={[CommanStyle.viewcardDiv, { padding: 10, marginTop: -15 }]}>
                                            <TouchableOpacity style={{ flexDirection: 'row' }}
                                                onPress={() => { this.setState({ hidecontent: !this.state.hidecontent }) }}
                                            >
                                                <Text allowFontScaling={false} style={[CommanStyle.normaltext, { color: '#4684da', fontFamily: 'Aileron-Regular' }]}>Hide</Text>

                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                }

                                {this.state.hidecontent === true &&
                                    <View style={[CommanStyle.viewcardDiv, { padding: 10, marginTop: -15 }]}>
                                        <TouchableOpacity style={{ flexDirection: 'row' }}
                                            onPress={() => { this.setState({ hidecontent: !this.state.hidecontent }) }}
                                        >
                                            <Text allowFontScaling={false} style={[CommanStyle.normaltext, { color: '#4684da', fontFamily: 'Aileron-Regular' }]}>See More</Text>

                                        </TouchableOpacity>
                                    </View>
                                }


                                <View style={{ flexDirection: 'row', marginTop: 0, padding: 10, paddingTop: 0 }}>
                                    <View style={{ flex: 2, flexDirection: 'column' }}>
                                        <View >
                                            <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext}>Avg. Quote Received: ${this.state.requestData.average_quote}</Text>
                                        </View>
                                        {this.state.quote_value == 0 && (
                                            <View>
                                                <View style={[CommanStyle.margin5Button, { flex: 1, flexDirection: 'row', marginTop: 25 }]}>

                                                    <View style={{ flex: 1 }}>
                                                        <Text allowFontScaling={false} style={[CommanStyle.normaltext, { marginTop: 6, fontSize: 19, fontFamily: 'Aileron-Regular' }]}>$</Text>
                                                    </View>
                                                    <View style={{ flex: 5 }}>
                                                        <TextInput
                                                            keyboardType="number-pad"
                                                            returnKeyType='done'
                                                            placeholder='Enter your quote here'
                                                            style={{ borderWidth: 1, borderColor: '#ccc', padding: 5, height: 37 }}

                                                            value={this.state.quotes}
                                                            onChangeText={(txt) => { this.setState({ quotes: txt }) }}
                                                        />
                                                    </View>
                                                    <View style={{ flex: 1 }}></View>
                                                </View>
                                                {this.state.requestData.sponsored_by == 0 &&
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>

                                                        <CheckBox
                                                            style={{ flex: 1, }}
                                                            onClick={() => {
                                                                this.setState({
                                                                    isChecked: !this.state.isChecked
                                                                });
                                                            }}
                                                            isChecked={this.state.isChecked}
                                                            rightText={"If you want to sponsor this quote"}
                                                            rightTextStyle={{ color: "black", fontSize: 14, fontWeight: '500' }}
                                                            checkedImage={
                                                                <Image
                                                                    source={require("../../assets/Icons/tick.png")}
                                                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                                />
                                                            }
                                                            unCheckedImage={
                                                                <Image
                                                                    source={require("../../assets/Icons/untick.png")}
                                                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                                />
                                                            }
                                                        />


                                                    </View>

                                                </View>
                                                }
                                                

                                            </View>
                                        )}
                                        {this.state.quote_value > 0 && (
                                            <View>
                                            <Text>Your Quote: $ {this.state.quote_value}</Text>
                                            
                                            {((this.state.requestData.sponsored_by == this.state.AppUserData.id) || (this.state.isChecked)) &&
                                                <View>
                                                    <Text style={{color:'green',fontWeight:'500'}}>This quote sponsored by you</Text>
                                                </View>
                                            }
                                            </View>
                                        )}
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <View >

                                            <CommonButton name="View Photos" background={(this.state.PhotosAvailable) ? "#4684da" : "#5c5c5c"} color="#ffffff" onPress={this._onPhotosPopUp} />


                                        </View>
                                        <View >

                                            <CommonButton name="Message" background="#4684da" color="#ffffff" style={CommanStyle.margin5Button} onPress={() => this.props.navigation.navigate('Chat', { 'service_request_id': this.state.requestData.request_id, 'other_user_id': this.state.requestData.user.id, 'user_id': this.state.AppUserData.id, 'username': this.state.requestData.user.first_name + ' ' + this.state.requestData.user.last_name, 'country_code': this.state.requestData.user.country_code, 'mobile': this.state.requestData.user.mobile })} />

                                        </View>
                                    </View>



                                </View>

                            </View>



                        </KeyboardAwareScrollView>
                    </View>
                )}

                {this.state.QuoteButtonShow == true && (
                    <TouchableOpacity
                        onPress={this._onSendPress}
                        //onPress={this._onSelectDetailer}
                        style={{ position: 'absolute', width: '100%', bottom: 0 }}
                    >
                        <LinearGradient



                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            style={{ padding: 10, alignItems: 'center', height: 50 }}


                        >

                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                                SEND QUOTE
  </Text>
                        </LinearGradient>
                    </TouchableOpacity>
                )}






            </View>
        )
    }
}


export class RequestQuote extends Component {
    static navigationOptions = { header: null }


    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation} userbar />
                <AuthTitle TitleName='Requests' />
                <ScrollView>
                    <View>
                        <Image
                            style={{ width: '100%', height: 200 }}
                            source={require('../../assets/Icons/map.png')}
                        />
                    </View>

                    <View style={{ marginTop: 10 }}>

                        <View style={[CommanStyle.viewcardDiv, { padding: 10 }]}>
                            <View style={{ flex: 3 }}>


                                <Text allowFontScaling={false} style={CommanStyle.normaltext}>

                                    <Text allowFontScaling={false}  >Brandon Fields</Text>
                                    <Text allowFontScaling={false}  > {"\n"}Inside/ Outside</Text>
                                    <Text allowFontScaling={false}  > City/ST./Zip: Tampa FI 33610</Text>
                                    <Text allowFontScaling={false}  > Vechicle(s):Z</Text>
                                    <Text allowFontScaling={false}  > Date:Tuesday, April 23rd @2:45 pm </Text>
                                </Text>
                                <View >
                                    <TouchableOpacity style={{ flexDirection: 'row' }}>
                                        <Text allowFontScaling={false} style={[CommanStyle.normaltext, { color: '#4684da', fontFamily: 'Aileron-Regular' }]}>See More</Text>
                                        {/* <Icon font={'Aileron-Regular'} name="angle-double-down" size={15} color="#4684da" style={{marginTop:6,marginLeft:5}} /> */}
                                    </TouchableOpacity>
                                </View>

                            </View>
                            <View style={{ flex: 2, bottom: 5, justifyContent: 'flex-end' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Text allowFontScaling={false}  >2.4mi</Text>
                                    <CommanTollTip tooltiptext={'This is the approximate distance from \nyour business address to the customers \nservice location.'} />

                                </View>

                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 15, padding: 10 }}>
                            <View style={{ flex: 2, flexDirection: 'column' }}>
                                <View >
                                    <Text allowFontScaling={false} style={CommanStyle.normaltext}>Avg. Quote Received: $35</Text>
                                </View>
                                <View style={[CommanStyle.margin5Button]}>
                                    <Text allowFontScaling={false} style={CommanStyle.normaltext}>Your Quote : $40</Text>
                                </View>

                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={CommanStyle.margin5Button}>

                                    <Button title="View Photos" color="#4684da" style={CommanStyle.margin5Button} />

                                </View>
                                <View style={CommanStyle.margin5Button}>

                                    <Button title=" Message " color="#4684da" style={CommanStyle.margin5Button} />

                                </View>
                            </View>



                        </View>

                    </View>



                </ScrollView>

            </View>
        )
    }
}


const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: '#ffffff'
    },
    uploadImg: {
        width: 150 * 2,
        height: 220 * 2,
        //resizeMode:'contain',
        alignSelf: 'center'
    }


})




const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 5,
        height: 40,
        color: '#000',
        paddingRight: 30, // to ensure the text is never behind the icon
        borderRadius: 5,
        width: '100%',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 10,
        justifyContent: 'center',



    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 5,
        paddingVertical: 8,
        height: 40,
        color: '#000',
        paddingRight: 30, // to ensure the text is never behind the icon
        width: '100%',

        borderRadius: 5,
        alignItems: 'center',
        alignSelf: 'center',

        marginBottom: 10,
        justifyContent: 'center'

    },
});