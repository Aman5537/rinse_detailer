
import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Button } from 'react-native'
import { CustomHeader, CardSection, AuthHeader, AuthTitle,HeaderTitle } from '../common';
import CommanStyle from '../config/Styles';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import LinearGradient from 'react-native-linear-gradient';

const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}


export default class CanceledAppointment extends Component {
    static navigationOptions = { header: null }
    // static navigationOptions = {
    //     header: null

    //     // headerVisible: false,
    //     // headerStyle: { backgroundColor: '#22578d' },
    //     // headerLeft: <BackArrrow />,
    //     // headerRight: <Bar />
    // };


    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation} userbar bArrow/>
                <HeaderTitle TitleName='Appointments' />
                <ScrollView>
                    <View>
                        <Image
                            style={{ width: '100%', height: 200 }}
                            source={require('../../assets/Icons/map.png')}
                        />
                    </View>

                    <View style={{ padding: 15,marginTop:10 }}>
                        <View>
                            <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle, { color: 'red',fontFamily: 'Aileron-Regular' }]}>Canceled Appointment</Text>
                        </View>

                        <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle,{fontWeight:'bold',marginTop:18,fontFamily: 'Aileron-Regular'}]}>Tomorrow, April 26th @ 10:00pm</Text>

                        <View style={[CommanStyle.viewcardDiv,{marginLeft:-6}]}>
                            <View style={{ flex: 3 }}>


                                <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle]}>Jerome King</Text>
                                <Text  allowFontScaling={false}   style={CommanStyle.normaltext}>
                                    2006 Nisaan CTS
                                    <Text  allowFontScaling={false}   > {"\n"}Outside (30-45min)</Text>
                                    <Text  allowFontScaling={false}   > {"\n"}Detailer - Larry Berry</Text>
                                    <Text  allowFontScaling={false}   > {"\n"}5667 Fotest Ln. Tampa FL</Text>
                                </Text>
                                
                            </View>
                            <View style={{ alignItems: 'center', flex: 1 }}>
                                <Text  allowFontScaling={false}   style={{ color: 'red', fontSize: 20, fontWeight: 'bold' }}>$35</Text>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
                                    <Text style={{fontFamily: 'Aileron-Regular'}}>2.4mi</Text>
                                    
                                </View>

                                
                            </View>
                        </View>

                        
                    </View>

                    

                </ScrollView>
                
            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },



})


