import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { CustomHeader ,CardSection, AuthHeader,AuthTitle,HeaderTitle} from '../common';
import  CommanStyle  from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';

class PermissionMsg extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
       
    }


    render(){
        return (
            <View style={{ height: 120, marginLeft: -10, marginRight: -10, marginTop: -5 }}>
                
                <View style={{ padding: 10 }}>
                    <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", marginTop: 10, fontFamily: 'Aileron-Regular' }}>You don’t have permission to use this feature. Contact the account holder.</Text>
    
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => {
                            global.state['modal'].setState({ modalVisible: false, Msg: "" });
                            this.props.navigation.navigate('SetHome');
                            
                        }}
                            style={{ flex: 1 }}
                        >
    
                            <LinearGradient
                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}
    
    
                            >
    
                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >OK</Text>
    
                            </LinearGradient>
    
    
                        </TouchableOpacity>
    
                       
                    </View>
                </View>
            </View>
        )
    }
    
}


export default class NotQuotePermission extends Component {
    static navigationOptions = { header: null }
    
    constructor(props) {
        super(props);
        this.state = {
            PermissionMsg: <PermissionMsg navigation={this.props.navigation} />,
            
        }

    }


    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.PermissionMsg })
        })
    }

    
    componentWillUnmount() {
        this.focusListener.remove()
    }


    
    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation}  userbar bArrow/>
                
            </View>
        )
    }
}

const style = StyleSheet.create({
    textArea: {
        
    }
    


})


