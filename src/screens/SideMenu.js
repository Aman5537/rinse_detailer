import PropTypes from 'prop-types';
import React, { Component ,} from 'react';
import { NavigationActions,DrawerActions } from 'react-navigation';
import { ScrollView, Text, View,Image ,TouchableOpacity,SafeAreaView,Platform} from 'react-native';
import { Footer } from '../common';
import Helper from '../Lib/Helper';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ChatController from '../Lib/ChatController';
import Rate, { AndroidMarket } from 'react-native-rate'

// const LogOut = (props) => {
class LogOut extends Component {

    constructor(props) {
        super(props);
        
    }

    logoutFun = () => {
        let token = Helper.getData('token');

        if(Platform.OS == 'ios'){
            var loader=false;
        }
        else{
            var loader=true;
        }

        Helper.makeRequest({ url: "logout", data: {device_type:Helper.device_type,device_token:Helper.device_token},loader:loader}).then((logoutdata) => {
            // console.log('----logindata :  ' + JSON.stringify(logindata));
            if (logoutdata.status == 'true') {
                
                
            }
            else {
                //Helper.showToast(logoutdata.message)
            }
            
            //Helper.hideLoader();
                ChatController.disconnectUser();
                
                Helper.appoCount = 0;
                Helper.messageCount = 0;
                Helper.requestCount = 0;

                Helper.setData('userdata', '');
                Helper.setData('token', '');
                setTimeout(() => {
                    Helper.hideLoader()
                }, 2000)
                this.props.navigation.navigate('LoginNew');
                Helper.showToast('Logout successful.');
                
            })

        
        
    }

    render() {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          
            <Text allowFontScaling={false}  style={{ padding: 5,fontSize: 15,fontFamily: 'Aileron-Regular' }}>Are you sure you want to sign out?</Text>
            <View style={{ flexDirection: 'row', }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" });
                
                this.logoutFun();
            }}>
                <View style={{ marginRight: 15, width: 110, height: 40, backgroundColor: '#007cc9', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                    <Text allowFontScaling={false}  style={{ color: 'white', fontSize: 18 }}>Yes</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" });
                
            }}>
                <View style={{ marginLeft: 15, width: 110, height: 40, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                    <Text allowFontScaling={false}  style={{ color: 'white', fontSize: 18 }}>No</Text>
                </View>
           </TouchableOpacity>
           </View>
  
  
        </View>
    )
    }
  }
  

  

  

class SideMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            LogOut: <LogOut navigation={this.props.navigation} />,
            active_page:'Home',
            userdata:'',
        }


        Helper.getData('userdata').then((responseData) => {
            // console.log('------userData  :  ' + JSON.stringify(responseData));
            if (responseData === null || responseData === 'undefined' || responseData === '') {
                
            } else {
               this.setState({userdata:responseData});
            }
        })

    }

    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
        if(route == 'DetailersStack'){
            this.props.navigation.navigate('Detailers');
        }
        this.setState({active_page:route});
    }


    _onLogOutPress = () => {
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.LogOut })
    }

    onHomePage = () => {
        const reset= StackActions.reset({
            index:0,
            actions: [NavigationActions.navigate({routeName:"Home"})],
            key:null
        })
    }

    goToHome = () => {
        this.setState({active_page:'Home'});
        this.props.navigation.navigate('SetHome')
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
    }
    render() {
        return (
            
            <SafeAreaView style={{flex:1}}>
            
                
                <ScrollView style={{marginBottom:50}}>
                    <View> 
                        <Text allowFontScaling={false}  font={'CenturyGothic'} style={{ paddingVertical: 10, paddingHorizontal: 10,fontSize:22}}>
                            {this.state.userdata.first_name} 
                         </Text>

                        <TouchableOpacity onPress={() => this.goToHome()}
                        
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}
                        >
                            <Image

                            source={this.state.active_page == 'Home' ? require ('../../assets/Icons/sideMenuIcon/homeb.png') : require ('../../assets/Icons/sideMenuIcon/home.png')}
                            style={{height:20,width:20,marginTop:4,resizeMode:'contain' }}
                            />
                               
                                <Text allowFontScaling={false}   font={'CenturyGothic'} style={{ margin:4,fontSize:16 }}> Home</Text> 
                        </TouchableOpacity>
                    </View>

                 
                    <TouchableOpacity onPress={this.navigateToScreen('Sales')}
                    style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'Sales' ? require ('../../assets/Icons/sideMenuIcon/sales_b.png') : require ('../../assets/Icons/sideMenuIcon/sales_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}   font={'CenturyGothic'} style={{  margin:4,fontSize:16 }}> Sales</Text>
                        </TouchableOpacity>

                        {this.state.userdata.is_sub_detailer == 0 && 
                            <TouchableOpacity onPress={this.navigateToScreen('DetailersStack')}
                            style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                                <Image
                                source={this.state.active_page == 'DetailersStack' ? require ('../../assets/Icons/sideMenuIcon/detailers_b.png') : require ('../../assets/Icons/sideMenuIcon/detailers_d.png')}
                                style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                                />
                                  
                                    <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{ margin:4,fontSize:16 }}> Detailers</Text>
                            </TouchableOpacity>
                        }
                        


                        {this.state.userdata.is_sub_detailer == 0 && 
                        <TouchableOpacity onPress={this.navigateToScreen('PayoutAccount')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'PayoutAccount' ? require ('../../assets/Icons/sideMenuIcon/payout_b.png') : require ('../../assets/Icons/sideMenuIcon/payout_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16 }}> Payout Account</Text>
                        </TouchableOpacity>
                        }
                        
                        <TouchableOpacity onPress={this.navigateToScreen('EditService')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'EditService' ? require ('../../assets/Icons/sideMenuIcon/services_b.png') : require ('../../assets/Icons/sideMenuIcon/services_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16 }}> Your Services</Text>
                        </TouchableOpacity>



                        <TouchableOpacity onPress={this.navigateToScreen('Profile')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'Profile' ? require ('../../assets/Icons/sideMenuIcon/profile_b.png') : require ('../../assets/Icons/sideMenuIcon/profile_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16 }}> Profile</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.navigateToScreen('Portfolio')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'Portfolio' ? require ('../../assets/Icons/sideMenuIcon/protfolio_active.png') : require ('../../assets/Icons/sideMenuIcon/protfolio.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16 }}> Portfolio</Text>
                        </TouchableOpacity>

                        {this.state.userdata.is_sub_detailer == 0 && 
                        <TouchableOpacity onPress={this.navigateToScreen('Report')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'Report' ? require ('../../assets/Icons/sideMenuIcon/reports_b.png') : require ('../../assets/Icons/sideMenuIcon/reports_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16  }}> Reports</Text>
                        </TouchableOpacity>
                        }


                        <TouchableOpacity onPress={this.navigateToScreen('Support')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'Support' ? require ('../../assets/Icons/sideMenuIcon/support_b.png') : require ('../../assets/Icons/sideMenuIcon/support_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                              
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16  }}> Support</Text>
                        </TouchableOpacity>


                        <TouchableOpacity onPress={this.navigateToScreen('Legal')}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={this.state.active_page == 'Legal' ? require ('../../assets/Icons/sideMenuIcon/legal_b.png') : require ('../../assets/Icons/sideMenuIcon/legal_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                               
                                <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16  }}> Legal</Text>
                        </TouchableOpacity>

                        <TouchableOpacity  onPress={this._onLogOutPress}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={require ('../../assets/Icons/sideMenuIcon/signout_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                     <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16  }}> Sign Out</Text>
                        </TouchableOpacity>

                   

                        <TouchableOpacity  onPress={()=>{
          const options = {
            AppleAppID:"1499620897",
            GooglePackageName:"com.rinsedetailers",
            //AmazonPackageName:"com.mywebsite.myapp",
            OtherAndroidURL:"http://www.randomappstore.com/app/47172391",
            preferredAndroidMarket: AndroidMarket.Google,
            preferInApp:false,
            openAppStoreIfInAppFails:true,
            //fallbackPlatformURL:"http://www.mywebsite.com/myapp.html",
          }
          Rate.rate(options, success=>{
            if (success) {
                //alert('success');
              // this technically only tells us if the user successfully went to the Review Page. Whether they actually did anything, we do not know.
              this.setState({rated:true})
            }
          })
        }}
                        style={{borderBottomWidth:1,borderColor:'#dddd' ,flexDirection:'row' ,margin:0,padding:10,paddingTop:0,marginTop:7}}>
                            <Image
                            source={require ('../../assets/Icons/sideMenuIcon/signout_d.png')}
                            style={{height:20,width:20 ,marginTop:4,resizeMode:'contain'}}
                            />
                     <Text allowFontScaling={false}  font={'CenturyGothic'}  style={{  margin:4,fontSize:16  }}> Rate App</Text>
                        </TouchableOpacity>
                   

                        
                
                </ScrollView>
                {this.state.userdata.is_sub_detailer == 0 && 
                <TouchableOpacity
                
                //onPress={this.navigateToScreen('SubscriptionView')}   
                onPress={this.navigateToScreen('Upgrade')}   
                style={{width:'100%',position:'absolute',bottom:0}}
                >
                <LinearGradient
                colors={['#5ced31', '#4acb23', '#37a313']}
                style={{ padding: 7, alignItems: 'center',marginTop:20,bottom:0}}
                >

                    <Text allowFontScaling={false}   font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >UPGRADE</Text>
                </LinearGradient>
                </TouchableOpacity>
                }
                {/* {this.state.userdata.is_sub_detailer == 0 && 
                <TouchableOpacity
                
                onPress={this.navigateToScreen('SubscriptionView')}   
                //onPress={this.navigateToScreen('Upgrade')}   
                style={{width:'100%',position:'absolute',bottom:0}}
                >
                <LinearGradient
                colors={['#5ced31', '#4acb23', '#37a313']}
                style={{ padding: 7, alignItems: 'center',marginTop:20,bottom:0}}
                >

                    <Text allowFontScaling={false}   font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >Receipt</Text>
                </LinearGradient>
                </TouchableOpacity>
                } */}
                
            </SafeAreaView>
           
        );
    }
}
SideMenu.propTypes = {
    navigation: PropTypes.object
};

export default SideMenu;