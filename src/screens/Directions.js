
import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Button } from 'react-native'
import { Input,CustomHeader, CardSection, AuthHeader, AuthTitle } from '../common';
import CommanStyle from '../config/Styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import MapView, {
    ProviderPropType,
    Marker,
    AnimatedRegion,
} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import Config from '../Lib/Config';


export default class Directions extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            
            coordinate: this.props.navigation.getParam('coordinate', ''),
            AppUserData:'',
            markers: {
                title: 'FINISH',
                description: 'You have found me!',
                coordinates: {
                 latitude: 37.3318456,
                 longitude: -122.0296002
                },
                key: '0',
                color: '#000' 
             },
             markers1: {
                title: 'FINISH',
                description: 'You have found me!',
                coordinates: {
                 latitude: 37.771707,
                 longitude: -122.4053769
                },
                key: '1',
                color: '#000' 
             },
             

        }
        
    }


    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {
            
                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    
                } else {
                    this.setState({AppUserData:responseData});
                    
                }
            })

            //alert(JSON.stringify(this.state.AppUserData));
            //return false;
             const origin = {latitude: 37.3318456, longitude: -122.0296002};
             const destination = {latitude: 37.771707, longitude: -122.4053769};
             

             this.setState({origin:origin,destination:destination});


            
        })


    }

    componentWillUnmount() {
        this.focusListener.remove()
    }

    


    render() {
        return (

            <View style={{ flex: 1, }}>

                
                <ScrollView>
                    <View>
                        <View style={{ height: 400, width: '100%' }}>
                            {/* <Image
                                style={{ width: '100%', height: 150 }}
                                source={require('../../assets/Icons/map.png')}
                            /> */}

                            

    
    
                    <MapView
                        initialRegion={this.state.coordinate}
                        style={style.map}
                    >
                    <MapView.Marker  coordinate={this.state.markers.coordinates} />
                    <MapView.Marker coordinate={this.state.markers1.coordinates} />
                    {/* <MapView.Marker coordinate={this.state.origin} />
                    <MapView.Marker coordinate={this.state.destination} /> */}
                    {(this.state.coordinate.length >= 1) && (
                        <MapViewDirections
                            origin={this.state.origin}
                            destination={this.state.destination}
                            apikey={Config.google_place_api_key}
                            strokeWidth={3}
                            strokeColor="hotpink"

                        />
                    )}
                    </MapView>
                           

                        </View>
                        <TouchableOpacity 
                        onPress={() => this.props.navigation.goBack()}
                        style={{position:'absolute',top:35,left:10,backgroundColor:'#fff',padding:10,borderRadius:10,borderWidth:1,borderColor:'#ccc'}}
                        >
                        <Image
                            style={{ width: 20, height: 20 }}
                            source={require('../../assets/Icons/leftarrow.png')}
                        />
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,marginTop:10,flexDirection:'row',padding:10}}>
                        <View style={{flexDirection:'column',flex:6}}>
                            <Text  allowFontScaling={false}   style={{fontSize:18,fontWeight:'bold',fontFamily: 'Aileron-Regular'}}>N Himes Ave</Text>
                            <Text  allowFontScaling={false}   style={CommanStyle.normaltext}>34 mi</Text>
                        </View>
                        <TouchableOpacity style={{flex:1}} 
                        onPress={() => this.props.navigation.goBack()}
                        >
                            {/* <Icon name="times-circle" size={35} color="#d0cfcb" style={{marginTop:6,marginLeft:5}} /> */}
                            <Image
                            style={{ width: 28, height: 28,opacity:0.5 }}
                            source={require('../../assets/Icons/cross-full.png')}
                        />
                        </TouchableOpacity>
                    </View>

                    <View>
                    <TouchableOpacity>
                        <LinearGradient 
                        colors={['#4fa4d8', '#4fa4d8', '#4fa4d8']}
                        style={{ padding: 5, alignItems: 'center',margin:10,borderRadius:5 }}

                        onPress={()=>this.props.navigation.navigate('DrawerRoot')} 
                        >
                        
  <Text  allowFontScaling={false}   style={{ color:'#fff',fontSize:18,fontFamily: 'Aileron-Regular'}} >
    Directions
  </Text>
  <Text  allowFontScaling={false}   style={{ color:'#fff',fontFamily: 'Aileron-Regular'}}>39 min drive</Text>
</LinearGradient>
</TouchableOpacity>

                    <View style={{padding:10}}>
                        <Text  allowFontScaling={false}   style={CommanStyle.normaltitle}>Address</Text>
                        <Text  allowFontScaling={false}   style={CommanStyle.normaltext}>N Himes Ave</Text>
                        <Text  allowFontScaling={false}   style={CommanStyle.normaltext}>Tampa, FL 33614</Text>
                        <Text  allowFontScaling={false}   style={CommanStyle.normaltext}>United States</Text>
                    </View>
                    
                    </View>

                    


                   

                    

                </ScrollView>
                
            </View>
        )
    }
}



const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },



})


