import React, { Component } from 'react'
import { Text, View ,Modal,TouchableOpacity,Image,FlatList,Dimensions} from 'react-native';
import { Input, Card, CardSection, CustomHeader, CustomTitle, HeaderTitle } from '../../common';
import  CommanStyle  from '../../config/Styles';
import Helper from '../../Lib/Helper';
import { EventRegister } from 'react-native-event-listeners';
import ChatController from '../../Lib/ChatController';
import Moment from "moment"
const DeviceH =Dimensions.get('window').height;
const DeviceW =Dimensions.get('window').width;
import LinearGradient from 'react-native-linear-gradient';

export default class Message extends Component {
    static navigationOptions = { header: null }
    
    constructor(props) {
        super(props);
        this.state ={
            allChats:[],
            isLoader:false,
            modalDeleteVisible: false,
            userdata:''
            
        }

    }

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((userdata) => {
                if (userdata) {
                    this.state.userdata = userdata;
                    ChatController.checkConnection("getinbox", (cb) => {
                        if (cb) {
                            Message.getInbox(this.state.userdata.id);
                        }
                    });
                    
                    this.getBadegesCount();
                }
            })
        })
        this.receive_messagelistener = EventRegister.addEventListener('receive_message', (data) => {
            Message.getInbox(this.state.userdata.id);
        })
    }

    componentWillMount() {
        this.inboxlistener = EventRegister.addEventListener('get_inbox_list_response', (data) => {
            console.log('get_inbox_list_response--', data)
            
            if (data.data.length > 0) {
                this.state.isLoader = false;
                this.setState({
                    allChats: data.data
                });
            } else {
                this.setState({allChats:[], isLoader: false,chatNotFoundMsg: 'Chat Not Found.' });
            }
        })


        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;
                

                this.props.navigation.setParams({ badgeCount: 123 })
                
            } else {
                this.setState({ isLoader: false });
            }

        })
    }


    componentWillUnmount() {
        EventRegister.removeEventListener(this.inboxlistener);
        EventRegister.removeEventListener(this.receive_messagelistener);
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove();
    }

    
    /* Bootom Tab Budget event */

    getBadegesCount() {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            user_id: this.state.userdata.id,
            role_id:2,
            utc_date_time: gettime
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);
        
    }


 

    /*Message Delete Model start here*/
    setModalVisible(visible) {
        
        this.setState({ modalDeleteVisible: visible });
    }

    deleteChatList(request_id,other_user,user){
        //alert(request_id+'--'+other_user+'--'+user);
        let tempdata = {
            user_id:user,
            service_request_id:request_id,
            other_user_id:other_user
        }

        

        Helper.makeRequest({ url: "user-delete-chat", data: tempdata }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == true) {
                
                this.setState({allChats:[]})
                Helper.showToast(data.message)
                this.setModalVisible(false)
                Message.getInbox(this.state.userdata.id);
            }
            else {
                Helper.showToast(data.message)
                this.setModalVisible(false)
            }
        })
        
    }

    modalDeleteRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalDeleteVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                        <View style={{ alignItems: 'center', height: 80 }}>


<Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
Are you sure want to delete this Chat?
</Text>

<View style={{ flex: 1, flexDirection: 'row' }}>
    <TouchableOpacity onPress={() => {
        this.deleteChatList(this.state.requestDeleteId,this.state.otherDeleteID,this.state.userDeleteId);
        
    }}

        style={{ flex: 1 }}
    >

        <LinearGradient
            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
            style={{ padding: 8, margin: 10, alignItems: 'center',height:40 }}


        >

            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Yes</Text>

        </LinearGradient>


    </TouchableOpacity>

    <TouchableOpacity onPress={() => {
        this.setModalVisible(false)
    }}

        style={{ flex: 1 }}
    >

        <LinearGradient
            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
            style={{ padding: 8, margin: 10, alignItems: 'center',height:40 }}
        >

            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >No</Text>

        </LinearGradient>

    </TouchableOpacity>
</View>
</View>

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*Message Delete Model End here*/



    static getInbox(user_id) {
        
        // this.setState({ isLoader: true });
        
        let form = {
            limit: 10,
            user_id: user_id,
            start: '-1',
        }
        
        ChatController.chatDataEvent('get_inbox_list', form);
    }


    _renderInbox = ({ item,index }) => (
        <View style={{flexDirection:'row',borderBottomWidth: 1, borderColor: '#e8e8e8',padding:10}}>
            <View style={{flex:7}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Chat',{'service_request_id':item.service_request_id,'other_user_id':((this.state.userdata.id == item.other_user_id) ? item.user_id : item.other_user_id ),'user_id':((this.state.userdata.id == item.other_user_id) ? item.other_user_id : item.user_id ),'username':(this.state.userdata.id == item.user_id) ? item.other_user_name : item.user_name,'country_code':(this.state.userdata.id == item.other_user_id) ? item.user_country_code : item.other_user_country_code,'mobile':(this.state.userdata.id == item.other_user_id) ? item.user_mobile : item.other_user_mobile})}
        style={{ flexDirection:'row'}}
        
        >
            <View style={{flex:6, flexDirection: 'column', paddingBottom: 5 }}>
                <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{fontWeight:'bold',fontFamily: 'Aileron-Regular'}]} >{(this.state.userdata.id == item.user_id) ? item.other_user_name : item.user_name} 
                {/* {item.service_request_id != 0 && (
                   <Text style={{color:'gray'}}>(Req. ID - #{item.service_request_id})</Text>
                )} */}
                </Text>
                {item.message_type == 'Text' && (
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} numberOfLines={2}>{item.message}</Text>
                )}
                {item.message_type == 'Image' && (
                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >Sent a Image</Text>
                
                )}
                
                <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular',fontSize:12}}>
                
                {Moment.utc(item.updated_at).local().format('MMM DD, YYYY, h:mm A')}
                </Text>
            </View>
            <View style={{flex:1,justifyContent:'center'}}>
            {item.unread_message > 0 && (
                <Text style={{width:30,padding:5,backgroundColor:'red',color:'white',borderRadius:50,textAlign:'center'}}>{item.unread_message}</Text>
            )}
            
            </View>
            <View style={{flex:1,justifyContent:'center'}}>
            <Image 
            style={{width: 40, height: 40,alignItems:'center',opacity:0.3}}
            source={require('../../../assets/Icons/arrowright.png')}
            
            />
            </View>
            
        </TouchableOpacity>
    
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
            <TouchableOpacity onPress={()=> {
                this.setState({
                    requestDeleteId:item.service_request_id,
                    otherDeleteID:((this.state.userdata.id == item.other_user_id) ? item.user_id : item.other_user_id ),
                    userDeleteId:((this.state.userdata.id == item.other_user_id) ? item.other_user_id : item.user_id )
                },() => {
                    this.setModalVisible(true)
                })
            }} >
            <Image 
            style={{width: 35, height: 35,alignItems:'center',opacity:0.5}}
            source={require('../../../assets/Icons/cross.png')}
            
            />
            </TouchableOpacity>
            </View>
        </View>
        
    )



    render() {
        
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                {/* backgroundColor: 'pink'  */}

                {this.modalDeleteRender()}
                <CustomHeader navigation={this.props.navigation}   userbar/>

               
                <HeaderTitle TitleName='Messages' />

                

                    <View style={{borderTopColor:'#e8e8e8',borderTopWidth:1,marginBottom:60}}>
                        {/* borderWidth: 1, */}
                        {this.state.allChats.length > 0 ?
                        <FlatList
                        data={this.state.allChats}
                        extraData={this.state}
                        keyExtractor={(item, index) => index}
                        renderItem={this._renderInbox}
                        

                        />

                        : !this.state.isLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>{this.state.chatNotFoundMsg}</Text>
                                    </View> : null
                        }
                        
                        
                       


                        {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate('Chat')}
                        style={{ flexDirection:'row',borderBottomWidth: 1, borderColor: '#e8e8e8',padding:10}}
                        
                        >
                            <View style={{flex:6, flexDirection: 'column', paddingBottom: 5 }}>
                                <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{fontWeight:'bold',fontFamily: 'Aileron-Regular'}]} >Stephanie Hann</Text>
                                <Text allowFontScaling={false} font={'CenturyGothic'} style={CommanStyle.normaltext} >The car will be ready at 4:00 if you</Text>
                                <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>5:55 pm</Text>
                            </View>
                            <View style={{flex:1,paddingTop:15}}>
                            <Image 
                            style={{width: 40, height: 40,alignItems:'center',opacity:0.3}}
                            source={require('../../../assets/Icons/arrowright.png')}
                            
                            />
                            </View>
                            
                        </TouchableOpacity> */}
                    </View>
                
            </View>
        )
    }
}
