import React, { Component } from 'react'
import { Text, View, Modal, TextInput, StyleSheet, FlatList, Image, Dimensions, ScrollView, Clipboard, CheckBox, ActivityIndicator, TouchableOpacity, KeyboardAvoidingView,Keyboard, Platform } from 'react-native'
import Ionc from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { Input, Card, CardSection, CustomHeader, CustomTitle, HeaderTitle } from '../../common';
import CommanStyle from '../../config/Styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Moment from "moment"
import ChatController from "../../Lib/ChatController"
import { EventRegister } from 'react-native-event-listeners';
import Hyperlink from 'react-native-hyperlink'
import CameraController from '../../Lib/CameraController';
import Helper from '../../Lib/Helper';
import ImageP from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import call from 'react-native-phone-call'
const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;
import LinearGradient from 'react-native-linear-gradient';
import Communications from 'react-native-communications';

const closeImg = require('../../../assets/Icons/colse.png');
class CallMsg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            country_code: this.props.country_code,
            mobile: this.props.mobile,
        }




    }


    callFun = () => {
        const args = {
            number: this.state.country_code + '' + this.state.mobile, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }
        var mobile = this.state.country_code + '' + this.state.mobile;
        Communications.phonecall(mobile, true)
        //call(args).catch(console.error)
    }

    render() {
        return (
            <View style={{ width: 300, height: 140, margin: -20, marginLeft: -30, marginRight: -30 }}>
                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })

                }}
                    style={{ margin: 10 }}
                >
                    <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>

                    {/* <Image source={require('../../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                </TouchableOpacity>
                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", marginTop: 10, fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>

                    {"+" + this.state.country_code + "-" + this.state.mobile.slice(0, 3) + "-" + this.state.mobile.slice(3, 6) + "-" + this.state.mobile.slice(6, 15)}</Text>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 20 }}>
                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1, }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center' }} >Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {

                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                        this.callFun(this.state.callNumber)
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1, borderLeftColor: '#e8e8e8', borderLeftWidth: 1 }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center', color: '#3c81c4' }} >Call</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}


class PhotosPopUp extends Component {

    constructor(props) {
        super(props);
        this.state = {

            isChecked: false,
            img_count: 1,
        }






    }

    render() {
        return (
            <View style={{ width: Dimensions.get('window').width, height: 400, marginTop: 50 }}>

                <View style={{ flexDirection: 'row', marginTop: 70, padding: 20 }}>
                    <TouchableOpacity onPress={() => {
                        global.state['modalBlack'].setState({ modalVisibleBlack: false, Msg: "" });

                    }}
                        style={{ flex: 2 }}
                    >
                        <Text allowFontScaling={false} style={{ textAlign: 'right', color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }}>Close</Text>
                    </TouchableOpacity>
                </View>

                <Image
                    style={{ width: '95%', height: '95%', alignSelf: 'center', resizeMode: 'contain' }}
                    source={{ uri: this.props.photo }}
                />

            </View>
        )
    }
}



const { width, height } = Dimensions.get('window')
export default class Chat extends Component {
    static navigationOptions = { header: null }
    static last_id = 0;
    static other_user_id = 0;
    static user_id = 0;
    static service_request_id = 0;


    constructor(props) {
        super(props);
        this.state = {
            first_message_id: 0,
            lastMsgId: 0,
            inputHeight: 0,
            message: '',
            allChatList: [],
            // user_id: this.props.navigation.state.params.user_id,
            // order_id: this.props.navigation.state.params.order_id, 
            other_user_id: this.props.navigation.getParam('other_user_id', 0),
            user_id: this.props.navigation.getParam('user_id', 0),
            service_request_id: this.props.navigation.getParam('service_request_id', 0),
            username: this.props.navigation.getParam('username', ''),
            country_code: this.props.navigation.getParam('country_code', ''),
            mobile: this.props.navigation.getParam('mobile', ''),
            isLoader: false,
            CallMsg: <CallMsg />,
            PhotosPopUp: <PhotosPopUp navigation={this.props.navigation} />,
            avatarSource: '',
            modalBlockVisible: false,
            modalCallVisible: false,
            is_chat_blocked: false,
            block_by_me: false,
            showBottomBar: false,
            shortHeight:0

        }




    }


    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            ChatController.checkConnection("gethistory", (cb) => {
                if (cb) {

                    Chat.last_id = 0;
                    Chat.other_user_id = this.state.other_user_id;
                    Chat.user_id = this.state.user_id;
                    Chat.service_request_id = this.state.service_request_id;

                    Chat.getChatHistory('after');
                }
            });

            if(Platform.OS == 'ios'){
                this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
                this.keyboardDidShowListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
        
            }
        })

       
    }

    _keyboardDidShow = (e) => {
        this.setState({
            //keyboardHeight: e.endCoordinates.height,
            //normalHeight: Dimensions.get('window').height, 
            shortHeight: e.endCoordinates.height,
            
        });
    }

    keyboardDidHide = (e) => {
        this.setState({
            //keyboardHeight: e.endCoordinates.height,
            //normalHeight: Dimensions.get('window').height, 
            shortHeight: 0,
        });
    }



    static getChatHistory(chattype) {
        //this.setState({ isLoader: true });



        let form = {
            last_id: Chat.last_id,
            limit: 10,
            other_user_id: Chat.other_user_id,
            type: chattype,
            user_id: Chat.user_id,
            service_request_id: Chat.service_request_id
        }

        ChatController.chatDataEvent('get_message_history', form);



        let form2 = {
            other_user_id: Chat.other_user_id,
            user_id: Chat.user_id
        }

        ChatController.chatDataEvent('check_user_status', form2);



        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form3 = {
            user_id: Chat.user_id,
            role_id: 2,
            utc_date_time: gettime

        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form3);


    }



    callFun = () => {
        const args = {
            number: this.state.country_code + '' + this.state.mobile, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }

        var mobile = this.state.country_code + '' + this.state.mobile;
        Communications.phonecall(mobile, true)
        //call(args).catch(console.error)
    }


    ChatBlockUser() {

        let form2 = {
            other_user_id: this.state.other_user_id,
            user_id: this.state.user_id
        }

        ChatController.chatDataEvent('user_block_unblock', form2);
    }

    componentWillMount() {
        this.historylistener = EventRegister.addEventListener('get_message_history_response', (data) => {
            console.log('history--', data)
            if (data.data.length > 0) {

                
                this.state.lastMsgId = data.data[data.data.length - 1].id;
                Chat.last_id = data.data[data.data.length - 1].id;

                this.state.first_message_id = data.first_message_id;
                this.state.isLoader = false;
                // this.setState({
                //     allChatList: [...this.state.allChatList, ...data.data]
                // });
                let newmessage = [];
                let allChatList = this.state.allChatList;
                for (let msg of data.data) {
                    if (!allChatList.some(allChatList => allChatList.id == msg.id)) {
                        newmessage.push(msg);
                    }
                }
                this.setState({
                    allChatList: [...this.state.allChatList, ...newmessage]
                });
                

            } else {
                this.setState({ isLoader: false });
            }

        })

        this.multi_userlistener = EventRegister.addEventListener('multi_user', (data) => {
            console.log('multi_usermulti_usermulti_usermulti_user', data)
            if (this.state.allChatList.length == 0) {
                this.state.first_message_id = data.data.id;
            }

            var sameId = false;
            this.state.allChatList.map((item) => {
                if (item.id == data.data.id) {
                    sameId = true
                }
            })
            if (!sameId) {
                this.state.allChatList.unshift(data.data);

                this.setState({ change: true });
            }

            //this.state.allChatList.unshift(data.data);
            //this.setState({ change: true });
        })

        this.receive_messagelistener = EventRegister.addEventListener('receive_message', (data) => {
            //alert(JSON.stringify(data));
            //alert(data.data.service_request_id+','+this.state.service_request_id)
            if (data.data.service_request_id == this.state.service_request_id) {

                this.ReadChat(data.data.id);

                //console.log("receive_messagelistenerreceive_messagelistener")


                var sameId = false;
                this.state.allChatList.map((item) => {
                    if (item.id == data.data.id) {
                        sameId = true
                    }
                })
                if (!sameId) {
                    this.state.allChatList.unshift(data.data);

                    this.setState({ change: true });
                }



                // this.checkUserMessages(data.data.id).then((sameId)=>{

                //     if(sameId ==false){                    
                //     this.state.allChatList.unshift(data.data);                
                //     this.setState({ change: true });
                //     }

                // });

            }
        })






        this.redResponse = EventRegister.addEventListener('read_message_update_response', (data) => {
            //console.log('read update--', data)
            //alert(data.data.service_request_id+','+this.state.service_request_id)

        })


        this.blocklistener = EventRegister.addEventListener('check_user_status_response', (data) => {
            console.log('block--', data)

            if (data.status == 'true') {

                if (data.is_blocked_by_me == 'true') {
                    this.setState({ is_chat_blocked: true, block_by_me: true, showBottomBar: true })
                }
                else if ((data.is_blocked == 'true') && (data.is_blocked_by_me == 'false')) {
                    this.setState({ is_chat_blocked: true, block_by_me: false, showBottomBar: true })
                }
                else {
                    this.setState({ is_chat_blocked: false, showBottomBar: true })
                }
            }


        })

        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;


                this.props.navigation.setParams({ badgeCount: 123 })

            }

        })

    }


    // async checkUserMessages(msgId){
    //     console.log("It is return here.... "+msgId)
    //     await this.state.allChatList.map((item) => {
    //         console.log("It is map here.... "+(item.id == msgId))
    //         if (item.id == msgId) {
    //           return true;  
    //         }
    //     })

    //     return false;
    // }



    ReadChat(msg_id) {

        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            other_user_id: this.state.other_user_id,
            user_id: this.state.user_id,
            read_status: 1,
            msg_id: msg_id.toString(),
            utc_date_time: gettime,
            role_id: 2


        }

        //alert(JSON.stringify(form));
        ChatController.chatDataEvent('read_message_update', form);
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(this.historylistener);
        EventRegister.removeEventListener(this.multi_userlistener);
        EventRegister.removeEventListener(this.receive_messagelistener);
        EventRegister.removeEventListener(this.blocklistener);
        EventRegister.removeEventListener(this.countlistener);
        EventRegister.removeEventListener(this.redResponse);



    }

    copyMessage(message) {
        Clipboard.setString(message);
        Helper.showToast('Message copied', 'TOP');
    }


    chooseImage = (data) => {

        var userdata = { ...this.state.userdata };
        CameraController.selecteImage((response) => {
            if (response.uri) {

                this.setState({ avatarSource: response.uri }, () => this.uploadImage());

            }
        });
    }


    uploadImage = () => {


        let tempdata = new FormData();

        if (this.state.avatarSource) {
            tempdata.append('image', {
                uri: this.state.avatarSource,
                name: 'test.jpg',
                type: 'image/jpeg'
            });
        }


        Helper.makeRequest({ url: "chat-upload-image", method: "POSTUPLOAD", data: tempdata }).then((data) => {
            if (data.status == 'true') {
                this.senImage(data.data);
            }
            else {
                Helper.showToast(data.message)
            }
        })

    }


    chatImageShow(url) {
        this.setState({ 'PhotosPopUp': <PhotosPopUp navigation={this.props.navigation} photo={url} /> }, () => {

            global.state['modalBlack'].setState({ modalVisibleBlack: true, Msg: this.state.PhotosPopUp })
        })
    }



    renderChatItem = ({ item, index }) => {
        return (
            <View>
                {item.user_id == this.state.user_id && (
                    <View style={styles.chatBbblerevers}>
                        {/* <View style={styles.uimagesrevers}>
                        <Image
                            source={this.state.userimage ? { uri: this.state.userimage } : require('../../assets/images/dummy.png')}
                            style={styles.imgs}
                        />
                    </View> */}
                        <View style={styles.utextrevers}>
                            <Text style={styles.daterevers}>{Moment.utc(item.user_time).local().format('MMM DD, YYYY, h:mm A')}</Text>
                            {item.message_type == 'Text' && (
                                <Hyperlink linkStyle={{ color: '#2980b9', textDecorationLine: 'underline', width: 20 }} linkDefault={true}>
                                    <Text selectable={true} onLongPress={() => this.copyMessage(item.message)} style={styles.chatMessrevers}>{item.message}</Text>

                                </Hyperlink>
                            )}
                            {item.message_type == 'Image' && (
                                <TouchableOpacity onPress={() => this.chatImageShow(Helper.getImageUrl(item.message))}>
                                    <ImageP
                                        source={{ uri: Helper.getImageUrl(item.message) }}
                                        style={styles.uploadImg}
                                        indicator={ProgressBar}
                                    />
                                </TouchableOpacity>

                            )}
                        </View>
                    </View>
                )}
                {item.user_id != this.state.user_id && (
                    <View style={styles.chatBbble}>
                        {/* <View style={styles.uimages}>
                            <Image
                                source={this.state.spimage ? { uri: this.state.spimage } : this.state.order_id == 0 ? require('../../assets/images/dummysp.png') : require('../../assets/images/dummy.png')}
                                style={styles.imgs}
                            />
                        </View> */}
                        <View style={styles.utext}>
                            <Text style={styles.date}>{Moment.utc(item.user_time).local().format('MMM DD, YYYY, h:mm A')}</Text>
                            {item.message_type == 'Text' && (
                                <Hyperlink linkStyle={{ color: '#2980b9', textDecorationLine: 'underline' }} linkDefault={true}>

                                    <Text selectable={true} onLongPress={() => this.copyMessage(item.message)} style={styles.chatMess}>{item.message}</Text>
                                </Hyperlink>
                            )}
                            {item.message_type == 'Image' && (
                                <TouchableOpacity onPress={() => this.chatImageShow(Helper.getImageUrl(item.message))}>
                                    <ImageP
                                        source={{ uri: Helper.getImageUrl(item.message) }}
                                        style={styles.uploadImg}
                                        indicator={ProgressBar}
                                    />
                                </TouchableOpacity>
                            )}

                        </View>
                    </View>
                )}
            </View>
        );
    }


    sendMessage() {
        //if(Platform.OS == 'ios'){
            Keyboard.dismiss();
        //}
        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        if (this.state.message.trim() != '') {


            let form = {
                other_user_id: this.state.other_user_id,
                message: this.state.message.trim(),
                upload_img: '',
                user_id: this.state.user_id,
                message_type: 'Text',
                other_data: '',
                service_request_id: this.state.service_request_id,
                role_id: 1,
                utc_date_time: gettime


            }
            console.log('formform', form);
            this.setState({ message: '' });
            ChatController.chatDataEvent('send_message', form);
        }
    }


    senImage(image) {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        if (image != '') {
            let form = {
                other_user_id: this.state.other_user_id,
                message: image,
                upload_img: '',
                user_id: this.state.user_id,
                message_type: 'Image',
                other_data: '',
                service_request_id: this.state.service_request_id,
                role_id: 1,
                utc_date_time: gettime

            }
            console.log('formform', form);
            this.setState({ message: '' });
            ChatController.chatDataEvent('send_message', form);
        }
    }

    onScrollpage = () => {
        if (!this.state.isLoader && this.state.first_message_id != this.state.allChatList[this.state.allChatList.length - 1].id) {

            Chat.getChatHistory('before');
        }
    }




    _onCallPress = () => {
        this.setState({ 'CallMsg': <CallMsg navigation={this.props.navigation} country_code={this.state.country_code} mobile={this.state.mobile} /> }, () => {

            global.state['modal'].setState({ modalVisible: true, Msg: this.state.CallMsg })
        })


    }




    /*User Block Model start here*/
    setModalVisible(visible) {

        this.setState({ modalBlockVisible: visible });
    }


    modalBlockRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalBlockVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                            <View style={{ alignItems: 'center', height: 100 }}>


                                <Text allowFontScaling={false} style={{ fontSize: 16, marginTop: 10, textAlign: 'center', fontFamily: 'Aileron-Regular' }}>
                                    Are you sure want to {this.state.block_by_me ? 'unblock' : 'block'} {this.state.username}?
</Text>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => {
                                        this.ChatBlockUser();
                                        this.setModalVisible(false)
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center',height:45,justifyContent:'center' }}


                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Yes</Text>

                                        </LinearGradient>


                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(false)
                                    }}

                                        style={{ flex: 1 }}
                                    >

                                        <LinearGradient
                                            colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                            style={{ padding: 8, margin: 10, alignItems: 'center',height:45,justifyContent:'center' }}
                                        >

                                            <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >No</Text>

                                        </LinearGradient>

                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*user Block Model End here*/



    /*User Block Model start here*/
    setModalCallVisible(visible) {

        this.setState({ modalCallVisible: visible });
    }


    modalCallRender() {
        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalCallVisible}
                onRequestClose={() => {
                    //Alert.alert('Modal has been closed.');
                }}>

                <View style={{ backgroundColor: 'rgba(0,0,0,0.9)', height: DeviceH }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
                        <View style={{ backgroundColor: "white", borderRadius: 10, padding: 20, paddingLeft: 30, paddingRight: 30, margin: 20, alignItems: "center" }}>
                        <View style={{ width: 300, height: 140, margin: -20, marginLeft: -30, marginRight: -30 }}>
                <TouchableOpacity onPress={() => {
                    this.setModalCallVisible(false)

                }}
                 style={{ margin: 6,padding:4 }}
                    
                >
                <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>

                {/* <Text>Close</Text> */}
                    {/* <Image source={closeImg}  style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }}/> */}
                </TouchableOpacity>
                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>

                    {"+" + this.state.country_code + "-" + this.state.mobile.slice(0, 3) + "-" + this.state.mobile.slice(3, 6) + "-" + this.state.mobile.slice(6, 15)}</Text>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 20 }}>
                    <TouchableOpacity onPress={() => {
                        this.setModalCallVisible(false)
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1,justifyContent:'center' }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center' }} >Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {

                        this.setModalCallVisible(false)
                        this.callFun(this.state.callNumber)
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1, borderLeftColor: '#e8e8e8', borderLeftWidth: 1,justifyContent:'center' }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center', color: '#3c81c4' }} >Call</Text>
                    </TouchableOpacity>
                </View>
            </View>

                        </View>
                    </View>
                </View>



            </Modal>



        );
    }

    /*user Block Model End here*/





    render() {
        return (
            <View style={{ flex: 1 }} >

                {this.modalBlockRender()}
                {this.modalCallRender()}
                <CustomHeader navigation={this.props.navigation} bArrow userbar />



                <HeaderTitle TitleName='Messages' />
                {/* <KeyboardAvoidingView style={{flex:1}}   > */}
                {/* <ScrollView style={{    padding:10,paddingBottom:100 }}> */}
                <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { fontWeight: 'bold', textAlign: 'center', fontFamily: 'Aileron-Regular' }]}>{this.state.username}</Text>
                {/* <View style={{flexDirection:'row',paddingTop:15}}>
                            <View style={{flex:5,flexDirection:'column'}}>
                                <Text allowFontScaling={false}  font={'CenturyGothic'} style={[CommanStyle.normaltext]}>Will the car be in the drive way<Text allowFontScaling={false}  style={{fontFamily:'Aileron-Regular'}}>?</Text></Text>
                                <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>25 Aug 2018</Text>
                            </View>
                            <View style={{flex:1,marginTop:5}}><Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>5:55 pm</Text></View>

                            
                        </View> */}


                <View style={styles.container}>
                    <View style={styles.container} contentContainerStyle={styles.contentContainer}>
                        {this.state.isLoader ? <ActivityIndicator size={'small'} /> : null}
                        <FlatList
                            data={this.state.allChatList}
                            extraData={this.state}
                            inverted={true}
                            renderItem={this.renderChatItem}
                            onEndReached={this.onScrollpage}
                            keyboardShouldPersistTaps={'always'}
                            onEndReachedThreshold={0.2}
                            keyExtractor={(item, index) => index.toString()}

                        />
                    </View>

                </View>



                {/* </ScrollView> */}

                {this.state.showBottomBar && (
                    <View style={{position:'absolute',bottom:this.state.shortHeight,width:DeviceW,backgroundColor:'white'}}>
                        {(this.state.is_chat_blocked == false) && (
                            <View>
                                <View style={{ flexDirection: 'row', padding: 10 }}>
                                    <TextInput placeholder="Type a message"
                                        multiline={true}
                                        numberOfLines={1}
                                        value={this.state.message}
                                        autoCapitalize="none"
                                        onContentSizeChange={(event) =>
                                            this.setState({ inputHeight: event.nativeEvent.contentSize.height })
                                        }
                                        onChangeText={(val) => this.setState({ message: val })}
                                        style={{ height: 42, flex: 5, backgroundColor: '#e8e8e8', padding: 10, borderRadius: 5, paddingTop: 12 }}
                                    />

                                    <TouchableOpacity
                                        onPress={() => this.sendMessage()}
                                        style={{ backgroundColor: '#4684da', marginLeft: 10, borderRadius: 3 }}>
                                        <Text allowFontScaling={false} style={{ color: 'white', padding: 10, paddingTop: 13, fontFamily: 'Aileron-Regular' }}>Send</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', alignSelf: 'flex-start', padding: 7, paddingTop: 0 }}>
                                    <TouchableOpacity onPress={this.chooseImage} >
                                        <Image
                                            resizeMode='contain'
                                            source={require('../../../assets/Icons/pin.png')}
                                            style={{ width: 25, height: 25, margin: 5 }}
                                        />
                                    </TouchableOpacity>

                                    <TouchableOpacity 
                                    //onPress={this._onCallPress}
                                    onPress={() => {
                                        this.setModalCallVisible(true)
                                    }}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require('../../../assets/Icons/call.png')}
                                            style={{ width: 25, height: 25, margin: 5 }}
                                        />
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(true)
                                    }}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require('../../../assets/Icons/block.png')}
                                            style={{ width: 25, height: 25, margin: 5, resizeMode: 'contain' }}
                                        />
                                    </TouchableOpacity>

                                </View>
                            </View>
                        )}

                        {(this.state.is_chat_blocked == true) && (
                            <View style={{ flexDirection: 'row', padding: 10, backgroundColor: '#ccc' }}>
                                <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>

                                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                                        {this.state.block_by_me &&
                                            'Blocked by me'
                                        }
                                        {!this.state.block_by_me &&
                                            'Blocked by ' + this.state.username
                                        }
                                    </Text>
                                </View>
                                {this.state.block_by_me &&
                                    <View style={{ flex: 1, alignItems: 'center' }}>

                                        <TouchableOpacity style={{ backgroundColor: '#fff', paddingHorizontal: 12, paddingVertical: 7 }}
                                            onPress={() => {
                                                this.setModalVisible(true)
                                            }}
                                        >
                                            <Text style={{ fontSize: 15, color: '#FA5858', fontWeight: 'bold' }}>Unblock</Text>
                                        </TouchableOpacity>

                                    </View>
                                }
                            </View>
                        )}
                    </View>
                )}





                {/* </KeyboardAvoidingView> */}

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom:50
    },

    contentContainer: {
        padding: 10
    },
    chatBbble: {
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 20
    },
    uimages: {
        width: 60,
        height: 60,
        overflow: 'hidden',
        borderRadius: 60,
    },

    utext: {
        maxWidth: 250,
        marginLeft: 5,
    },

    date: {
        fontSize: 12,
        color: '#b3b3b3',
        fontFamily: 'Avenir',
        marginBottom: 5,
    },

    chatMess: {
        backgroundColor: '#4684da',
        fontFamily: 'Avenir',
        padding: 8,
        color: 'white',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderTopRightRadius: 8,
    },

    chatBbblerevers: {
        flexDirection: 'row-reverse',
        marginTop: 5,
        marginBottom: 20
    },

    uimagesrevers: {
        width: 60,
        height: 60,
        overflow: 'hidden',
        borderRadius: 60,
    },

    utextrevers: {
        maxWidth: 250,
        marginRight: 5,
    },


    daterevers: {
        fontSize: 12,
        fontFamily: 'Avenir',
        color: '#b3b3b3',
        marginBottom: 5,
        textAlign: 'right',
    },

    chatMessrevers: {
        backgroundColor: '#eaeaea',
        padding: 8,
        color: '#000000',
        fontFamily: 'Avenir',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderTopLeftRadius: 8,
    },

    imgs: {
        width: 60,
        height: 60,
        borderRadius: 60,
    },

    footer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
    },

    input: {
        width: '85%',
        textAlignVertical: 'center',
        backgroundColor: '#eaeaea',
        borderRadius: 15,
        paddingHorizontal: 10,
        paddingVertical: 0,
    },
    uploadImg: {
        width: 150,
        height: 220,
        resizeMode: 'contain'
    }

})

