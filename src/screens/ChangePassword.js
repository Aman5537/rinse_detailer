import React, { Component } from 'react'
import { Text, View, TextInput, CheckBox, ScrollView, TouchableOpacity, Image } from 'react-native'
import { createAccount } from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import CommanStyle from '../config/Styles';
import Helper from '../Lib/Helper';

const SuccessMsg = (props) => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={require('../../assets/Icons/tik.png')} style={{ height: 50, width: 50, margin: 10 }} />
            <Text  allowFontScaling={false}   style={{ fontSize: 18, textAlign: "center",fontFamily: 'Aileron-Regular' }}>Your password has been changed.</Text>

            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" });
                if(props.pass_reset == 1){
                    props.navigation.navigate('SetHome');
                }else{
                    props.navigation.navigate('Profile');
                }
                
            }}>

                <LinearGradient

                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                    borderRadius={5}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:100 }}


                >

                    <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 22,fontFamily: 'Aileron-Regular' }} >Ok</Text>
                </LinearGradient>


                
            </TouchableOpacity>
        </View>
    )
}


export default class ChangePassword extends Component {

    static navigationOptions = { header: null }
    constructor(props) {
        super(props);
        this.state = {
            userdata:'',
            passwordChange: false,
            pass_reset:this.props.navigation.getParam('pass_reset', 0),
            SuccessMsg: <SuccessMsg navigation={this.props.navigation}/>,
            submitform: {
                current_password: "", new_password: "",
                confirm_password: "",
                validators: {
                current_password: { "required": true },
                new_password: { "required": true, "minLength": 8, "maxLength": 16 },
                confirm_password: { "required": true, "minLength": 8, "maxLength": 16 },
                }
            }
        }
    }



    setValues(key, value) {
        let submitform = { ...this.state.submitform }
        submitform[key] = value;
        this.setState({ submitform })
    }


    onChangePassword = () => {
        let isValid = Helper.validate(this.state.submitform);
        if (isValid) {

            if(this.state.submitform.new_password != this.state.submitform.confirm_password){
                Helper.showToast("New Password & Confirm Password must be the same.");
                return;
            }
            Helper.makeRequest({ url: "change-password", data: this.state.submitform }).then((submitData) => {
            // console.log('----logindata :  ' + JSON.stringify(logindata));
            if (submitData.status == 'true') {
                Helper.setData('userdata', submitData.data);
                this._onSendPress();
                
            }
            else {
                Helper.showToast(submitData.message)
            }
            })
        }
    }


    _onSendPress = () => {
        this.setState({ passwordChange: true })
        this.setState({ 'SuccessMsg': <SuccessMsg navigation={this.props.navigation} pass_reset={this.state.pass_reset}/> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.SuccessMsg })
        })
        
    }


    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: 'white', }}>
                    {this.state.pass_reset == 1 && (
                        <CustomHeader navigation={this.props.navigation} />
                    )}
                    {this.state.pass_reset == 0 && (
                        <CustomHeader navigation={this.props.navigation} bArrow />
                    )}
                    
                    <HeaderTitle TitleName='Change Password' />
                    <Card>


                        <TextInput 
                        secureTextEntry={true}  
                        placeholder='Current Password' 
                        style={{ backgroundColor: '#e9e9e9', borderRadius: 5, marginBottom: 10, height: 50, padding: 15 }} 
                        onChangeText={(password) => this.setValues('current_password', password)}
                        onSubmitEditing={() => { this.passwordInput.focus(); }}
                        />

                        <TextInput 
                        secureTextEntry={true}  
                        placeholder='New Password' 
                        style={{ backgroundColor: '#e9e9e9', borderRadius: 5, marginBottom: 10, height: 50, padding: 15 }} 
                        onChangeText={(password) => this.setValues('new_password', password)}
                        ref={(input) => { this.passwordInput = input; }}
                        onSubmitEditing={() => { this.confirmPasswordInput.focus(); }}
                        />

                        <TextInput 
                        secureTextEntry={true}  
                        placeholder='Confirm Password' 
                        style={{ backgroundColor: '#e9e9e9', borderRadius: 5, marginBottom: 10, height: 50, padding: 15 }} 
                        onChangeText={(cpassword) => this.setValues('confirm_password', cpassword)}
                        ref={(input) => { this.confirmPasswordInput = input; }}
                        onSubmitEditing={() => this.onChangePassword()}
                        returnKeyType='done'
                        />

                        <TouchableOpacity 
                        //onPress={this._onSendPress}
                        onPress={() => this.onChangePassword()}
                        >
                            <LinearGradient



                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                borderRadius={5}
                                style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20,height:52}}


                            >

                                <Text  allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >
                                    SUBMIT
  </Text>
                            </LinearGradient>
                        </TouchableOpacity>



                    </Card>
                </View>
            </ScrollView>
        )
    }
}