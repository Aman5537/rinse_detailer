import React, { Component } from 'react';
//import rect in our project
import {
  StyleSheet,
  View,
  ActivityIndicator,
  Image,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  Dimensions,
  Text, DeviceEventEmitter
} from 'react-native';
//import all the components we will need
import CameraScreen from './CameraScreen';
const { width, height } = Dimensions.get('window');
const size = Math.floor((Dimensions.get('window').width) / 3);
import { SimpleHeader } from '../common/SimpleHeader';
import LinearGradient from 'react-native-linear-gradient';
var self;

import {
  CameraKitCamera,
  CameraKitGallery,
  CameraKitGalleryView
} from 'react-native-camera-kit';
import Permissions from 'react-native-permissions';

var arr = [];

export default class Camera extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props);
    self  = this;
    this.state = {
      is_skip: this.props.navigation.getParam('is_skip', 0),
      album: this.props.albumName,
      presentedImage: undefined,
      selectedImages: [],
      showPresentedImage: false,
      shouldRenderCameraScreen: false,
      ButtonShow: 0,
      allSelectedImages: [],

    }
    

    Permissions.request('camera').then(response => {
            
      if (response == 'authorized') {
          Permissions.request('camera').then(response => {
              if (response == 'authorized') {
                  //this.selecteImage(cb);
              } else {
                  Helper.confirm("Allow " + Config.app_name + " access to your device's photo,media and camera.", (status) => {
                      if (status) {
                          Permissions.canOpenSettings().then(response => {
                              if(response){
                                  Permissions.openSettings();
                              }
                          })
                      }
                  })
              }
          })
      } else {
          Helper.confirm("Allow " + Config.app_name + " access to your device's photo,media and camera.", (status) => {
              if (status) {
                  Permissions.canOpenSettings().then(response => {
                      if(response){
                          Permissions.openSettings();
                      }
                  })
              }
          })
      }
  })


    // this.onImageLoad = this.onImageLoad.bind(this);
    const subscription = DeviceEventEmitter.addListener('onImageLoad', (data)=> {
      const refrest =  self.gallery.refreshGalleryView() 
      
     
      // console.log('keyboard opened');
    });

    //subscription.remove(); 
    
  }

  async onTapImage(event) {
    var array = [...this.state.allSelectedImages];
    var index = array.indexOf(event.nativeEvent.selected);
    
    if (index > -1) {

      array.splice(index, 1);
      this.setState({ allSelectedImages: array, ButtonShow: this.state.ButtonShow - 1 });
    }
    else {
      this.state.allSelectedImages.push(event.nativeEvent.selected);
      this.setState({ allSelectedImages: this.state.allSelectedImages, ButtonShow: this.state.ButtonShow + 1 })

    }

    const isSelected = event.nativeEvent.isSelected;
    const image = await CameraKitGallery.getImageForTapEvent(event.nativeEvent);

    if (!isSelected || _.get(image, 'selectedImageId') === _.get(this.state, 'presentedImage.selectedImageId')) {
      this.setState({ presentedImage: undefined, showPresentedImage: false });

    } else if (image) {
      this.setState({ presentedImage: image, showPresentedImage: true });
    }

  }

  // onImageLoad = (event) => {
  //   alert("Gourav")
  //   console.log(event);
  // }


  componentDidMount() {   
    this.setState({'allSelectedImages':[]});
  }

  componentWillUnmount(){
    DeviceEventEmitter.removeListener('onImageLoad', this.subscription)
  }


  addPhotos() {
    this.props.navigation.navigate('Portfolio', { images: this.state.allSelectedImages });
  }



  renderPresentedImage() {
    return (
      <View style={{ position: 'absolute', width, height, backgroundColor: 'green' }}>
        <View style={styles.container}>
          <Image
            resizeMode={"cover"}
            style={{ width: 300, height: 300 }}
            source={{ uri: this.state.presentedImage.imageUri }}
          />

          <Button
            title={'Back'}
            onPress={() => this.setState({ showPresentedImage: false })}
          />
        </View>
      </View>
    )
  }

  gotocamera() {
    this.props.navigation.navigate('CameraScreen');
  }

  render() {

    // if (this.state.shouldRenderCameraScreen) {
    //   return (<CameraScreen/>);
    // }
    return (
      <View style={{ flex: 1 }}>
        {this.state.is_skip === 1 && (
          <SimpleHeader pagetitle='Portfolio' navigation={this.props.navigation} bArrow />
        )}

        {this.state.is_skip !== 1 && (
          <SimpleHeader pagetitle='Portfolio' navigation={this.props.navigation} bArrow />
        )}
        <CameraKitGalleryView
          ref={gallery => this.gallery = gallery}
          style={{ flex: 1, margin: 0, }}
          albumName={this.state.album}
          minimumInteritemSpacing={5}
          minimumLineSpacing={5}
          columnCount={3}
          selection={{
            selectedImage: require('../../images/selected.png'),
            imagePosition: 'top-right',
          }}
          onTapImage={event => this.onTapImage(event)}
          getUrlOnTapImage={true}
          remoteDownloadIndicatorType={'progress-pie'} //spinner / progress-bar / progress-pie
          remoteDownloadIndicatorColor={'white'}
          // fileTypeSupport={{
          //   supportedFileTypes: ['image/jpeg','image/jpg','image/png'],
          //   unsupportedOverlayColor: "#00000055",
          //   unsupportedImage: require('../../images/unsupportedImage.png'),
          //   //unsupportedText: 'JPEG!!',
          //   unsupportedTextColor: '#ff0000'
          // }}
          selectedImages={this.state.allSelectedImages}
          customButtonStyle={{
            image: require('../../images/openCameraNew.png'),
            backgroundColor: '#FFFFFF'
          }}
          onCustomButtonPress={() => this.gotocamera()}
        />
        {this.state.showPresentedImage && this.renderPresentedImage()}

        {this.state.ButtonShow > 0 && (
          <TouchableOpacity
            onPress={() => this.addPhotos()}
          //onPress={() => this.props.navigation.navigate('DetailersWithoutNav',{is_skip:1})}
          >
            <LinearGradient
              colors={['#4fa4d8', '#488cd8', '#3b55d9']}
              style={{ padding: 10, alignItems: 'center', marginTop: 20, height: 52 }}
            >
              <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >
                ADD
                          </Text>
            </LinearGradient>
          </TouchableOpacity>
        )}
      </View>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});