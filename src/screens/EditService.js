import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, Platform, FlatList } from 'react-native';
import CommanStyle from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from "react-native-check-box";
import Helper from '../Lib/Helper';
import HTML from 'react-native-render-html';
export default class EditService extends Component {
    static navigationOptions = { header: null }




    constructor(props) {
        super(props);
        this.state = {
            is_skip: this.props.navigation.getParam('is_skip', 0),
            checkArray: [],
            serviceData: [],
            userData: ''

        }


        this.getAllService();


    }


    componentDidMount() {
        Helper.getData('userdata').then((responseData) => {

            if (responseData === null || responseData === 'undefined' || responseData === '') {
                //this.props.navigation.navigate('LoginNew');
            } else {
                this.setState({ userData: responseData });

            }
        })

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            this.getAllService();
        })
    }



    componentWillUnmount() {
        this.focusListener.remove()
    }

    getAllService = () => {

        //console.log(JSON.stringify(this.state.registerform));


        Helper.makeRequest({ url: "get-detailer-services" }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                this.setState({ 'serviceData': data.data });
                var newArray = []
                data.data.map((item,index) => {
                    newArray.push(item.id);
                    
                })
                //alert(newArray);
                this.setState({checkArray:newArray})
            }
            else {
                Helper.showToast(data.message)
            }
        })

    }






    render() {
        //alert(this.state.checkArray)
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>

                {this.state.is_skip === 1 && (
                    <CustomHeader navigation={this.props.navigation} />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow userbar />
                )}


                <AuthTitle TitleName='Services You Offer' />


                <ScrollView>
                    <View >

                        {this.state.serviceData.length > 0 &&
                            <FlatList
                                data={this.state.serviceData}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item, index }) =>
                                    <View style={{ flex: 1, marginTop: 10, flexDirection: 'row' }}>

                                        <View style={{ flex: 1, paddingLeft: 10, marginTop: -5 }}>
                                            <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { fontFamily: 'Aileron-Regular', fontWeight: 'bold' }]}>{item.service} ({item.average_time_from}-{item.average_time_to}mins)</Text>
                                            {/* <Text  allowFontScaling={false}  font={'CenturyGothic'} style={[CommanStyle.smallnormaltext,{marginRight:15}]} >
                        {item.description}
                            </Text> */}
                                            <HTML html={item.description} />

                                        </View>
                                        {/* <View style={{flex:2,marginTop:-5,alignItems:'flex-end',paddingRight:10}}>
                    <TouchableOpacity 
                    onPress= {this._onDeletePress}
                    style={{ height: 35, width: 35, flex: 1, alignItems: 'flex-end' }}>
                        <Image
                            resizeMode='contain'
                            style={{ height: 25, width: 25 }}
                            source={require('../../assets/Icons/cross.png')}
                        />
                    </TouchableOpacity>
                    </View> */}
                                    </View>



                                }
                                keyExtractor={item => item.email}
                            />
                        }
                        {this.state.serviceData.length == 0 &&
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text>Services not available</Text>
                            </View>

                        }


                    </View>
                </ScrollView>



                {this.state.userData.is_sub_detailer == 0 && (
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ServicesYouOffer',{checkArray:this.state.checkArray})}>
                        <LinearGradient



                            colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                            borderRadius={0}
                            style={{ padding: 10, alignItems: 'center', marginTop: 20 }}


                        >


                            <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#fff', fontSize: 22 }} >EDIT</Text>


                        </LinearGradient>
                    </TouchableOpacity>
                )}


            </View>
        )
    }
}
