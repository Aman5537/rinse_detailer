
import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity,ScrollView,Button,FlatList,ActivityIndicator } from 'react-native'
import { CustomHeader ,CardSection, AuthHeader,AuthTitle,HeaderTitle} from '../common';
import  CommanStyle  from '../config/Styles';
import Helper from '../Lib/Helper';
import { EventRegister } from 'react-native-event-listeners';
import Moment from "moment";

const BackArrrow = () => {
    return (
        <TouchableOpacity>
            <Image
                style={{ height: 25, width: 25, margin: 10 }}
                source={require('../../assets/Icons/harrow.png')}
            />
        </TouchableOpacity>

    )
}

const Bar = () => {
    return (
        <View style={{ flexDirection: 'row' }}> 
            <TouchableOpacity>
                <Image
                    style={{ height: 30, width: 30, margin: 10 }}
                    source={require('../../assets/Icons/profile.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={{}}>
                <Image
                    style={{ height: 25, width: 35, margin: 10 }}
                    source={require('../../assets/Icons/bar.png')}
                />
            </TouchableOpacity>


        </View>


    )
}


export default class AppointmentLists extends Component {
    static navigationOptions = { header: null }
    static navigationOptions = { header: null }

    state = {
        ActiveReqTab: 1,
        todayData:[],
        futureData:[],
        pastData:[],
        notefound1:'',
        notefound2:'',
        notefound3:'',
        todayPage: 1, todayIsLoading: false,
        todayNext_page_url: null,
        todayRefreshing: false,
        futurePage: 1, futureIsLoading: false,
        futureNext_page_url: null,
        futureRefreshing: false,
        pastPage: 1, pastIsLoading: false,
        pastNext_page_url: null,
        pastRefreshing: false,
        AppUserData:[],
       
    }


    componentDidMount() {

        

        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    //this.props.navigation.navigate('LoginNew');
                } else {
                    this.setState({ AppUserData: responseData });
                    
                    this._pullToRefreshToday();


                }
            })


        })
    }


    


    componentWillMount() {
        
        this.listenerRefreshApp = EventRegister.addEventListener('appoTabBarEvent', (data) => {
            if(data.refresh == true){
                this.ActiveTabAction(this.state.ActiveReqTab)
            }
            
        })


        

    }


    componentWillUnmount() {
        this.focusListener.remove()
        EventRegister.removeEventListener(this.listenerRefreshApp);
        
    }


    _pullToRefreshToday = () => {
        this.setState({ todayRefreshing: true,todayPage:1 },() => {
            this.getTodayData(false);
        });
        
    }


    _pullToRefreshFuture = () => {
        this.setState({ futureRefreshing: true,futurePage:1 },() => {
            this.getFutureData(false);
        });
        
    }

    _pullToRefreshPast = () => {
        

        this.setState({ pastRefreshing: true,pastPage:1 },() => {
            this.getPastData(false);
        });
        
    }




    getTodayData = (loadstatus) => {
        this.setState({ todayIsLoading: loadstatus, todayRefreshing: false });
        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();

        let form = {
            page: this.state.todayPage,
            type: 'today',
            utc_date_time: gettime,
            time:TimeZone
        }

        var MyLoader = false;
        if(this.state.todayPage == 1){
            MyLoader = true;
        }


        Helper.makeRequest({ url: "detailer-get-service-request-by-date", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                
                
                    if(this.state.todayPage == 1){
                        this.setState({
                            todayData:data.data.data,
                            todayIsLoading: false,
                            todayRefreshing: false,
                            todayNext_page_url: data.data.next_page_url
                        });
                    }
                    else{
                        this.setState({
                            todayData: [...this.state.todayData, ...data.data.data],
                            todayIsLoading: false,
                            todayRefreshing: false,
                            todayNext_page_url: data.data.next_page_url
                        });
                    }

                    this.state.todayPage++;
                
                
            }
            else {
                Helper.showToast(data.message)
            }

            this.setState({'notefound1': 'No Appointments Scheduled' });
        })
        .catch(err => {
            this.setState({ todayIsLoading: false, todayRefreshing: false });
           
        })
    }


    getFutureData = (loadstatus) => {
        this.setState({ futureIsLoading: loadstatus, futureRefreshing: false });
        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();

        let form = {
            page: this.state.futurePage,
            type: 'future',
            utc_date_time: gettime,
            time:TimeZone
        }

        var MyLoader = false;
        if(this.state.futurePage == 1){
            MyLoader = true;
        }


        Helper.makeRequest({ url: "detailer-get-service-request-by-date", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                
                
                    if(this.state.futurePage == 1){
                        this.setState({
                            futureData:data.data.data,
                            futureIsLoading: false,
                            futureRefreshing: false,
                            futureNext_page_url: data.data.next_page_url
                        });
                    }
                    else{
                        this.setState({
                            futureData: [...this.state.futureData, ...data.data.data],
                            futureIsLoading: false,
                            futureRefreshing: false,
                            futureNext_page_url: data.data.next_page_url
                        });
                    }

                    this.state.futurePage++;
                
                
            }
            else {
                Helper.showToast(data.message)
            }

            this.setState({'notefound2': 'No Appointments Scheduled' });
        })
        .catch(err => {
            this.setState({ futureIsLoading: false, futureRefreshing: false });
           
        })
    }


    getPastData = (loadstatus) => {
        this.setState({ pastIsLoading: loadstatus, pastRefreshing: false });
        
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        var TimeZone = tempdate.getTimezoneOffset();

        let form = {
            page: this.state.pastPage,
            type: 'past',
            utc_date_time: gettime,
            time:TimeZone
        }

        var MyLoader = false;
        if(this.state.pastPage == 1){
            MyLoader = true;
        }


        Helper.makeRequest({ url: "detailer-get-service-request-by-date", loader: MyLoader, method: "POST", data: form }).then((data) => {
            //alert(JSON.stringify(data));
            if (data.status == 'true') {
                
                
                
                    if(this.state.pastPage == 1){
                        this.setState({
                            pastData:data.data.data,
                            pastIsLoading: false,
                            pastRefreshing: false,
                            pastNext_page_url: data.data.next_page_url
                        });
                    }
                    else{
                        this.setState({
                            pastData: [...this.state.pastData, ...data.data.data],
                            pastIsLoading: false,
                            pastRefreshing: false,
                            pastNext_page_url: data.data.next_page_url
                        });
                    }

                    this.state.pastPage++;
                
                
            }
            else {
                Helper.showToast(data.message)
            }

            this.setState({'notefound3': 'No Appointments Scheduled' });
        })
        .catch(err => {
            this.setState({ pastIsLoading: false, pastRefreshing: false });
           
        })
    }


    ActiveTabAction = (val) => {
        if(val == 1){
            this.setState({ todayData: [] })
            this._pullToRefreshToday();
                    
        }
        if(val == 2){
            this.setState({ futureData: [] })
            this._pullToRefreshFuture();
                    
        }
        if(val == 3){
            this.setState({ pastData: [] })
            this._pullToRefreshPast();
        }
        this.setState({ ActiveReqTab: val })

    }


    onScrollToday = (e) => {
        this.getTodayData(true);
        
    }

    onScrollFuture = (e) => {
        this.getFutureData(true);
        
    }

    onScrollPast = (e) => {
        this.getPastData(true);
        
    }




    _renderTodayItem = ({ item,index }) => (
        <View style={CommanStyle.AppmyBorderDiv}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView',{ 'appo_id': item.id})} >
                <View>
                  <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{padding:10,fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>{Helper.getDateNewFormate(item.service_utc_date_time).split('@ ')[0]}@{Helper.getDateNewFormate(item.service_utc_date_time).split('@ ')[1]}</Text>
                </View>
                <View style={CommanStyle.AppmycardDiv}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                    
                    {item.is_finished != 1 && item.payment_request.status == 'Requested' && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'red'}]}>
                        Payment Override
                        </Text>
                    )}

                    {((item.confirm_quote.is_canceled == 1 || item.confirm_quote.status == 'Canceled' || item.is_service_hired_cancel == 1) && item.is_finished != 1 && item.is_canceled_by_detailer == 1) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'black'}]}>
                        Detailer Canceled
                        </Text>
                    )}
                    {((item.confirm_quote.is_canceled == 1 || item.confirm_quote.status == 'Canceled' || item.is_service_hired_cancel == 1) && item.is_finished != 1 && item.is_canceled_by_detailer == 0) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'black'}]}>
                        Customer Canceled
                        </Text>
                    )}

                    {(item.confirm_quote.is_canceled != 1 && item.confirm_quote.status != 'Canceled' && item.is_finished != 1 && item.confirm_quote.status != 'Completed' && item.is_service_hired_cancel == 0) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular'}]}>
                        {Helper.getDateNewFormate(item.service_utc_date_time).split('@ ')[1]}
                        </Text>
                    )}    

                    {(item.confirm_quote.status == 'Completed' && item.is_finished != 1 && item.payment_request == '' && item.is_service_hired_cancel == 0) && (
                        <Text allowFontScaling={false} font={'Aileron-Regular-bold'} style={[CommanStyle.normaltitle, { textAlign:'center',color: 'red', fontFamily: 'Aileron-Regular' }]}>Payment Pending</Text>

                    )}

                    {(item.is_finished == 1) && (
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'center',fontFamily: 'Aileron-Regular',color:'green'}]}>
                        Completed
                        </Text>
                    )}
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                        {item.user.first_name} {item.user.last_name}
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >
                          {item.vehicles.map((vitem,vindex)=> (
                    (vindex == 0) ? vitem.vehicle.model_detail.name : ', '+vitem.vehicle.model_detail.name
                    
                    
            ))}
                          </Text>
                          {/* <Text allowFontScaling={false}   >{'\n'}Detailer-Larry</Text> */}
                          <Text allowFontScaling={false}   >{'\n'}{item.location.location}</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                   
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
        </TouchableOpacity>  
        </View>  
    )


    renderSeparator = () => (
        <View
          style={{
            borderBottomColor:'#ccc',
            borderBottomWidth:1,
            
          }}
        />
    );


    renderFooter = () => (
        <View
          style={{
            borderBottomColor:'#ccc',
            borderBottomWidth:1,
            
          }}
        />
    );



    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation}  userbar bArrow/>
                
                <HeaderTitle TitleName='Appointments' date2 navigation={this.props.navigation}/>
                <View style={{borderTopColor:'#d9d9d9',borderTopWidth:1}}></View>
                <View style={{ flexDirection: 'row', height: 55, justifyContent: 'center' }}>

                    <TouchableOpacity onPress={() => {
                        this.ActiveTabAction(1)

                    }}
                        style={{ flex: 1 }}>
                        <View style={[CommanStyle.tabButton, this.state.ActiveReqTab === 1 ? style.ActiveTab : '']} >

                            <Text allowFontScaling={false} font={'TypoGroteskRounded'} style={[CommanStyle.tabButtonText, { fontFamily: 'Aileron-Regular' }]}>PENDING</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.ActiveTabAction(2)
                    }}
                        style={{ flex: 1 }}
                    >
                        <View style={[CommanStyle.tabButton, this.state.ActiveReqTab === 2 ? style.ActiveTab : '']}>

                            <Text allowFontScaling={false} font={'TypoGroteskRounded'} style={[CommanStyle.tabButtonText, { fontFamily: 'Aileron-Regular' }]}>FUTURE</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.ActiveTabAction(3)
                    }}
                        style={{ flex: 1 }}
                    >
                        <View style={[CommanStyle.tabButton, this.state.ActiveReqTab === 3 ? style.ActiveTab : '']} >

                            <Text allowFontScaling={false} font={'TypoGroteskRounded'} style={[CommanStyle.tabButtonText, { fontFamily: 'Aileron-Regular' }]}>PAST</Text>

                        </View>
                    </TouchableOpacity>
                </View>

                
                <View style={{ flex: 1, padding: 10, paddingTop: 0 }}>

                    {this.state.ActiveReqTab === 1 && (
                        
                        <View>
                            {this.state.todayData.length > 0 ?
                                <FlatList
                                    data={this.state.todayData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderTodayItem}
                                    refreshing={this.state.todayRefreshing}
                                    onRefresh={this._pullToRefreshToday}
                                    onEndReached={this.onScrollToday}
                                    onEndReachedThreshold={0.5}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}
                                />
                                : !this.state.todayIsLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>{this.state.notefound1}</Text>
                                    </View> : null
                            }
                            {this.state.isLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            }



                            





                        </View>

                    )}


                    {this.state.ActiveReqTab === 2 && (
                        <View>

                            {this.state.futureData.length > 0 ?
                                <FlatList
                                    data={this.state.futureData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderTodayItem}
                                    refreshing={this.state.futureRefreshing}
                                    onRefresh={this._pullToRefreshFuture}
                                    onEndReached={this.onScrollFuture}
                                    onEndReachedThreshold={0.5}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}
                                />
                                : !this.state.futureIsLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>{this.state.notefound2}</Text>
                                    </View> : null
                            }
                            {this.state.isLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            }

                            
                        </View>
                    )}



                    {this.state.ActiveReqTab === 3 && (
                        <View>
                            {this.state.pastData.length > 0 ?
                                <FlatList
                                    data={this.state.pastData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index}
                                    renderItem={this._renderTodayItem}
                                    refreshing={this.state.pastRefreshing}
                                    onRefresh={this._pullToRefreshPast}
                                    onEndReached={this.onScrollPast}
                                    onEndReachedThreshold={0.3}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    ListFooterComponent={this.renderFooter}
                                />
                                : !this.state.pastIsLoading ?
                                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>{this.state.notefound3}</Text>
                                    </View> : null
                            }
                            {this.state.appIsLoading ?
                                <View style={{ marginTop: 30, marginBottom: 30 }}>
                                    <ActivityIndicator size="large" color="#000000" />
                                </View>
                                : null
                            }
                             
                        </View>
                    )}
                </View>

                  
                

                {/* <View>
                  <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{padding:10,fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>Mon Jan 31</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')}>
                <View style={[CommanStyle.AppmycardDiv,{backgroundColor:'#f6f6f6'}]}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',paddingLeft:10,fontFamily: 'Aileron-Regular'}]}>10:00 am</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >Outside (30-45min)</Text>
                          <Text allowFontScaling={false}  >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false}  >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>

                <View>
                  <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{padding:10,fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>Wed Jan 31</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')}>
                
                <View style={[CommanStyle.AppmycardDiv,{backgroundColor:'#f6f6f6'}]}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',paddingLeft:10,color:'red',fontFamily: 'Aileron-Regular'}]}>Canceled</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >Outside (30-45min)</Text>
                          <Text allowFontScaling={false}  >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false}  >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                   
                    </View>
                </View>
                </TouchableOpacity>

                


                <View>
                  <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{padding:10,fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>Mon Jan 31</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')}>
                <View style={[CommanStyle.AppmycardDiv,{backgroundColor:'#f6f6f6'}]}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',paddingLeft:10,fontFamily: 'Aileron-Regular'}]}>10:00 am</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >Outside (30-45min)</Text>
                          <Text allowFontScaling={false}  >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false}  >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>

                <View>
                  <Text allowFontScaling={false} style={[CommanStyle.normaltitle,{padding:10,fontFamily: 'Aileron-Regular',fontWeight:'bold'}]}>Wed Jan 31</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('AppointmentView')}>
                <View style={[CommanStyle.AppmycardDiv,{backgroundColor:'#f6f6f6'}]}>
                    <View style={{flex:2,borderRightColor:'#d8d8d8',borderRightWidth:1,justifyContent:'center',alignItems:'center'}}>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle,{textAlign:'left',paddingLeft:10,color:'red',fontFamily: 'Aileron-Regular'}]}>Canceled</Text>
                        
                        
                    </View>
                    <View style={{flex:5,padding:10}}>
                    
                    <View>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltitle]}>
                          Jerome King
                        </Text>
                        <Text allowFontScaling={false}  style={[CommanStyle.normaltext]}>
                          <Text allowFontScaling={false}  >Outside (30-45min)</Text>
                          <Text allowFontScaling={false}  >{'\n'}Detailer-Larry</Text>
                          <Text allowFontScaling={false}  >{'\n'}343324 Caldy st {'\n'}Tampa FL 333623</Text>
                        
                        </Text>
                        
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <Image
                            style={{ height: 30, width: 30,opacity:0.5,alignSelf:'center'}}
                            source={require('../../assets/Icons/arrowright.png')}
                            
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>
 */}

                

                
               
            </View>
        )
    }
}

const style = StyleSheet.create({
    containerView: {
        margin: 2,
        height: 170,
        justifyContent: 'space-evenly',
        alignItems: 'center',

        width: Dimensions.get('window').width / 2 - 6
    },
    textStyle: {

        fontSize: 18,
        fontWeight: 'bold'
    },

    BoxView: {
        backgroundColor: '#22578d',
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width / 3.5,
        height: Dimensions.get('window').width / 3.5,
        alignItems: 'center', justifyContent: 'center'
    },
    ActiveTab: {
        backgroundColor: '#4684da'
    }
    


})


