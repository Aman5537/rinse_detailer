import React, { Component } from 'react'
import { Text, View, Dimensions, Image, StyleSheet, TouchableOpacity,Alert } from 'react-native'
import { CustomHeader ,CardSection, AuthHeader,AuthTitle,HeaderTitle} from '../common';
import  CommanStyle  from '../config/Styles';
//import GeolocationExample from '../common/GeolocationExample'
import Geolocation from '@react-native-community/geolocation';






export default class Legal extends Component {
    static navigationOptions = { header: null }
    
    constructor(props) {
        super(props);
        
        //Geolocation.getCurrentPosition(info => alert(JSON.stringify(info)));
    }

    

    render() {
        return (

            <View style={{ flex: 1, }}>

                <CustomHeader navigation={this.props.navigation}  userbar bArrow/>
                <HeaderTitle TitleName='Legal' />
                <View style={{borderTopColor:'#e8e8e8',borderTopWidth:1}}>
                
                </View>
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('Term')}
                    style={{}}>
                <View style={{flexDirection:'row',padding:10,borderBottomColor:'#e8e8e8',borderBottomWidth:1}}>
                    <View style={{flex:4}}>
                        <Text  allowFontScaling={false}   style={{fontSize:18,fontFamily: 'Aileron-Regular'}} >Terms & Conditions</Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                    
                        <Image
                        source={require("../../assets/Icons/arrowright.png")}
                        style={{ width: 20, height: 20,opacity:0.5,marginTop:5 }}
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('Policy')}
                    style={{}}>
                <View style={{flexDirection:'row',padding:10,borderBottomColor:'#e8e8e8',borderBottomWidth:1}}>
                    <View style={{flex:4}}>
                        <Text  allowFontScaling={false}   style={{fontSize:18,fontFamily: 'Aileron-Regular'}}>Privacy Policy</Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                    
                        <Image
                        source={require("../../assets/Icons/arrowright.png")}
                        style={{ width: 20, height: 20,opacity:0.5,marginTop:5 }}
                        />
                    
                    </View>
                </View>
                </TouchableOpacity>
                
                
            </View>
        )
    }
}

const style = StyleSheet.create({
    textArea: {
        
    }
    


})


