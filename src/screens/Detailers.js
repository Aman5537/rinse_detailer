import React, { Component } from 'react'
import { Text, View, TextInput,RefreshControl, ScrollView, Dimensions, TouchableOpacity, Image, Button, FlatList, ActivityIndicator } from 'react-native'
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import Helper from '../Lib/Helper';
import CheckBox from "react-native-check-box";
import ChatController from '../Lib/ChatController';
import { EventRegister } from 'react-native-event-listeners';
import Moment from "moment";

var is_upgraded = true;

const UpgradeMsg = (props) => {
    return (
        <View style={{ marginLeft: -10, marginRight: -10, marginTop: -5 }}>
            <TouchableOpacity onPress={() => {
                global.state['modal'].setState({ modalVisible: false, Msg: "" })
            }}

            >
            <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>

                {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
            </TouchableOpacity>

            <View style={{ padding: 10 }}>
                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", marginTop: 10, fontFamily: 'Aileron-Regular' }}>You must upgrade to add detailers to your account.</Text>

                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                    props.navigation.navigate('Upgrade', { is_skip: 1 });
                }}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                >

                    <LinearGradient

                        colors={['#5ced31', '#4acb23', '#37a313']}
                        borderRadius={5}
                        style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20, width: 200 }}


                    >

                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22, fontFamily: 'Aileron-Regular' }} >UPGRADE</Text>
                    </LinearGradient>



                </TouchableOpacity>

                {/* <TouchableOpacity 
                onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                    props.navigation.navigate('AddDetailer');
                }}
                style={{justifyContent: 'center', alignItems: 'center' }}
            >

                <LinearGradient 
                    colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:200}}
                >
                        
                    <Text  allowFontScaling={false}   style={{ color:'#fff',fontSize:18}} >Add Detailer</Text>
               
                </LinearGradient>

            </TouchableOpacity> */}
            </View>
        </View>
    )
}


class DeleteMsg extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
       
    }


    render(){
        return (
            <View style={{ height: 120, marginLeft: -10, marginRight: -10, marginTop: -5 }}>
                {/* <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })
                }}
    
                >
                    <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} />
                </TouchableOpacity> */}
                <View style={{ padding: 10 }}>
                    <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center", marginTop: 10, fontFamily: 'Aileron-Regular' }}>Are you sure you want to delete this detailer?</Text>
    
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => {
                            global.state['modal'].setState({ modalVisible: false, Msg: "" });
                            
                            this.props.callApi(this.props.deleteID);
                        }}
                            style={{ flex: 1 }}
                        >
    
                            <LinearGradient
                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}
    
    
                            >
    
                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >Yes</Text>
    
                            </LinearGradient>
    
    
                        </TouchableOpacity>
    
                        <TouchableOpacity onPress={() => {
                            global.state['modal'].setState({ modalVisible: false, Msg: "" })
                        }}
    
                            style={{ flex: 1 }}
                        >
    
                            <LinearGradient
                                colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                                style={{ padding: 8, margin: 10, alignItems: 'center', height: 40 }}
                            >
    
                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18 }} >Cancel</Text>
    
                            </LinearGradient>
    
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
    
}


export default class Detailers extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            page: 1, isLoading: false,
            DetailersData: [], next_page_url: null,
            refreshing: false,

            UpgradeMsg: <UpgradeMsg navigation={this.props.navigation} />,
            DeleteMsg: <DeleteMsg />,
            is_skip: this.props.navigation.getParam('is_skip', 0),
            AppUserData:{},
            totalDitailer:0
        }

    }

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {

            Helper.getData('userdata').then((responseData) => {
            
                if (responseData === null || responseData === 'undefined' || responseData === '') {
                    //this.props.navigation.navigate('LoginNew');
                } else {
                    this.setState({AppUserData:responseData});
                    ChatController.checkConnection("detailer", (cb) => {
                        if (cb) {
                            Detailers.getBadegesCount(this.state.AppUserData.id)
                        }
    
                    })
                    
                    
                }
            })
            
            this._pullToRefresh();
        })
    }

    /* Bootom Tab Budget event */
    static getBadegesCount(user_id) {
        let tempdate = new Date();
        let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")

        let form = {
            user_id: user_id,
            role_id:2,
            utc_date_time: gettime
            
        }
        ChatController.getAppoinmentMessageCount('get_appointment_message_count', form);
    }


    componentWillMount() {
        this.countlistener = EventRegister.addEventListener('get_appointment_message_count_response', (data) => {
            console.log('count--', data)
            if (data.status == 'true') {
                Helper.appoCount = data.total_unread_appointment;
                Helper.messageCount = data.total_unread_message;
                Helper.requestCount = data.total_unread_request;
                
                this.props.navigation.setParams({ badgeCount: 123 })
                
            } else {
                this.setState({ isLoader: false });
            }

        })

    }

    componentWillUnmount() {
        
        EventRegister.removeEventListener(this.countlistener);
        this.focusListener.remove()
    }

    /* Bootom Tab Budget event */
    

    _pullToRefresh = () => {
        this.setState({ refreshing: true,page:1 },() => {
            this.getAllDetailers(false);
        });
        
    }



    getAllDetailers = (loadstatus) => {
        this.setState({ isLoading: loadstatus, refreshing: false });
        
        let form = {
            page: this.state.page,
            is_password_reset:''
        }

        var MyLoader = false;
        if(this.state.page == 1){
            MyLoader = true;
        }
        Helper.makeRequest({ url: "get-sub-detailers", loader: MyLoader, method: "POST", data: form }).then((responsedata) => {
            if (responsedata.status) {
                
                if(this.state.page == 1){
                    this.setState({
                        DetailersData:responsedata.data.data,
                        isLoading: false,
                        refreshing: false,
                        next_page_url: responsedata.data.next_page_url,
                        totalDitailer:responsedata.data.total
                    });
                }
                else{
                    this.setState({
                        DetailersData: [...this.state.DetailersData, ...responsedata.data.data],
                        isLoading: false,
                        refreshing: false,
                        next_page_url: responsedata.data.next_page_url,
                        totalDitailer:responsedata.data.total
                    });
                }
                
                this.state.page++;
            }
            else {
                this.setState({ isLoading: false, refreshing: false });
                Helper.showToast(responsedata.message);
            }
        }).catch(err => {
            this.setState({ isLoading: false, refreshing: false });
            //Helper.showToast("Please try again");
        })
    }



    _onSendPress = () => {
        if(this.state.totalDitailer < this.state.AppUserData.access_list){
            this.props.navigation.navigate('AddDetailer', { is_skip: this.state.is_skip })
        }
        else{                
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.UpgradeMsg })
        }

        //is_upgraded = !is_upgraded;
    }

    _onDeletePress = (id) => {
        this.setState({ 'DeleteMsg': <DeleteMsg deleteID={id}  callApi={this.callApi}/> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.DeleteMsg })
        });

    }

    callApi = (id) => {
        Helper.makeRequest({ url: "delete-detailer",loader:false,  data: {detailer_id:id} }).then((data) => {
            if (data.status == 'true') {
                
                let arr = [];
                for (var key in this.state.DetailersData) {
                    if(this.state.DetailersData[key].id !== id) {
                        arr.push(this.state.DetailersData[key]);
                    }
                }
                    
                this.setState({DetailersData:arr},() => {
                    if(this.state.DetailersData.length < 1){
                        this._pullToRefresh()
                    }
                    
                });
            }
            else {
                Helper.showToast(data.message)
            }
        })
    }


    setCheckedValue = (index,id) => {
        const newArray = [...this.state.DetailersData];
        newArray[index].quotes_permission = !newArray[index].quotes_permission;
        var CheckType = newArray[index].quotes_permission; 

        Helper.makeRequest({ url: "detailer-qupte-permission", data: {sub_detailer_id:id,quotes_permission:CheckType} }).then((data) => {
            if (data.status == 'true') {
                this.setState({ DetailersData: newArray });
                Helper.showToast(data.message)
                
            }
            else {
                //this.setState({ DetailersData: newArray });
                Helper.showToast(data.message)
            }
        })


        
        
        


        

        //let data = arr.toString();

    }


    _renderItem = ({ item,index }) => (
        <View style={{ paddingBottom: 15, borderBottomColor: '#d8d8d8', borderBottomWidth: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                <Text allowFontScaling={false} style={{ flex: 5, fontFamily: 'Aileron-Regular', fontWeight: 'bold' }}>
                    {item.first_name} {item.last_name}
                    {(item.is_password_reset == 1) ? <Text style={{color:'orange'}}> (Pending Login)</Text> : '' }
                    
                </Text>
                <TouchableOpacity
                    onPress={() => this._onDeletePress(item.id)}
                    style={{ height: 35, width: 35, flex: 1, alignitem: 'flex-end' }}>
                    <Image
                        resizeMode='contain'
                        style={{ height: 30, width: 30,alignSelf:'flex-end' }}
                        source={require('../../assets/Icons/cross.png')}
                    />
                </TouchableOpacity>

            </View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1, paddingLeft: 10 }}>
                    <CommonButton name="Message" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('Chat',{'service_request_id':0,'other_user_id':item.id,'user_id':this.state.AppUserData.id,'username':item.first_name+' '+item.last_name,country_code:item.country_code,mobile:item.mobile})} />
                </View>
                <View style={{ flex: 1, paddingLeft: 10 }}>
                {((item.is_password_reset == 0) && (item.is_location != 0)) && (
                    <CommonButton name="Track" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('TrackDetailer',{detailer_id:item.id,detailer_name:item.first_name+' '+item.last_name,detailer_country_code:item.country_code,detailer_mobile:item.mobile})} /> 
                )}
                </View>
                <View style={{ flex: 1 }}></View>


            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center',flex:1,paddingHorizontal: 10 }}>
                        <TouchableOpacity
                        onPress={() => {
                            this.setCheckedValue(
                                index,item.id
                            )
                        }}
                        style={{ flexDirection: 'row'}}
                        >
                        <View style={{ flexDirection: 'row',flex:1}}>
                                {/* <CheckBox
                                    // style={{backgroundColor:'white'}}
                                    value={this.state.checked}
                                    onValueChange={() => this.setState({ checked: !this.state.checked })}
                                /> */}

<CheckBox
                                    style={{ flex: 1, paddingTop: 18 }}
                                    onClick={() => {
                                        this.setCheckedValue(
                                            index,item.id
                                        )
                                    }}
                                    isChecked={item.quotes_permission}
                                    
                                    
                                    checkedImage={
                                        <Image
                                            source={require("../../assets/Icons/tick.png")}
                                            style={{ width: 20, height: 20,resizeMode:'contain' }}
                                        />
                                    }
                                    unCheckedImage={
                                        <Image
                                            source={require("../../assets/Icons/untick.png")}
                                            style={{ width: 20, height: 20,resizeMode:'contain' }}
                                        />
                                    }
                                />


            </View>
                                <View style={{paddingTop:15,flex:7}}>
                                    <Text  allowFontScaling={false}  style={{fontSize:15}}>I want this detailer to be able to submit quotes.</Text>
                                </View>
                                 
                                
                            </TouchableOpacity>
            </View>
        </View>
    )

    _distanceFromEnd(event){
        let {
            contentSize,
            contentInset,
            contentOffset,
            layoutMeasurement,
        } = event.nativeEvent;

        let contentLength;
        let trailingInset;
        let scrollOffset;
        let viewportLength;
        let horizontal = false;
        if (horizontal) {
            contentLength = contentSize.width;
            trailingInset = contentInset.right;
            scrollOffset = contentOffset.x;
            viewportLength = layoutMeasurement.width;
        } else {
            contentLength = contentSize.height;
            trailingInset = contentInset.bottom;
            scrollOffset = contentOffset.y;
            viewportLength = layoutMeasurement.height;
        }

        return contentLength + trailingInset - scrollOffset - viewportLength;
    }

    onScroll = (e) => {
        if (this._distanceFromEnd(e) < 30 && !this.state.isLoading && this.state.next_page_url) {
            this.getAllDetailers(true);
        }
    }

    render() {
        
        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>

                {this.state.is_skip === 1 && (
                    <CustomHeader navigation={this.props.navigation} />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader navigation={this.props.navigation} userbar />
                )}


                <HeaderTitle TitleName='Detailers' AddD onAddClick={this._onSendPress} />
                {/* <TouchableOpacity style={{position:'absolute',top:90,right:15}} onPress={this._onSendPress}>
                <Image 
                            style={{width: 35, height: 35}}
                             source={require('../../assets/Icons/add-icon.png')}
                            />
                </TouchableOpacity> */}

                <ScrollView style={{ flex: 1, borderColor: '#d8d8d8', borderWidth: 1 }}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._pullToRefresh}
                        />
                    }
                    onScroll={this.onScroll} >
                    {this.state.DetailersData.length > 0 ?
                        <FlatList
                            data={this.state.DetailersData}
                            extraData={this.state}
                            keyExtractor={(item, index) => index}
                            renderItem={this._renderItem}
                            
                        />
                        : !this.state.isLoading ?
                            <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>Not found</Text>
                            </View> : null
                    }
                    {this.state.isLoading ?
                        <View style={{ marginTop: 30, marginBottom: 30 }}>
                            <ActivityIndicator size="large" color="#000000" />
                        </View>
                        : null
                    }
                </ScrollView>

                {this.state.is_skip === 1 && (
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Upgrade', { is_skip: 1 })}>
                            <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { textAlign: 'center', fontSize: 20, textDecorationLine: 'underline' }]} >SKIP</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Upgrade', { is_skip: 1 })}
                        >
                            <LinearGradient



                                colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                                style={{ padding: 10, alignItems: 'center', marginTop: 20 }}


                            >

                                <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >
                                    CONTINUE
  </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                )}

                {/* <Text  allowFontScaling={false}   style={[CommanStyle.normaltitle, { textAlign: 'center', fontSize: 20, textDecorationLine: 'underline' }]} >SKIP</Text>

                <TouchableOpacity>
                    <LinearGradient



                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 10, alignItems: 'center', marginTop: 20 }}

                        onPress={() => this.props.navigation.navigate('DrawerRoot')}
                    >

                        <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 22 }} >
                            CONTINUE
  </Text>
                    </LinearGradient>
                </TouchableOpacity> */}
            </View>

        )
    }
}



