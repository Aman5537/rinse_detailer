import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView,StyleSheet, Dimensions, TouchableOpacity,Image } from 'react-native'
import { createAccount } from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from "react-native-check-box";
import CountryList from '../../components/CountryList';
import Helper from '../Lib/Helper';

const SuccessMsg = (props) => {
    return (
        <View style={{}}>
            
            <Text  allowFontScaling={false}   style={{ fontSize: 16,textAlign: 'center',fontFamily: 'Aileron-Regular' }}>We will send this person an email inviting them to join.</Text>

            <View style={{alignItems: 'center',}}>
            <TouchableOpacity 
                onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" });
                    props.navigation.goBack(null);
                }}
                style={{justifyContent: 'center' }}
            >

                <LinearGradient
                    colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                    borderRadius={5}
                    style={{ padding: 7, alignItems: 'center', borderRadius: 3, marginTop: 20,width:80 }}
                >
                    <Text  allowFontScaling={false}   style={{ color: '#fff', fontSize: 22,fontFamily: 'Aileron-Regular' }} >Ok</Text>
                </LinearGradient>
            </TouchableOpacity>

            
            </View>
        </View>
    )
}



export default class AddDetailer extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            SuccessMsg: <SuccessMsg navigation={this.props.navigation}/>, 
            is_skip: this.props.navigation.getParam('is_skip', 0), 
            submitform: {
                first_name: '',
                last_name: '',
                email: '',
                mobile: '',
                country_code: '1',
                validators: {
                    first_name: { required: true, "minLength": 3,"maxLength": 20  },
                    last_name: { required: true, "minLength": 3,"maxLength": 20 },
                    email: { required: true, "email": true},
                    mobile: { required: true, "minLength": 7, "maxLength": 15 },
                    country_code:{ required: true}
                    
                }
            },  
        }
    }
    

    _onSuccessPress = () => {
       
        global.state['modal'].setState({ modalVisible: true, Msg: this.state.SuccessMsg })
    }

    setValues(key, value) {
        let submitform = { ...this.state.submitform }
        
        if (key == 'mobile') {
            if(this.state.submitform.mobile.length > value.length){
                submitform[key] = value;
                this.setState({ submitform })
            }
            else{
                value = value.replace(/-/g,"");
                if(value.length > 3){
                    value = value.slice(0,3)+"-"+value.slice(3,6)+"-"+value.slice(6,15);
                }

                submitform[key] = value;
                this.setState({ submitform })
            }
        }

        submitform[key] = value;
        this.setState({ submitform });



    }

    saveDetailer(){
        

        let isValid = Helper.validate(this.state.submitform);
        if (isValid) {

            let tempdata = new FormData();
            
            tempdata.append('first_name', this.state.submitform.first_name);
            tempdata.append('last_name', this.state.submitform.last_name);
            tempdata.append('country_code', this.state.submitform.country_code);
            //tempdata.append('mobile', this.state.submitform.mobile);
            tempdata.append('email', this.state.submitform.email);
            
            tempdata.append('mobile', this.state.submitform.mobile.replace(/-/g,""));
            
            if(this.state.isChecked == true){
                tempdata.append('quotes_permission', 1);
            }
            else{
                tempdata.append('quotes_permission', 0);
            }
            
            
            Helper.makeRequest({ url: "add-detailer", method: "POSTUPLOAD", data: tempdata }).then((data) => {
                if (data.status == 'true') {
                    //Helper.showToast(data.message);

                    this.setValues('first_name', '');
                    this.setValues('last_name', '');
                    this.setValues('country_code', 1);
                    this.setValues('mobile', '');
                    this.setValues('email', '');
                    
                    this.setState({isChecked:false});
                    
                    this._onSuccessPress();
                    //this.props.navigation.navigate('Detailers');
                }
                else {
                    Helper.showToast(data.message)
                }
            })
        }
    }


    render() {
        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>

                {this.state.is_skip === 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow />
                )}

                {this.state.is_skip !== 1 && (
                    <CustomHeader navigation={this.props.navigation} bArrow userbar />
                )}
                

                <HeaderTitle TitleName='Add Detailer' />

                <ScrollView>

                    <Card>
                       
                        <TextInput 
                        placeholder='First Name' 
                        style={styles.myTextBox} 
                        value={this.state.submitform.first_name}
                        onChangeText={(txt) => { this.setValues('first_name', txt) }}
                        onSubmitEditing={() => { this.lastNameInput.focus(); }}
                        ref={(input) => { this.firstNameInput = input; }}
                        />
                        <TextInput 
                        placeholder='Last Name' 
                        style={styles.myTextBox}
                        value={this.state.submitform.last_name}
                        onChangeText={(txt) => { this.setValues('last_name', txt) }}
                        onSubmitEditing={() => { this.EmailInput.focus(); }}
                        ref={(input) => { this.lastNameInput = input; }}
                        />

                        <TextInput 
                        placeholder='Email Address' 
                        style={styles.myTextBox} 
                        value={this.state.submitform.email}
                        onChangeText={(txt) => { this.setValues('email', txt) }}
                        ref={(input) => { this.EmailInput = input; }}
                        />


                        
                        {/* <TextInput placeholder='Phone Number' returnKeyType='done' keyboardType="number-pad" style={styles.myTextBox} /> */}

                        <CountryList
            form={this.state.submitform}
            showtoggleicon={false}
            setValues={(mobile_number) => this.setValues('mobile', mobile_number)}
            //onSelect={this.selectCat}
            inputlable="Phone No."
            selectedItems={this.state.submitform.country_code}
            mvalue={this.state.submitform.mobile}
        />

                            
                        <View style={{ flexDirection: 'row', alignItems: 'center',flex:1 }}>
                        <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                isChecked: !this.state.isChecked
                            });
                        }}
                        style={{ flexDirection: 'row'}}
                        >
                        <View style={{ flexDirection: 'row',flex:1}}>
                                {/* <CheckBox
                                    // style={{backgroundColor:'white'}}
                                    value={this.state.checked}
                                    onValueChange={() => this.setState({ checked: !this.state.checked })}
                                /> */}

<CheckBox
                                    style={{ flex: 1, paddingTop: 18 }}
                                    onClick={() => {
                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        });
                                    }}
                                    isChecked={this.state.isChecked}
                                    
                                    
                                    checkedImage={
                                        <Image
                                            source={require("../../assets/Icons/tick.png")}
                                            style={{ width: 20, height: 20,resizeMode:'contain' }}
                                        />
                                    }
                                    unCheckedImage={
                                        <Image
                                            source={require("../../assets/Icons/untick.png")}
                                            style={{ width: 20, height: 20,resizeMode:'contain' }}
                                        />
                                    }
                                />


            </View>
                                <View style={{paddingTop:15,flex:7}}>
                                    <Text  allowFontScaling={false}  style={{fontSize:15}}>I want this detailer to be able to submit quotes.</Text>
                                </View>
                                 
                                
                            </TouchableOpacity>
                        </View>
                    <TouchableOpacity 
                    onPress={() => this.saveDetailer()}
                    //onPress={this._onSuccessPress}
                    >
                        <LinearGradient 
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        borderRadius={5}
                        style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20,height:52 }}

                        
                        >
                        
  <Text  allowFontScaling={false} style={{ color:'#fff',fontSize:22}} >
    ADD
  </Text>
</LinearGradient>
</TouchableOpacity>
                        
                        
                    </Card>
                </ScrollView>
            </View>

        )
    }
}



const styles = StyleSheet.create({
    myTextBox: {
        color:'#000',
        paddingLeft:15,
        fontSize:18,
        lineHeight:23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius:5,
        height:50,
        fontFamily:'CenturyGothic'
    },


})