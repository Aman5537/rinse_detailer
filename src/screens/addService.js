import React, { Component } from 'react'
import { Text, View, TextInput, CheckBox, ScrollView, Dimensions, TouchableOpacity,Image,StyleSheet } from 'react-native'
import  CommanStyle  from '../config/Styles';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader, Button } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import Helper from '../Lib/Helper';


export default class AddService extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            checkArray: this.props.navigation.getParam('checkArray', []),
            submitform: {
                service: '',
                average_time_from: '',
                average_time_to: '',
                description: '',
                validators: {
                    service: { required: true, "minLength": 3,"maxLength": 60 },
                    average_time_from: { required: true},
                    average_time_to: { required: true},
                    description: { required: true, "maxLength": 350 },
                    
                }
            },
        }

        //
    }


    onSubmit = () => {

        //console.log(JSON.stringify(this.state.submitform));
        
        let isValid = Helper.validate(this.state.submitform);
        if (isValid) {

            


            Helper.makeRequest({ url: "add-service", data: this.state.submitform }).then((data) => {
                if (data.status == 'true') {
                    
                    //this.props.navigation.goBack()
                    this.props.navigation.navigate('ServicesYouOffer', { is_skip: 1,checkArray:this.state.checkArray });
                    //Helper.showToast(data.message);
                }
                else {
                    Helper.showToast(data.message)
                }
            })
        }
    }


    
    


    setValues(key, value) {
        let submitform = { ...this.state.submitform }
        submitform[key] = value;
        this.setState({ submitform });



    }

    render() {
        return (


            <View style={{ flex: 1, backgroundColor: '#ffff', }}>

                <CustomHeader navigation={this.props.navigation} bArrow />

                <AuthTitle TitleName='Add Services' />

                <KeyboardAwareScrollView extraScrollHeight={50} extraHeight={50} enableOnAndroid={true}>

                    <Card>
                       
                        <TextInput 
                        placeholder='Name this Service' 
                        //style={{ width: Dimensions.get('window').width / 2 }} 
                        value={this.state.submitform.service}
                        onChangeText={(txt) => { this.setValues('service', txt) }}
                        style={[styles.myTextBox]} 
                        onSubmitEditing={() => { this.min1.focus(); }}
                        
                        />

                        <Text  allowFontScaling={false}  style={CommanStyle.normaltext} >Average Time to Complete <Text  allowFontScaling={false}   style={CommanStyle.smallnormaltext}>(10-20 min, 50-60 min, etc)</Text></Text>
                        <View style={{ flexDirection: 'row',marginTop:10,flex:1 }}>

                            <View style={{ flex:3}}>
                                <TextInput 
                                placeholder='min' 
        //                         style={{textAlign: 'right',color:'#000',
        // paddingRight:15,
        // fontSize:18,
        // lineHeight:23,
        // backgroundColor: '#e9e9e9',
        // marginBottom: 10,
        // borderRadius:5,
        // height:50,}} 
        
        keyboardType="number-pad" 
        returnKeyType='done' 
        value={this.state.submitform.average_time_from}
        onChangeText={(txt) => { this.setValues('average_time_from', txt) }}
        style={[styles.myTextBox,{textAlign: 'right',paddingRight:15}]} 
        onSubmitEditing={() => { this.min2.focus(); }}
        ref={(input) => { this.min1 = input; }}

        />
                            </View>
                            <Text  allowFontScaling={false} style={[CommanStyle.normaltext,{flex:1,textAlign:'center',marginTop:15}]}>to</Text>
                            <View style={{ flex:3}}>
                            <TextInput placeholder='min'  
                            keyboardType="number-pad" 
                            returnKeyType='done' 
                            value={this.state.submitform.average_time_to}
                            onChangeText={(txt) => { this.setValues('average_time_to', txt) }}
                            style={[styles.myTextBox,{textAlign: 'right',paddingRight:15}]} 
                            onSubmitEditing={() => { this.discriptionInput.focus(); }}
                            ref={(input) => { this.min2 = input; }} />
                            </View>

                        </View>


                       
                        <TextInput 
                        placeholder='Describe this Service' 
                        multiline = {true}
                        numberOfLines = {4} 
                        style={{backgroundColor:'#e9e9e9',borderRadius:5,padding:12,textAlignVertical: 'top',fontSize:18,height:150 }} 
                        value={this.state.submitform.description}
                        onChangeText={(txt) => { this.setValues('description', txt) }}
                        ref={(input) => { this.discriptionInput = input; }} 
                        />

                       
                    <TouchableOpacity
                    onPress={() => this.onSubmit()}
                    >
                        <LinearGradient 
                        

                     
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        borderRadius={5}
                        style={{ padding: 10, alignItems: 'center', borderRadius: 5, marginTop: 20,height:52 }}

                         
                        >
                        
  <Text  allowFontScaling={false} style={{ color:'#fff',fontSize:22}} >
    ADD
  </Text>
</LinearGradient>
</TouchableOpacity>
                        
                        
                    </Card>
                </KeyboardAwareScrollView>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    myTextBox: {
        color: '#000',
        paddingLeft: 15,
        fontSize: 18,
        lineHeight: 23,
        backgroundColor: '#e9e9e9',
        marginBottom: 10,
        borderRadius: 5,
        height: 50,
        fontFamily:'CenturyGothic'

    },
});

