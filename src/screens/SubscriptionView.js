
import React, { Component } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Platform,
	Alert,
	ToastAndroid,
	Image,
	ActivityIndicator,
	Modal,
	FlatList,
    AsyncStorage,
	StyleSheet
} from "react-native";

import RNIap,{
	InAppPurchase,
	PurchaseError,
	SubscriptionPurchase,
	acknowledgePurchaseAndroid,
	consumePurchaseAndroid,
	finishTransaction,
	finishTransactionIOS,
	purchaseErrorListener,
	purchaseUpdatedListener,
  } from "react-native-iap";
import { ScrollView } from "react-native-gesture-handler";
const bannerImg = require("../../assets/Icons/banner_image.png");
const icAndroid = require("../../assets/Icons/icon_android.png");
const iciPhone = require("../../assets/Icons/icon_apple.png");
const checkMark = require("../../assets/Icons/checkmark.png");

const itemSubs = Platform.select({
	ios: [
		"rinse01" ,
		"rinse02" ,
		"rinse03" 
	],
	android: [
		"rinse01" ,
		"rinse02" ,
		"rinse03" 
	]
});

export default class SubscriptionView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			productList: [],
			receipt: "",
			availableItemsMessage: "",
			purchaseIndicator: false,
			subscriptionIndicator: false,
			availableIndicator: false,
			modalVisible: false,
			validateItem: [],
			purchaseList:[]
		};
	}

	async componentDidMount() {
		this.afterSetStateChangeSubscription();
	}

	// getItemPurchaseInfo = async () => {
	// 	try {
	// 		this.setState({
	// 			availableIndicator: true

	// 		});
	// 		const products = await RNIap.getProducts(itemSkus);
	// 		if (Platform.OS === "ios") {
	// 			this.setState(
	// 				{
	// 					productList: []
	// 				},
	// 				() => {
	// 					this.getSubscriptionsInfo();
	// 				}
	// 			);
	// 		} else {
	// 			this.setState(
	// 				{
	// 					productList: products
	// 				},
	// 				() => {
	// 					this.getSubscriptionsInfo();
	// 				}
	// 			);
	// 		}
	// 	} catch (err) {
	// 		console.warn(err.code, err.message);
	// 		if (Platform.OS === "ios") {
	// 			this.setState(
	// 				{
	// 					productList: []
	// 				},
	// 				() => {
	// 					this.getSubscriptionsInfo();
	// 				}
	// 			);
	// 		} else {
	// 			this.setState(
	// 				{
	// 					productList: products
	// 				},
	// 				() => {
	// 					this.getSubscriptionsInfo();
	// 				}
	// 			);
	// 		}

	// 	}
	// };


	purchaseUpdateSubscription = purchaseUpdatedListener(
		
		async (purchase: InAppPurchase | SubscriptionPurchase) => {
		  const receipt = purchase.transactionReceipt;
		  if (receipt) {
			alert('osc');
			try {
			  // if (Platform.OS === 'ios') {
			  //   finishTransactionIOS(purchase.transactionId);
			  // } else if (Platform.OS === 'android') {
			  //   // If consumable (can be purchased again)
			  //   consumePurchaseAndroid(purchase.purchaseToken);
			  //   // If not consumable
			  //   acknowledgePurchaseAndroid(purchase.purchaseToken);
			  // }
			  
			  const ackResult = await finishTransaction(purchase);
			} catch (ackErr) {
			  console.warn('ackErr', ackErr);
			}
  
			this.setState({receipt}, () => this.goNext());
		  }
		},
	);
	
	goNext = (): void => {
		Alert.alert('Receipt', this.state.receipt);
	};


	getSubscriptionsInfo = async () => {
		this.setState({productList:''});
		try {
			const products = await RNIap.getSubscriptions(itemSubs);
			this.setState({
				productList: [...this.state.productList, ...products],
				availableIndicator: false,
				modalVisible: true
			});
		} catch (err) {
			this.setState({
				availableIndicator: false,
				modalVisible: true
			});
			console.warn(err.code, err.message);
		}
	};

	// afterSetStateChangePurchase = async () => {
	// 	try {
	// 		const products = await RNIap.getProducts(itemSkus);
	// 		if (Platform.OS === "ios") {
	// 			this.onPressSubscription(itemSkus[0]);
	// 		} else {
	// 			this.onPressProduct(products[0].productId);
	// 		}
	// 	} catch (err) {
	// 		this.setState({
	// 			purchaseIndicator: false,
	// 		}, () => {
	// 			console.warn(err.code, err.message);
	// 		});
	// 	}
	// }

	// // Purchase for android / iOS 
	// getPurchases = async () => {
	// 	this.setState({
	// 		purchaseIndicator: true,
	// 	}, () => {
	// 		this.afterSetStateChangePurchase();
	// 	});
	// };

	
	

	  

	afterSetStateChangeSubscription = async () => {
        
		const products = await RNIap.getSubscriptions(itemSubs);
		
		if (Platform.OS === "ios") {
			this.onPressSubscription(itemSubs[0]);
		} else {
			this.onPressSubscription(products[0].productId);
		}
	}

	// Subscription for android / iOS 
	

	onPressSubscription = async sku => {
		try {
			this.setState({
				purchaseIndicator: false,
				subscriptionIndicator: false,
			});

			const purchase = await RNIap.getPurchaseHistory(sku);

			if (Platform.OS === "ios") {
				this.receiptValidateIOS(purchase.transactionReceipt);
			} else {
				//alert(JSON.stringify(purchase));
				this.setState({
					purchaseList:purchase
				})
				// Do stuff here for android server side validate receipt 
			}
		} catch (err) {
			this.setState({
				purchaseIndicator: false,
				subscriptionIndicator: false,
			}, () => {
				Alert.alert("Inapp", err.message);
			});
		}
	};

	// purchaseSubscribe = (id) => {
	// 	//alert(id);
	// 	RNIap.getPurchaseHistory(id).then(purchase => {
	// 		this.setState({
	// 		 receipt: purchase.transactionReceipt
	// 		},() => {
	// 			alert(JSON.stringify(purchase.transactionReceipt)+'new')
	// 		});
	// 	   // handle success of purchase product
	// 	   }).catch((error) => {
	// 		   alert(error.message+'ok');
	// 		console.log(error.message);
	// 	})

	// 	this.setState({modalVisible:!this.state.modalVisible});
	// }

	requestSubscription = async (sku): void => {
		try {
		  RNIap.requestSubscription(sku);
		} catch (err) {
		  alert(err.message);
		}
	};

	// onPressProduct = async sku => {
	// 	try {
	// 		this.setState({
	// 			purchaseIndicator: false
	// 		});
	// 		const purchase = await RNIap.buyProduct(sku);
	// 		const transaction = JSON.parse(purchase.transactionReceipt);
	// 		if (Platform.OS === "android") {
	// 			this.onConsumeProduct(transaction);
	// 		} else {
	// 			this.receiptValidateIOS(purchase.transactionReceipt);
	// 		}
	// 	} catch (err) {
	// 		this.setState({
	// 			purchaseIndicator: false
	// 		}, () => {
	// 			if (Platform.OS === "ios") {
	// 				const subscription = RNIap.addAdditionalSuccessPurchaseListenerIOS(
	// 					async purchase => {
	// 						subscription.remove();
	// 					}
	// 				);
	// 			}
	// 		});
	// 	}
	// };

	// onConsumeProduct = async sku => {
	// 	try {
	// 		await RNIap.consumePurchase(sku.purchaseToken);
	// 		// Do stuff here for server side validate receipt 
	// 	} catch (err) {
	// 		if (Platform.OS === "ios") {
	// 			const subscription = RNIap.addAdditionalSuccessPurchaseListenerIOS(
	// 				async purchase => {
	// 					subscription.remove();
	// 				}
	// 			);
	// 		}
	// 	}
	// };

	// saveData = async (result) => {
	// 	try {
	// 		var countries = await AsyncStorage.getItem('key');
	// 		if (countries != null) {
	// 			countries = JSON.parse(countries)
	// 			if (!countries.includes(result)) {
	// 				countries.push(result)
	// 			}
	// 			this.setState({
	// 				validateItem: [],
	// 				validateItem: countries,
	// 			})
	// 		}
	// 		else {
	// 			let arrProduct = []
	// 			arrProduct.push(result)
	// 			this.setState({
	// 				validateItem: [],
	// 				validateItem: arrProduct,
	// 			})
	// 		}
	// 		console.log(this.state.validateItem);

	// 		AsyncStorage.setItem('key', JSON.stringify(this.state.validateItem));

	// 		console.log('success');
	// 	} catch (error) {
	// 		console.log('fail', error);

	// 	}
	// }

	// retriveData = async () => {
	// 	try {
	// 		var myArray = await AsyncStorage.getItem('key');
	// 		myArray = JSON.parse(myArray)
	// 		if (myArray !== null) {
	// 			this.setState({
	// 				validateItem: myArray
	// 			})
	// 			console.log(this.state.validateItem);
	// 		}
	// 	} catch (error) {
	// 		console.log(error);
	// 	}
	// }

	// receiptValidateIOS = async receipt => {
	// 	const receiptBody = {
	// 		"receipt-data": receipt,
	// 		password: "a740150a6e844879a53adcf1aacee812"
	// 	};
	// 	const result = await RNIap.validateReceiptIos(receiptBody, 1);
	// 	const product = result.receipt.in_app[0].product_id
	// 	this.setState({
	// 		validateItem: [...this.state.validateItem, result.receipt.in_app[0].product_id],
	// 		purchaseIndicator: false
	// 	})
	// 	this.saveData(result.receipt.in_app[0].product_id)
	// };

	setModalVisible = visible => () => {
		this.setState({ modalVisible: visible });
	};

	renderItem = ({ item, index }) => {
		console.log(this.state.validateItem[index]);
		console.log(item);
		return (
			<View style={{ flex: 1 }} key={index}>
				<TouchableOpacity
					activeOpacity={0.8}
					onPress={() => this.requestSubscription(item.productId)}>

					<Text style={{ fontWeight: "bold",  }}>
						{item.localizedPrice} 
						
						{/* {item.currency} */}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					{item.title}
					</Text>
					
					<Text style={{ fontWeight: "200", }}>
						{item.description}
					</Text>

					{
						this.state.validateItem[index] === item.productId && (<Image style={styles.checkmark} source={checkMark} />)
					}

					<View style={styles.dividerContainer} />
				</TouchableOpacity>
			</View>
		);
	};


	renderItemPurchase = ({ item, index }) => {
		console.log(this.state.validateItem[index]);
		console.log(item);
		return (
			<View style={{ flex: 1 }} key={index}>
				<TouchableOpacity
					activeOpacity={0.8}
					//onPress={() => this.requestSubscription(item.productId)}
					>

					<Text style={{ fontWeight: "bold",  }}>
						{item.localizedPrice} 
						{/* {item.currency} */}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					productId::{item.productId}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					transactionReceipt::{item.transactionReceipt}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					transactionId::{item.transactionId}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					transactionDate::{item.transactionDate}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					purchaseToken::{item.purchaseToken}
					</Text>
					<Text style={{ fontWeight: "bold", }}>
					signatureAndroid::{item.signatureAndroid}
					</Text>
					
					<Text style={{ fontWeight: "200", }}>
						{item.description}
					</Text>

					{
						this.state.validateItem[index] === item.productId && (<Image style={styles.checkmark} source={checkMark} />)
					}

					<View style={styles.dividerContainer} />
				</TouchableOpacity>
			</View>
		);
	};

	


	

	_keyExtractor = (item, index) => item.productId;

	render() {
		//alert(JSON.stringify(this.state.purchaseList))
		return (
			<View style={styles.container}>
				<ScrollView>
					<View style={styles.viewContainer}>
						
						<Text style={[styles.textContainer, { marginTop: 20 }]}>
							Subscriptions Plan:
            			</Text>

						<View>
						<FlatList
							data={this.state.productList}
							extraData={this.state}
							keyExtractor={this._keyExtractor}
							renderItem={this.renderItem}
						/>
						</View>
						<View>
						<FlatList
							data={this.state.purchaseList}
							extraData={this.state}
							keyExtractor={this._keyExtractor}
							renderItem={this.renderItemPurchase}
						/>
						</View>
						
					</View>

					
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
textContainer: {
    fontWeight: "bold",
    fontStyle: "normal"
  },
  viewContainer: {
    padding: 15,
    width: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    margin: 10
  },
  buttonSubscription: {
    width: "100%",
    height:40,
    borderRadius: 6,
    backgroundColor: "#1c4d89",
    
    alignSelf: "center",
    justifyContent: "center",
    paddingHorizontal: 15,
    paddingVertical: 5
  },
  buttonTextContainer: {
    color: "white",
    textAlign: "center"
  },
  bannerContainer: {
    width: 200,
    height: 100,
    alignSelf: "center"
  },
  rowTextContainer: {
    width: "100%",
    marginTop: 5
  },
  elevationContainer: {
    backgroundColor: "white",
    elevation: 4
  },
  platformContainer: {
    height: 100,
    width: 100,
    alignSelf: "center",
    resizeMode: "contain"
  },
  checkmark: {
    height: 30,
    width: 30,
    alignItems:'center',
    marginLeft: 300,

  },
  bottomIconContainer: {
    flexDirection: "row",
    marginTop: 20
  },
  fullImageContainer: {
    height: "100%",
    width: "100%"
  },
  modalContainer: {
    flex: 1,
    margin: 25,
    backgroundColor: "white",
    borderRadius: 10,
    elevation: 4
  },
  modelCancelButton: {
    height: 40,
    width: "100%",
    alignSelf: "center",
    backgroundColor: "orange",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    justifyContent: "center",
    bottom: 0,
    position: "absolute"
  },
  modalTextButton: {
    color: "white",
    textAlign: "center",
    fontWeight: "bold"
  },
  purchaseListContainer: {
    flex: 1,
    marginBottom: 40,
    backgroundColor: "#FF8C00",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 15
  },
  dividerContainer: {
    height: 1,
    width: "100%",
    backgroundColor: "white",
    marginTop: 10
  }

})