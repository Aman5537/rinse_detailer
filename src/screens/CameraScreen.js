import React, { Component } from 'react';
import {
  Alert,
  DeviceEventEmitter
} from 'react-native';
// import { CameraKitCameraScreen } from '../../src';
import {
    CameraKitCameraScreen
} from 'react-native-camera-kit';
import { NavigationActions,DrawerActions } from 'react-navigation';
import Helper from '../Lib/Helper';





export default class CameraScreen extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props);
    this.state = {
       
        
    }

  }

  onBottomButtonPressed=(event)=> {
    const captureImages = JSON.stringify(event.captureImages);
    
    setTimeout(()=>{
        DeviceEventEmitter.emit('onImageLoad',captureImages);
      },1000);

    this.props.navigation.goBack(null);
     
    //this.props.navigation.goBack(null);
  }
  

  render() {
    return (
      <CameraKitCameraScreen
        actions={{ rightButtonText: 'Done', leftButtonText: 'Cancel' }}
        onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
        flashImages={{
          on: require('../../images/flashOn.png'),
          off: require('../../images/flashOff.png'),
          auto: require('../../images/flashAuto.png')
        }}
        cameraFlipImage={require('../../images/cameraFlipIcon.png')}
        captureButtonImage={require('../../images/cameraButton.png')}
      />
    );
  }
}



