import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Platform, Dimensions, Image, StyleSheet, CheckBox, ScrollView, FlatList, Modal, Keyboard,SafeAreaView } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Input, Card, CardSection, CustomHeader, CustomTitle, Button, HeaderTitle, AuthHeader, AuthTitle, Footer } from '../common'
import CommanStyle from '../config/Styles';
import Helper from '../Lib/Helper';
import StarRating from 'react-native-star-rating';
import ProgressBar from 'react-native-progress/Bar';
import { Avatar } from 'react-native-elements';
import Moment from "moment";
import { TextInput } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;

// class HireConfrm extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {

//     }
//   }
//   render() {
//     return (
//       <View style={{ justifyContent: 'center', alignItems: 'center' }}>


//         <Text  allowFontScaling={false} style={{textAlign:'center',fontFamily: 'Aileron-Regular'}}>You card on file ending in 5364 will be charged the amount indicated. However,the payment won't be released to the detaileruntil after the detail has been completed and you are satisfied. if you cancel anytime before an hour prior to your appointment you will receive 100% refund.If you cancel within an hour prior to your appointment this will result in a $20 free due to lost time and work for the detailer.</Text>
//         <Text allowFontScaling={false} style={{ padding: 5,fontFamily: 'Aileron-Regular' }}>Would you like to hire this detailer?</Text>

//         <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
//           <TouchableOpacity onPress={() => {
//             global.state['modal'].setState({ modalVisible: false, Msg: "" })
//             global.state['modal'].state.navigation.navigate("ServiceReqQuotesHire");
//           }}>
//             <View style={{ marginRight: 15, width: 110, height: 40, backgroundColor: '#007cc9', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//               <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18,fontFamily: 'Aileron-Regular' }}>Yes</Text>
//             </View>
//           </TouchableOpacity>

//           <TouchableOpacity onPress={() => {
//             global.state['modal'].setState({ modalVisible: false, Msg: "" })
//           }}>
//             <View style={{ marginLeft: 15, width: 110, height: 40, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//               <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18,fontFamily: 'Aileron-Regular' }}>Cancel</Text>
//             </View>
//           </TouchableOpacity>
//         </View>

//         <TouchableOpacity onPress={() => {
//           global.state['modal'].setState({ modalVisible: false, Msg: "" })
//           global.state['modal'].state.navigation.navigate("Payments");
//         }}>
//           <View style={{ width: 250, height: 40, backgroundColor: '#007cc9', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//             <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18,fontFamily: 'Aileron-Regular' }}>Change payment method</Text>
//           </View>
//         </TouchableOpacity>
//         <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
//           <CheckBox
//             //  style={{backgroundColor:'white'}}
//             value={this.state.checked}
//             onValueChange={() => this.setState({ checked: !this.state.checked })}
//           />
//           <Text allowFontScaling={false} style={{ margin: 5, fontSize: 10, fontFamily: 'Montserrat-Regular' }}>Don't show this message again</Text>
//         </View>
//       </View>
//     )
//   }
// }






// const CancelMsg = (props) => {
//   return (
//     <View>
//       <Text allowFontScaling={false} style={{ fontSize: 18, textAlign: "center", fontWeight: 'bold',fontFamily: 'Aileron-Regular' }}>
//         Deluxe Detailling
//             </Text>
//       <Text allowFontScaling={false} style={{ textAlign: "center",fontFamily: 'Aileron-Regular' }}>Your Detailer : Johnny</Text>
//       <View style={{ marginTop: 8 }}>
//         <Text allowFontScaling={false} style={{ fontSize: 18, fontWeight: 'bold',fontFamily: 'Aileron-Regular' }}>
//           March 1st, 8:00am
//             </Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}} >1970 Chevy Impala</Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>Inside / Outside</Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>19446 West Field Rd. Tampa Fl.33610</Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>No Additional Info</Text>
//         <View style={{ borderWidth: 1, borderColor: '#dddd', marginTop: 10, marginBottom: 10,fontFamily: 'Aileron-Regular' }}></View>
//         <Text allowFontScaling={false} style={{ fontSize: 18, fontWeight: 'bold',fontFamily: 'Aileron-Regular' }}>
//           March 1st, 8:00am
//             </Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>1970 Chevy Impala</Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>Inside / Outside</Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>19446 West Field Rd. Tampa Fl.33610</Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>No Additional Info</Text>

//         <View style={{ borderWidth: 1, borderColor: '#dddd', marginTop: 10, marginBottom: 10 }}></View>
//         <Text allowFontScaling={false}  style={{ fontSize: 18, color: '#007cc9',fontFamily: 'Aileron-Regular' }}>
//           Total $90
//             </Text>
//         <Text allowFontScaling={false} style={{fontFamily: 'Aileron-Regular'}}>****5934</Text>
//       </View>

//       <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
//         <TouchableOpacity onPress={() => {
//           global.state['modal'].setState({ modalVisible: false, Msg: "" })
//           setTimeout(() => {
//             global.state['modal'].setState({ modalVisible: true, Msg: <HireConfrm /> })
//           }, 1000)
//           // props.navigation.navigate('ServiceReqQuotesHire')
//         }}>
//           <View style={{ marginRight: 15, width: 110, height: 40, backgroundColor: '#007cc9', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//             <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18,fontFamily: 'Aileron-Regular' }}>HIRE</Text>
//           </View>
//         </TouchableOpacity>

//         <TouchableOpacity onPress={() => {
//           global.state['modal'].setState({ modalVisible: false, Msg: "" })
//         }}>
//           <View style={{ marginLeft: 15, width: 110, height: 40, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//             <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18,fontFamily: 'Aileron-Regular' }}>CANCEL</Text>
//           </View>
//         </TouchableOpacity>
//       </View>

//     </View>
//   )
// }


// const Msg = () => {
//   return (
//     <View>
//       <Text allowFontScaling={false} >Hi therer</Text>
//       <TouchableOpacity onPress={() => {
//         global.state['modal'].setState({ modalVisible: false, Msg: "" })

//       }} style={{ flexDirection: 'row', alignSelf: 'center' }}>
//         <View style={{ marginRight: 15, width: 110, height: 40, backgroundColor: '#007cc9', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//           <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18 }}>HIRE</Text>
//         </View>
//         <View style={{ marginLeft: 15, width: 110, height: 40, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
//           <Text allowFontScaling={false} style={{ color: 'white', fontSize: 18 }}>CANCLE</Text>
//         </View>
//       </TouchableOpacity>
//     </View>
//   )
// }




// const width = Dimensions.get('window').width / 1.5;
export default class SubmiteRequest extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props);
    this.state = {
      // CancelMsg: <CancelMsg navigation={this.props.navigation} />,
      // Msg: <Msg navigation={this.props.navigation} />
      //reviewdata:'',
      AppUserData: {},
      Page: 1, IsLoading: false,
      Next_page_url: null,
      Refreshing: false,
      UserData: '',
      rat1: 0,
      rat2: 0,
      rat3: 0,
      rat4: 0,
      rat5: 0,
      modalInfoVisible: false,
      keyBoardShow: true,
      ErrorMsg:'',
      reply_text:''
    }
  }

  // _onPress = () => {
  //   global.state['modal'].setState({ modalVisible: true, Msg: this.state.CancelMsg, navigation: this.props.navigation, })
  // }



  componentDidMount() {

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );


    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      Helper.getData('userdata').then((responseData) => {

        if (responseData === null || responseData === 'undefined' || responseData === '') {
          //this.props.navigation.navigate('LoginNew');
        } else {
          this.setState({ AppUserData: responseData }, () => {
            this.getReqData()
          });
        }
      })

    })
  }



  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow = () => {
    this.setState({ keyBoardShow: false })
  }

  keyboardDidHide = () => {
    this.setState({ keyBoardShow: true })
  }





  getReqData = () => {

    MyLoader = true;



    Helper.makeRequest({ url: "getProfile", loader: MyLoader, method: "POST" }).then((data) => {
      //alert(JSON.stringify(data));
      if (data.status == 'true') {
        console.log(data);
        var Totalrat = data.data.first_rating + data.data.second_rating + data.data.third_rating + data.data.fourth_rating + data.data.five_rating;

        if (Totalrat == 0) {
          this.setState({
            UserData: data.data

          })
        }
        else {
          this.setState({
            UserData: data.data,
            rat1: (data.data.first_rating * 100) / Totalrat,
            rat2: (data.data.second_rating * 100) / Totalrat,
            rat3: (data.data.third_rating * 100) / Totalrat,
            rat4: (data.data.fourth_rating * 100) / Totalrat,
            rat5: (data.data.five_rating * 100) / Totalrat
          })
        }





      }
      else {
        Helper.showToast(data.message)
      }


    })
  }


  openReplyBox(item) {
    this.setState({ review_id: item.id })
    this.setModalInfoVisible(true)
  }

  setModalInfoVisible(visible) {

    this.setState({ modalInfoVisible: visible });
  }

  setReply(value){
    if(value.length > 2){
      this.setState({'reply_text':value})
      this.setState({'ErrorMsg':''})
    }else{
      this.setState({'reply_text':value})
    }
    
  }

  sendReply() {
    Keyboard.dismiss();
    //alert(this.state.review_id+'----'+this.state.reply_text);
    this.setState({'ErrorMsg':''})
    if(this.state.reply_text.trim().length == 0){
      
      this.setState({'ErrorMsg':'Please Enter Your Reply'})
      return false;
    }else{
      if(this.state.reply_text.trim().length < 3){
        
        this.setState({'ErrorMsg':'Reply should be greater than or equal to 3 characters'})
        return false;
      }
    }
    
    let form = {
      detailer_id:this.state.AppUserData.id,
      review_id:this.state.review_id,
      comment:this.state.reply_text.trim()
    }
    Helper.makeRequest({ url: "review-reply", data: form }).then((responseData) => {
      
      if (responseData.status == 'true') {
        this.setModalInfoVisible(false)
        setTimeout(() => {
          this.getReqData()
        }, 1000)
      }else{
        this.setModalInfoVisible(false)
      }
    })
  }


  modalInfoRender() {
    return (

      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalInfoVisible}
        onRequestClose={() => {
          this.setModalInfoVisible(false)
        }}>

        <SafeAreaView style={{ backgroundColor: 'rgba(0,0,0,0.8)', height: DeviceH }}>
          <View style={(this.state.keyBoardShow == false) ? { flex: 1,alignItems: 'center' } : { flex: 1, alignItems: 'center', justifyContent: "center" }}>
            <View style={{ backgroundColor: "#ffffff", borderRadius: 10, padding: 20, margin: 20, alignItems: "center" }}>


              <View style={{ minHeight: 200, maxHeight: 400, width: DeviceW - 150 }}>

                <TouchableOpacity
                  onPress={() => this.setModalInfoVisible(false)}
                  style={{}}
                >
                  <Text style={{ fontSize: 20, justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>
                  {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                </TouchableOpacity>

                <View style={{ marginTop: 10 }}>
                  <View>
                  <TextInput
                    placeholder={'Enter Your Reply'}
                    onChangeText={(value) => this.setReply(value)}
                    style={{ height: 120, borderWidth: 1, borderColor: '#ddd', textAlignVertical: 'top', padding: 5 }}
                    numberOfLines={6}
                    multiline={true}
                  />
                  </View>
                  {this.state.ErrorMsg != '' &&
                  <View>
                    <Text style={{color:'red',fontWeight:'500'}}>{this.state.ErrorMsg}</Text>
                  </View>
                  }
                  <View style={{width:'100%', flexDirection: 'row', marginTop: 10,justifyContent:'space-around' }}>

                    <TouchableOpacity 
                      onPress={() => this.setModalInfoVisible(false)}
                      style={{ width:'47%', }}
                    >

                      <LinearGradient
                        colors={['#5c5c5c', '#5c5c5c', '#5c5c5c']}
                        style={{ padding: 8, alignItems: 'center', height: 45, justifyContent: 'center' }}
                      >

                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Cancel</Text>

                      </LinearGradient>

                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                      this.sendReply()
                    }}

                      style={{ width:'47%', }}
                    >

                      <LinearGradient
                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 8, alignItems: 'center', height: 45, justifyContent: 'center' }}


                      >

                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 18, fontFamily: 'Aileron-Regular' }} >Reply</Text>

                      </LinearGradient>


                    </TouchableOpacity>

                  </View>

                </View>

              </View>


            </View>
          </View>
        </SafeAreaView>



      </Modal>



    );
  }

  _renderSalesItem = ({ item, index }) => (

    <View style={{ paddingLeft: 7, }}>

      <Text allowFontScaling={false} >{Moment(item.created_at).format('l')}</Text>
      <Text allowFontScaling={false} style={{ fontSize: 16, fontWeight: 'bold' }} >{item.user_full_name}</Text>
      <View style={{ flexDirection: 'row' }}>
        <StarRating
          disabled={true}
          maxStars={5}
          rating={item.rating}
          //selectedStar={(rating) => this.onStarRatingPress(rating)}
          fullStarColor={'#eab803'}
          halfStarColor={'#eab803'}
          emptyStarColor={'#d3d3d5'}
          starSize={15}
          starStyle={{ margin: 2 }}

        />
      </View>
      <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { color: '#818181' }]} >{item.comment}</Text>
      
      {item.reply == '' ?
      <TouchableOpacity
        onPress={() => this.openReplyBox(item)}
      >
        <Text style={{ color: 'blue', fontWeight: 'bold' }}>Reply</Text>
      </TouchableOpacity>
      :
      <View style={{paddingLeft:30,marginTop:10}}>
        <Text allowFontScaling={false} >{Moment(item.reply.created_at).format('l')}</Text>
      
        <Text allowFontScaling={false} style={{ fontSize: 16, fontWeight: 'bold' }} >{item.reply.detailer.first_name+' '+item.reply.detailer.last_name}</Text>
      
        <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { color: '#818181' }]} >{item.reply.comment}</Text>
      
      </View> 
    }

      <View style={{ borderWidth: 0.5, borderColor: '#ddd', marginTop: 12 }}></View>
    </View>

  )


  render() {


    return (
      <View style={{ flex: 1 }}>
        {this.modalInfoRender()}
        <CustomHeader navigation={this.props.navigation} bArrow userbar />
        {this.state.UserData != '' && (
          <ScrollView>
            <View style={{ flex: 1, justifyContent: 'space-between', padding: 2, paddingTop: 20 }}>
              <View style={{ flexDirection: 'row', margin: 0 }}>
                {/* backgroundColor: 'grey', */}
                <View style={{ width: 50, height: 50, overflow: 'hidden', margin: 8 }}>
                  {/* <Image
                    source={require("../../assets/Icons/pict4.png")}
                    style={{ width: 50, height: 50 }}
                  /> */}
                  {this.state.UserData.profile_picture != '' && <View>
                    <Image
                      style={{ width: 45, height: 45, borderRadius: 20 }}
                      resizeMode={'contain'}
                      source={{
                        uri: Helper.getImageUrl(this.state.UserData.profile_picture),
                        cache: 'force-cache',
                      }}

                    />

                    {/* <Avatar rounded size={100} source={{ uri: Helper.getImageUrl(this.state.UserData.profile_picture) }} /> */}
                  </View>}
                  {this.state.UserData.profile_picture == '' && <View>
                    <Image
                      style={{ width: 45, height: 45, borderRadius: 20 }}
                      resizeMode={'contain'}
                      source={require('../../assets/Icons/profile_user.png')}

                    />
                    {/* <Avatar rounded size={100} source={require('../../assets/Icons/profile_ico.png')} /> */}
                  </View>}
                </View>
                <View style={{ flex: 1, }}>
                  {/* backgroundColor: 'orange' */}
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ flexDirection: 'column', }}>
                      <Text allowFontScaling={false} font={'CenturyGothic-bold'} style={{ fontSize: 15 }}>{this.state.UserData.business_name}</Text>
                      <Text allowFontScaling={false} font={'CenturyGothic'} >Detailer-{this.state.UserData.first_name} {this.state.UserData.last_name}</Text>
                    </View>
                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Message')}
                  style={{ padding: 5, backgroundColor: '#4d4d4d', margin: 5 }}>
                  <Text style={{ marginLeft: 15, marginRight: 15, color: 'white' }}>Message</Text>
                </TouchableOpacity> */}
                  </View>
                  <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#818181', fontSize: 13 }}>Ontime Rate:{Helper.convertHoursMin(this.state.UserData.avg_late_time)} late</Text>
                  <Text allowFontScaling={false} font={'CenturyGothic'} style={{ color: '#818181', fontSize: 13 }}>{this.state.UserData.total_cancellation} Cancellations/No Shows</Text>
                  <View>
                  </View>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', paddingTop: 0 }}>
                {/* backgroundColor: 'red', */}
                <View style={{ flex: 1.3, marginLeft: 0 }}>
                  {/* backgroundColor: 'white', */}
                  <View style={{ flexDirection: 'row', padding: 10 }}>
                    <StarRating
                      disabled={true}
                      maxStars={5}
                      rating={this.state.UserData.overall_rating}
                      fullStarColor={'#eab803'}
                      halfStarColor={'#eab803'}
                      emptyStarColor={'#d3d3d5'}
                      starSize={15}
                      starStyle={{ margin: 2 }}

                    />
                    <Text allowFontScaling={false} style={[CommanStyle.normaltext, { fontSize: 14, left: 10, color: '#818181' }]}>{Number(this.state.UserData.overall_rating).toFixed(1)}</Text>
                  </View>
                  <Text allowFontScaling={false} font={'CenturyGothic'} style={[CommanStyle.normaltext, { fontSize: 14, padding: 10, marginTop: -20, color: '#818181' }]}>{this.state.UserData.total_review} Reviews</Text>
                </View>

                <View style={{ flex: 1.50, marginTop: 5 }}>
                  {/* Numbers rating Start here  */}
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.05 }}>
                      <Text style={styles.ratingText}>5</Text>
                    </View>
                    <View style={{ flex: 0.10, justifyContent: 'center' }}>
                      <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 12, height: 12, }}
                      />
                    </View>
                    <View style={{ flex: 0.55, justifyContent: 'center' }}>
                      <ProgressBar progress={this.state.rat5 / 100} width={null} style={[styles.margin5, { flex: 0, width: '88%' }]} color={'#f1c411'} borderColor={'#dddddd'} height={8} unfilledColor={'#dddddd'} />
                    </View>
                    <View style={{ flex: 0.30, alignItems: 'flex-start', paddingLeft: 0 }}>
                      <Text style={styles.ratingText}>{Helper.getFlotingValue(this.state.rat5)}%</Text>
                    </View>
                  </View>


                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.05 }}>
                      <Text style={styles.ratingText}>4</Text>
                    </View>
                    <View style={{ flex: 0.10, justifyContent: 'center' }}>
                      <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 12, height: 12, }}
                      />
                    </View>
                    <View style={{ flex: 0.55, justifyContent: 'center' }}>
                      <ProgressBar progress={this.state.rat4 / 100} width={null} style={[styles.margin5, { flex: 0, width: '88%' }]} color={'#f1c411'} borderColor={'#dddddd'} height={8} unfilledColor={'#dddddd'} />
                    </View>
                    <View style={{ flex: 0.30, alignItems: 'flex-start', paddingLeft: 0 }}>
                      <Text style={styles.ratingText}>{Helper.getFlotingValue(this.state.rat4)}%</Text>
                    </View>
                  </View>

                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.05 }}>
                      <Text style={styles.ratingText}>3</Text>
                    </View>
                    <View style={{ flex: 0.10, justifyContent: 'center' }}>
                      <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 12, height: 12, }}
                      />
                    </View>
                    <View style={{ flex: 0.55, justifyContent: 'center' }}>
                      <ProgressBar progress={this.state.rat3 / 100} width={null} style={[styles.margin5, { flex: 0, width: '88%' }]} color={'#f1c411'} borderColor={'#dddddd'} height={8} unfilledColor={'#dddddd'} />
                    </View>
                    <View style={{ flex: 0.30, alignItems: 'flex-start', paddingLeft: 0 }}>
                      <Text style={styles.ratingText}>{Helper.getFlotingValue(this.state.rat3)}%</Text>
                    </View>
                  </View>

                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.05 }}>
                      <Text style={styles.ratingText}>2</Text>
                    </View>
                    <View style={{ flex: 0.10, justifyContent: 'center' }}>
                      <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 12, height: 12, }}
                      />
                    </View>
                    <View style={{ flex: 0.55, justifyContent: 'center' }}>
                      <ProgressBar progress={this.state.rat2 / 100} width={null} style={[styles.margin5, { flex: 0, width: '88%' }]} color={'#f1c411'} borderColor={'#dddddd'} height={8} unfilledColor={'#dddddd'} />
                    </View>
                    <View style={{ flex: 0.30, alignItems: 'flex-start', paddingLeft: 0 }}>
                      <Text style={styles.ratingText}>{Helper.getFlotingValue(this.state.rat2)}%</Text>
                    </View>
                  </View>

                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.05 }}>
                      <Text style={styles.ratingText}>1</Text>
                    </View>
                    <View style={{ flex: 0.10, justifyContent: 'center' }}>
                      <Image
                        source={require("../../assets/Icons/star_g.png")}
                        style={{ width: 12, height: 12, }}
                      />
                    </View>
                    <View style={{ flex: 0.55, justifyContent: 'center' }}>
                      <ProgressBar progress={this.state.rat1 / 100} width={null} style={[styles.margin5, { flex: 0, width: '88%' }]} color={'#f1c411'} borderColor={'#dddddd'} height={8} unfilledColor={'#dddddd'} />
                    </View>
                    <View style={{ flex: 0.30, alignItems: 'flex-start', paddingLeft: 0 }}>
                      <Text style={styles.ratingText}>{Helper.getFlotingValue(this.state.rat1)}%</Text>
                    </View>
                  </View>

                  <View></View>
                  {/* Numbers rating End here  */}
                </View>
                {/* <Image
                resizeMode={'contain'}
                style={{height:90,width:180,marginRight:10,marginTop:10}}
                source={require('../../assets/Icons/rat1.png')}
              /> */}


                {/* <View style={{ width:width,backgroundColor:'red',marginRight:10, }}>
             
              <Image
                resizeMode={'contain'}
                style={{height:'90%',width:'90%',alignSelf:'flex-end'}}
                source={require('../../assets/Icons/rat1.png')}
              />
           

            </View> */}
              </View>
            </View>

            <View style={{ flex: 1.8, padding: 12, paddingTop: 5 }}>
              {/* backgroundColor: 'yellow' */}
              {/* <View style={{ margin: 10, padding: 5, backgroundColor: '#dddd' }}>
           
            <Text allowFontScaling={false} style={{ fontSize: 15 }}>Picture</Text>
          </View>


          <TouchableOpacity onPress={() => this.props.navigation.navigate('ImageComponet')}
            style={{ alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-evenly' }}
          >

            <Image
              resizeMode='cover'
              style={{ height: 55, width: 75, margin: 2, borderRadius: 10 }}
              source={require('../../assets/Icons/pict1.png')}

            />
            <Image
              resizeMode='cover'
              style={{ height: 55, width: 75, margin: 2, borderRadius: 10 }}
              source={require('../../assets/Icons/pict2.png')}
            />
            <Image
              resizeMode='cover'
              style={{ height: 55, width: 75, margin: 2, borderRadius: 10 }}
              source={require('../../assets/Icons/pict3.png')}
            />
            <Image
              resizeMode='cover'
              style={{ height: 55, width: 75, margin: 2, borderRadius: 10 }}
              source={require('../../assets/Icons/pict4.png')}
            />

          </TouchableOpacity> */}
              <View style={{ marginLeft: 0, marginRight: 0, padding: 5, backgroundColor: '#ececec' }}>
                {/* backgroundColor: 'red', */}
                <Text allowFontScaling={false} style={{ fontSize: 15, color: '#7f7f7f' }}>Customer Reviews</Text>
              </View>


              <View style={{ flex: 1 }}>
                {this.state.UserData.reviews.length > 0 ?
                  <FlatList
                    data={this.state.UserData.reviews}
                    extraData={this.state}
                    keyExtractor={(item, index) => index}
                    renderItem={this._renderSalesItem}

                  />
                  : !this.state.IsLoading ?
                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ color: '#000', fontSize: 16, fontWeight: 'bold' }}>Not Found</Text>
                    </View> : null
                }

              </View>



            </View>
            {/* <Footer backgroundColor='#1d87dd' FooterText='SUMBIT REQUEST' onPress={this._onPress} />
        <View style={{ backgroundColor: '#1d87dd', }}>
                    <Text style={{color:'white', fontSize: 15,textAlign:'center',marginTop:15,marginBottom:15 }}>SUMBIT REQUEST</Text>
                </View> */}

          </ScrollView>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  vr: { paddingHorizontal: 10, paddingTop: 10, flex: 1 },
  vrheading: { fontWeight: '500', fontSize: 16 },
  vrcaption: { marginVertical: 10 },
  vrList: {},
  vrListItem: {},
  vrListItemTitle: { fontWeight: '500', fontSize: 15, color: '#777', marginBottom: 5 },
  vrListItemRatings: { flexDirection: 'row', marginBottom: 15 },
  vrLikeDislike: { flexDirection: 'row' },
  dislike: { marginLeft: 20 },
  vrpng: { height: 20, width: 20, marginRight: 7, resizeMode: 'contain' },
  vrRev: {},

  overallReview: {
    flex: 1,
    backgroundColor: '#fbfbfb'
  },
  avgtotal: {
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 15,
    marginHorizontal: 10
  },
  avgrate: {
    height: 40,
    width: 40,
    backgroundColor: 'darkgreen',
    color: '#fff',
    borderRadius: 3,
    justifyContent: 'center',
    marginRight: 10,

  },
  avgrateTxt: {
    color: '#fff',
    textAlign: 'center',
  },
  revResult: {
    width: '60%',
    justifyContent: 'center',
    fontSize: 18,
    marginTop: 7,
    fontWeight: '500',
    color: 'darkgreen'
  },
  totalRev: {
    fontWeight: '400',
    color: '#333'
  },
  genre: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 20,
    marginHorizontal: 10,
    justifyContent: 'space-between'
  },
  genrelist: {
    width: '49%',
    marginVertical: 5
  },
  genretitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 3
  },
  genreTitleTxt: {
    fontSize: 12,
  },
  from10: {
    fontSize: 12,
  },
  rateRange: {},
  genreListContainer: {
    flex: 1
  },
  recentReviews: {
    marginHorizontal: 10,
    marginTop: 10,
    flex: 1
  },
  rrheader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  rrTitle: {
    fontSize: 16,
    fontWeight: '500'
  },
  rrMenu: {
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 15,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 2,
    paddingHorizontal: 8,
    alignItems: 'center'
  },
  rrMenuSelectedTxt: {
    fontSize: 10,
    color: '#999'
  },
  rrMenuArrow: {
    height: 5,
    width: 10,
    resizeMode: 'contain',
    marginTop: 3,
    opacity: 0.5
  },
  rrAllReviews: {
    /* borderWidth: 1,
    borderColor: '#000', */
    // paddingBottom: 30,

  },
  reviewDetail: {
    paddingTop: 8,
    borderTopWidth: 1,
    borderColor: '#eee',
  },
  reviewdesc: {
    flexDirection: 'row',
    /* borderWidth: 1,
    borderColor: '#000' */
  },
  rrnameDate: {
    /* borderWidth: 1,
    borderColor: '#000' */
  },
  rrname: {
    fontWeight: '500',
    fontSize: 14
  },
  rrdate: {},
  rrReviewTxt: {
    marginTop: 5,
    marginBottom: 10
  },
  margin5: {
    margin: 0
  },
  ratingText: {
    fontSize: 12
  }
});






// import { StyleSheet, ImageBackground, Text, View, Image, TouchableOpacity, ScrollView, Dimensions, StatusBar, AsyncStorage } from 'react-native';

export class Progress extends React.Component {
  constructor(props) {
    super(props)
    this.state = { completed: props.completed || 80, colorCompleted: props.colorCompleted || "red", colorUncompleted: props.colorUncompleted || "#cdcdcd" }

  }
  render() {
    let props = this.state;
    return (

      <View style={{ flex: 1, flexDirection: "row" }} >
        <View style={{ width: props.completed + "%", backgroundColor: props.colorCompleted, height: 5, borderTopLeftRadius: 2, borderBottomLeftRadius: 2 }} />
        <View style={{ width: (100 - props.completed) + "%", backgroundColor: props.colorUncompleted, height: 5, borderTopRightRadius: 2, borderBottomRightRadius: 2 }} />
      </View>

    )
  }
}
