import React, { Component } from 'react'
import { Text, View, TextInput, CheckBox, ScrollView, Dimensions, TouchableOpacity, Image, Button, StyleSheet } from 'react-native'
import CommanStyle from '../config/Styles';
import CommonButton from '../common/CommonButton';
import { Input, Card, CardSection, CustomHeader, AuthTitle, HeaderTitle, AuthHeader } from '../common'
import LinearGradient from 'react-native-linear-gradient';
import Helper from '../Lib/Helper';
import MapView, {
    ProviderPropType,
    Marker,
    AnimatedRegion,
} from 'react-native-maps';
import call from 'react-native-phone-call'


class CallMsg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            callNumber: this.props.callNumber,
            country_code: this.props.country_code
        }




    }


    callFun = () => {
        const args = {
            number: this.state.country_code + '' + this.state.callNumber, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }

        call(args).catch(console.error)
    }

    render() {
        return (
            <View style={{ width: 300, height: 140, margin: -20, marginLeft: -30, marginRight: -30 }}>
                <TouchableOpacity onPress={() => {
                    global.state['modal'].setState({ modalVisible: false, Msg: "" })

                }}
                    style={{ margin: 10 }}
                >
                <Text style={{fontSize:20,justifyContent: 'flex-end', alignSelf: 'flex-end' }}>&#10005;</Text>

                    {/* <Image source={require('../../assets/Icons/colse.png')} style={{ height: 15, width: 15, justifyContent: 'flex-end', alignSelf: 'flex-end' }} /> */}
                </TouchableOpacity>
                <Text allowFontScaling={false} style={{ fontSize: 16, textAlign: "center",fontWeight: 'bold', fontFamily: 'Aileron-Regular' }}>

                    {'+' + this.state.country_code + '-' + this.props.callNumber.slice(0, 3) + "-" + this.props.callNumber.slice(3, 6) + "-" + this.props.callNumber.slice(6, 15)}</Text>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 20 }}>
                    <TouchableOpacity onPress={() => {
                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1, }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center' }} >Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {

                        global.state['modal'].setState({ modalVisible: false, Msg: "" })
                        this.callFun(this.state.callNumber)
                    }}
                        style={{ flex: 1, padding: 10, borderTopColor: '#e8e8e8', borderTopWidth: 1, borderLeftColor: '#e8e8e8', borderLeftWidth: 1 }}
                    >
                        <Text allowFontScaling={false} style={{ color: '#000', fontSize: 18, textAlign: 'center', color: '#3c81c4' }} >Call</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}



export default class TrackDetailer extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
        super(props);
        this.state = {
            CallMsg: <CallMsg navigation={this.props.navigation} />,
            userdata: {},
            detailer_id: this.props.navigation.getParam('detailer_id', 0),
            detailer_name: this.props.navigation.getParam('detailer_name', ''),
            detailer_country_code: this.props.navigation.getParam('detailer_country_code', ''),
            detailer_mobile: this.props.navigation.getParam('detailer_mobile', ''),
            mapShow: false,
            mapCoordinate: {},
            msgShow:false

        }


        //const loopLoad = this.detailerTrack();
        //setInterval(function(loopLoad){ loopLoad }, 5000);

        

    }


    componentDidMount() {

        this.focusListener = this.props.navigation.addListener("didFocus", () => {

            Helper.getData('userdata').then((responseData) => {

                if (responseData === null || responseData === 'undefined' || responseData === '') {

                } else {
                    this.setState({ userdata: responseData }, () => {
                        this.detailerTrack()
                        
                        this.myVar = setInterval(() => {
                            this.detailerTrack();
                        }, 10000);
                    });

                }
            })




        });
    }



    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
        clearInterval(this.myVar);

    }


    detailerTrack = () => {
        //this.setState({mapShow: false})
        let tempdata = {
            detailer_id: this.state.detailer_id
        }

        Helper.makeRequest({ url: "get-sub-detailer-location", method: "POST", data: tempdata }).then((data) => {
            if (data.status == 'true') {
                //data.data.angle

                

                if((this.state.mapCoordinate.latitude != parseFloat(data.data.latitude)) ||  (this.state.mapCoordinate.longitude != parseFloat(data.data.longitude))){
                    this.setState({mapShow: false})
                }

                if(data.data == ''){
                    
                    this.setState({mapShow: false,msgShow:true})
                }else{
                    if(data.data.latitude != '' && data.data.longitude != ''){
                        const newCoordinate = {
                            latitude: parseFloat(data.data.latitude),
                            longitude: parseFloat(data.data.longitude),
                            latitudeDelta: 0.012,
                            longitudeDelta: 0.012,
                        };
        
                        this.setState({ mapCoordinate: newCoordinate, mapShow: true })
                    }
                }
                

            }

        })
    }

    _onCallPress = (country_code, callNumber) => {
        this.setState({ 'CallMsg': <CallMsg callNumber={callNumber} country_code={country_code} /> }, () => {
            global.state['modal'].setState({ modalVisible: true, Msg: this.state.CallMsg })
        })
    }

    render() {
        return (


            <View style={{ flex: 1, backgroundColor: '#ffff' }}>

                <CustomHeader navigation={this.props.navigation} bArrow />

                <HeaderTitle TitleName='Track Detailer' />

                <Text allowFontScaling={false} style={{ textAlign: 'center', marginBottom: 10, fontFamily: 'Aileron-Regular' }}>{this.state.detailer_name}'s Location</Text>

                <View style={{ flex: 1 }}>
                    <View style={{ flex: 2, width: '100%',alignItems:'center',justifyContent:'center' }}>
                        {/* <Image 
                            style={{width:'100%',height:'100%'}}
                            source={require('../../assets/Icons/map_img.png')}
                            /> */}


                        {/* <Image
                            style={{ width: '100%', height: 200 }}
                            source={require('../../assets/Icons/map.png')}
                        /> */}
                        {this.state.mapShow && (


                            <MapView
                                initialRegion={this.state.mapCoordinate}
                                style={style.map}
                                liteMode
                            >
                                <Marker
                                    coordinate={this.state.mapCoordinate}
                                />
                            </MapView>
                        )}
                        {(!this.state.mapShow && this.state.msgShow) && (
                            <View style={{alignItems:'center',justifyContent:'center'}}>
                                <Text>Loaction Not found!</Text>
                            </View>

                        )}


                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 30 }}>
                        <View style={{ flex: 1, padding: 10 }}>
                            <CommonButton name="Message" background="#4684da" color="#ffffff" onPress={() => this.props.navigation.navigate('Chat', { 'service_request_id': 0, 'other_user_id': this.state.detailer_id, 'user_id': this.state.userdata.id, 'username': this.state.detailer_name, 'country_code': this.state.detailer_country_code, 'mobile': this.state.detailer_mobile })} />
                        </View>
                        <View style={{ flex: 1, padding: 10 }}>
                            <CommonButton name="Call" background="#4684da" color="#ffffff" onPress={() => this._onCallPress(this.state.detailer_country_code, this.state.detailer_mobile)} />
                        </View>
                        <View style={{ flex: 1 }}>

                        </View>
                    </View>
                </View>
                {/* <View>
                    <Image 
                            style={{width: 35, height: 35}}
                             source={require('../../assets/Icons/map_img.png')}
                            />
                    </View> */}


                {/* <Text allowFontScaling={false} style={[CommanStyle.normaltitle, { textAlign: 'center', fontSize: 20, textDecorationLine: 'underline' }]} >SKIP</Text>

                <TouchableOpacity>
                    <LinearGradient



                        colors={['#4fa4d8', '#488cd8', '#3b55d9']}
                        style={{ padding: 10, alignItems: 'center', marginTop: 20 }}

                        onPress={() => this.props.navigation.navigate('DrawerRoot')}
                    >

                        <Text allowFontScaling={false} style={{ color: '#fff', fontSize: 22 }} >
                            CONTINUE
  </Text>
                    </LinearGradient>
                </TouchableOpacity> */}
            </View>

        )
    }
}



const style = StyleSheet.create({
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },

})