import ImagePicker from 'react-native-image-picker';
import { PermissionsAndroid, Platform } from "react-native";

export default class CameraController {
    static async open(cb) {
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.selecteImage(cb);
            } else {
                // console.log('--------Camera permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    static selecteImage(cb) {
        var options = {
            title: 'Select type',
            mediaTypes: 'Images',
            quality:0.3,
            //allowsEditing: true,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            // console.log('---------Response:  '+ response.fileName);

            if (response.didCancel) {
                
                // console.log('-----User cancelled image picker');
            } else if (response.error) {
                
                // console.log('-------ImagePicker Error: '+ response.error);
            } else {
                
                response.uri = Platform.OS === "android" ? response.uri : response.uri.replace("file://", "");
                
                cb(response);
            }
        });
    }

}