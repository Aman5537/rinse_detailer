/*
Install below npm before use it 

npm install --save react-native-loading-spinner-overlay@latest

add this line to App.js in constructor
 Helper.registerLoader(this);
 in render function
 <View style={{ flex: 1 }}>
        <Spinner visible={this.state.loader} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
 </View>
      
npm install react-native-simple-toast --save
 

*/
import * as React from 'react';
import Config from "./Config";
import { NavigationActions, StackActions } from 'react-navigation';
//import Toast from 'react-native-simple-toast';
import Toast from 'react-native-root-toast';
import { AsyncStorage, Alert } from 'react-native';
//import { Facebook, } from 'expo';
import Moment from "moment";


export default class Helper extends React.Component {
    url = "";
    static tototalCount=true;
    static mainApp;
    static toast;
    static user = {};
    static navigationRef;
    static appoCount = 0;
    static messageCount =0;
    static requestCount = 0;
    static permissionAlert=true;
    static device_token='';
    static device_type='';
    static localNotiShow= false;
    static user_location= '';
    static NotiType='';
    static NotiId='';
    static nav = {
        push: (routeName, params) => {
            Helper.navigationRef.dispatch(
                NavigationActions.navigate({
                    routeName, params
                })
            );
            //  self.props.navigation.push (page)
        },
        setRoot: (page, params) => {

            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: page, params })
                ]
            })
            Helper.navigationRef.dispatch(resetAction)




        },
        setNavigate: (page, params) => {
            Helper.navigationRef.navigate(page,params);
        }
        
    };


    constructor() {
    }
    static registerNavigator(ref) {
        Helper.navigationRef = ref;

    }
    static registerLoader(mainApp) {
        Helper.mainApp = mainApp;
    }




    static showLoader() {
        Helper.mainApp.setState({ loader: true })
    }


    static hideLoader() {
        Helper.mainApp.setState({ loader: false })
    }


    static registerToast(toast) {
        Helper.toast = toast;
    }

    static showToast(msg) {
        if (msg){
            //Toast.show(msg);
            let toast = Toast.show(msg, {
                duration: 6000,
                position: Toast.positions.CENTER,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                onShow: () => {
                    // calls on toast\`s appear animation start
                },
                onShown: () => {
                    // calls on toast\`s appear animation end.
                },
                onHide: () => { 
                    // calls on toast\`s hide animation start.
                },
                onHidden: () => {
                    // calls on toast\`s hide animation end.
                }
            }); 

        }
            

    }
    static alert(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    // static fbAuth(cb) {

    //     this.showLoader();
    //     try {
    //         const {
    //             type,
    //             token,
    //             expires,
    //             permissions,
    //             declinedPermissions,
    //         } = Facebook.logInWithReadPermissionsAsync(Config.fbappid, {
    //             permissions: ['public_profile', 'email']
    //         }).then((response) => {
    //             if (response.type === 'success') {
    //                 fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + response.token)
    //                     .then((response) => response.json())
    //                     .then((json) => {
    //                         console.log("socialData", json);
    //                         this.hideLoader();



    //                         /* 
    //                                                     fetch('https://graph.facebook.com/'+response.token+'/picture?type=large')
    //                                                         .then((responseimage) => responseimage.json())
    //                                                         .then((jsonimage) => {
    //                                                             console.log(jsonimage);
                            
    //                                                         }) */

    //                         /* fetch('https://graph.facebook.com/v3.2/id/permissions?access_token=' + response.token)
    //                             .then((response) => response.json())
    //                             .then((json) => {

    //                             }) */

    //                         let form = {
    //                             social_id: json.id,
    //                             email: '',
    //                             social_type: 'facebook',
    //                             device_token: 'ANDROID',
    //                             device_type: 'ANDROID',
    //                             name: json.name,
    //                             image: ''
    //                         }

    //                         if (json.picture.data.url) {
    //                             form.image = json.picture.data.url;
    //                         }

    //                         if (json.email) {
    //                             form.email = json.email;
    //                             cb(form);
    //                         } else {
    //                             cb(form);
    //                         }

    //                     })
    //                     .catch(() => {
    //                         this.hideLoader();
    //                         console.log('ERROR GETTING DATA FROM FACEBOOK')
    //                     })
    //             } else {
    //                 this.hideLoader();
    //             }
    //         }).catch((err) => {
    //             this.hideLoader();
    //             console.log(JSON.stringify(err), 'ERROR GETTING DATA FROM FACEBOOK')
    //         })


    //     } catch ({ message }) {
    //         this.hideLoader();
    //     }
    // }

    static confirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: 'Cancel', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static permissionConfirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'NOT NOW', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
                { text: 'SETTINGS', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static async  setData(key, val) {
        try {
            let tempval = JSON.stringify(val);
            await AsyncStorage.setItem(key, tempval);
        } catch (error) {
            console.error(error, "AsyncStorage")
            // Error setting data 
        }
    }

    static async  getData(key) {
        try {
            let value = await AsyncStorage.getItem(key);
            if (value) {
                let newvalue = JSON.parse(value);
                return newvalue;
            } else {
                return value;
            }

        } catch (error) {

            console.error(error, "AsyncStorage")
            // Error retrieving data
        }
    }

    static validate(form, validations) {
        let isValidForm = true;
        let errors = {};

        let message = '';
        if (!validations) {
            validations = form.validators;
        }
        let customvalidator = {
            service: "Service name"
        };

        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;; //email
        var numberRegex = /^\d+$/; // number
        for (let val in validations) {
            if (!isValidForm) break;
            if (form[val]) {
                for (let i in validations[val]) {
                    var valData = validations[val][i];

                    console.log(form[val], "form[val]")
                    let tempval = form[val];
                    

                    if (i == "required" && !form[val].trim()) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = value + " is required";

                    }
                    
                    // else if ((i == "minLength" || i == "minLengthDigit") && form[val].length < valData) {
                    //     isValidForm = false;
                    //     let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                    //     var cStr = i == "minLengthDigit" ? " digit" : " characters";
                    //     message = value + " should be greater than " + valData + cStr;

                    // }
                    else if ((i == "minLength" || i == "minLengthDigit") && (form[val].length < valData+2) && (val == 'mobile')) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "minLengthDigit" ? " digit" : " chars";
                        message = value + " should be greater than or equal to " + valData + cStr;

                    }
                    else if ((i == "minLength" || i == "minLengthDigit") && form[val].length < valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "minLengthDigit" ? " digit" : " chars";
                        message = value + " should be greater than or equal to " + valData + cStr;

                    }
                    else if ((i == "maxLength" || i == "maxLengthDigit") && form[val].length > valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "maxLengthDigit" ? " digit" : " characters";
                        message = value + " should be smaller than " + valData + cStr;
                    }
                    else if (i == "matchWith" && form[val] != form[valData]) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        let value2 = (valData.charAt(0).toUpperCase() + valData.slice(1)).split("_").join(" ");
                        message = value + " and " + value2 + " should be same";
                    }
                    else if (i == "email" && reg.test(form[val]) == false) {
                        isValidForm = false;
                        message = "Please enter valid email address";
                    }

                    else if (i == "numeric" && numberRegex.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = value + " should be number only";
                    }

                    if (message) {
                        this.showToast(message);
                        break;
                    }

                }
            }
            else {
                let tempmsg;

                if (customvalidator[val]) {
                    tempmsg = customvalidator[val];
                } else {
                    tempmsg = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                }

                this.showToast(tempmsg + " is required");
                isValidForm = false;
                break;
            }
        }
        return isValidForm;
    }



    static async makeRequest({ url, data, method = "POST", loader = true }) {

        let finalUrl = Config.url + url;

        console.log(finalUrl, "finalUrl");

        let form;
        let methodnew;
        let token = await this.getData("token");
        console.log(token, "tokentoken")
        let varheaders;
        

        if (method == "POSTUPLOAD") {
            methodnew = "POST";
            /* if (data !== "" && data !== undefined && data !== null) {
                for (var property in data) {
                        form.append(property, data[property]);
                }
            } */

            varheaders = {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data;',
                Authorization: 'Bearer ' + token
            }
            form = data;

            /* form = new FormData();
            for (let i in data)
                form.append(i, data[i]); */ // you can append anyone.

        }
        else if (method == "POST") {
            methodnew = "POST";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
            form = JSON.stringify(data);
        }
        else {
            console.log('yes get');
            methodnew = "GET";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }

            
        }

        
        if (loader) this.showLoader();
        

        console.log("Api chali", form, "form");

        return fetch(finalUrl, {
            body: form,
            method: methodnew,
            headers: varheaders,
        })
            .then((response) => {

                //console.log(response, "response")
                return response.json()
            })
            .then((responseJson) => {

                console.log(responseJson, "responseJson")
                //if (loader) this.hideLoader();
                
                this.hideLoader();
                if (responseJson.hasOwnProperty('userStatus')) {
                    
                    AsyncStorage.removeItem('userdata');
                    AsyncStorage.removeItem('token');
                    Helper.navigationRef.navigate('LoginNew');
                    this.showToast(responseJson.message);
                }

                if (responseJson.loginstatus == false) {
                    AsyncStorage.removeItem('userdata');
                    AsyncStorage.removeItem('token');
                    
                    Helper.navigationRef.navigate('LoginNew');
                    this.alert(responseJson.message);
                }
                else
                    return responseJson;
            })
            .catch((error, a) => {
                //if (loader) this.hideLoader();
                this.hideLoader();
                this.showToast("Please check your internet connection.");
                console.log(error);
            });
    }


    static getImageUrl(url){
        return finalUrl = Config.imageUrl + url;
    }


    static getDateNewFormate(dateFormate){
        dateFormate = Moment.utc(dateFormate).local().format('YYYY-MM-DD HH:mm:ss')
        // if(Helper.tototalCount === true){
        //     alert(dateFormate + 'UTC');

        //     Helper.tototalCount = false
            
        // }
        
        return Moment(dateFormate).format('ddd, MMM Do YYYY @ h:mm A');
         
        
        
        //var new24Time = this.ConvertTimeformat(time);
        //dateFormate = dateFormate+' '+new24Time+':00';
        //dateFormate = Moment.utc(dateFormate).local()
        // var ordinal;
        // var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        
        // date = new Date(dateFormate.replace(' ', 'T'));
        
        // const monthNames = ["January", "February", "March", "April", "May", "June",
        //   "July", "August", "September", "October", "November", "December"
        // ];
        

        
        // if(date.getUTCDate()>3 && date.getUTCDate()<21) ordinal = 'th'; // thanks kennebec
        //     switch (date.getUTCDate() % 10) {
        //           case 1:  ordinal = "st";
        //           case 2:  ordinal = "nd";
        //           case 3:  ordinal = "rd";
        //           default: ordinal = "th";
        // }
        
        
        // var hours = date.getHours();
        // var minutes = date.getMinutes();
        // var ampm = hours >= 12 ? 'pm' : 'am';
        // hours = hours % 12;
        // hours = hours ? hours : 12; // the hour '0' should be '12'
        // minutes = minutes < 10 ? '0'+minutes : minutes;
        // var strTime = hours + ':' + minutes + ' ' + ampm;
        
        
        
        // return days[date.getDay()]+', '+monthNames[date.getMonth()]+' ' +date.getUTCDate() +ordinal +' @ '+ strTime;
      
    }


    


    static ConvertTimeformat(time) {
        var format = 24;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "pm" && hours < 12) hours = hours + 12;
        if (AMPM == "am" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }
    

    static getDistanceMile(myStr){
        var myStr = myStr.toString();
        return myStr.substring(0, myStr.indexOf(".")+2);
        
    }

    static getFlotingValue(myStr){
        return myStr.toFixed(2);
        //return myStr.substring(0, myStr.indexOf(".")+2);
        
    }

    static getTimeDiffrence(date){
        let tempdate = new Date();
        
        let gettime = Moment(tempdate).format("YYYY-MM-DD")+' '+'00:00:00'
        var ReturnArr = {};
        //var dateFormate = Moment.utc(date).local().format('YYYY-MM-DD HH:mm:ss')
        var dateFormate = Moment.utc(date).local().format('YYYY-MM-DD HH:mm:ss');
        var dateFormate2 = Moment.utc(date).local().format('YYYY-MM-DD');
        //alert('getTime'+gettime+',,,localTime-'+dateFormate);
        var days = Moment(dateFormate).from(gettime);
        //alert(days);
        var daysAr = days.split(' ');
        //alert(dateFormate2 +'=='+ Moment(tempdate).format("YYYY-MM-DD"));
        if(dateFormate2 == Moment(tempdate).format("YYYY-MM-DD")){
            ReturnArr = {'type':2,'number':1,'comment':'Today'};
            return ReturnArr;
        }
        if(days == 'in a day'){
            ReturnArr = {'type':3,'number':1,'comment':parseInt(1)+' Day Away'};
            return ReturnArr;
        }

        if(days == 'in a hour'){
            ReturnArr = {'type':1,'number':1,'comment':'In Progress'};
            return ReturnArr;
        }
        else{
            
            if((daysAr[2] == 'hour' || daysAr[2] == 'hours') && (daysAr[3] != 'ago')){
                ReturnArr = {'type':2,'number':1,'comment':'Today'};
                return ReturnArr;
                
            }
            if((daysAr[2] == 'day' || daysAr[2] == 'days') && (daysAr[3] != 'ago')){
        
                var weeks = daysAr[1]/7;
                //alert(weeks);
                if(parseInt(weeks) > 0){
                    //return parseInt(weeks)+' weeks';
                    if(parseInt(weeks) == 1){
                        ReturnArr = {'type':4,'number':parseInt(weeks),'comment':parseInt(weeks)+' Week Away'};
                    }else{
                        ReturnArr = {'type':4,'number':parseInt(weeks),'comment':parseInt(weeks)+' Weeks Away'};
                    }
                    
                    return ReturnArr;

                }else{
                    if(daysAr[1] == 1){
                        ReturnArr = {'type':3,'number':daysAr[1],'comment':parseInt(daysAr[1])+' Day Away'};
                    }
                    else{
                        ReturnArr = {'type':3,'number':daysAr[1],'comment':parseInt(daysAr[1])+' Days Away'};
                    }
                    
                    return ReturnArr;
                }
            }
        }
        
        
    }
 
    static calculateStripeAccount(total_amount,fixed,percent){
        //return total_amount;
        if(total_amount == 0){
            return 0;
        }
        else{
            //return Number(percent);
            var newvalue = (Number(total_amount) + Number(fixed)) / (1 - Number(percent) / 100);
            var newstripe =  newvalue - Number(total_amount);
            var laststripe =  Number(total_amount) - newstripe;
            return laststripe.toFixed(2);
        }
        
    }

    // static NotificationJump(data){
    //     if(data.type == 'ServiceRequest'){
    //         Helper.nav.setNavigate('SendQuote',{ 'req_id': data.service_request_id });
    //     }
    // }

    static NotToZero(val){
        if(val == '' || val == null){
            return '0.00'
        }
        else{
            return val
        }
        
    }

    static convertHoursMin(totalMinutes){
        var hours = Math.floor(totalMinutes / 60);          
        var minutes = totalMinutes % 60;
        if(hours > 0){
            if(hours == 1){
                return hours+" hour "+Math.trunc(minutes)+' mins'
            }else{
                return hours+" hours "+Math.trunc(minutes)+' mins'
            }
            
        }else{
            return Math.trunc(minutes)+' mins'
        }
        
    }
}



