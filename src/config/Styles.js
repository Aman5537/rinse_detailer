import { StyleSheet, Dimensions } from 'react-native';


export default createAccount = StyleSheet.create({
    CreateText: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 15
    },

    InputView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 10,
        marginRight:10,
        marginTop:10,
       
    },

    InputBox: {
        backgroundColor: '#ddd',
        borderRadius: 3,
        width: 160,
        paddingLeft: 20,
    },

    BigInputView: {
        backgroundColor: '#ddd',
        marginLeft: 10,
        marginRight:10,
        marginTop:10,
        
        paddingLeft: 20,
         borderRadius: 5,
    },
    normaltext:{
        fontSize:15,
        color:'#4d4d4d',
        //lineHeight:24,
    },
    smallnormaltext:{
        fontSize:14,
        color:'#808080',
        //lineHeight:18,
        textAlign:'left',
        textAlign:'justify',
        
    },
    normaltitle:{
        fontSize:15,
        color:'#000',
       
    },
    salesTitle:{
        fontSize:13,
        color:'#000',
        fontWeight:'bold'
       
    },
    tabButton:{
        padding:5,
        backgroundColor:'#5c5c5c',
        borderWidth:1,
        borderColor:'#fff',
        flex:1,
        alignItems:'center',
        justifyContent:'center'


    },
    tabButtonText:{
        color:'#fff',
        textAlign:'center',
        fontSize:16,
    },
    mycardDiv:{
        //borderBottomColor:'#ccc',
        //borderBottomWidth:1,
        padding:6,
        flexDirection:'row',
        flex:1,
        marginTop:10
    },

    AppmycardDiv:{
        //borderBottomColor:'#f6f6f6',
        backgroundColor:'#fff',
        //borderBottomColor:'#ccc',
        //borderBottomWidth:1,
        flexDirection:'row',
        flex:1
        
    },
    AppmyBorderDiv:{
        //borderBottomColor:'#ccc',
        //borderBottomWidth:1,
        backgroundColor:'#ffffff',
        
    },
    viewcardDiv:{
        padding:6,
        flexDirection:'row',
        flex:1,
        marginTop:10,
        backgroundColor: "#ffffff"
    },
    margin5Button:{
        margin:2,
        backgroundColor: "#ffffff"
    },
    margin5ButtonEnd:{
        margin:2,
        alignItems:'flex-end',
        flex:1
    }

})


