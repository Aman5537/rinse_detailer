 const color ={
 blue:'#22578d',
 green:'#1d87dd',
 lgreen:'#0083c7',
 grey:'#4d4d4d',
 black:'#1f1f1f',
 lgrey:'#818181',
 white:'#d9d9d9'
}

export default color

