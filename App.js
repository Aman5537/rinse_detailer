import React, { cloneElement, Component } from 'react';
import { Platform, StatusBar, StyleSheet, View, Text, TextInput, AppState } from 'react-native';
import AppNavigator from './navigation/AppNavigator';
import AppNavigatorSub from './navigation/AppNavigatorSub';
import AppNavigatorSubNot from './navigation/AppNavigatorSubNot';

import PopUp from './src/common/Popup';
import { loadFont, loadFonts } from 'react-native-dynamic-fonts';
import PopUpBlack from './src/common/PopUpBlack';
import Intro from './src/screens/Intro';
import SubmiteRequest from './src/screens/SubmiteRequest';
import Portfolio from './src/screens/portfolio';
import Gallery from './src/screens/Gallery';
import ChatController from './src/Lib/ChatController';
import fonts from './src/config/fonts'
import Spinner from 'react-native-loading-spinner-overlay';
import Helper from './src/Lib/Helper';
import { EventRegister } from 'react-native-event-listeners';
import SplashScreen from 'react-native-splash-screen'
import firebase, {
  Notification,
  NotificationOpen,
  Firebase
} from 'react-native-firebase';

import LocalNotification from './src/common/LocalNotification';

import MaintananceScreen from './src/screens/MaintananceScreen';



global.state = {
  stripeAlert: 1
};

let oldRender = Text.render;
Text.render = function (...args) {
  let origin = oldRender.call(this, ...args);
  return cloneElement(origin, {
    style: [{ fontFamily: origin.props.fonts ? origin.props.fonts : 'CenturyGothic' }, origin.props.style],
    allowFontScaling: false
  });
};



//Text.origin.defaultProps.allowFontScaling=false;

// let oldRender = Text.render;
//   Text.render = function (...args) {
//       let origin = oldRender.call(this, ...args);

//       return cloneElement(origin, {
//           style: [origin.props.style, {fontFamily: origin.props.font?origin.props.font:'Aileron-Regular'}],
//           allowFontScaling:false
//       });
//   };



// let oldInputRender = TextInput.prototype.render;

// TextInput.prototype.render= function (...args) {
//       let originInput = oldInputRender.call(this, ...args);

//       return cloneElement(originInput, {
//           style: [originInput.props.style, {fontFamily: originInput.props.font?originInput.props.font:'CenturyGothic'}]

//       });
// };


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isfour: 0,
      isLoadingComplete: false,
      modalVisible: false,
      load: false,
      modalVisibleBlack: false,
      is_upgraded: 1,
      activeTab: 'RequestsStack',
      loader: false,
      isLogedin: false,
      quote_permission: 0,
      stripeAlert: 1,
      localNotiShow: Helper.localNotiShow,
      NotiAllData: '', isHandler: false,
    }
    Helper.registerLoader(this);

    Helper.getData('userdata').then((responseData) => {
      if (responseData === null || responseData === 'undefined' || responseData === '') {

      } else {
        this.setState({ isfour: responseData.is_sub_detailer, quote_permission: responseData.quotes_permission })
        this.setState({ userdata: responseData });
      }
    })

    //firebase.crashlytics().crash();

  }

  // componentDidMount() {

  // }


  _handleAppStateChange = (nextAppState) => {
    if (nextAppState == 'background') {
      console.log("----dddlll:  ", nextAppState)
      ChatController.disconnectUser();

      
      let form = {
        user_id:this.state.userdata.id,
        device_token:Helper.device_token
      }

      Helper.makeRequest({ url: "clear-badge", data:form,loader:false  }).then((data) => {
        if (data.status == 'true') {
          
        }
        else {
          Helper.showToast(data.message)
        }
      })
      

    }
    else {
      ChatController.connectUser(this.state.userdata.id);
    }
    //this.setState({appState: nextAppState});
  }



  async componentDidMount() {
    await Helper.getData('userdata').then((responseData) => {
      if (responseData) {
        AppState.addEventListener('change', this._handleAppStateChange);
        this.setState({ isHandler: true });
      }
    });

    loadFonts(fonts).then((names) => {
      console.log('Loaded all fonts successfully. Font names are: ', names);
      this.setState({ load: true });
    });

    if (Platform.OS === 'ios') {
      this.checkPermission();
      Helper.device_type = 'IOS'
      await this.getToken();
    } else {

      Helper.device_type = 'ANDROID'
      await this.getToken();
    }
    //this.createNotificationListeners();

  }

  async componentWillMount() {
    this.listener = EventRegister.addEventListener('userTabBarEvent', (data) => {
      this.setState({ isfour: data.is_sub_detailer, quote_permission: data.quotes_permission })
    })

    if (Platform.OS === 'ios') {

      Helper.device_type = 'IOS'
      this.checkPermission();

    } else {
      Helper.device_type = 'ANDROID'
      await this.getToken();
    }


    this.localNotilistener = EventRegister.addEventListener('service_notification_response', (data) => {
      console.log('localNotilistener--', data)
      if (data.status == 'true') {
        //alert('yes');
        //Helper.NotificationJump(data);
        this.setState({ localNotiShow: true, NotiAllData: data })
      }
    })


    this.NotilistenerHide = EventRegister.addEventListener('localNotificationHide', (data) => {
      Helper.localNotiShow = true;
      this.setState({ localNotiShow: false, NotiAllData: '' })
    })





  }

  componentWillUnmount() {
    EventRegister.removeEventListener(this.listener)
    //this.notificationListener();
    //this.notificationOpenedListener();
    EventRegister.removeEventListener(this.localNotilistener);
    EventRegister.removeEventListener(this.NotilistenerHide);

    if (this.state.isHandler) {
      AppState.removeEventListener('change', this._handleAppStateChange);
    }

    // if(this._handleAppStateChange){
    //   AppState.removeEventListener('change', this._handleAppStateChange);
    // }

  }


  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }


  async getToken() {
    await firebase.messaging().getToken().then(token => {
      console.log("Device Token--->>> " + token);
      Helper.device_token = token;
    }).catch((error) => {
      console.log(JSON.stringify(error));
    });
  }


  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      await this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }

  // async createNotificationListeners() {
  //   /*
  //   * Triggered when a particular notification has been received in foreground
  //   * */
  //   this.notificationListener = firebase.notifications().onNotification((notification) => {

  //     const { title, body } = notification;
  //     console.log("onNotification.... ", notification)
  //   });

  //   /*
  //   * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
  //   * */
  //   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
  //     const { title, body } = notificationOpen.notification;
  //     console.log("notificationOpenedListener.... ", notificationOpen)
  //   });

  //   /*
  //   * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
  //   * */
  //   const notificationOpen = await firebase.notifications().getInitialNotification();
  //   if (notificationOpen) {
  //     const { title, body, type } = notificationOpen.notification._data;
  //     // console.log("notificationOpen.... ", notificationOpen)
  //     Helper.openNotificationScreen = true
  //     Helper.notification_type = type;

  //   }
  //   /*
  //   * Triggered for data only payload in foreground
  //   * */
  //   this.messageListener = firebase.messaging().onMessage((message) => {
  //     //process data message
  //     console.log("Notification foreground-->> ", JSON.stringify(message));

  //   });
  // }


  // componentDidMount() {
  //   SplashScreen.hide();
  //   loadFonts(fonts).then((names) => {
  //     console.log('Loaded all fonts successfully. Font names are: ', names);
  //     this.setState({ load: true });
  //   });



  // }


  // componentWillMount() {
  //   this.listener = EventRegister.addEventListener('userTabBarEvent', (data) => {
  //     this.setState({ isfour: data.is_sub_detailer, quote_permission: data.quotes_permission })

  //   })
  // }

  // componentWillUnmount() {
  //   EventRegister.removeEventListener(this.listener)
  // }






  render() {
    console.disableYellowBox = true;
    // if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
    //   return (
    //     <AppLoading
    //       startAsync={this._loadResourcesAsync}
    //       onError={this._handleLoadingError}
    //       onFinish={this._handleFinishLoading}


    //     />
    //   );
    // } else {

    return (

      <View style={styles.container}>

        <Spinner visible={this.state.loader} textContent={""} textStyle={{ color: '#FFF' }} />
        <PopUp modalVisible={this.state.modalVisible} />
        <PopUpBlack modalVisibleBlack={this.state.modalVisibleBlack} />


        

        {this.state.isfour == 0 &&
          <AppNavigator is_upgraded={this.state.is_upgraded} />
        }

        {this.state.isfour == 1 && this.state.quote_permission == 1 &&
          <AppNavigatorSub is_upgraded={this.state.is_upgraded} />
        }


        {this.state.isfour == 1 && this.state.quote_permission == 0 &&
          <AppNavigatorSubNot is_upgraded={this.state.is_upgraded} />
        }


        <LocalNotification localNotiShow={this.state.localNotiShow} NotiAllData={this.state.NotiAllData} />


        





      </View>

    )

  }





  // }

  // _loadResourcesAsync = async () => {

  //   return Promise.all([
  //     Asset.loadAsync([
  //       require('./assets/images/robot-dev.png'),
  //       require('./assets/images/robot-prod.png'),
  //     ]),
  //     Font.loadAsync({
  //       // This is the font that we are using for our tab bar
  //       ...Icon.Ionicons.font,
  //       // We include SpaceMono because we use it in HomeScreen.js. Feel free
  //       // to remove this if you are not using it in your app
  //       'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
  //       'CenturyGothic': require('./assets/fonts/CenturyGothic.ttf'),
  //       'CenturyGothic-bold': require('./assets/fonts/CenturyGothic-bold.ttf'),
  //       'Aileron-Regular': require('./assets/fonts/Aileron-Regular.otf'),
  //       'Aileron-Regular-bold': require('./assets/fonts/Aileron-Bold.otf'),
  //       'Aileron-semi-bold': require('./assets/fonts/Aileron-SemiBold.otf'),
  //       'TypoGroteskRounded': require('./assets/fonts/TypoGroteskRounded.otf'),
  //       'TypoGroteskRounded-bold': require('./assets/fonts/TypoGroteskRounded-bold.otf'),



  //     }),
  //   ]);
  // };

  // _handleLoadingError = error => {
  //   // In this case, you might want to report the error to your error
  //   // reporting service, for example Sentry
  //   console.warn(error);
  // };

  // _handleFinishLoading = () => {
  //   this.setState({ isLoadingComplete: true });
  // };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
