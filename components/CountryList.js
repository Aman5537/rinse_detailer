import React from 'react';
import { Platform, StyleSheet, Text, Image, View, TouchableOpacity, TextInput, Alert, AsyncStorage } from 'react-native'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Helper from '../src/Lib/Helper';

export default class CountryList extends React.Component {
    constructor(props, ctx) {
        super(props, ctx);
        this.state = {
            visible: false,
            picked: props.picked ? '+'+props.picked : "+1",
            countryCodes: [],
            width: props.width ? props.width : '60%'
        };
        this.getCountryCodes();

        
    }

    async getCountryCodes() {
        var country = [];
        let codes = await Helper.getData("countryCode");

        if (codes) {
            this.setState({ countryCodes: codes });
        }
        else {
            Helper.makeRequest({ url: "get-country", method: "GET" }).then((data) => {
                
                //console.log('--------getCountryList :  '+ JSON.stringify(data));
                if (data.status == 'true') {

                    
                    for (let i of data.data) {
                        country.push({
                            key: i.phonecode,
                            label: "(" + i.phonecode + ") " + i.name,
                            code: i.phonecode,
                        })
                    }
                   
                    Helper.setData("countryCode", country);
                    
                    this.setState({ countryCodes: country });
                    
                    
                }
                else {
                    // console.log(data, "data.responseData.data")
                }
            })
        }
    }
    onShow = () => {
        this.setState({ visible: true });
    }
    componentWillReceiveProps(props) {
        this.setState({ width: props.width ? props.width : '60%' })
    }
    // onSelect = (data) => {
    //     // console.log("data" + data);
    //     this.props.selectedItems = "" + data + "";
    //     this.setState({
    //         picked: "+" + data,
    //         visible: false
    //     })
    // }

    onSelect = (data) => {
        
        //console.log("data" + data);
        this.props.form.country_code = "" + data + "";
        this.setState({
            picked: "+" + data,
            visible: false
        })
    }

    onCancel = () => {
        this.setState({
            visible: false
        });
    }

    render() {
        const { visible, picked, countryCodes } = this.state;
        
        const options = countryCodes;
        console.log('hello country',JSON.stringify(options));
        
        return (
            <View style={Platform.OS === 'android' ? styles.inputViewAndroid : styles.inputViewIOS}>
                {/* <View style={[styles.inputItemsView, { flex: .7, alignItems: 'center' }]}><Image style={styles.mob_logo} source={require('../images/mob_ico.png')} /></View>*/}
                <TouchableOpacity style={[styles.inputItemsView, { flex: 2.2,paddingLeft:5 }]} onPress={this.onShow}> 
                    <Text style={{ padding: 5, color: '#000',fontSize:16 }}>{this.state.picked}</Text>
                </TouchableOpacity>

                 <ModalFilterPicker
                        visible={visible}
                        keyboardShouldPersistTaps="handle"
                        onSelect={this.onSelect}
                        onCancel={this.onCancel}
                        options={options}
                    />

                <View style={[styles.inputItemsView, { flex: 5 }]}>
                    <TextInput
                        // onChange= {({nativeEvent}) => {
                            
                        //     if (nativeEvent.target === 275) {
                        //         alert('ok');
                        //         this.props.setValues(nativeEvent.text);
                            
                        //     }
                            
                        // }}
                        // onKeyPress={({ nativeEvent }) => {
                        //     if (nativeEvent.key === 'Backspace') {
                                
                        //         this.props.setValuesBack();
                            
                        //     }
                        // }}
                        onChangeText={this.props.setValues}
                        
                        placeholderTextColor={'#969696'}
                        maxLength={16}
                        keyboardType='numeric'
                        ref={this.props.getFocus}
                        onSubmitEditing={this.props.setFocus}
                        value={this.props.mvalue}
                        placeholder={this.props.inputlable} 
                        returnKeyType='done' 
                        style={{fontSize:18,fontFamily:'CenturyGothic'}}
                        />
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    formInput: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#a09a9b',
        width: '100%'
    },
    code: {
        marginLeft: 5
    },
    input: {
        color: '#fff',
        marginVertical: 7,
        marginLeft: 6,
        fontSize: 14,
        width: '100%'
    },
    icon: {
        height: 20,
        width: 20,
        marginRight: 15,
        resizeMode: 'contain',
    },
    inputViewAndroid: {
        marginHorizontal: 0, flexDirection: 'row',  borderRadius: 4, paddingHorizontal: 5,backgroundColor:'#e9e9e9'
    },
    inputViewIOS: {
        marginHorizontal: 0, flexDirection: 'row', borderRadius: 4, paddingVertical: 10, paddingHorizontal: 5,backgroundColor:'#e9e9e9'
    },
    inputItemsView: {
        justifyContent: 'center'
    },
    mob_logo: {
        height: 20, width: 20, resizeMode: 'contain'
    },
});