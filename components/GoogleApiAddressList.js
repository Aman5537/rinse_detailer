import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Toolbar from '../components/ToolBar';
import Config from '../src/Lib/Config';

export default class GoogleApiAddressList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            long: this.props.long,
            lat: this.props.lat,
            addressname: this.props.addressname,
        }
        this.gotoBack = this.gotoBack.bind(this);
        this.clickToRightIcon = this.clickToRightIcon.bind(this);
    }

    handleAddress = (address) => { 
        this.props.onSelectAddress(address); 
    }

    gotoBack() {
        this.props.hideModal();
    }
    clickToRightIcon() {
        // console.log('right icon is empty');
    }
    render() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={this.props.hideModal}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Toolbar gotoBack={this.gotoBack} LeftIcon={require('../assets/Icons/back_bttn.png')} title="Search Location" clickToRightIcon={this.clickToRightIcon} />
                    <View style={{
                        elevation: 5,
                        top: 55,
                        width: '95%',
                        position: "absolute",
                        backgroundColor: 'transparent',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <GooglePlacesAutocomplete
                            placeholder="Enter the location"
                            placeholderTextColor={'#000'}
                            minLength={2} // minimum length of text to search
                            autoFocus={true}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed="auto" // true/false/undefined
                            fetchDetails={true}
                            renderDescription={row => row.description} // custom description render
                            onPress={(data, details = null) => {
                                let tempform = {
                                    addressname: data.description,
                                    lat: details.geometry.location.lat,
                                    long: details.geometry.location.lng
                                } 
                                this.handleAddress(tempform);
                                // console.log('-------data :  ' + JSON.stringify(data));
                                // console.log('-------name :  ' + data.description);
                                // console.log('-------lat :  ' + details.geometry.location.lat);
                                // console.log('-------lng :  ' + details.geometry.location.lng);
                                
                                this.props.hideModal()
                            }}
                            getDefaultValue={() => {
                                return ''; // text input default value
                            }}
                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: Config.google_place_api_key,
                                language: 'en', // language of the results
                                // types: '(cities)', // default: 'geocode'
                            }}
                            styles={{
                                description: {
                                    fontWeight: 'bold',
                                    color: '#000'
                                },
                                textInputContainer: {
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0
                                },
                                textInput: {
                                    backgroundColor: '#ccc',
                                    // marginLeft: 0,
                                    // marginRight: 0,
                                    height: 45,
                                    color: '#000',
                                    fontSize: 18
                                },
                            }}
                            // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                            // currentLocationLabel="Current location"
                            nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                types: 'food',
                            }}
                            filterReverseGeocodingByTypes={[
                                'locality',
                                'administrative_area_level_3',
                            ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                            debounce={200}
                        />
                    </View>
                </View>
            </Modal>
        );
    }
}