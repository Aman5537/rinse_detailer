import React, { Component } from 'react';
import { Platform, StyleSheet, Text, Image, View, TouchableOpacity, } from 'react-native';


export default class ToolBar extends Component<Props> {
    render() {
        return (
            <View style={Platform.OS === 'ios' ? styles.ViewOfToolbarIOS : styles.ViewOfToolbarAndroid}>
                <TouchableOpacity style={styles.backBttnView} onPress={() => { this.props.gotoBack() }}>
                    <Image style={styles.backbttn} source={this.props.LeftIcon} />
                </TouchableOpacity>

                {this.props.status ?
                    <View style={{ flex: 5, flexDirection: 'row', justifyContent: 'center', paddingVertical: 15 }}>
                        <View style={{ flex: 1, justifyContent: 'center' }}><Image style={{ height: 40, width: 40, borderRadius: 20, resizeMode: 'contain' }} source={this.props.ChatIcon} /></View>
                        <View style={{ flex: 4, justifyContent: 'center' }}><Text style={styles.headertitle}>{this.props.title}</Text></View>
                    </View>
                    :
                    <View style={styles.headTitleView}><Text style={styles.headertitle}>{this.props.title}</Text></View>
                }

                {this.props.title == "SELECT AGENTS" &&
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} onPress={() => { this.props.clickToRightIcon() }}>
                        <View><Text style={styles.rightText}>{this.props.RightsecText}</Text></View>
                    </TouchableOpacity>
                }

                {this.props.title == "MY ACCOUNTS" ?
                    <TouchableOpacity style={styles.viewOfRightText} onPress={() => { this.props.clickToRightIcon() }}>
                        <View><Text style={styles.rightText}>{this.props.RightText}</Text></View>
                    </TouchableOpacity> :
                    <TouchableOpacity style={styles.backBttnView} onPress={() => { this.props.clickToRightIcon() }}>
                        <Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={this.props.RightIcon} />
                    </TouchableOpacity>
                }
            </View >
        )
    }
}
const styles = StyleSheet.create({
    ViewOfToolbarIOS: {
        flexDirection: 'row', backgroundColor: '#fff', elevation: 5, marginTop: 20
    },
    ViewOfToolbarAndroid: {
        flexDirection: 'row', backgroundColor: '#fff', elevation: 5
    },
    headTitleView: {
        flex: 5, justifyContent: 'center', paddingVertical: 15
    },
    headertitle: {
        fontSize: 18, fontWeight: 'bold', color: '#000'
    },
    backBttnView: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    backbttn: {
        height: 25, width: 25, resizeMode: 'contain'
    },
    viewOfRightText: {
        flex: 3.5, justifyContent: 'center', alignItems: 'center'
    },
    rightText: {
        fontSize: 15, color: '#000'
    }
});