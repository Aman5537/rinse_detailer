package com.rinsedetailers;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import org.devio.rn.splashscreen.SplashScreen;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "RinseDetailer";
    }

    public final String TAG = "Camera Screen";

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);
        super.onCreate(savedInstanceState);
//        isCameraPermissionGranted();
//        isReadStoragePermissionGranted();
//
//        isWriteStoragePermissionGranted();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        checkAndRequestPermissions();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

     @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Override
      protected ReactRootView createRootView() {
       return new RNGestureHandlerEnabledRootView(MainActivity.this);
      }
      
    };
  }


    private boolean checkAndRequestPermissions() {
        int permissionReadExtenal = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

        int cameraPermission = checkSelfPermission(Manifest.permission.CAMERA);

        int writePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);



        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionReadExtenal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {


            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {


                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "read granted");

                        }
                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "storage granted");

                        }
                    } else if (permissions[i].equals(Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "camera granted");

                        }
                    }


                }

            }


        }
    }


//    public  boolean isReadStoragePermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG,"Permission is granted1");
//                return true;
//            } else {
//
//                Log.v(TAG,"Permission is revoked1");
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
//                return false;
//            }
//        }
//        else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG,"Permission is granted1");
//            return true;
//        }
//    }
//
//    public  boolean isWriteStoragePermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG,"Permission is granted2");
//                return true;
//            } else {
//
//                Log.v(TAG,"Permission is revoked2");
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
//                return false;
//            }
//        }
//        else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG,"Permission is granted2");
//            return true;
//        }
//    }
//
//    public  boolean isCameraPermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (checkSelfPermission(Manifest.permission.CAMERA)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG,"Permission is granted2");
//                return true;
//            } else {
//
//                Log.v(TAG,"Permission is revoked2");
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
//                return false;
//            }
//        }
//        else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG,"Permission is granted2");
//            return true;
//        }
//    }
//
//    /**
//     * Returns the name of the main component registered from JavaScript.
//     * This is used to schedule rendering of the component.
//     */
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case 2:
//                Log.d(TAG, "External storage2");
////                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
////                    Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
////                    //resume tasks needing this permission
//////                    downloadPdfFile();
////                }else{
//////                    progress.dismiss();
////                }
//                break;
//
//            case 3:
//                Log.d(TAG, "External storage1");
////                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
////                    Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
////                    //resume tasks needing this permission
//////                    SharePdfFile();
////                }else{
//////                    progress.dismiss();
////                }
//                break;
//            case 1:
//                Log.d(TAG, "External storage1");
////                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
////                    Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
////                    //resume tasks needing this permission
//////                    SharePdfFile();
////                }else{
//////                    progress.dismiss();
////                }
//                break;
//        }
//    }
  
}